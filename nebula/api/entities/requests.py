from datetime import datetime


class Requests(object):
    """A class used to represent the Request's collection

            Attributes
            -------
            data : data
                Json with the Request information.
    """

    def __init__(self, request):
        """The constructor of the Request class

        :param request: dict, information about the Request. By default, an empty dictionary will be created.
        """
        self.data = {}
        self._build_data()
        if request != {}:
            self.data["request_number"] = request["request_number"]
            self.data["user"] = request["user"]
            self.data["action"] = request["action"]
            self.data["target"] = request["target"]
            self.data["idProject"] = request["idProject"]
            self.data["idUsecase"] = request["idUsecase"]
            self.data["idBacklog"] = request["idBacklog"]
            if "oldBacklog" in request:
                self.data["oldBacklog"] = request["oldBacklog"]
            if "oldUseCase" in request:
                self.data["oldUseCase"] = request["oldUseCase"]

    def __str__(self):
        """ Returns a formatted string with the request's information

        :return: str, a formatted string to print out the Request's information.
        """
        return str(self.data)

    def _build_data(self):
        """ Sets the default values for the attribute data.

            Default values's list:
            ---------------------
                *stateRequest, P
                *created_time, current date
        """
        self.data["request_number"] = 0
        self.data["action"] = ""
        self.data["target"] = ""
        self.data["idProject"] = ""
        self.data["idUsecase"] = ""
        self.data["idBacklog"] = ""
        self.data["stateRequest"] = "P"
        self.data["created_time"] = datetime.now()
        self.data["modifications"] = []
