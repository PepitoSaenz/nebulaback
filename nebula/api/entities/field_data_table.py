
from nebula.api.entities.naming import *
from nebula.api.entities.check import *



class Field_data_table(object):
    """A class used to represent the Field Master's collection

            Attributes
            -------
            data : data
                Json with the field master information.
        """

    def __init__(self, data={}, column=0):
        """The constructor of the Field_Master class
        """
        self.data = data
        if self.data == {}:
            self._build_data()
        self.data["column"] = column

    def __str__(self):
        """Returns a formatted string with the field review's information

               Returns
               -------
               str
                    A formatted string to print out the field master's information.
        """
        return str(self.data)

    def _build_data(self):
        """ Sets the default values for the attribute data.

            Default values's list:
            ---------------------
                *Country, naming, logic, description, outFormat, catalog, key, mandatory, legacy, modification
                *origin, destinationType, logicalFormat, blocker, comments, tokenization, domain, tds, commentResponse
                *subdomain, ownership, conceptualEntity, operationalEntity
        """
        self.data["naming"] = Naming({}).data
        self.data["logic"] = "empty"
        self.data["description"] = "empty"
        self.data["operation"] = "empty"
        self.data["catalogue"] = "N/A"
        self.data["dataType"] = "empty"
        self.data["format"] = "empty"
        self.data["logicalFormat"] = "empty"
        self.data["key"] = 0
        self.data["mandatory"] = 0
        self.data["default"] = "empty"
        self.data["tokenization"] = "empty"
        self.data["originNamings"] = []
        self.data["column"] = 0
        self.data["direct"] = 0
        self.data["length"] = 0
        self.data["tds"] = 0
        self.data["comments"] = []
        self.data["modification"] = []
        self.data["check"] = Check({}).data
        """
        self.data["example"] = "empty"
        self.data["blocker"] = []
        self.data["domain"] = "Unassigned"
        self.data["subdomain"] = "Unassigned"
        self.data["ownership"] = "Unassigned"
        self.data["conceptualEntity"] = ""
        self.data["operationalEntity"] = ""
        """
