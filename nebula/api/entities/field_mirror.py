
from nebula.api.persistence.data_dictionary_repository import *


class node(object):
    """A class that represent a table fase

    Attributes
            -------
            old_data : collection
                Json with field data
            field: str
                String that represent a field to check
            column: int
                Integer that represent what column it is in a field
            new_data: collection
                Json with a new field data

    """

    def __init__(self, old_data={}, field="FieldReview", column=0, new_data={}):
        """Create a node object

        :param old_data: collection, json with field data
        :param field: str, string that represent a table field
        :param column: int, integer that represent a field column
        :param new_data: collection, json with new field data
        """
        self.field = field
        self.old_data = old_data
        self.new_data = new_data
        self.column = column
        self.build_data()
        self.query = {}
        self.query[field + "." + str(column)] = old_data

    def build_data(self):
        """Change data between old and new data in a correct format to be save
        in a database
        if one attribute of a field hasn't here doesn't do a mirror

        """
        self.old_data["logic"] = self.new_data["logic"]
        self.old_data["description"] = self.new_data["description"]
        self.old_data["catalogue"] = self.new_data["catalogue"]
        self.old_data["key"] = self.new_data["key"]
        self.old_data["mandatory"] = self.new_data["mandatory"]
        self.old_data["format"] = self.new_data["format"]
        self.old_data["modification"] = self.new_data["modification"]
        self.old_data["default"] = self.new_data["default"]
        self.old_data["blocker"] = self.new_data["blocker"]
        for i in self.new_data["modification"]:
            i["user"] = ObjectId(i["user"])
        self.old_data["naming"] = self.new_data["naming"]
        for i in self.new_data["comments"]:
            i["user"] = ObjectId(i["user"])
        self.old_data["commentResponse"] = self.new_data["commentResponse"]
        self.old_data["comments"] = self.new_data["comments"]

    def go_to(self):
        """Find a node father

        :return:
        """
        go_to = []
        if "origin" in self.old_data:
            go_to = self.old_data["origin"].split(".")
        return go_to

    def resutl(self):
        """Get field a column that represent that node

        :return: json, dict that contains field and column data
        """
        return {"field": self.field, "column": self.column}

    def _find_son(self):
        """Get node data as database field format

        :return: str, String that represent a field origin format
        """
        return self.field+"."+str(self.column)


class graph(object):

    """
    A class that represent a connection field as a graph or a mirror
    to connect similar data

    Attributes
            -------
            data_dictionary_repository : data dictionary repository object
                data to consult data dictionary data

    """

    def __init__(self):
        """
        Get a data dictionary repository connection
        """
        self.data_dictionary_repository = Data_dictionary_repository()

    def _start(self):
        """
        Build what are connections nodes
        :return:
        """
        self.fields = {"FieldReview": False, "fieldsRaw": False, "fieldsMaster": False}
        self.nodes = []

    def match_fields(self, table_id, field, column):
        """F ind a field that are connect

        :param table_id: str, String that represent table id
        :param field: str, String that represent a field to search
        :param column: int, integer that represent a column number
        :return: list, list that represent a field connected
        """
        data = self.data_dictionary_repository.generate_aggreation([
            {'$match': {'_id': ObjectId(table_id)}},
            {'$unwind': {'path': '$' + field}},
            {"$match": {field+".column": column}},
            {'$project': {field: 1}}
        ])
        return list(data)[0][field]

    def build_graph(self,table_id,data,field="FieldReview"):
        """Build a graph starting with one simple node and save data transfer

        :param table_id: str, string that represent a table id
        :param data: collection, json that contains data to transfer
        :param field: str, string that represent with what field start
                    by defect if FieldReview a initial node
        :return:
        """
        self._start()
        self.data = data
        index = 0
        column = data["column"]
        old_data = data
        while True:
            if not self.fields[field]:
                #  Create a node
                self.nodes.append(node(old_data=old_data,field=field,column=column,new_data=self.data))
                self.fields[field] = True
                #  Find what node can go next
                tmp = self.nodes[index].go_to()
                index += 1
                try:
                    #  Check data to build a next node to check
                    field, column = tmp[0], tmp[1]
                    old_data = self.match_fields(table_id, field, int(column))
                except:
                    break
            else:
                break
        #  Find all son nodes
        while not self._graph_build():
            self._check_all(table_id)
        #  Update all nodes that was create with the data
        for i in self.nodes:
            self.data_dictionary_repository.update_field(table_id, i.query)

    def _graph_build(self):
        """Check that node wasn't visited

        :return: boolean, boolean that represen if one node was or wasn't visited
        """
        res = True
        for i in self.fields:
            if not self.fields[i]:
                res = False
                break
        return res

    def _check_all(self,table_id):
        """Check if one node was visit or find a son node to visit

        :param table_id: str, string that represent table id
        :return:
        """
        for i in self.fields:
            if not self.fields[i] and self.data_dictionary_repository.exists_field(table_id,i) is not None:
                flag = False
                for j in self.nodes:
                    #  Check all node son that has one node
                    check = self.data_dictionary_repository.generate_aggreation([
                        {'$match': {'_id': ObjectId(table_id)}},
                        {'$unwind': {'path': '$' + str(i)}},
                        {'$match': {str(i) + '.origin': j._find_son()}},
                        {'$project': {str(i) + '.column': 1}}
                    ])
                    try:
                        column = list(check)[0][i]["column"]
                        old_data = self.match_fields(table_id, i, column)
                        self.nodes.append(node(old_data=old_data, field=i, column=column, new_data=self.data))
                        self.fields[i] = True
                        flag = True
                    except:
                        continue
                if not flag:
                    self.fields[i] = True
            else:
                self.fields[i] = True
