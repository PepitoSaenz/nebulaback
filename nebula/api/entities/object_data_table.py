import datetime


class Object_data_table(object):
    """A class used to represent the data table's collection

        Attributes
        -------
        data : data
            Json with the data table information.

    """
    def __init__(self, data={}):
        """The constructor of the data table class

            Parameters
            ----------
            data : dict
                Information about the data table. If it's an empty dictionary, data will be taken default values.
        """
        self.data = data
        if self.data == {}:
            self._build_data()

    def __str__(self):
        """Returns formatted string with the data table's information

            Returns
            -------
            str
                 A formatted string to print out the data table's information.
        """
        return str(self.data)

    def _build_data(self):
        """ Sets the default values for the attribute data.
        """
        self.data["stateTable"] = "N"
        self.data["alias"] = ""
        self.data["master_name"] = ""
        self.data["baseName"] = ""
        self.data["observationField"] = ""
        self.data["information_group_level_1"] = ""
        self.data["information_group_level_2"] = ""
        self.data["perimeter"] = ""
        self.data["information_level"] = ""
        self.data["data_source"] = ""
        self.data["modelVersion"] = ""
        self.data["deploymentType"] = ""
        self.data["securityLevel"] = ""
        self.data["master_path"] = ""
        self.data["uuaaMaster"] = ""
        self.data["partitions"] = ""
        self.data["periodicity"] = ""
        self.data["loading_type"] = ""
        self.data["current_depth"] = 0
        self.data["required_depth"] = 0
        self.data["estimated_volume_records"] = 0
        self.data["origin_tables"] = []
        self.data["target_file_type"] = ""
        self.data["target_file_delimiter"] = ""
        self.data["tacticalObject"] = ""
        self.data["fields"] = []
        self.data["master_comment"] = ""
        self.data["modifications"] = []
        self.data["user"] = ""
        self.data["project_owner"] = ""
        self.data["people"] = []
        self.data["created_time"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
