
from nebula.api.entities.naming import *


class Field_master(object):
    """A class used to represent the Field Master's collection

            Attributes
            -------
            data : data
                Json with the field master information.
        """

    def __init__(self, data={}, column=0):
        """The constructor of the Field_Master class
        """
        self.data = data
        if self.data == {}:
            self._build_data()
        self.data["column"] = column

    def __str__(self):
        """Returns a formatted string with the field review's information

               Returns
               -------
               str
                    A formatted string to print out the field master's information.
        """
        return str(self.data)

    def _build_data(self):
        """ Sets the default values for the attribute data.

            Default values's list:
            ---------------------
                *Country, naming, logic, description, outFormat, catalog, key, mandatory, legacy, modification
                *origin, destinationType, logicalFormat, blocker, comments, tokenization, domain, tds, commentResponse
                *sub-domain, ownership, conceptualEntity, operationalEntity
        """
        self.data["column"] = 0
        self.data["naming"] = Naming({}).data
        self.data["logic"] = "empty"
        self.data["description"] = "empty"
        self.data["catalogue"] = "N/A"
        self.data["tds"] = 0
        self.data["destinationType"] = "empty"
        self.data["format"] = "empty"
        self.data["logicalFormat"] = "empty"
        #self.data["outFormat"] = "empty" #Generated
        self.data["key"] = 0
        self.data["mandatory"] = 0
        self.data["default"] = "empty"
        self.data["legacy"] = {"legacy": "Calculated", "legacyDescription": "Calculated"}
        self.data["origin"] = ""
        self.data["generatedField"] = ""
        self.data["tokenization"] = "empty"
        self.data["comments"] = []
        self.data["commentResponse"] = ""
        self.data["modification"] = []
        self.data["blocker"] = []
        #to replace
        self.data["domain"] = "Unassigned"
        self.data["subdomain"] = ""
        self.data["ownership"] = ""
        self.data["conceptualEntity"] = ""
        self.data["operationalEntity"] = ""
