
class Check(object):
    """A class used to represent the check of VoBo object

        Attributes
        -------
        data : data
            Json with the naming information.

    """
    def __init__(self, data={}):
        """The constructor of the Naming class
        """
        self.data = data
        if self.data == {}:
            self._build_data()

    def __str__(self):
        """Returns formatted string with the naming's information

            Returns
            -------
            str
                 A formatted string to print out the naming's information.
        """
        return str(self.data)

    def _build_data(self):
        """ Sets the default values for the attribute data.

                Default values's list:
                ---------------------
                    *status: "NOK"
                    *user: ""
                    *lastComment: ""
                    *comments: []
        """
        self.data["status"] = "NOK"
        self.data["user"] = ""
        self.data["lastComment"] = ""
        self.data["comments"] = []
