import datetime


class Object_data_dictionary(object):
    """A class used to represent the data dictionary's collection

        Attributes
        -------
        data : data
            Json with the data dictionary information.

    """
    def __init__(self, data={}):
        """The constructor of the data dictionary class

            Parameters
            ----------
            data : dict
                Information about the data dictionary. If it's an empty dictionary, data will be taken default values.
        """
        self.data = data
        if self.data == {}:
            self._build_data()

    def __str__(self):
        """Returns formatted string with the data dictionary's information

            Returns
            -------
            str
                 A formatted string to print out the data dictionary's information.
        """
        return str(self.data)

    def _build_data(self):
        """ Sets the default values for the attribute data.
        """
        self.data["stateTable"] = "N"
        self.data["alias"] = ""
        self.data["raw_name"] = ""
        self.data["master_name"] = ""
        self.data["baseName"] = ""
        self.data["observationField"] = ""
        self.data["information_group_level_1"] = ""
        self.data["information_group_level_2"] = ""
        self.data["perimeter"] = ""
        self.data["information_level"] = ""
        self.data["data_source"] = ""
        self.data["deploymentType"] = ""
        self.data["securityLevel"] = ""
        self.data["modelVersion"] = ""
        self.data["objectVersion"] = ""
        self.data["objectType"] = ""
        self.data["raw_route"] = ""
        self.data["master_route"] = ""
        self.data["uuaaRaw"] = ""
        self.data["uuaaMaster"] = ""
        self.data["partitions_raw"] = ""
        self.data["partitions_master"] = ""
        self.data["periodicity"] = ""
        self.data["periodicity_master"] = ""
        self.data["loading_type"] = ""
        self.data["current_depth"] = 0
        self.data["required_depth"] = 0
        self.data["estimated_volume_records"] = 0
        self.data["entryType"] = ""
        self.data["originSystem"] = ""
        self.data["physical_name_source_object"] = ""
        self.data["raw_path"] = ""
        self.data["master_path"] = ""
        self.data["typeFile"] = ""
        self.data["separator"] = ""
        self.data["tacticalObject"] = ""
        self.data["FieldReview"] = []
        self.data["fieldsRaw"] = []
        self.data["fieldsMaster"] = []
        self.data["raw_comment"] = ""
        self.data["master_comment"] = ""
        self.data["modifications"] = []
        self.data["user"] = ""
        self.data["people"] = []
        self.data["created_time"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
