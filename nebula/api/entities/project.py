import datetime


class Project(object):
    """A class used to represent the Project's collection

            Attributes
            -------
            data : data
                Json with the project information.
        """

    def __init__(self, data={}):
        """The constructor of the Project class

            Parameters
            ----------
            data : dict
                Information about the project. If it's an empty dictionary, data will be taken default values.
        """
        self.data = data
        if self.data == {}:
            self._build_data()

    def __str__(self):
        """Returns a formatted string with the project's information

               Returns
               -------
               str
                    A formatted string to print out the project's information.
        """
        return str(self.data)

    def _build_data(self):
        """
            Sets the default values for the attribute data.
        """
        self.data["name"] = ""
        self.data["shortDesc"] = ""
        self.data["teams"] = []
        self.data["useCases"] = []
        self.data["countries"] = []
        self.data["owners"] = []
        self.data["VoBo"] = []
        self.data["technicalResponsible"] = "datahub.co.group@bbva.com"
        self.data["created_time"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.data["modifications"] = []
