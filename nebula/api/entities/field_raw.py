
from nebula.api.entities.naming import *


class Field_raw(object):

    """A class used to represent the Field Raw's collection

            Attributes
            -------
            data : data
                Json with the field Raw information.
        """

    def __init__(self, data={}, column=0):
        """The constructor of the Field_Raw class

            Parameters
            ----------
            column : int
                Field raw's index. If the column is not given, the column will be zero.

            data : dict
                Information about the naming. If it's an empty dictionary, data will be taken default values.
        """
        self.data = data
        if self.data == {}:
            self._build_data()
        self.data["column"] = column

    def __str__(self):
        """Returns a formatted string with the Field Raw's information

               Returns
               -------
               str
                    A formatted string to print out the field raw's information.
        """
        return str(self.data)

    def _build_data(self):
        """ Sets the default values for the attribute data.

            Default values's list:
            ---------------------
                *Country, naming, logic, description, outFormat, catalog, key, mandatory, legacy, modification
                *origin, destinationType, logicalFormat, blocker, comments, tokenization, domain, tds, commentResponse
                *sub-domain, ownership, conceptualEntity, operationalEntity
        """
        self.data["column"] = 0
        self.data["naming"] = Naming({}).data
        self.data["logic"] = "empty"
        self.data["description"] = "empty"
        self.data["catalogue"] = "N/A"
        self.data["tds"] = 0
        self.data["destinationType"] = "String" #Always String
        self.data["format"] = "empty" 
        self.data["logicalFormat"] = "ALPHANUMERIC(0)" #Always Alphanumeric
        #Para campos generados TO BE DEPRECATED
        self.data["outFormat"] = "empty"
        self.data["outGov"] = "empty" 
        self.data["key"] = 0
        self.data["mandatory"] = 0
        self.data["default"] = "empty"
        self.data["legacy"] = {"legacy": "Calculated", "legacyDescription": "Calculated"}
        self.data["origin"] = ""
        self.data["generatedField"] = ""
        self.data["tokenization"] = "empty"
        self.data["comments"] = []
        self.data["commentResponse"] = ""
        self.data["modification"] = []
        self.data["check"] = {
            "status": "NOK",
            "user": "",
            "lastComment": "",
            "comments": []
        }
        self.data["blocker"] = []
        #to replace
        self.data["domain"] = "Unassigned"
        self.data["subdomain"] = ""
        self.data["ownership"] = ""
        self.data["conceptualEntity"] = ""
        self.data["operationalEntity"] = ""
        
        
