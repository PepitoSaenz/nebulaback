import datetime


class Use_case(object):
    """A class used to represent the Use case's collection

            Attributes
            -------
            data : data
                Json with the Use case information.
        """

    def __init__(self, data={}):
        """The constructor of the Use case class

            Parameters
            ----------
            data : dict
                Information about the Use case. If it's an empty dictionary, data will be taken default values.
        """
        self.data = data
        if self.data == {}:
            self._build_data()

    def __str__(self):
        """Returns a formatted string with the Use case's information

               Returns
               -------
               str
                    A formatted string to print out the Use case's information.
        """
        return str(self.data)

    def _build_data(self):
        """
            Sets the default values for the attribute data.
        """
        self.data["name"] = ""
        self.data["description"] = ""
        self.data["startDate"] = ""
        self.data["finishDate"] = ""
        self.data["tables"] = []
        self.data["tablones"] = []
        self.data["created_time"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.data["modifications"] = []
