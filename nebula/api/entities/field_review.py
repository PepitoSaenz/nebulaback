
from nebula.api.entities.naming import *


class Field_review(object):
    """A class used to represent the Field Review's collection

            Attributes
            -------
            data : data
                Json with the field review information.
        """

    def __init__(self, data={}, column=0):
        """The constructor of the Field_Review class
        """
        self.data = data
        if self.data == {}:
            self._build_data()
        self.data["column"] = column

    def __str__(self):
        """Returns a formatted string with the field review's information

               Returns
               -------
               str
                    A formatted string to print out the field review's information.
        """
        return str(self.data)

    def _build_data(self):
        """ Sets the default values for the attribute data.

            Default values's list:
            ---------------------
                *length, mandatory, key, token, decimals, integers, symbol, outLength, startPosition, endPosition : 0
                *description, legacyDescription, governanceFormat, example, destinationType, outFormat, legacy, logic, originType, outRec, outVarchar : "empty"
                *comments, modification : []
                *governanceFormat : "N"
                *naming : new instance Naming's object (With default values).
        """
        self.data["column"] = 0
        self.data["startPosition"] = 0
        self.data["endPosition"] = 0
        self.data["length"] = 0
        self.data["naming"] = Naming({}).data
        self.data["logic"] = "empty"
        self.data["description"] = "empty"
        self.data["catalogue"] = "N/A"
        self.data["tds"] = "NO"
        self.data["destinationType"] = "empty" #Data Type
        self.data["format"] = "empty"
        self.data["logicalFormat"] = "empty" #Logical Format  
        self.data["governanceFormat"] = "empty" #Logical Format  
        self.data["outFormat"] = "empty" #Logical Format para generar fases TO BE DEPRECATED
        self.data["key"] = 0
        self.data["mandatory"] = 0
        self.data["default"] = "empty"
        self.data["originType"] = "CHAR"
        self.data["legacy"] = {"legacy": "empty", "legacyDescription": "empty"}
        #for review only
        self.data["decimals"] = 0
        self.data["integers"] = 0
        self.data["symbol"] = 0
        self.data["comments"] = []
        self.data["commentResponse"] = ""
        self.data["modification"] = []
        #self.data["commentResponse"] = ""
        #campos HOST para considerar eliminar
        self.data["outRec"] = "empty"
        self.data["outVarchar"] = "empty"
        self.data["outLength"] = 0
        #to replace
        self.data["blocker"] = []
        self.data["example"] = "empty" #TO BE DEPRECATED
        self.data["domain"] = "Unassigned"
        self.data["subdomain"] = ""
        self.data["ownership"] = ""
        self.data["conceptualEntity"] = ""
        self.data["operationalEntity"] = ""
