import datetime


class Backlog(object):
    """A class used to represent the backlog's collection

            Attributes
            -------
            data : data
                Json with the backlog information.
    """

    def __init__(self, data={}):
        """The constructor of the Backlog class

        :param data: dict, information about the backlog. By default, an empty dictionary will be created.
        """
        self.data = data
        if self.data == {}:
            self._build_data()

    def __str__(self):
        """ Returns a formatted string with the backlog's information

        :return: str, a formatted string to print out the backlog's information.
        """
        return str(self.data)

    def type_file(self):
        """Gets an array which contains the options for the type's file.

        :return: array
        """
        return ["", "Fixed", "CSV", "PARQUET"]

    def _build_data(self):
        """
            Sets the default values for the attribute data.
        """
        self.data["baseName"] = ""
        self.data["originSystem"] = ""
        self.data["periodicity"] = ""
        self.data["history"] = 0
        self.data["observationField"] = ""
        self.data["typeFile"] = ""
        self.data["idTable"] = ""
        self.data["tableRequest"] = "P"
        self.data["uuaaRaw"] = ""
        self.data["uuaaMaster"] = ""
        self.data["productOwner"] = ""
        self.data["tacticalObject"] = "NO"
        self.data["perimeter"] = ""
        self.data["created_time"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.data["modifications"] = []
