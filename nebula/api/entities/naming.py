
class Naming(object):
    """A class used to represent the naming's collection

        Attributes
        -------
        data : data
            Json with the naming information.

    """
    def __init__(self, data={}):
        """The constructor of the Naming class
        """
        self.data = data
        if self.data == {}:
            self._build_data()

    def __str__(self):
        """Returns formatted string with the naming's information

            Returns
            -------
            str
                 A formatted string to print out the naming's information.
        """
        return str(self.data)

    def _build_data(self):
        """ Sets the default values for the attribute data.

                Default values's list:
                ---------------------
                    *suffix: -1
                    *words: []
                    *naming: "empty"
                    *isGlobal: "N"
        """
        self.data["naming"] = "empty"
        self.data["isGlobal"] = "N"
        self.data["code"] = ""
        self.data["suffix"] = -1
        self.data["Words"] = []
        self.data["architectureState"] = ""
        self.data["governanceState"] = "A"
        self.data["hierarchy"] = ""
