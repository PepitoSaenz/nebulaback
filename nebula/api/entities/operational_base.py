from nebula.api.entities.field_operational_base import *
import datetime


class Operational_base(object):
    """A class used to represent the Operational base's collection

            Attributes
            -------
            data : data
                Json with the Operational base information.
        """

    def __init__(self, data={}, fields=0):
        """The constructor of the Operational base class

            Parameters
            ----------
            data : dict
                Information about the Operational base. If it's an empty dictionary, data will be taken default values.
        """
        self.data = data
        if self.data == {}:
            self._build_data()
        for i in range(0, fields):
            self.data["fields"].append(Field_operational_base({}, i).data)

    def __str__(self):
        """Returns a formatted string with the operational base's information

               Returns
               -------
               str
                    A formatted string to print out the Operational base's information.
        """
        return str(self.data)

    def _build_data(self):
        """
            Sets the default values for the attribute data.
        """
        self.data["object_name"] = ""
        self.data["baseName"] = ""
        self.data["observationField"] = ""
        self.data["db_type"] = ""
        self.data["category_id"] = ""
        self.data["uuaaMaster"] = ""
        self.data["alias"] = ""
        self.data["periodicity"] = ""
        self.data["tacticalObject"] = "NO"
        self.data["stateTable"] = "N"
        self.data["delivery_owner"] = ""
        self.data["data_source"] = ""
        self.data["information_group_level_1"] = ""
        self.data["information_group_level_2"] = ""
        self.data["perimeter"] = ""
        self.data["information_level"] = ""
        self.data["storage_zone"] = ""
        self.data["base_path"] = ""
        self.data["object_type"] = "Table"
        self.data["current_depth"] = 0
        self.data["required_depth"] = 0
        self.data["estimated_volume_records"] = 0
        self.data["comment"] = ""
        self.data["fields"] = []
        self.data["origin_desc"] = ""
        self.data["modifications"] = []
        self.data["user"] = ""
        self.data["project_owner"] = ""
        self.data["created_time"] = datetime.datetime.now()
        self.data["people"] = []
        
        self.data["country"] = ""
        self.data["technical_responsible"] = ""
        self.data["object_type"] = ""
        self.data["partition"] = ""
        self.data["timing"] = ""
        self.data["upload_type"] = ""
        self.data["storage_type_source"] = ""
        self.data["object_physical_name"] = ""
        self.data["origin_contact"] = ""
        self.data["origin_route"] = ""
        self.data["scheme_route"] = ""
        self.data["input_file_type"] = ""
        self.data["input_file_delimiter"] = ""
        self.data["output_file_type"] = ""
        self.data["output_file_delimiter"] = ""
        self.data["governance_validation_status"] = ""
        self.data["governance_validation_comments"] = ""
        self.data["po_validation_status"] = ""
        self.data["po_validation_comments"] = ""
        self.data["architecture_validation_status"] = ""
        self.data["architecture_validation_comments"] = ""
        
