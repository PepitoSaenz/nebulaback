import datetime


class Team(object):
    """A class used to represent the team's collection

        Attributes
        -------
        data : data
            Json with the team information.
    """

    def __init__(self,data={}):
        """The constructor of the Team class
        """
        self.data = data
        if self.data == {}:
            self._build_data()

    def __str__(self):
        """A formatted string with the team's information

            Returns
            -------
            str
                 A formatted string to print out the team's information.
        """
        return str(self.data)

    def countries(self):
        """Gets an array with available countries' names for the team.

        :return: array, the countries's names.
        """
        return [
            "Colombia", "Mexico", "Peru", "USA", "España"
        ]

    def _build_data(self):
        """
            Sets the default values for the attribute data.
        """
        self.data["name"] = ""
        self.data["country"] = ""
        self.data["creationDate"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.data["modifications"] = []
