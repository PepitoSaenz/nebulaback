from nebula.api.entities.naming import *


class Field_operational_base(object):
    """A class used to represent the operational base's collection

            Attributes
            -------
            data : data
                Json with the field master information.
        """

    def __init__(self, data={}, column=0):
        """The constructor of the Operational base class

            Parameters
            ----------
            column : int
                Field master's index. If the column is not given, the column will be zero.

            data : dict
                Information about the naming. If it's an empty dictionary, data will be taken default values.
        """
        self.data = data
        if self.data == {}:
            self._build_data()
        self.data["column"] = column

    def __str__(self):
        """Returns a formatted string with the field operational base's information

               Returns
               -------
               str
                    A formatted string to print out the field operational base's information.
        """
        return str(self.data)

    def _build_data(self):
        """ Sets the default values for the attribute data.
        """
        self.data["length"] = 0
        self.data["naming"] = Naming({}).data
        self.data["logic"] = "empty"
        self.data["description"] = "empty"
        self.data["catalogue"] = "N/A"
        self.data["key"] = 0
        self.data["mandatory"] = 0
        self.data["dataType"] = "empty"
        self.data["format"] = "empty"
        self.data["logicalFormat"] = "empty"
        self.data["origin_desc"] = "empty"
        self.data["comments"] = []
        self.data["modification"] = []
        self.data["column"] = 0
        self.data["check"] = {
            "status": "NOK",
            "user": "",
            "lastComment": "",
            "comments": []
        }

