import datetime


class User(object):
    """A class used to represent the user's collection

        ...

        Attributes
        ----------
        data : data
            Json with the user information.
    """

    def __init__(self, data={}):
        """The constructor of the User class

            Parameters
            ----------
            data : dict
                Information about the user. By default, an empty dictionary will be created.
        """
        self.data = data
        if self.data == {}:
            self._build_data()

    def __str__(self):
        """A formatted string with the user's information

            Returns
            -------
            str
                 A formatted string to print out the user's information.
        """
        return str(self.data)

    def _build_data(self):
        """
            Sets the default values for the attribute data.
        """
        self.data["name"] = ""
        self.data["teams"] = []
        self.data["email"] = ""
        self.data["password"] = ""
        self.data["rol"] = ""
        self.data["created_time"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.data["modifications"] = []
