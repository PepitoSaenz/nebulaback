
from django.conf import settings
import bson


class Settings_repository(object):
    """A class used to represent the settings collection.

            Attributes
            -------
            collection : collection
                Creates the word collection and applies the queries and operations.

    """
    def __init__(self):
        """
            The constructor of the Word_repository class.
        """
        self.collection = settings.MONGO_DATABASE['settings']

    def find_load_op_settings(self):
        """
            Finds the word document in the word's collection which the longName or
            abbreviation fields is equal to the word parameter.

        :param word: str, a string with the word to find.
        :return: bson or None, a bson with the word document found,otherwise return a None.
        """
        return self.collection.find_one({"_id": "BASEOP"})
    
    def update_all_op_setting(self, data):
        """
            Updates the head values with the new ones

        :param operational_id: str. String that represent the operational base id.
        :param data: json. Instance of the entity class Field_operational_base.
        """
        self.collection.update_one({"_id": "BASEOP"}, {"$set": {"arrayFields": data}})
    
    def update_one_load_op_setting(self, pos, data):
        """
            Updates the head values with the new ones

        :param operational_id: str. String that represent the operational base id.
        :param data: json. Instance of the entity class Field_operational_base.
        """
        self.collection.update_one({"_id": "BASEOP"}, {"$set": {
            "arrayFields." + str(pos): data,
        }})

    def update_load_op_settings(self, data):
        """
            Updates the head values with the new ones

        :param operational_id: str. String that represent the operational base id.
        :param data: json. Instance of the entity class Field_operational_base.
        """
        self.collection.update_one({"_id": "BASEOP"}, {
            "$set": {
                "arrayFields": data["arrayFields"],
                "arrayObject": data["arrayObject"],
                "defaultFields": data["defaultFields"],
                "defaultObject": data["defaultObject"]
            }
        })
