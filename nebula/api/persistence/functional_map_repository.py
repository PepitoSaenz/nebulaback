
import pymongo
from bson import ObjectId
from django.conf import settings


class Functional_map_repository(object):
    """A class used to represent the functionalMap collection.

            Attributes
            -------
            collection : collection
                Creates the functionalMap collection and applies the queries and operations.

    """
    def __init__(self):
        """
            The constructor of the Functional_map_repository class.
        """
        self.collection = settings.MONGO_DATABASE['functionalMap']
        
    def find_one(self, uuaa_id):
        """
            Returns the uuaa document in the functionalMapMaster collection which id is equal to
            the uuaa_id parameter. In other case returns None.

        :param uuaa_id: str, a string that represents the id of the data dictionary document
                in the functionalMapMaster collection.
        :return: bson or None, a bson with the data dictionary document found, otherwise return a None.
        """
        return self.collection.find_one({"_id": ObjectId(str(uuaa_id).strip())})

    def find_uuaa(self, uuaa):
        """
            Finds one functional map which uuaa field is equal to the uuaa parameter.

        :param uuaa: str. Application unit. eg. SAN, COG.
        :return: A bson or None, a bson that contains the query's result, otherwise return a None.
        """
        return self.collection.find_one({
            "short_name": str(uuaa).upper()
        })

    def find_all(self):
        """
            Finds all functional map documents in the functionalMap's collection.

        :return: A bson or None, a bson that contains the query's result, otherwise return a None.
        """
        return self.collection.find({}).sort([("uuaa", pymongo.ASCENDING)])
    
    def find_all_fullname(self):
        """
            Finds all functional map documents in the functionalMap's collection.

        :return: A bson or None, a bson that contains the query's result, otherwise return a None.
        """
        return self.collection.find({}).sort([("full_name", pymongo.ASCENDING)])
    
    def find_all_shortname(self):
        """
            Finds all functional map documents in the functionalMap's collection.

        :return: A bson or None, a bson that contains the query's result, otherwise return a None.
        """
        return self.collection.find({}).sort([("short_name", pymongo.ASCENDING)])
    
    def find_by_levels(self, level):
        """
            Find a functional map that has one level or uuaa selected.

        :param level: str, string that represent a level or uuaa to find
        :return: bson, dictionary with a functional map that check a conditions
        """
        return self.collection.find_one({"$or": [{"level1": level}, {"level2": level}, {"uuaa": level}]})

    def update_uuaa(self, uuaa_id, uuaa):
        """
            Updates datasource and description for a uuaa

        :param table_id: str, string that represent a table id
        :param query: json, dict that represent any query to update a data dictionary
        """
        self.collection.update_one({"_id": ObjectId(str(uuaa_id).strip())}, {"$set": {"uuaa": uuaa["uuaa"],
                                    "level1": uuaa["level1"], 
                                    "level2": uuaa["level2"],
                                    "example": uuaa["example"],
                                    "data_source": uuaa["data_source"], "functionalGroupDataSource": uuaa["functionalGroupDataSource"],
                                    "systemDescription": uuaa["systemDescription"], "systemDescriptionEng": uuaa["systemDescriptionEng"],
                                    "systemName": uuaa["systemName"], # "tecnicalResponsible": uuaa["tecnicalResponsible"],
                                    "route": uuaa["route"],
                                    "modifications": uuaa["modifications"]}})
        
    def remove_uuaa(self, uuaa):
        """
            Updates datasource and description for a uuaa

        :param table_id: str, string that represent a table id
        :param query: json, dict that represent any query to update a data dictionary
        """
        self.collection.deleteOne({"uuaa": uuaa})
    
    def insert_uuaa(self, new_uuaa):
        """
            Inserts one operational data base to the collections.

        :param operational_base: str. Base operational entity class.
        :return operational data base id in the collection.
        """
        return self.collection.insert_one(new_uuaa).inserted_id
    
    def find_distinct_property(self, property_name):
        """
            Finds the various values, distinct that a single property of the uuaa contains

        :return: A bson or None, a bson that contains the query's result, otherwise return a None.
        """
        return self.collection.distinct(property_name)