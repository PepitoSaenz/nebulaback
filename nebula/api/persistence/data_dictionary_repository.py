
from django.conf import settings
from bson import ObjectId
import bson


class Data_dictionary_repository(object):
    """A class used to represent the dataDictionary collection.

            Attributes
            -------
            collection : collection
                Creates the dataDictionary collection and applies the queries and operations.
    """

    def __init__(self):
        self.collection = settings.MONGO_DATABASE['dataDictionary']

    def find_one(self, data_dictionary_id):
        """
            Returns the data dictionary document in the dataDictionary collection which id is equal to
            the data_dictionary_id parameter. In other case returns None.

        :param data_dictionary_id: str, a string that represents the id of the data dictionary document
                in the dataDictionary collection.
        :return: bson or None, a bson with the data dictionary document found, otherwise return a None.
        """
        return self.collection.find_one({"_id": ObjectId(str(data_dictionary_id).strip())})

    def insert_one(self, data_dictionary):
        """
            Inserts the data_dictionary parameter, which is a data dictionary document,to the dataDictionary collection.

        :param data_dictionary: Data_dictionary entity, a instance of the Data_dictionary class.
        """
        return self.collection.insert_one(data_dictionary).inserted_id

    def find_any_query(self, query):
        """
            Returns the result of the query in the dataDictionary collection. In other case return None.

        :param query: str, a string  with the query.
        :return: An array of bson, bson or None, an array of bson that contains the query's result
                when its more than one data dictionary document.
                If the query's result is one data dictionary document returns a bson, otherwise return a None.
        """
        return self.collection.find(bson.BSON.decode(bson.BSON.encode(query)))

    def find_one_by_alias(self, alias):
        """
            Finds one data dictionary by alias.

        :param alias: str, represents the alias of the table
        :return: bson or None
        """
        return self.collection.find_one({"alias": alias})

    def find_one_by_physical_name(self, name):
        """
            Finds one data dictionary by physical name object.

        :param alias: str, represents the alias of the table
        :return: bson or None
        """
        return self.collection.find_one({"raw_name": name})

    def find_naming(self, data_dictionary_id, naming):
        """
            Returns the data dictionaries which its id field is equal to the data_dictionary_id parameter and
            its fieldReview field contains the naming parameter.

        :param data_dictionary_id: Data_dictionary entity, an instance of the class Data_dictionary.
        :param naming: str, a string with the naming to find.
        :return: An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data dictionary documents,
                If the query's result is one data dictionary document returns a bson, otherwise return a None.
        """
        return self.collection.find({"_id": {"$ne": ObjectId(str(data_dictionary_id).strip())},
                                     "FieldReview": {"$elemMatch": {"naming.naming": naming}}})

    def find_logical_name(self, data_dictionary_id, logical_name):
        """
            Returns the data dictionaries which its id field is equal to the data_dictionary_id parameter and
           its fieldReview field contains the logical name parameter.

        :param data_dictionary_id: Data_dictionary entity, an instance of the class Data_dictionary.
        :param logical_name: str, a string with the logical name to find.
        :return: An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data dictionary documents,
                If the query's result is one data dictionary document returns a bson, otherwise return a None.
        """
        return self.collection.find({"_id": {"$ne": ObjectId(str(data_dictionary_id).strip())},
                                     "FieldReview": {"$elemMatch": {"logic": logical_name}}})

    def update_field(self, table_id, query):
        """
            Updates any field in one data dictionary

        :param table_id: str, string that represent a table id
        :param query: json, dict that represent any query to update a data dictionary
        """
        self.collection.update_one({"_id": ObjectId(str(table_id).strip())},
                                   {"$set": bson.BSON.decode(bson.BSON.encode(query))})

    def same_naming(self, naming):
        """
            Finds one naming in all FieldReview data dictionary's fields.

        :param naming: str. string that represent a naming
        :return: An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data dictionary documents,
                If the query's result is one data dictionary document returns a bson, otherwise return a None.
        """
        return self.collection.find({"FieldReview": {"$elemMatch": {"naming.naming": naming}}})

    def same_naming_different_table(self, naming, table_id, field):
        """
            Finds one naming in field's parameter in all data dictionaries.

        :param naming: str, string that represent a naming
        :param table_id: str, string that represent a table id
        :param field: str. Expects FieldReview, fieldsRw or fieldsMaster.
        :return: bson, dict that represent all table field with one exactly naming
        """
        return self.collection.find({"_id": {"$ne": ObjectId(str(table_id).strip())},
                                     field: {"$elemMatch": {"naming.naming": naming}}}, {field: 1})

    def find_user_tables(self, user_id):
        """
            Finds all data dictionaries which the user creates.

        :param user_id: str, string that represent user id
        :return An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data dictionary documents,
                If the query's result is one data dictionary document returns a bson, otherwise return a None
        """
        return self.collection.find({"user": ObjectId(str(user_id).strip())},
                                    {"raw_name": 1, "master_name": 1, "alias": 1, "baseName": 1, "FieldReview": 1})

    def find_user_tables_state(self, user_id, state):
        """
            Finds all data dictionaries by stateTable and user (who creates the data dictionary).

        :param user_id: str, string that represent a user id
        :param state: str, string that represent a table state
        :return: bson, dict that represent user tables data in one state
        """
        return self.collection.find({"$and": [{"user": ObjectId(str(user_id).strip())},
                                              {"stateTable": state.strip().upper()}]}, {"raw_name": 1, "master_name": 1,
                                                                                        "stateTable": 1, "baseName": 1,
                                                                                        "alias": 1, "FieldReview": 1})

    def find_rol_tables_state(self, state):
        """
            Finds all data dictionaries by stateTable.

        :param state: str, string that represent a table state
        :return: bson, dict that represent a data dictionary in one state
        """
        return self.collection.find({"stateTable": state}, {
            "raw_name": 1, "master_name": 1, "stateTable": 1, "baseName": 1, "alias": 1, "FieldReview": 1})

    def find_tables_state(self, state):
        """
            Finds all data dictionaries by stateTable.

        :param state: str, string that represent a table state
        :return: bson, dict that represent the data dictionary in one state
        """
        return self.collection.find({"stateTable": state}, {"raw_name": 1, "master_name": 1, "baseName": 1, "user": 1,
                                                            "alias": 1, "stateTable": 1, "FieldReview": 1, "fieldsRaw": 1, "fieldsMaster": 1, "modifications": 1})

    def sent_to(self, table_id, state):
        """
            Updates the property stateTable for the data dictionary which table_id parameter is equal to
            id property in data dictionary collection.

        :param table_id: str, string that represent a table id.
        :param state: str, string that represent data dictionary state.
        """
        self.collection.update_one({"_id": ObjectId(str(table_id).strip())}, {
                                   "$set": {"stateTable": state}})

    def find_all_tables_to_word(self):
        """
            Finds all data dictionaries, which stateTable is equal to "I"("In ingesting process"), "IN"(Ingested),
            "M"(job meshes), "I"(Quality) or "P"(Production)
        :return:
        """
        return self.collection.find({"stateTable": {"$in": ["I", "IN", "M", "Q", "P"]}}, {"_id": 1})

    def find_fields(self, table_id, data):
        """
            Finds one data dictionary with the properties defined in the data parameter.

        :param table_id: str, string that represent a table id
        :param data: dict, dictionary that represent a query with what columns need
        :return: bson, dict with columns required
        """
        return self.collection.find_one({"_id": ObjectId(str(table_id).strip())}, bson.BSON.decode(bson.BSON.encode(data)))

    def find_field(self, table_id, field):
        """
           re Finds one data dictionary with the property defined in the field parameter.

        :param table_id: str, string that represent a table id
        :param field: str, string that represent a field
        :return: bson, dict with a field data
        """
        return self.collection.find_one({"_id": ObjectId(str(table_id).strip())}, {field: 1})

    def find_legacys(self, table_id, field, legacy):
        """
            Finds all the data dictionaries which parameter fields includes the property legacy.legacy equals to
            legacy parameter without compare to the data dictionary which id is equal to table_id parameter.

        :param table_id: str, string that represent a table id
        :param field: str, string that represent a field to get
        :param legacy: str, string that represent a legacy name to get
        :return: bson, dict that represent a data dictionary with a one specific legacy
                in a field
        """
        return self.collection.find({"_id": {"$ne": ObjectId(str(table_id).strip())}, field: {"$elemMatch": {"legacy.legacy": legacy}}})

    def generate_aggreation(self, pipeline):
        """
            Generates any pipeline aggregation. A Pipeline is a complex query, which contains operations group values
            from multiple documents together, and can perform a variety of operations on the grouped data
            to return a single result.

        :param pipeline: json. A dict that represent any pipeline.
        :return: bson, result of aggregation pipeline.
        """
        return self.collection.aggregate(pipeline)

    def user_modification_table(self, user_id, start_date, end_date, field):
        """
            Finds all the data dictionaries modified by one user between two days.

        :param user_id: str, string that represent user id
        :param start_date: datetime, start date to check
        :param end_date: datetime, end date to check
        :param field: str, string that represent a field table
        :return: bson, dict with field that were modified between two days
        """
        return self.collection.find(
            {field: {'$elemMatch': {'modification': {'$elemMatch': {'user': ObjectId(str(user_id).strip()),
                                                                    'startDate': {'$gte': start_date, '$lte': end_date}}
                                                     }}}},
            {"_id": 0, field: 1}
        )

    def find_naming_table(self, naming, field):
        """
            Finds all data dictionaries that has one exactly naming
            in the property field (FieldReview, fieldsMaster or fieldsRaw).

        :param naming: str, string that represent a naming
        :param field: str, string that represent a field
        :return: bson, with tables that has one exactly naming
        """
        return self.collection.find({field: {"$elemMatch": {"naming.naming": str(naming).lower()}}})

    def exists_field(self, table_id, field):
        """
            Checks if one data dictionary has the property field parameter in their data.

        :param table_id: str, string that represent a table id
        :param field: str, string that represent a field to check
        :return: bson, dictionary that has data dictionary with that field
        """
        return self.collection.find_one({"_id": ObjectId(str(table_id).strip()), field: {"$exists": True}})

    def find_tables_filter(self, table_name, alias, table_state, legacy_name, legacy_desc):
        """
            Finds all data dictionaries which has

        :param table_name: str, string that represent a physical name of the data dictionary
        :param alias: str, string that represent the alias of the data dictionary
        :param table_state: str, string that represents the state of the data dictionary
        :param legacy_name: str, string that represents the base name of the data dictionary
        :param legacy_desc: str, string that represents the observation field of the data dictionary
        :return: bson, dictionary that has data dictionary with that field
        """
        return self.collection.find({"$or": [{"alias": str(alias).strip().lower()},
                                             {"raw_name": str(
                                                 table_name).strip().lower()},
                                             {"master_name": str(
                                                 table_name).strip().lower()},
                                             {"stateTable": str(
                                                 table_state).strip().upper()},
                                             {"baseName": str(
                                                 legacy_name).strip().upper()},
                                             {"observationField": str(legacy_desc).strip()}]
                                     }, {"baseName": 1, "uuaaRaw": 1, "uuaaMaster": 1, "raw_name": 1,
                                         "master_name": 1, "user": 1, "stateTable": 1,
                                         "observationField": 1, "alias": 1})

    def find_one_filter(self, data_dictionary_id):
        """
            Finds one the data dictionary document in the dataDictionary collection
            which id is equal to the data_dictionary_id parameter.
           In other case returns None.

        :param data_dictionary_id: str, a string that represents the id of the data dictionary document
                in the dataDictionary collection.
        :return: bson or None, a bson with the data dictionary document found, otherwise return a None.
        """
        return self.collection.find_one({"_id": ObjectId(str(data_dictionary_id).strip())},
                                        {"baseName": 1, "uuaaRaw": 1, "uuaaMaster": 1, "raw_name": 1, "master_name": 1,
                                         "user": 1, "stateTable": 1, "observationField": 1, "alias": 1})

    def find_table_by_alias(self, alias):
        """
            Finds if the alias exist in other data dictionary.
        :param alias: str, represents the alias of the table
        :return: True or False
        """
        alias_exist = True
        if self.collection.find_one({"alias": str(alias).lower()}) is None:
            alias_exist = False
        return alias_exist

    def find_alias_repeat(self, alias, table_id):
        """
            Finds if the alias exist in other data dictionary.

        :param alias: str, represents the alias of the data dictionary.
        :param table_id: str, represents id of the dictionary.
        :return: True or False
        """
        alias_exist = True
        if self.collection.find_one({"alias": alias, "_id": {"$ne": ObjectId(str(table_id).strip())}}) is None:
            alias_exist = False
        return alias_exist

    def find_all_full(self):
        """
            Finds all data dictionaries with all data

        :return: An array with all data dictionaries.
        """
        return self.collection.find({})

    def find_all(self):
        """
            Finds all data dictionaries without fields (FieldReview, fieldsRaw, fieldsMaster).

        :return: An array with all data dictionaries.
        """
        return self.collection.find({}, {"_id": 1, "baseName": 1, "uuaaRaw": 1, "uuaaMaster": 1, "raw_name": 1,
                                         "master_name": 1, "user": 1, "stateTable": 1, "observationField": 1,
                                         "alias": 1, "fieldsRaw": 1, "fieldsMaster": 1})

    def find_all_statistics(self):
        """
            Finds all data dictionaries for statistics usage, method can vary

        :return: An array with all data dictionaries.
        """
        return self.collection.find({}, {"_id": 1, "uuaaRaw": 1, "uuaaMaster": 1, "periodicity": 1,
                                         "periodicity_master": 1, "originSystem": 1, "stateTable": 1})

    def update_check_status(self, table_id, array_name, user, check, column, comments):
        """
            Updates property check of the data dictionary which id is equal to table_id parameter.
            The property check is used for approving the data dictionary by the reviewer.

        :param table_id: str, represents id of the dictionary
        :param user: str, represents id of the reviewer
        :param check: str, Expects NOK or OK.
        :param column: str, column of the field (FieldReview, fieldsRaw or fieldsMaster)
        :param comments: array, comments by the reviewer.
        """
        self.collection.update_one({"_id": ObjectId(str(table_id).strip())}, {"$set": {
            array_name + "." + str(column) + ".check.status": check, array_name + "." + str(column) + ".check.user": user,
            array_name + "." + str(column) + ".check.comments": comments}})

    def update_modifications(self, table_id, array_name, modification, column):
        """
            Updates the property modification by the reviewer.
            The property modifications saved information related to who edit a data dictionary and the date.

        :param table_id: str. Represents id of the dictionary.
        :param modification: array. Modifications by an user.
        :param column: str. Column of the fieldsRaw.
        """
        self.collection.update_one({"_id": ObjectId(str(table_id).strip())},
                                    {"$set": {array_name+"."+str(column)+".modification": modification}})

    def update_people(self, table_id, people):
        """
             Updates the property people.
             The property people saved information related to the people who edits a data dictionary.

        :param table_id: str. Represents id of the dictionary.
        :param people: array. People who changed the data dictionary.
        """
        self.collection.update_one({"_id": ObjectId(str(table_id).strip())}, {
                                   "$set": {"people": people}})

    def find_to_check(self, table_id, status):
        """
            Finds if the data dictionary is available to check by the reviewer.

        :param table_id: str. Represents id of the dictionary.
        :return  An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data dictionary documents,
                If the query's result is one data dictionary document returns a bson, otherwise return a None.
        """
        return self.collection.find_one({"$and": [{"_id": ObjectId(str(table_id).strip())},
                                                    {"stateTable": status}, {"fieldsRaw": {"$exists": True}}]},
                                                    {"fieldsRaw": 1, "fieldsMaster": 1, "baseName": 1, "master_name": 1, "raw_name": 1, "uuaaMaster": 1,
                                                    "uuaaRaw": 1, "observationField": 1, "alias": 1, "typeFile": 1})

    def update_change_backlog(self, table_id, backlog_info):
        """
            Updates one data dictionary with the information which changes for the backlog associated.

        :param table_id: str. Represents id of the dictionary.
        :param backlog_info: json. Contains the perimeter and require depth  of the data dictionary.
        """
        self.collection.update_one({"_id": ObjectId(str(table_id).strip())},
                                   {"$set": {"perimeter": backlog_info["perimeter"],
                                             "required_depth": backlog_info["history"]}})

    def update_raw_phase_field(self, table_id, type_master, alphanumeric, format_type, column):
        """
            Updates the properties logicalFormat, format and outFormat for a data dictionary which id is equal
            to table_id parameter.

        :param table_id: str. Represents id of the dictionary.
        :param type_master: str. Data type for master phase.
        :param alphanumeric: str. Logic format for raw phase.
        :param format_type: str. Data format for raw phase. (e.g dd-MM-yyyy)
        :param column: str. Position in the raw fields.
        """
        self.collection.update_one({"_id": ObjectId(str(table_id).strip())}, {"$set": {
            "fieldsRaw." + str(column) + ".logicalFormat": alphanumeric,
            "fieldsRaw." + str(column) + ".format": format_type,
            "fieldsRaw." + str(column) + ".outFormat": type_master,
        }})

    def update_master_phase_field(self, table_id, ingest_type, governance_type, format_type, column):
        """
             Updates the properties logicalFormat, format and destinationType for a data dictionary which id is equal
            to table_id parameter.

        :param table_id: str. Represents id of the dictionary.
        :param ingest_type: str. Data type for master phase.
        :param governance_type: str. Logic format for master phase.
        :param format_type: str. Data format for master phase. (e.g dd-MM-yyyy)
        :param column: str. Position in the master fields.
        """
        self.collection.update_one({"_id": ObjectId(str(table_id).strip())}, {"$set": {
            "fieldsMaster." + str(column) + ".logicalFormat": governance_type,
            "fieldsMaster." + str(column) + ".format": format_type,
            "fieldsMaster." + str(column) + ".destinationType": ingest_type,
        }})

    def find_raw_field_origin(self, table_id, origin):
        """
            Finds if a raw field in the data dictionary is generated by the parameter origin.

        :param table_id: str. Represents id of the dictionary.
        :param origin: str. Origin for a raw field.
        :return  An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data dictionary documents,
                If the query's result is one data dictionary document returns a bson, otherwise return a None.
        """
        return self.collection.find({"_id": ObjectId(str(table_id).strip()),
                                     "fieldsRaw": {"$elemMatch": {"origin": origin}}}, {"fieldsRaw.$": 1})

    def find_master_field_origin(self, table_id, origin):
        """
            Finds if a master field in the data dictionary is generated by the parameter origin.

        :param table_id: str. Represents id of the dictionary.
        :param origin:  str. Origin for a master field.
        :return  An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data dictionary documents,
                If the query's result is one data dictionary document returns a bson, otherwise return a None.
        """
        return self.collection.find({"_id": ObjectId(str(table_id)),
                                     "fieldsMaster": {"$elemMatch": {"origin": origin}}}, {"fieldsMaster.$": 1})

    def find_origin_data_tables(self):
        """
            Finds all the data dictionaries available to be a data source for a data table.

        :return  An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data dictionary documents,
                If the query's result is one data dictionary document returns a bson, otherwise return a None.
        """
        return self.collection.find({"stateTable": {"$nin": ["G", "N", "RN"]}},
                                    {"_id": 1, "baseName": 1, "uuaaRaw": 1, "uuaaMaster": 1, "raw_name": 1,
                                     "master_name": 1, "user": 1,
                                     "observationField": 1, "alias": 1, "physical_name_source_object": 1,
                                     "master_route": 1, "master_path": 1,
                                     "periodicity": 1, "typeFile": 1, "originSystem": 1, "raw_route": 1,
                                     "raw_path": 1, "periodicity_master": 1})

    def find_inside_naming(self, data_dictionary_id, phase, naming):
        """
            Finds one naming in field's parameter in the data dictionary phase parameter..

        :param data_dictionary_id: str. Represents id of the dictionary.
        :param phase: str. Expects FieldReview, fieldsRaw or fieldsMaster.
        :param naming: str. Naming for search inside of the data dictionary.
        :return  An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data dictionary documents,
                If the query's result is one data dictionary document returns a bson, otherwise return a None.
        """
        return self.collection.find({"$and": [{"_id": ObjectId(str(data_dictionary_id).strip())},
                                              {phase: {"$elemMatch": {"naming.naming": naming.strip().lower()}}}]},
                                    {phase + ".$": 1, "raw_name": 1, "master_name": 1})

    def find_raw_name(self, raw_name):
        """
            Finds one data dictionary by raw_name property.

        :param raw_name: str. Raw name of the data dictionary phase.
        :return  An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data dictionary documents,
                If the query's result is one data dictionary document returns a bson, otherwise return a None.
        """
        return self.collection.find_one({"raw_name": raw_name.strip().lower()},
                                        {"fieldsRaw": 0, "fieldsMaster": 0, "FieldsReview": 0})

    def find_master_name(self, master_name):
        """
            Finds one data dictionary by master_name property.

        :param master_name: str. Master name of the data dictionary phase.
        :return  An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data dictionary documents,
                If the query's result is one data dictionary document returns a bson, otherwise return a None.
        """
        return self.collection.find_one({"master_name": master_name.strip().lower()},
                                        {"fieldsRaw": 0, "fieldsMaster": 0, "FieldsReview": 0})

    def update_modifications_audit(self, table_id, modifications):
        """
            Updates the modifications property for the data dictionary which id is equal to table_id parameter.

        :param table_id: str. Represents id of the dictionary.
        :param modifications: array. Modifications made by users.
        """
        self.collection.update_one({"_id": ObjectId(str(table_id))}, {
                                   "$set": {"modifications": modifications}})

    def find_dd_property(self, property_name, property_value):
        """
            Finds all DDs that has the property with the respective value

        :param table_id: str. Represents id of the dictionary.
        :param property_name: str. Represents the name of the property to look for inside the table
        :param property_value: str. Represents the value of the property to look for inside the table
        """
        return self.collection.find({property_name: property_value}, {"_id": 1, "baseName": 1, "uuaaRaw": 1, "uuaaMaster": 1, "raw_name": 1,
                                                                      "master_name": 1, "FieldReview": 1, "fieldsRaw": 1, "fieldsMaster": 1,
                                                                      "alias": 1, "periodicity": 1, "typeFile": 1, "originSystem": 1, "raw_route": 1, "master_route": 1})

    def find_distinct_property(self, property_name):
        """
            Finds all functional map documents in the functionalMap's collection.

        :return: A bson or None, a bson that contains the query's result, otherwise return a None.
        """
        return self.collection.distinct(property_name)

    def update_raw(self, table_id, options):
        """
            Deletes raw props from the table and makes it as there's no raw phase.

        :return: A bson or None, a bson that contains the query's result, otherwise return a None.
        """
        self.collection.update_one({"_id": ObjectId(str(table_id))},
                                   {"$set": {"fieldsRaw": [], "raw_path": "", "raw_route": "", "partitions_raw": "", "modelVersion": "", "objectVersion": ""}})

    def update_master(self, table_id, options):
        """
            Deletes master props from the table and makes it as there's no master phase.

        :return: A bson or None, a bson that contains the query's result, otherwise return a None.
        """
        self.collection.update_one({"_id": ObjectId(str(table_id))},
                                   {"$set": {"fieldsMaster": [], "master_name": "", "master_path": "", "master_route": "", "partitions_master": ""}})
        """"self.collection.update_one({"_id": ObjectId(str(table_id))}, {"$set": {"master_name": "" }})"""

    def dd_count(self):
        """
            Return the estimated count of the number of data dictionaries, documents in this collection.

        :return: number, result of the estimated count
        """
        return self.collection.estimated_document_count()
