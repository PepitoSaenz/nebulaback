import pymongo
from django.conf import settings


class Excels_repository(object):
    """
    A class used to represent the all collection extract from dynamics excels

    Attributes
            -------
            origin_system : collection
                Creates the origin system collection and applies the queries and operations.
            periodicity: collection
                Creates the periodicity system collection and applies the queries and operations.
            governance_statistics: collection
                Creates the governance statistics collection and applies the queries and operations.

    """

    def __init__(self):
        """
            Create a collections to consult.
        """
        self.origin_system = settings.MONGO_DATABASE['originSystem']
        self.periodicity = settings.MONGO_DATABASE['periodicity']
        self.governance_statistics = settings.MONGO_DATABASE['statisticsClassification']

    def find_origin_systems(self):
        """
            Finds all origin systems by ascending sort.

        :return: bson, dict with origin systems
        """
        return self.origin_system.find({}, {"system": 1, "_id": 0}).sort([("system", pymongo.ASCENDING)])

    def find_periodicities(self):
        """
            Finds all periodicity options sorting by ascending.

        :return: bson, dictionary with all periodicity
        """
        return self.periodicity.find({}, {"periodicity": 1, "_id": 0}).sort([("periodicity", pymongo.ASCENDING)])
    
    def find_periodicities_complete(self):
        """
            Finds all periodicity options sorting by ascending.

        :return: bson, dictionary with all periodicity
        """
        return self.periodicity.find({}, {"_id": 0}).sort([("periodicity", pymongo.ASCENDING)])

    def find_governance_statistics(self):
        """
            Finds all governance classification for review options

        :return: bson, dictionary with all governance classification.
        """
        return self.governance_statistics.find({}, {"_id": 0, "statistics": 0}).sort([("error", pymongo.ASCENDING)])

    def find_periodicity_field(self, periodicity, field):
        """
            Finds one periodicity and shows field parameter.

        :param periodicity: str, string that represent a periodicity
        :param field: str, string that represent a field to search
        :return: bson, dictionary with one periodicity option.
        """
        return self.periodicity.find_one({"periodicity": str(periodicity).upper()}, {field: 1, "_id": 0})

    def find_one_periodicity(self, periodicity):
        """
            Finds one periodicity by the property periodicity.
            Periodicity e.g DIARIO, SEMANAL, MENSUAL, ANUAL, etc.

        :param periodicity: str. Periodicity name.
        :return: bson, dictionary with the periodicity option.
        """
        return self.periodicity.find_one({"periodicity": str(periodicity).strip().upper()})

    def find_state_periodicity(self, state):
        """
            Finds one periodicity option by state property.
            State e.g DAILY, WEEKLY, MONTHLY, ANNUALLY, etc.

        :param state: str. State option.
        :return: bson, dictionary with the periodicity option.
        """
        return self.periodicity.find_one({"state": str(state).strip().upper()})

    def find_one_system(self, system):
        """
            Finds one origin system option by the property system.
            System e.g LAR, OMEGA, ORACLE, HOST, etc.

        :param: system, str. System name.
        :return: bson, dictionary with origin system option.
        """
        return self.origin_system.find_one({"system": str(system).strip()})

    def find_abbreviation_system(self, system):
        """
           Finds one origin system by the property system.

        :param system: str, string that represent a origin system.
        :return: bson, dictionary with one origin system data
        """
        return self.find_one_system(system)
