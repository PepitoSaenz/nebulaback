from django.conf import settings
from bson import ObjectId
import datetime
import pymongo


class User_repository(object):
    """A class used to represent the backlog collection.

                Attributes
                -------
                collection : collection
                    Creates the users collection and applies the queries and operations.
    """
    def __init__(self):
        """
            The constructor of the Data_dictionary_repository class.
        """
        self.collection = settings.MONGO_DATABASE['users']

    def insert_one(self, user):
        """
            Inserts the user parameter, which is a users document, to the users's collection.

        :param user: User entity, a instance of the User class.
        """
        self.collection.insert_one(user)

    def find_one(self, user_id):
        """
            Finds the user document in the users's collection which id is equal to the user_id parameter.

        :param user_id: str, a string that represents the id of the user document in the users's collection.
        :return: A bson or None, a bson with the user document found, otherwise return a None.
        """
        return self.collection.find_one({
            "_id": ObjectId(str(user_id).strip())
        })

    def find_by_email(self, email):
        """
           Finds the user document in the users's collection which email is equal to the email parameter.

        :param email: str, a string with the user email.
        :return: A bson or None, a bson with the user document found, otherwise return a None.
        """
        return self.collection.find_one({"email": email})

    def login(self, email, password):
        """
           Finds the user document in the users's collection which email field is equal to the email parameter and
           password field is equal to password parameter.

        :param email: str.
        :param password: str.
        :return: A bson or None, a bson with the user document found, otherwise return a None.
        """
        return self.collection.find_one({"email": email, "password": password})

    def find_team(self, team_id):
        """
            Finds the user documents in the users's collection which teams fields contains id_team.

        :param team_id: str, a string that represents the id of the team document in the collection.
        :return: A bson or None, a bson with the user document found, otherwise return a None.
        """
        return self.collection.find({"teams": {"$in": [ObjectId(team_id)]}}).sort([("name", pymongo.ASCENDING)])

    def find_all(self):
        """
            Finds all user documents in the users's collection sorted by name. If the users's collection is empty, returns None.

        :return: Array of bson or None, an Arrays of bson that contains all user documents
        found in the users's collection, otherwise return a None.
        """
        return self.collection.find({}).sort([("name", pymongo.ASCENDING)])

    def get_registered_users(self):
        """
            Returns all user documents in the users's collection sorted by name if the have a role, this means having something on the rol attribute.

        :return: Array of bson or None, an Arrays of bson that contains all user documents
        found in the users's collection, otherwise return a None.
        """
        return self.collection.find({"rol": { "$ne": "" }}, {"name": 1, "email": 1, "rol": 1, "last_login": 1, "teams": 1}).sort([("name", pymongo.ASCENDING)])

    def find_all_names(self):
        """
            Finds all the users's name in the users collection sorted by name. If the users's collection is empty, returns None.

        :return: Array of bson or None, an Arrays of bson that contains all user documents
        found in the users's collection, otherwise return a None.
        """
        return self.collection.find({}, {"name": 1, "email": 1, "_id": 1}).sort([("name", pymongo.ASCENDING)])

    def find_by_rol(self, rol):
        """
            Finds all users with one specific rol.

        :param rol: str, string that represent a rol to find
        :return: bson, dictionary that represent a users data with a rol
        """
        return self.collection.find({"rol": rol}, {"name": 1, "email": 1, "_id": 1}).sort([("name", pymongo.ASCENDING)])

    def generate_aggregate(self, pipeline):
        """
            Generates any pipeline aggregation. A Pipeline is a complex query, which contains operations group values
            from multiple documents together, and can perform a variety of operations on the grouped data
            to return a single result.

        :param pipeline: json. A dict that represent any pipeline.
        :return: bson, result of aggregation pipeline.
        """
        return self.collection.aggregate(pipeline)

    def update_password(self, user_data):
        """
            Updates user password.
        :param user_data: dict.
        """
        self.collection.update_one({"_id": ObjectId(user_data["user"])}, {"$set": {"password": user_data["newPasswrd"]}})

    def find_pending(self):
        """
            Finds all the users who are pending for assigning rol. The result is sorted by name

        :return: bson, dictionary that represent a users data with a rol.
        """
        return self.collection.find({"rol": ""}).sort([("name", pymongo.ASCENDING)])

    def update_user_data(self, user_id, user_data):
        """
            Updates one user information.

        :param user_id: str. Represents the property id.
        :param user_data: json. Instance of the user class entity.
        """
        self.collection.update_one({"_id": ObjectId(str(user_id))}, {"$set": user_data})

    def last_login(self, user_id):
        """
            Updates one the las login for the user which user_id parameter is equal to property id in the collection.

        :param user_id: str. Represents the property id.
        """
        self.collection.update_one({"_id": ObjectId(str(user_id))}, {"$set": {"last_login": datetime.datetime.now()}})

    def find_partners_by_name(self, teams):
        """
            Finds all the users who are part of the teams array.

        :param teams: array.
        :return: bson, dictionary that represents the query results.
        """
        query = {"$or": []}
        for i in teams:
            query["$or"].append({"teams": {"$in": [ObjectId(str(i))]}})
        return self.collection.find(query, {"_id": 1, "name": 1, "user": 1}).sort([("name", pymongo.ASCENDING)])
