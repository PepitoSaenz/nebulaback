
from django.conf import settings
import bson


class Word_repository(object):
    """A class used to represent the word collection.

            Attributes
            -------
            collection : collection
                Creates the word collection and applies the queries and operations.

    """
    def __init__(self):
        """
            The constructor of the Word_repository class.
        """
        self.collection = settings.MONGO_DATABASE['words']

    def find_one(self, word):
        """
            Finds the word document in the word's collection which the longName or
            abbreviation fields is equal to the word parameter.

        :param word: str, a string with the word to find.
        :return: bson or None, a bson with the word document found,otherwise return a None.
        """
        return self.collection.find_one({
            "$or": [{"longName": word.strip().upper()}, {"abbreviation": word.strip().upper()}]
        })

    def find_one_by_id(self, word_id):
        """
            Finds the word document in the words's collection which id field is equal to the id_word parameter.

        :param word_id: str, a string that represents the id of the word document in the collection.
        :return: bson or None, a bson that contains the word document found, otherwise return a None.
        """
        return self.collection.find_one({
            "_id": word_id
        })

    def find_by_long_name(self, long_name):
        """
            Finds the word document in the words's collection which longName field is equal to the long_name parameter.

        :param long_name: str, a string with the long name to find.
        :return: bson or None, a bson that contains the word document found, otherwise return a None.
        """
        return self.collection.find_one({
            "longName": long_name.strip().upper()
        })

    def find_all(self):
        """
            Finds all the 1000 first documents in the collection.

        :return: bson or None, a bson that contains the word document found, otherwise return a None.
        """
        return self.collection.find(
            {"abbreviation": {"$ne": ""}}, {"_id": 0, "longName": 1, "abbreviation": 1, "spa_word": 1,
                                            "synonymous": 1, "community": 1}).limit(1000)

    def find_any_query(self, query):
        """
            Generates any pipeline aggregation. A Pipeline is a complex query, which contains operations group values
            from multiple documents together, and can perform a variety of operations on the grouped data
            to return a single result.

        :param query: json. A dict that represent any pipeline.
        :return: bson, result of aggregation pipeline.
        """
        return self.collection.find(
            bson.BSON.decode(bson.BSON.encode(query)), {"_id": 0, "longName": 1, "abbreviation": 1, "spa_word": 1,
                                                        "synonymous": 1, "community": 1}).limit(1000)

    def find_long_or_abbreviation(self, word):
        """
            Finds one word by the property longName or abbreviation.
            For example, The client or people its longName is customer and its abbreviation is cust.

        :param word: str.
        :return: bson or None, a bson that contains the word document found, otherwise return a None.
        """
        return self.collection.find_one({"$or": [{"longName": str(word).upper()}, {"abbreviation": str(word).upper()}]})
