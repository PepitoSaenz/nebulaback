
import pymongo
from bson import ObjectId
from django.conf import settings


class Functional_map_raw_repository(object):
    """A class used to represent the functionalMap collection.

            Attributes
            -------
            collection : collection
                Creates the functionalMap collection and applies the queries and operations.

    """
    def __init__(self):
        """
            The constructor of the Functional_map_repository class.
        """
        self.collection = settings.MONGO_DATABASE['functionalMapRaw']

    def insert_one(self, new_uuaa):
        """
            Inserts the new_uuaa parameter, which is an uuaa document,to the functionalMapRaw collection.

        :param new_uuaa: the new uuaa entity, a instance of the uuaa class.
        """
        return self.collection.insert_one(new_uuaa).inserted_id

    def find_one(self, uuaa_id):
        """
            Returns the uuaa document in the functionalMapRaw collection which id is equal to
            the uuaa_id parameter. In other case returns None.

        :param uuaa_id: str, a string that represents the id of the data dictionary document
                in the functionalMapRaw collection.
        :return: bson or None, a bson with the data dictionary document found, otherwise return a None.
        """
        return self.collection.find_one({"_id": ObjectId(str(uuaa_id).strip())})

    def find_uuaa(self, uuaa):
        """
            Finds one functional map which uuaa field is equal to the uuaa parameter.

        :param uuaa: str. Application unit. eg. SAN, COG.
        :return: A bson or None, a bson that contains the query's result, otherwise return a None.
        """
        return self.collection.find_one({
            "uuaa": str(uuaa).upper()
        })

    def find_all(self):
        """
            Finds all functional map documents in the functionalMap's collection.

        :return: A bson or None, a bson that contains the query's result, otherwise return a None.
        """
        return self.collection.find({}).sort([("uuaa", pymongo.ASCENDING)])

    def find_distinct_property(self, property_name):
        """
            Finds all functional map documents in the functionalMap's collection.

        :return: A bson or None, a bson that contains the query's result, otherwise return a None.
        """
        return self.collection.distinct(property_name)

    def update_uuaa(self, uuaa_id, uuaa_data):
        """
            Updates datasource and description for a uuaa

        :param table_id: str, string that represent a table id
        :param query: json, dict that represent any query to update a data dictionary
        """
        self.collection.update_one({"_id": ObjectId(str(uuaa_id).strip())}, {"$set": {
                                    "uuaa": uuaa_data["uuaa"], "nameSimple": uuaa_data["nameSimple"],
                                    "level1": uuaa_data["level1"], "level2": uuaa_data["level2"],
                                    "level3": uuaa_data["level3"],
                                    "descriptionApp": uuaa_data["descriptionApp"],
                                    "descriptionFunctional": uuaa_data["descriptionFunctional"],
                                    "geography": uuaa_data["geography"],
                                    "area": uuaa_data["area"],
                                    "originCountry": uuaa_data["originCountry"],
                                    "example": uuaa_data["example"],
                                    "route": uuaa_data["route"],
                                    "modifications": uuaa_data["modifications"]}})


