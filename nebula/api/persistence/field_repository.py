
from django.conf import settings
import bson


class Field_repository(object):
    """
        A class used to represent the field collection.

            Attributes
            -------
            collection : collection
                Creates the field collection and applies the queries and operations.
    """
    def __init__(self):
        """
            The constructor of the Field_repository class.
        """
        self.collection = settings.MONGO_DATABASE['field']
    
    def find_all_limit(self, limit):
        return self.collection.find().limit(int(limit))

    def find_all_skip(self, skip):
        return self.collection.find().skip(int(skip))
    
    def find_hierarchy(self):
        """
            Finds the distinct values for the level property in the all field documents.

        :return: An array of bson or None,
                 An array of bson that contains the query's result, otherwise return a None.
        """
        return sorted(self.collection.distinct("level"))

    def find_functional_levels_1(self):
        """
            Finds the sorted distinct values for the funtionalLevel1 property in the all field documents.

        :return: An array of bson or None, an array of bson that contains the query's result, otherwise return a None.
        """
        return sorted(self.collection.distinct("funtionalLevel1"))

    def find_functional_levels_2(self):
        """
            Finds the sorted distinct values for the funtionalLevel2 field in the all field documents.

        :return: An array of bson or None, an array of bson that contains the query's result, otherwise return a None.
        """
        return sorted(self.collection.distinct("functionalLevel2"))

    def find_any_query(self, query):
        """
            Finds the result of the query in the field's collection.

        :param query: str, a string  with the query.
        :return: An array of bson, bson or None, an array of bson that contains the query's result when its more than one field document,
                If the query's result is one field document returns a bson, otherwise return a None.
        """
        return self.collection.find(bson.BSON.decode(bson.BSON.encode(query)),
                                    {"descEnglish": 0, "logicEnglish": 0, "dataOwnerInformation": 0})

    def find_exactly_naming(self, naming):
        """
            Finds one the field document which contains in the naming.naming field the naming parameter.

        :param naming: str.
        :return: A bson or None, a bson with the field document's information, otherwise return a None.
        """
        return self.collection.find_one({
            "naming.naming": naming.lower()}, {"descEnglish": 0, "logicEnglish": 0})

    def find_exactly_logical_name(self, logical_name):
        """
            Finds one the field document which contains in the logic field the logical name parameter.

        :param logical_name: str, that represent the logical name
        :return: A bson or None, a bson with the field document's information, otherwise return a None
        """
        return self.collection.find_one({
            "logic": logical_name.upper()}, {"descEnglish": 0, "logicEnglish": 0})

    def find_exactly_description(self, description):
        """
            Finds one the field document which contains in the logic field the description parameter.

        :param description: str, that represent the description
        :return: A bson or None, a bson with the field document's information, otherwise return a None
        """
        return self.collection.find_one({
            "description": description.upper()}, {"descEnglish": 0, "logicEnglish": 0})

    def find_naming_data_owner_information(self, naming):
        """
            Finds data owner information associate to a naming.

        :param naming: str, string that represent a naming
        :return: bson, dictionary with data owner information associate to a naming
        """
        return self.collection.find_one({
            "naming.naming": naming.lower()
        }, {
            "dataOwnerInformation": 1, "_id": 0
        })

    def find_naming_all_data(self, naming):
        """
            Finds one naming in the collection.

        :param naming: str, string that represent a naming
        :return: bson, dictionary with naming data
        """
        return self.collection.find_one({"naming.naming": naming.lower()})

    def find_naming_all(self, naming):
        """
            Finds one the field document which contains in the naming.naming field the naming parameter.

        :param naming: str
        :return: A bson or None, a bson with the field document's information, otherwise return a None.
        """
        return self.collection.find_one({
            "naming.naming": naming.lower()}, {"descEnglish": 0, "logicEnglish": 0})

    def find_global_query(self, query):
        """
            Generates any pipeline aggregation. A Pipeline is a complex query, which contains operations group values
            from multiple documents together, and can perform a variety of operations on the grouped data
            to return a single result.

        :param query: json. A dict that represent any pipeline.
        :return: A bson or None, a bson with the field document's information, otherwise return a None.
                the result is up to 1000. , 'level': {'$ne': "SPECIFIC"} 
        """
        pipeline = [
            {'$match': {'naming.suffix': {'$ne': -1}}},
            {'$match': query},
            {'$project': {
                'naming': 1,
                'logic': 1,
                'originalDesc': 1,
                'logicEnglish': 1,
                'descEnglish': 1,
                'level': 1,
                'code': 1,
                'codeLogic': 1,
                'countriesUse': 1,
                'governanceFormat': 1,
                'funtionalLevel1': 1,
                'functionalLevel2': 1,
                'colombianFormat': 1,
                'created_time': 1,
                'modification': 1
                }},
            {'$sort': {'countriesUse': -1}},
            {'$limit': 100000}
        ]
        return self.collection.aggregate(pipeline)
    

    def namings_count(self):
        """
            Return the estimated count of the number of namings, documents in this collection.

        :return: number, result of the estimated count
        """
        return self.collection.estimated_document_count()
