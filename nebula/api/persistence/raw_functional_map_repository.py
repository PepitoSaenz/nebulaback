
import pymongo
from django.conf import settings


class Raw_functional_map_repository(object):
    """A class used to represent the functionalMap collection.

            Attributes
            -------
            collection : collection
                Creates the functionalMap collection and applies the queries and operations.

    """
    def __init__(self):
        """
            The constructor of the Functional_map_repository class.
        """
        self.collection = settings.MONGO_DATABASE['rawFunctionalMap']

    def find_uuaa(self, uuaa):
        """
            Finds one functional map which uuaa field is equal to the uuaa parameter.

        :param uuaa: str. Application unit. eg. SAN, COG.
        :return: A bson or None, a bson that contains the query's result, otherwise return a None.
        """
        return self.collection.find_one({
            "uuaa": str(uuaa).upper()
        })

    def find_all(self):
        """
            Finds all functional map documents in the functionalMap's collection.

        :return: A bson or None, a bson that contains the query's result, otherwise return a None.
        """
        return self.collection.find({}).sort([("uuaa", pymongo.ASCENDING)])

    def update_uuaa(self, uuaa):
        """
            Updates datasource and description for a uuaa

        :param table_id: str, string that represent a table id
        :param query: json, dict that represent any query to update a data dictionary
        """
        self.collection.update_one({"uuaa": uuaa["uuaa"]}, {"$set": {"app": uuaa["app"], "originSystem":uuaa["originSystem"], "description": uuaa["description"], "insertDate": uuaa["insertDate"], "userId": uuaa["userId"]}})
        
    def remove_uuaa(self, uuaa):
        """
            Updates datasource and description for a uuaa

        :param table_id: str, string that represent a table id
        :param query: json, dict that represent any query to update a data dictionary
        """
        #self.collection.deleteOne({"uuaa": uuaa})
    
    def insert_uuaa(self, uuaa):
        """
            Inserts one operational data base to the collections.

        :param operational_base: str. Base operational entity class.
        :return operational data base id in the collection.
        """
        return self.collection.insert_one(uuaa).inserted_id