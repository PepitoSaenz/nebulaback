
from django.conf import settings
import bson


class Acl_repository(object):
    """A class used to represent the alhambra excels

              Attributes
              -------
              alhambra_work_collection : collection
                  Creates the alhambraWork collection and applies the queries and operations.

              alhambra_live_collection : collection
                  Creates the alhambraLive collection and applies the queries and operations.

              users_work_collection : collection
                  Creates the alhambraGroupsWork collection and applies the queries and operations.

              users_live_collection : collection
                  Creates the alhambraGroupsLive collection and applies the queries and operations.

    """

    def __init__(self):
        """
            The constructor of the Acl_repository class.
        """
        self.alhambra_work_collection = settings.MONGO_DATABASE['alhambraWork']
        self.alhambra_live_collection = settings.MONGO_DATABASE['alhambraLive']
        self.users_work_collection = settings.MONGO_DATABASE['alhambraGroupsWork']
        self.users_live_collection = settings.MONGO_DATABASE['alhambraGroupsLive']

    def find_one_group(self, group, env):
        """
            Finds the users for a group.
        :param group: str. Alhambra group name.
        :param env: str. Expects work or live.
        :return: result: array. Result for the query.
        """
        result = []
        if env == "work":
            result = self.users_work_collection.find_one({
                "group": group
            })
        elif env == "live":
            result = self.users_live_collection.find_one({
                "group": group
            })
        return result

    def find_user_in_group(self, group, user, env):
        """
            Returns True if user's mail is in the Alhambra group name, otherwise False.

        :param group: str. Alhambra group name.
        :param user: str. User mail.
        :param env: str. Expects work or live.
        :return: result: Boolean.
        """
        result = False
        if env == "work":
            if self.users_work_collection.find_one({"$and": [{"group": group},
                                                             {"$elemMatch": {"mail": user}}]}).count() == 1:
                result = True
        elif env == "live":
            if self.users_live_collection.find_one({"$and": [{"group": group},
                                                             {"$elemMatch": {"mail": user}}]}).count() == 1:
                result = True
        return result

    def find_all_groups(self, env):
        """
            Finds all the Alhambra groups live or work with its users.

        :param env: str. Expects work or live.
        :return: result: array. Result for the query.
        """
        result = []
        if env == "work":
            result = self.users_work_collection.find({})
        elif env == "live":
            result = self.users_live_collection.find({})
        return result

    def find_route_by_group(self, env, group):
        """
            Finds one Alhambra group name and returns the information of the HDFS's route,
            permissions for the group and role.

        :param env: str. Expects work or live.
        :param group: str. Alhambra Group name
        :return: result: array. Result for the query.
        """
        if env == "work":
            result = self.alhambra_work_collection.find({"group": group})
        elif env == "live":
            result = self.alhambra_live_collection.find({"group": group})
        else:
            result = []
        return result

    def find_route(self, env, route):
        """
            Finds one Alhambra  HDFS's route and returns the information of the Alhambra group name,
            permissions for the group and role.

        :param env: str. Expects work or live.
        :param route: str. Alhambra HDFS's route
        :return result: array. Result for the query.
        """
        if env == "work":
            result = self.alhambra_work_collection.find({"route": route})
        elif env == "live":
            result = self.alhambra_live_collection.find({"route": route})
        else:
            result = []
        return result

    def find_all_alhambra(self, env):
        """
            Finds all Alhambra work or live groups with its information, such as:
                - HDFS's routes,
                - Group's permissions
                - Group's role

        :param env: str. Expects work or live.
        :return result: array. Result for the query.
        """
        if env == "work":
            result = self.alhambra_work_collection.find({})
        elif env == "live":
            result = self.alhambra_live_collection.find({})
        else:
            result = []
        return result

    def find_route_group_permissions(self, env, group, route, permissions):
        """
            Finds one an Alhambra group by name, route and permissions.

        :param env: str. Expects work or live.
        :param route: str. Group's HDFS route
        :param group: str. Alhambra group name
        :param permissions: str. Group's permissions
        :return result: array. Result for the query.
        """
        if env == "work":
            result = self.alhambra_work_collection.find({"$and": [{"group": group.strip().upper()}, {"route": route},
                                                                  {"permissions": permissions.strip().upper()}]})
        elif env == "live":
            result = self.alhambra_live_collection.find({"$and": [{"group": group.strip().upper()}, {"route": route},
                                                                  {"permissions": permissions.strip().upper()}]})
        else:
            result = []
        return result

    def find_route_group(self, env, group, route):
        """
            Finds one an Alhambra group by name and route.

        :param env: str. Expects work or live.
        :param group: str. Group's name
        :param route: str. Group's HDFS group
        :return result: array. Result for the query.
        """
        if env == "work":
            result = self.alhambra_work_collection.find({"$and": [{"group": group}, {"route": route}]})
        elif env == "live":
            result = self.alhambra_live_collection.find({"$and": [{"group": group}, {"route": route}]})
        else:
            result = []
        return result

    def distinct_query(self, env, query):
        """
            Finds the distinct values for a specified field across a single collection
            and returns the results in an array.

        :param env: str. Expects work or live.
        :param query: str. Collection property name.
        :return result: array. Result for the query.
        """
        if env == "work":
            result = self.alhambra_work_collection.distinct(query)
        elif env == "live":
            result = self.alhambra_live_collection.distinct(query)
        else:
            result = []
        return result

    def find_any_query(self, env, query):
        """
            Returns the result of the query in the Alhambra work or live collection. In other case return None.

        :param env: str. Expects work or live.
        :param query: str, a string  with the query.
        :return: An array of bson, bson or None, an array of bson that contains the query's result
                when its more than one use case document,
                If the query's result is one use case document returns a bson, otherwise return a None.
        """
        if env == "work":
            result = self.alhambra_work_collection.find(bson.BSON.decode(bson.BSON.encode(query)))
        elif env == "live":
            result = self.alhambra_live_collection.find(bson.BSON.decode(bson.BSON.encode(query)))
        else:
            result = []
        return result
