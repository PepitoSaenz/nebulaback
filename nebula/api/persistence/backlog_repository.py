
from django.conf import settings
from bson import ObjectId
import bson
from pymongo.collation import Collation
from bson.json_util import dumps, loads 




class Backlog_repository(object):
    """A class used to represent the backlog collection.

            Attributes
            -------
            collection : collection
                Creates the backlog collection and applies the queries and operations.
    """

    def __init__(self):
        """
            The constructor of the Backlog_repository class.
        """
        self.collection = settings.MONGO_DATABASE['backlog']

    def insert_one(self, backlog):
        """
            Creates the backlog parameter, which is a backlog document, to the backlog collection.

        :param backlog: Backlog entity, a instance of the Backlog class (Entity).
        """
        return self.collection.insert_one(backlog).inserted_id

    def find_one(self, backlog_id):
        """
            Returns the backlog document in the backlog  collection which id is equal to the backlog_id.
            In other case returns None.

        :param backlog_id: str, a string that represents the id of the backlog document in the backlog collection.
        :return: bson or None, a bson with the backlog document found, otherwise return a None.
        """
        return self.collection.find_one({
            "_id": ObjectId(str(backlog_id))
        })

    def find_all(self):
        """
            Returns all backlog documents in the backlog collection. If the backlog collection is empty, returns None.

        :return: Array of bson or None, an Arrays of bson that contains all backlog documents found in
                the backlog collection, otherwise return a None.
        """
        return self.collection.find({})

    def joint_to_table(self, backlog_id, table_id):
        """
            Updates the table field with the table_id parameter in the backlog document which id is equal to
            the backlog_id parameter.

        :param backlog_id: str, a string that represents the id of the backlog document in the backlog collection.
        :param table_id: str, a string that represents the id of a data dictionary in the dataDictionary collection.
        """
        self.collection.update({"_id": ObjectId(backlog_id)}, {"$set": {"idTable": ObjectId(table_id)}})

    def approve_table_backlog(self, backlog_id):
        """
            Updates the tableRequest field to "A" (Approved) in the backlog document which id is equal to
            the backlog_id parameter.

        :param backlog_id: str, a string that represents the id of the backlog document in the backlog collection.
        """
        self.collection.update_one({"_id": ObjectId(backlog_id)}, {"$set": {"tableRequest": "A"}})

    def find_approved(self, backlog_id):
        """
            Returns the backlog document which id is equal to the backlog_id parameter and
            its tableRequest field is "A" (Approved).
            In other case return None.

        :param backlog_id: str, a string that represents the id of the backlog document in the backlog collection.
        :return: bson or None, a bson with the backlog document found, otherwise return a None.
        """
        return self.collection.find({"_id": ObjectId(backlog_id), "tableRequest": "A"})

    def find_to_approved(self):
        """
            Returns all backlog documents in the backlog collection which tableRequest field is equal to "P" (Pending).
           In other case return None.

        :return: Array of bson or None, an Arrays of bson that contains all backlog documents found in
                 the backlog collection, otherwise return a None.
        """
        return self.collection.find({"tableRequest": "P"})

    def find_by_id_table(self, table_id):
        """
            Returns the backlog documents in the backlog collection
            which idTable field is equal to the table_id parameter.
            In other case return None.

        :param table_id: str, a string that represents the id of a data dictionary in the dataDictionary collection.
        :return: bson or None, a bson that contains the backlog document found, otherwise return a None.
        """
        return self.collection.find_one({"idTable": ObjectId(table_id)})
    
    def find_by_basename(self, baseName):
        """
            Returns the backlog documents that match the base name parameter, case insensitive.
            
        :param baseName: str, a string that represents the id of a data dictionary in the dataDictionary collection.
        :return: bson or None, a bson that contains the backlog document found, otherwise return a None.
        """
        cursor = list(self.collection.find({"baseName": baseName}, {"modifications": 0, "user": 0}).collation(Collation(locale='en', strength=2)))
        return loads(dumps(cursor))


    def remove_one(self, backlog_id):
        """
            Deletes the backlog document in the backlog collection which id field is equal to the backlog_id parameter.

        :param backlog_id: str, a string that represents the id of the backlog document in the backlog collection.
        """
        self.collection.remove({"_id": ObjectId(backlog_id)})

    def find_any_query(self, query):
        """
            Returns the result of the query in the backlog collection. In other case return None.

        :param query:
        :return: An array of bson, bson or None, an array of bson that contains the query's result
                when its more than one backlog document,
                If the query's result is one backlog document returns a bson, otherwise return a None.
        """
        return self.collection.find(bson.BSON.decode(bson.BSON.encode(query)))

    def update_one(self, backlog_id, backlog):
        """
            Updates the backlog document with the backlog parameter and which id is equal to the backlog_id parameter.

        :param backlog_id: str, a string that represents the id of the backlog document in the backlog collection.
        :param backlog: Backlog entity, a instance of the Backlog class.
        """
        self.collection.update_one({"_id": ObjectId(backlog_id)}, {"$set": backlog})

    def update_state_request(self, backlog_id, state):
        """
            Updates the property tableRequest with state parameter and which id is equal to the backlog_id parameter

        :param backlog_id: str. A string that represents the id of the backlog document in the backlog collection.
        :param state: str. State request for the backlog. Expects "A" (Approved) or "P" (Pending)
        """
        self.collection.update_one({"_id": ObjectId(backlog_id)}, {"$set": {"tableRequest": state}})

    def update_state_unassigned(self, backlog_id):
        """
            Updates the property tableRequest to "D" (dismissed) for which id is equal to the backlog_id parameter.
            A backlog with tableRequest equals to "D" means that the backlog is not used by any project.

        :param backlog_id: str. A string that represents the id of the backlog document in the backlog collection.
        """
        self.collection.update_one({"_id": ObjectId(backlog_id)}, {"$set": {"tableRequest": "D"}})

    def update_modifications(self, backlog_id, modifications):
        """
            Updates the property modifications for backlog which id is equal to the backlog_id parameter.
            The property modifications is used for the audit process.

        :param backlog_id: str. A string that represents the id of the backlog document in the backlog collection.
        :param modifications: array. Modifications for the backlog.
        """
        self.collection.update_one({"_id": ObjectId(str(backlog_id))}, {"$set": {"modifications": modifications}})
    
    def delete_request(self, request_id):
        """
            Delete a single DT request by the id of it. CAREFUL if the request has a backlog still 
            associated as it could break other parts of the program.

        :return: 
        """
        return self.collection.delete_one({"_id": ObjectId(str(request_id).strip())})

    def generate_aggregate(self, pipeline):
        """
            Generates any pipeline aggregation. A Pipeline is a complex query, which contains operations group values
            from multiple documents together, and can perform a variety of operations on the grouped data
            to return a single result.

        :param pipeline: json. A dict that represent any pipeline
        :return: bson, result of aggregation pipeline
        """
        return self.collection.aggregate(pipeline)

    def available_governance(self):
        """
            Generates the pipeline for consulting the available backlogs for governance.
              A Pipeline is a complex query, which contains operations group values from multiple documents together,
              and can perform a variety of operations on the grouped data to return a single result.

        :return: bson, result of aggregation pipeline
        """
        #  Builds the pipeline.
        query = [{'$match': {'$and': [{'tableRequest': 'A'}, {'idTable': ''}]}},
                 {'$lookup': {
                     'from': 'useCase',
                     'localField': '_id',
                     'foreignField': 'tables',
                     'as': 'use_case'}},
                 {'$project': {
                     '_id': 1,
                     'baseName': 1,
                     'originSystem': 1,
                     'periodicity': 1,
                     'history': 1,
                     'observationField': 1,
                     'typeFile': 1,
                     'uuaaRaw': 1,
                     'uuaaMaster': 1,
                     'tacticalObject': 1,
                     'perimeter': 1,
                     'productOwner': 1,
                     'idTable': 1,
                     'tableRequest': 1,
                     'use_case._id': 1,
                     'use_case.name': 1}},
                 {'$lookup': {
                     'from': 'projects',
                     'localField': 'use_case._id',
                     'foreignField': 'useCases',
                     'as': 'project'}},
                 {'$project': {
                     '_id': 1,
                     'baseName': 1,
                     'originSystem': 1,
                     'periodicity': 1,
                     'history': 1,
                     'observationField': 1,
                     'typeFile': 1,
                     'uuaaRaw': 1,
                     'uuaaMaster': 1,
                     'tacticalObject': 1,
                     'perimeter': 1,
                     'use_case._id': 1,
                     'use_case.name': 1,
                     'project._id': 1,
                     'project.name': 1}}]
        return self.collection.aggregate(query)
