
from datetime import datetime
from django.conf import settings
from bson import ObjectId
import bson


class Requests_repository(object):
    """
        A class used to represent the requests collection.

        Attributes
        -------
        collection : collection
            Creates the requests collection and applies the queries and operations.

    """

    def __init__(self):
        """
            The constructor of the Backlog_repository class.
        """
        self.collection = settings.MONGO_DATABASE['requests']

    def find_one(self, request_id):
        """
            Finds one request by the property id.

        :param request_id: str. Represents the id.
        :return Array of bson or None, an Arrays of bson that contains all requests documents
            found in the requests's collection, otherwise return a None.
        """
        return self.collection.find_one({"_id": ObjectId(str(request_id).strip())})

    def find_all(self):
        """
            Finds all Request documents in the Requests's collection.
            If the Requests's collection is empty, returns None.

        :return: Array of bson or None, an Arrays of bson that contains all requests documents
        found in the requests's collection, otherwise return a None.
        """
        return self.collection.find({})

    def find_backlogs_to_approved(self):
        """
            Finds all requests documents in the requests's collection which stateRequest field is equal to "P" (Pending)
            and target field is equal to "B" (Backlog). In other case return None.

        :return: Array of bson or None, an Arrays of bson that contains all requests
        documents found in the requests's collection, otherwise return a None.
        """
        return self.collection.find({"$and": [{"stateRequest": "P"}, {"target": "B"}]})

    def find_use_cases_to_approved(self):
        """
            Finds all requests documents in the requests's collection which stateRequest field is equal to "P" (Pending)
            and target field is equal to "U" (Use case). In other case return None.

        :return: Array of bson or None, an Arrays of bson that contains all
        requests documents found in the requests's collection, otherwise return a None.
        """
        return self.collection.find({"$and": [{"stateRequest": "P"}, {"target": "U"}]})

    def insert_one(self, request):
        """
            Inserts the request parameter, which is a request document, to the request's collection.

        :param request: Request entity, a instance of the Request class.
        """
        return self.collection.insert_one(request).inserted_id

    def update_one(self, request_id, state, spo, comment):
        """
            Updates approved or dismissed a requests.

        :param request_id: str, a string that represents the id of the requests document in the requests collection.
        :param state: str. Expects "A" (Approved) or "D" (Dismissed)
        :param spo: str.  String that represents the id of super product owner.
        :param comment: str. String comment for the super product owner.
        """
        self.collection.update_one({"_id": ObjectId(request_id)},
                                   {"$set": {"stateRequest": state,
                                             "approved_time": datetime.now(),
                                             "spo": ObjectId(spo), "comment": comment}})

    def find_request_by_filter(self, query):
        """
            Finds the result of the query in the requests collection. In other case return None.

        :param query: str. Query for searching in the collection.
        :return: Array of bson or None, an Arrays of bson that contains all
        requests documents found in the requests's collection, otherwise return a None.
        """
        return self.collection.find(bson.BSON.decode(bson.BSON.encode(query)))

    def find_backlog_request(self, backlog_id):
        """
            Finds all the requests which are pending for approved for the backlog_id parameter.

        :param backlog_id: str. String that represents the id of the backlog.
        :return: Array of bson or None, an Arrays of bson that contains all
        requests documents found in the requests's collection, otherwise return a None.
        """
        return self.collection.find({"$and": [{"idBacklog": ObjectId(str(backlog_id).strip())}, {"stateRequest": "P"}]})

    def find_use_case_request(self, use_case_id):
        """
            Finds all the requests of the use case parameter which are pending for approved

        :param use_case_id: str. String that represents the id of the backlog.
        :return: Array of bson or None, an Arrays of bson that contains all
        requests documents found in the requests's collection, otherwise return a None.
        """
        return self.collection.find({"$and": [{"idUsecase": ObjectId(use_case_id)}, {"stateRequest": "P"},
                                              {"target": "U"}]})

    def find_request(self, request_id):
        """
            Finds one requests by its id property.

        :param request_id: str. String that represents the id of the request.
        :return: Bson or None.
        """
        return self.collection.find_one({"_id": ObjectId(request_id)})

    def find_backlog_request_ne(self, backlog_id, request_id):
        """
            Finds all the requests backlogs which are pending for be approved expects
            the requests with its property id is equal to request_id parameter.

        :param backlog_id: str. String that represents the id of the backlog.
        :param request_id: str. String that represents the id of the request.
        :return:  Array of bson or None, an Arrays of bson that contains all
        requests documents found in the requests's collection, otherwise return a None.
        """
        return self.collection.find({"$and": [{"idBacklog": ObjectId(str(backlog_id).strip())},
                                              {"stateRequest": "P"}, {"_id": {"$ne": ObjectId(str(request_id).strip())}}]})

    def find_use_case_request_ne(self, use_case_id, request_id):
        """
            Finds all the requests use cases which are pending for be approved expects
            the requests with its property id is equal to request_id parameter.

        :param use_case_id: str. String that represents the id of the use case.
        :param request_id: str. String that represents the id of the request.
        :return Array of bson or None, an Arrays of bson that contains all
        requests documents found in the requests's collection, otherwise return a None.
        """
        return self.collection.find({"$and": [{"idUsecase": ObjectId(str(use_case_id).strip())}, {"stateRequest": "P"},
                                              {"_id": {"$ne": ObjectId(str(request_id).strip())}}]})

    def find_any_query(self, query):
        """
            Finds the result of the query in the requests collection. In other case return None.

        :param query: str, a string  with the query.
        :return: An array of bson, bson or None, an array of bson that contains the query's result when
        its more than one requests document, if the query's result is one requests document returns a bson,
        otherwise return a None.
        """
        return self.collection.find(bson.BSON.decode(bson.BSON.encode(query)))

    def find_data_tables_to_approved(self):
        """
            Finds all requests of data tables which are pending for approved in the requests collection
            which stateRequest field is equal to "P" (Pending) and target field is equal to "T" (data tables).
            In other case return None.

        :return: Array of bson or None, an Arrays of bson that contains all requests documents
        found in the requests collection, otherwise return a None.
        """
        return self.collection.find({"$and": [{"stateRequest": "P"}, {"target": "T"}]})

    def find_request_data_table(self, data_table_id):
        """
            Finds all the requests of data tables which are pending for approved.

        :param data_table_id: str. Represents id property.
        :return: Array of bson or None, an Arrays of bson that contains all requests documents
        found in the requests collection, otherwise return a None.
        """
        return self.collection.find({"$and": [{"action": "Add"}, {"target": "T"},
                                    {"idBacklog": ObjectId(str(data_table_id).strip())}]})

    def update_modifications(self, request_id, modifications):
        """
            Updates the modifications property for the request which id is equal to request_id parameter.

        :param request_id: str. Represents id of the operational base.
        :param modifications: array. Modifications made by users.
        """
        self.collection.update_one({"_id": ObjectId(str(request_id).strip())}, {"$set": {"modifications": modifications}})
    
    def delete_request(self, request_id):
        """
            Delete a single DT request by the id of it. CAREFUL if the request has a backlog still 
            associated as it could break other parts of the program.

        :return: 
        """
        return self.collection.delete_one({"_id": ObjectId(str(request_id).strip())})

