
from django.conf import settings
from bson import ObjectId
import bson
import pymongo


class Team_repository(object):
    """A class used to represent the teams collection.

            Attributes
            -------
            collection : collection
                Creates the teams collection and applies the queries and operations.

    """
    def __init__(self):
        """
            The constructor of the Team_repository class.
        """
        self.collection = settings.MONGO_DATABASE['teams']

    def insert_one(self, team):
        """
            Inserts the team parameter, which is a team document, to the teams collection.

        :param team: an instance of the Team class.
        """
        self.collection.insert_one(team)

    def find_one(self, team_id):
        """
            Finds the team document in the teams collection which id is equal to the team_id parameter.
            In other case returns None.

        :param team_id: str, a string that represents the id of the backlog document in the teams collection.
        :return: A bson or None, a bson with the backlog document found, otherwise return a None.
        """
        return self.collection.find_one({
            "_id": ObjectId(str(team_id).strip())
        })

    def exist(self, name):
        """
            Finds True if the team's name is already registered else False.

        :param name: str. Team's name.
        :return: Boolean
        """
        return True if self.collection.count({"name": name}) > 0 else False

    def find_all(self):
        """
            Finds all teams documents in the teams collection. If the teams collection is empty, returns None.

        :return: Array of bson or None, an Arrays of bson
        that contains all teams documents found in the teams collection, otherwise return a None.
        """
        return self.collection.find({}, {"name": 1, "_id": 1})

    def find_country_teams(self, country):
        """
            Finds all the project documents which country field is equal to the country parameter.

        :param country: str, country's name.
        :return: Array of bson or None, an Arrays of bson that contains
        all teams documents found in the teams collection, otherwise return a None.
        """
        return self.collection.find({"country": country})

    def update_one(self, team_id, data):
        """
            Updates the team document with the data parameter and which id is equal to the team_id parameter.

        :param team_id: str, a string that represents the id of the team document in the teams collection.
        :param data: Team entity, a instance of the Team class.
        """
        self.collection.update_one({"_id": ObjectId(team_id)}, {
            "$set": bson.BSON.decode(bson.BSON.encode(data))
        })

    def find_teams(self, teams):
        """
            Finds all the teams in the teams array sorted by name.

        :param teams: array.
        :return: Array of bson or None, an Arrays of bson that contains
        all teams documents found in the teams collection, otherwise return a None.
        """
        query = {"$or": []}
        for i in teams:
            query["$or"].append({"_id": ObjectId(str(i))})
        return self.collection.find(query, {"_id": 1, "name": 1}).sort([("name", pymongo.ASCENDING)])
