
from django.conf import settings
import pymongo
from bson import ObjectId
import bson


class Project_repository(object):
    """A class used to represent the project collection.

                Attributes
                -------
                collection : collection
                    Creates the project collection and applies the queries and operations.
        """

    def __init__(self):
        """
            The constructor of the Project_repository class.
        """
        self.collection = settings.MONGO_DATABASE['projects']

    def find_all(self):
        """
            Finds all project documents in the projects's collection.
            If the projects's collection is empty, returns None.

        :return: Array of bson or None, an Arrays of bson that contains all project documents
        found in the projects's collection, otherwise return a None.
        """
        return self.collection.find({}, {"countries": 0, "owners": 0}).sort([("name", pymongo.ASCENDING)])

    def find_countries(self, country):
        """
            Finds all project documents in the projects's collection which countries field contains the country parameter.

        :param country: str, a country name.
        :return: Array of bson or None, an Arrays of bson that contains all
        project documents found in the projects's collection, otherwise return a None.
        """
        return self.collection.find({"countries": {"$in": [country]}})

    def find_teams(self, team):
        """
            Finds all project documents in the projects's collection which teams field contains the team parameter.

        :param team: str, a string that represents the id of the teams document in the teams's collection.
        :return: Array of bson or None, an Arrays of bson that contains all project documents
        found in the projects's collection, otherwise return a None.
        """
        return self.collection.find({"teams": {"$in": [ObjectId(str(team).strip())]}})

    def find_one(self, project_id):
        """
           Finds the project document in the projects's collection which id is equal to the project_id parameter.
           In other case returns None.

        :param project_id: str, a string that represents the id of the project document in the projects's collection.
        :return: A bson or None, a bson with the backlog document found,otherwise return a None.
        """
        return self.collection.find_one({"_id": ObjectId(str(project_id).strip())})

    def insert_one(self, project):
        """
            Inserts the project parameter, which is a project document, to the projects's collection.

        :param project: Project entity, a instance of the class Project.
        """
        self.collection.insert_one(project)

    def find_any_query(self, query):
        """
            Finds the result of the query in the operational data base collection. In other case return None.

        :param query: str, a string  with the query.
        :return: An array of bson, bson or None, an array of bson that contains the query's result
                when its more than one operational base document,
                If the query's result is one data table document returns a bson, otherwise return a None.
        """
        return self.collection.find(bson.BSON.decode(bson.BSON.encode(query)))

    def update_one(self, project_id, project):
        """
            Updates the project document with the project parameter and which id is equal to the project_id parameter.

        :param project_id: str, a string that represents the id of the project document in the projects's collection.
        :param project: an instance of the Project class.
        """
        self.collection.update_one({"_id": ObjectId(str(project_id))}, {
            "$set": bson.BSON.decode(bson.BSON.encode(project))
        })

    def insert_data_to_project(self, project_id, field, data):
        """
            Updates the field parameter with the parameter data of the project document
            which id field is equal to project_id.

        :param project_id: str, a string that represents the id of the project document in the projects's collection.
        :param field: str, the field name of the project document.
        :param data: str. Instance of the project entity class
        """
        self.collection.update({"_id": ObjectId(str(project_id).strip())}, {
            "$push": {
                field: {"$each": [ObjectId(str(data))]}
            }
        })

    def remove_data_to_project(self, project_id, field, data):
        """
            Deletes the field parameter with the parameter data of the project document which id field is equal to project_id.

        :param project_id: str, a string that represents the id of the project document in the projects's collection.
        :param field: str, the field name of the project document.
        :param data: str. Instance of the project entity class
        """
        self.collection.update({"_id": ObjectId(str(project_id).strip())}, {"$pull": {field: {"$in": [ObjectId(str(data))]}}})

    def find_product_owner_projects(self, product_owner_id):
        """
            Finds all the project documents with id and name fields,
            which the id_project_owner parameter is in owners field.

        :param product_owner_id: str, a string that represents the id of the users document in the users's collection.
        :return: Array of bson or None, an Arrays of bson that contains all project
        documents found in the projects's collection, otherwise return a None.
        """
        return self.collection.find({"owners": {"$in": [ObjectId(str(product_owner_id).strip())]}},
                                    {"_id": 1, "name": 1})

    def find_project_use_cases(self, project_id):
        """
            Finds one project document with useCases field which the project_id parameter is equal to id field.

        :param project_id: str, a string that represents the id of the project document in the projects's collection.
        :return: A bson or None, a bson with the project document found, otherwise return a None.
        """
        return self.collection.find_one({"_id": ObjectId(str(project_id).strip())}, {"useCases": 1, "_id": 0})

    def find_name(self, project_id):
        """
            Finds one project collection with the name field which id field is equal to project_id parameter

        :param project_id: str, a string that represents the id of the project document in the projects's collection.
        :return: A bson or None, a bson with the project document found, otherwise return a None.
        """
        return self.collection.find_one({"_id": ObjectId(str(project_id).strip())}, {"name": 1, "_id": 0})

    def find_all_use_cases_contains(self, use_case_id):
        """
            Finds all projects that contains one use case.

        :param use_case_id: str, string that represent a use case id
        :return: bson, dictionary with project data that contains one use case
        """
        return self.collection.find({"useCases": {"$in": [ObjectId(str(use_case_id))]}})

    def generate_aggreation(self, pipeline):
        """
            Generates any pipeline aggregation. A Pipeline is a complex query, which contains operations group values
            from multiple documents together, and can perform a variety of operations on the grouped data
            to return a single result.

        :param pipeline: json. A dict that represent any pipeline.
        :return: bson, result of aggregation pipeline.
        """
        return self.collection.aggregate(pipeline)

    def update_use_cases(self, project_id, use_cases):
        """
            Updates the uses cases for the project.

        :param project_id: str. Represents id of the project.
        :param use_cases: array.
        """
        self.collection.update_one({"_id": ObjectId(str(project_id))}, {"$set": {"useCases": use_cases}})

    def add_vobo(self, project_id, user_id):
        """
            Updates the VoBo property adding the user_id parameter.
            The VoBo property is for saving the user who are reviewer for the project.

        :param project_id: str. Represents id of the project.
        :param user_id: str. Represents id of the user.
        """
        if self.collection.find({"$and": [{"VoBo": {"$in": [ObjectId(str(user_id).strip())]}},
                                          {"_id": ObjectId(str(project_id))}]}).count() == 0:

            self.collection.update_one({"_id": ObjectId(str(project_id.strip()))},
                                       {"$push": {"VoBo": {"$each": [ObjectId(str(user_id))]}}})
    
    def remove_vobo(self, project_id, user_id):
        """
            Updates the VoBo property adding the user_id parameter.
            The VoBo property is for saving the user who are reviewer for the project.

        :param project_id: str. Represents id of the project.
        :param user_id: str. Represents id of the user.
        """
        if self.collection.find({"$and": [{"VoBo": {"$in": [ObjectId(str(user_id).strip())]}},
                                          {"_id": ObjectId(str(project_id))}]}).count() > 0:

            self.collection.update_one({"_id": ObjectId(str(project_id.strip()))},
                                       {"$pull": { "VoBo": { "$in": [ObjectId(str(user_id).strip())]}}})

    def update_modifications(self, project_id, modifications):
        """

            Updates the property modification by the reviewer.
            The property modifications saved information related to who edit a data table and the date.

        :param project_id: str. Represents id of the project.
        :param modifications: array. Modifications by an user.
        """
        self.collection.update_one({"_id": ObjectId(str(project_id))}, {"$set": {"modifications": modifications}})

    def find_vobo(self, user_id):
        """

        :param user_id:
        :return:
        """
        return self.collection.find({"VoBo": {"$in": [ObjectId(str(user_id))]}}, {"_id": 1, "name": 1, "shortDesc": 1, "useCases": 1, "modifications": 1})
    
    def project_count(self):
        """
            Return the estimated count of the number of projects, documents in this collection.

        :return: number, result of the estimated count
        """
        return self.collection.estimated_document_count()
    
    def update_project_data(self, project_id, project_data):
        """
            Updates one user information.

        :param project_id: str. Represents the property id.
        :param project_data: json. Instance of the user class entity.
        """
        self.collection.update_one({"_id": ObjectId(str(project_id))}, {"$set": project_data})
