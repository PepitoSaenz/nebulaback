import pymongo
from django.conf import settings
from bson import ObjectId
import bson


class Operational_base_repository(object):
    """A class used to represent the Operational base collection.

            Attributes
            -------
            collection : collection
                Creates the operationalBase collection and applies the queries and operations.
    """
    def __init__(self):
        """
            The constructor of the Operational base class.
        """
        self.collection = settings.MONGO_DATABASE['operationalBase']

    def insert_one(self, operational_base):
        """
            Inserts one operational data base to the collections.

        :param operational_base: str. Base operational entity class.
        :return operational data base id in the collection.
        """
        return self.collection.insert_one(operational_base).inserted_id

    def find_repeat_base(self, name, db):
        """
            Finds if exists a operational base with the object_name and db_type parameters.

        :param name: str. Name of the operational base.
        :param db: str. Type of the operational base. e.g Oracle, Elastic, etc.
        :return True or False.
        """
        flag = False
        if self.collection.find({
                "$and": [{
                    "object_name": {
                        '$regex': name,
                        "$options": 'i'
                    }
                }, {
                    "db_type": {
                        '$regex': db,
                        "$options": 'i'
                    }
                }]
        }).count() != 0:
            flag = True
        return flag

    def find_repeat_base_ne(self, operational_id, name, db):
        """
             Finds if exists a operational base with the object_name and db_type parameters.
             Ignores the operational base which property id is equal to the operational_id parameter.

        :param name: str. Name of the operational base.
        :param db: str. Type of the operational base. e.g Oracle, Elastic, etc.
        :param operational_id: str. Represents the property id.
        :return True or False
        """
        flag = False
        if self.collection.find({
                "$and": [{
                    "object_name": {
                        '$regex': name,
                        "$options": 'i'
                    }
                }, {
                    "db_type": {
                        '$regex': db,
                        "$options": 'i'
                    }
                }, {
                    "_id": {
                        "$ne": ObjectId(str(operational_id))
                    }
                }]
        }).count() != 0:
            flag = True
        return flag

    def find_one(self, operational_id):
        """
            Finds one operational data base by the property id.

        :param operational_id: str. Represents the property id.
        :return An array of bson or None,
                An array of bson that contains the query's result, otherwise return a None.
        """
        return self.collection.find_one({"_id": ObjectId(str(operational_id))})

    def find_head(self, operational_id):
        """
            Finds one operational data base without fields property.

        :param operational_id: str. Represents the property id.
        :return An array of bson or None,
                An array of bson that contains the query's result, otherwise return a None.
        """
        return self.collection.find_one({"_id": ObjectId(str(operational_id))},
                                        {"fields": 0})

    def find_fields(self, operational_id):
        """
            Finds one operational data base's fields which property id is equal to the parameter operational_id.

        :param operational_id:  str. Represents the property id.
        :return An array of bson or None,
                An array of bson that contains the query's result, otherwise return a None.
        """
        return self.collection.find_one({"_id": ObjectId(str(operational_id))},
                                        {"fields": 1})

    def find_all(self):
        """
            Finds all the operational data base with the basic information.

        :return An array of bson or None,
                An array of bson that contains the query's result, otherwise return a None.
        """
        return self.collection.find({}, {
            "_id": 1,
            "baseName": 1,
            "observationField": 1,
            "object_name": 1,
            "db_type": 1,
            "category_id": 1,
            "uuaaMaster": 1
        })

    def update_row_fields(self, operational_id, column, data):
        """
            Updates one field for the operational data base which property id is equal to the parameter operational_id.

        :param operational_id: str. String that represent the operational base id.
        :param data: json. Instance of the entity class Field_operational_base.
        :param column: str. Position for the field.
        """
        self.collection.update_one({"_id": ObjectId(str(operational_id))},
                                   {"$set": {
                                       "fields." + str(column): data
                                   }})

    def update_head(self, operational_id, data):
        """
            Updates the head values with the new ones

        :param operational_id: str. String that represent the operational base id.
        :param data: json. Instance of the entity class Field_operational_base.
        """
        self.collection.update_one({"_id": ObjectId(str(operational_id))}, {
            "$set": {
                "uuaaMaster": data["uuaaMaster"],
                "data_source": data["data_source"],
                "information_group_level_1": data["information_group_level_1"],
                "information_group_level_2": data["information_group_level_2"],
                "object_name": data["object_name"],
                "baseName": data["baseName"],
                "observationField": data["observationField"],
                "db_type": data["db_type"],
                "alias": data["alias"],
                "periodicity": data["periodicity"],
                "tacticalObject": data["tacticalObject"],
                "current_depth": data["current_depth"],
                "required_depth": data["required_depth"],
                "perimeter": data["perimeter"],
                "information_level": data["information_level"],
                "storage_zone": data["storage_zone"],
                "base_path": data["base_path"],
                "user": data["user"],
                "project_owner": data["project_owner"]
            }
        })

    def update_all_fields(self, operational_id, data):
        """
            Updates all the fields for the operational base which id property is equal to the operational_id parameter.

        :param operational_id: str. String that represent the operational base id.
        :param data: json. Instance of the entity class Field_operational_base.
        """
        self.collection.update_one({"_id": ObjectId(str(operational_id))},
                                   {"$set": {
                                       "fields": data
                                   }})

    def change_state_table(self, operational_id, state):
        """
            Updates the property stateTable with the state parameter fot the operational base
            which parameter operational_id is equal to property id.

        :param operational_id: str. String that represent the operational base id.
        :param state: str, string that represent table state
        """
        self.collection.update_one({"_id": ObjectId(operational_id)},
                                   {"$set": {
                                       "stateTable": state
                                   }})

    def update_head(self, operational_id, head):
        """
            Updates object information for the operational base which parameter operational_id is equal to property id.

        :param operational_id: str. String that represent the operational base id.
        :param head: json. Instance of the operational base entity.
        """
        self.collection.update_one(
            {"_id": ObjectId(str(operational_id))},
            {"$set": bson.BSON.decode(bson.BSON.encode(head))})

    def update_modifications_audit(self, operational_id, modifications):
        """

            Updates the modifications property for the operational base which id is equal to operational_id parameter.

        :param operational_id: str. Represents id of the operational base.
        :param modifications: array. Modifications made by users.
        """
        self.collection.update_one({"_id": ObjectId(str(operational_id))},
                                   {"$set": {
                                       "modifications": modifications
                                   }})

    def find_name_base(self, name, base):
        """
            Finds one operational base by property object_name and db_type.

        :param name: str. Name of the operational base.
        :param base: str. Type of the operational base. e.g Oracle, Elastic, etc.
        :return: Bson or None.
        """
        return self.collection.find_one({
            "$and": [{
                "object_name": {
                    "$regex": name,
                    "$options": 'i'
                }
            }, {
                "db_type": {
                    "$regex": base,
                    "$options": 'i'
                }
            }]
        })

    def find_by_state(self, state, project_id):
        """
            Finds all the operational data base by the property project_owner or state.

        :param state: str. State of the operational base.
        :param project_id: str. Represents id of the project owner.
        :return: Bson or None.
        """
        return self.collection.find({
            "stateTable": state,
            "project_owner": ObjectId(project_id)
        })

    def update_modifications(self, operational_id, modification, column):
        """
            Updates the property modification by the reviewer.
            The property modifications saved information related to who edit a data table and the date.

        :param operational_id: str. Represents id of the dictionary.
        :param modification: array. Modifications by an user.
        :param column: str. Position in the fields.
        """
        self.collection.update_one({"_id": ObjectId(str(operational_id))}, {
            "$set": {
                "fields." + str(column) + ".modification": modification
            }
        })

    def update_check_status(self, operational_id, user, check, column,
                            comments):
        """
            Updates property check of the operational base which id is equal to operational_id parameter.
            The property check is used for approving the  by the reviewer.

        :param operational_id: str, represents id of the data table
        :param user: str, represents id of the reviewer
        :param check: str, Expects NOK or OK.
        :param column: str, column of the fields
        :param comments: array, comments by the reviewer.
        """
        self.collection.update_one(
            {"_id": ObjectId(str(operational_id).strip())}, {
                "$set": {
                    "fields" + str(column) + ".check.status": check,
                    "fields." + str(column) + ".check.user": user,
                    "fields." + str(column) + ".check.comments": comments
                }
            })
