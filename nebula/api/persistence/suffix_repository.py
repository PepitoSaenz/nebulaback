
from django.conf import settings


class Suffix_repository(object):
    """A class used to represent the suffix collection.

            Attributes
            -------
            collection : collection
                Creates the suffix collection and applies the queries and operations.

    """

    def __init__(self):
        self.collection = settings.MONGO_DATABASE['suffix']

    def find_one(self, suffix):
        """
            Finds the suffix document in the suffix collection which suffix field is equal to the suffix parameter.
           In other case returns None.

        :param suffix: str, a string with the suffix to find.
        :return: bson or None, a bson with the suffix document found,otherwise return a None.
        """
        return self.collection.find_one({
            "suffix": suffix.strip().lower()
        })

    def find_suffix_by_id(self, suffix_id):
        """
           Finds the suffix document in the suffix collection which suffix field is equal to the suffix parameter.
           In other case returns None.
        :param suffix_id: int, An id with the suffix to find.
        :return: bson or None, a bson with the suffix document found,otherwise return a None.
        """
        return self.collection.find_one({
            "_id": suffix_id
        })

    def find_all(self):
        """
            Finds all suffix options.

        :return: bson, dictionary with all suffix data.
        """
        return self.collection.find({})
