
from django.conf import settings
import bson
from bson import ObjectId
import datetime


class Use_case_repository(object):
    """A class used to represent the useCase collection.

            Attributes
            -------
            collection : collection
                Creates the useCase collection and applies the queries and operations.
    """

    def __init__(self):
        """
            The constructor of the Use_case_repository class.
        """
        self.collection = settings.MONGO_DATABASE['useCase']

    def insert_one(self, use_case):
        """
            Inserts the use_case parameter, which is a use case document, to the useCase collection.

        :param use_case: Use_case entity, a instance of the Use_case class.
        """
        return self.collection.insert_one(use_case).inserted_id

    def update_one(self, use_case_id, use_case):
        """
            Updates the use case document with the use_case parameter and which
            id is equal to the use_case_id parameter.

        :param use_case_id: str, a string that represents the id of the use case document in the useCase collection.
        :param use_case: Use_case entity, a instance of the Use_case class.
        """
        self.collection.update_one({"_id": ObjectId(use_case_id)}, {
            "$set": bson.BSON.decode(bson.BSON.encode(use_case))
        })

    def update_head(self, use_case_id, use_case):
        """
             Updates the use case information such as name and dates with the use_case parameter and
             which id is equal to the use_case_id parameter.

        :param use_case_id: str, a string that represents the id of the use case document in the useCase collection.
        :param use_case: Use_case entity, a instance of the Use_case class.
        """
        tmp = [int(item) for item in use_case["startDate"].split('-')]
        tmp2 = [int(item) for item in use_case["finishDate"].split('-')]
        self.collection.update_one(
            {"_id": ObjectId(str(use_case_id))},
            {"$set": {"name": use_case["name"], "description": use_case["description"],
                      "startDate": datetime.datetime(tmp[0], tmp[1], tmp[2]),
                      "finishDate": datetime.datetime(tmp2[0], tmp2[1], tmp2[2])}})

    def update_tables(self, use_case_id, use_case_tables):
        """
            Updates the tables that are part of the use case.

        :param use_case_id: str. A string that represents the id.
        of the use case document in the useCase collection.
        :param use_case_tables: array of id objects which represents the tables that are part of the use case.
        """
        self.collection.update_one(
            {"_id": ObjectId(str(use_case_id).strip())},
            {"$set": {"tables": use_case_tables}}
        )

    def update_tablones(self, use_case_id, tablones):
        """
             Updates the tables that are part of the use case.

        :param use_case_id: str. A string that represents the id.
        :param tablones: array of id objects which represents the data tables that are part of the use case.
        """
        self.collection.update_one(
            {"_id": ObjectId(str(use_case_id).strip())},
            {"$set": {"tablones": tablones}}
        )

    def insert_one_table(self, use_case_id, table):
        """
            Inserts the table parameter in the tables array field of use case document
            which id field is equal to use_case_id parameter.

        :param use_case_id: str, a string that represents the id of the use case document in the useCase collection.
        :param table: str, a string that represents the id of
        the data dictionary document in the dataDictionary collection.
        """
        """
        self.collection.update({
            "_id": ObjectId(str(use_case_id).strip())
        }, {"$push": {
            "tables": {"$each": [ObjectId(str(table).strip())]}
        }})
        """
        self.collection.update({"_id": ObjectId(str(use_case_id).strip())}, {"$push": {"tables": ObjectId(str(table).strip()) }})

    def find_one(self, use_case_id):
        """
            Finds the use case document in the useCase collection which id is equal to the use_case_id parameter.
           In other case returns None.

        :param use_case_id: str, a string that represents the id of the use case document in the useCase collection.
        :return: A bson or None, a bson with the use case document found, otherwise return a None.
        """
        return self.collection.find_one({
            "_id": ObjectId(str(use_case_id).strip())
        })

    def contains_backlog(self, use_case_id, backlog_id):
        """
            Finds one use case document which id field is equal to use_case_id parameter and
            tables field contains the backlog_id parameter.

        :param use_case_id: str, a string that represents the id of the use case document in the useCase's collection.
        :param backlog_id: str, a string that represents the id of the backlog document in the backlog's collection.
        :return: A bson or None, a bson with the use case document found,otherwise return a None.
        """
        return self.collection.find_one({
            "_id": ObjectId(str(use_case_id).strip()), "tables": {"$in": [ObjectId(str(backlog_id).strip())]}
        })

    def all_backlog_contains(self, backlog_id):
        """
        :param backlog_id: str
        :return:
        """
        return self.collection.find({
            "tables": {"$in": [ObjectId(str(backlog_id).strip())]}
        })

    def remove_one_table(self, use_case_id, table):
        """
            Deletes all instances of table parameter from tables field of the use case document
            which id field is equal to use_case_id parameter.

        :param use_case_id: str, a string that represents the id of the use case document in the useCase collection.
        :param table: str, a string that represents the id of the data dictionary document in the dataDictionary collection.
        """
        self.collection.update_one({
            "_id": ObjectId(str(use_case_id).strip())
        }, {
            "$pull": {"tables": ObjectId(str(table).strip())}
        })

    def remove_all_tables(self, table, user_id):
        """
            Deletes all instances of table parameter from tables field of all use case documents in the
            useCase collection.

        :param table: str, a string that represents the id of the data dictionary document
        in the dataDictionary collection.
        :param user_id: str.
        """
        self.collection.update_many({}, {
            "$pull": {"tables": ObjectId(str(table).strip())},
            "$push": {"modifications": {"user": ObjectId(str(user_id)),
                                        "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}}
        })

    def find_any_query(self, query):
        """
            Finds the result of the query in the useCase collection. In other case return None.

        :param query: str, a string  with the query.
        :return: An array of bson, bson or None, an array of bson that contains the query's result
        when its more than one use case document, if the query's result is one use case document
        returns a bson, otherwise return a None.
        """
        return self.collection.find(bson.BSON.decode(bson.BSON.encode(query)))

    def find_all(self):
        """
            Finds all use case in the collection.

        :return: An array of bson, bson or None, an array of bson that contains the query's result
        when its more than one use case document, if the query's result is one use case document
        returns a bson, otherwise return a None.
        """
        return self.collection.find({"_id": {"$exists": True}})

    def update_state_request(self, use_case_id, state):
        """
            Updates the state for the use case. Expects "A"(Approved) or "P"(Pending)

        :param use_case_id: str.
        :param state: str.
        """
        self.collection.update_one({"_id": ObjectId(use_case_id)}, {"$set": {"stateRequest": state}})

    def update_state_unassigned(self, use_case_id):
        """
            Updates the property stateRequest to "D" (dismissed) for which id is equal to the use_case_id parameter.
            A use case with tableRequest equals to "D" means that the use case is not used by any project.

        :param use_case_id: str. Represents the property id for the use case.
        """
        self.collection.update_one({"_id": ObjectId(use_case_id)}, {"$set": {"stateRequest": "D"}})

    def update_modifications(self, use_case_id, modifications):
        """
            Updates the property modifications for the use case which id is equal to the  parameter.
            The property modifications is used for the audit process.

        :param use_case_id: str.
        :param modifications: array.
        """
        self.collection.update_one({"_id": ObjectId(str(use_case_id))}, {"$set": {"modifications": modifications}})

    def generate_aggreation(self, pipeline):
        """
            Generates any pipeline aggregation. A Pipeline is a complex query, which contains operations group values
            from multiple documents together, and can perform a variety of operations on the grouped data
            to return a single result.

        :param pipeline: json. A dict that represent any pipeline.
        :return: bson, result of aggregation pipeline.
        """
        return self.collection.aggregate(pipeline)
    
    def use_case_count(self):
        """
            Return the estimated count of the number of use cases, documents in this collection.

        :return: number, result of the estimated count
        """
        return self.collection.estimated_document_count()
    