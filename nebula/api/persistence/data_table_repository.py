
from django.conf import settings
from bson import ObjectId
import bson


class Data_table_repository(object):
    """A class used to represent the dataTable collection.

            Attributes
            -----------
            collection : collection
                Creates the dataTable collection and applies the queries and operations.

    """
    def __init__(self):
        """The constructor of the Data_table_repository class.

            Attributes
            ----------
            :collection: collection
                Creates the dataTable collection and applies the queries.
        """
        self.collection = settings.MONGO_DATABASE['dataTable']

    def insert_one(self, data_table):
        """
            Inserts the data_table parameter, which is a data table document to the dataTable's collection.

        :param data_table: Data_table entity, a instance of the Data_table class.
        """
        return self.collection.insert_one(data_table).inserted_id

    def change_state_table(self, table_id, state):
        """
            Updates the property stateTable for the data table which table_id parameter is equal to data table id.

        :param table_id: str, string that represent a table id
        :param state: str, string that represent table state
        """
        self.collection.update_one({"_id": ObjectId(str(table_id).strip())}, {"$set": {"stateTable": state}})

    def update_field(self, table_id, query):
        """
            Updates any field in one data table.

        :param table_id: str, string that represent a table id
        :param query: json, dict that represent any query to update a data dictionary
        """
        self.collection.update_one({"_id": ObjectId(table_id)}, {"$set": bson.BSON.decode(bson.BSON.encode(query))})

    def find_one(self, data_table_id):
        """
            Finds the data table document in the dataTable collection which id is equal to the data_table_id parameter.
           In other case returns None.

        :param data_table_id: str, a string that represents the id
                of the data table document in the dataTable collection.
        :return: bson or None, a bson with the dataTable document found,otherwise return a None.
        """
        return self.collection.find_one({
            "_id": ObjectId(str(data_table_id).strip())
        })

    def find_any_query(self, query):
        """
            Finds the result of the query in the dataTable collection. In other case return None.

        :param query: str, a string  with the query.
        :return: An array of bson, bson or None, an array of bson that contains the query's result
                when its more than one data table document,
                If the query's result is one data table document returns a bson, otherwise return a None.
        """
        return self.collection.find(bson.BSON.decode(bson.BSON.encode(query)))

    def find_table_by_alias(self, alias):
        """
            Finds if the alias exist in other data table.

        :param alias: str, represents the alias of the data table
        :return: True or False
        """
        alias_exist = True
        if self.collection.find_one({"alias": alias}) is None:
            alias_exist = False
        return alias_exist

    def find_one_by_alias(self, alias):
        """
            Finds one data table by alias.

        :param alias: str, represents the alias of the table
        :return: bson or None
        """
        return self.collection.find_one({"alias": alias})

    def update_state_request(self, data_table_id, state):
        """
            Updates the property stateTable for the data table which id is equal to data_table_id parameter.

        :param data_table_id: str. Represents the id of the data table
        :param state: str. New state for the data table.
        """
        self.collection.update_one({"_id": ObjectId(data_table_id)}, {"$set": {"stateTable": state}})

    def update_state_unassigned(self, data_table_id):
        """
            Updates the property tableRequest to "D" (dismissed) for which id is equal to the data_table_id parameter.
            A backlog with stateTable equals to "D" means that the data table is not used by any project.

        :param data_table_id: str. A string that represents the id of
                the data table document in the dataTable collection.
        """
        self.collection.update_one({"_id": ObjectId(data_table_id)}, {"$set": {"stateTable": "D"}})

    def find_head(self, data_table_id):
        """
            Finds the basic information for the data table which data table id is equal to the data_table_id parameter

        :param data_table_id:  A string that represents the id of
                the data table document in the dataTable collection.
        :return An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data table documents,
                If the query's result is one data table document returns a bson, otherwise return a None
        """
        return self.collection.find_one({"_id": ObjectId(data_table_id)}, {
            "_id": 1, "master_name": 1, "baseName": 1, "observationField": 1,
            "information_group_level_1": 1, "information_group_level_2": 1,
            "uuaaMaster": 1, "perimeter": 1, "information_level": 1,
            "data_source": 1, "periodicity": 1, "partitions": 1,
            "loading_type": 1, "current_depth": 1, "required_depth": 1,
            "estimated_volume_records": 1, "tacticalObject": 1,
            "user": 1, "alias": 1,  "stateTable": 1, "project_owner": 1,
            "master_comment": 1, "master_path": 1, "origin_tables": 1, "target_file_type": 1 ,
            "target_file_delimiter": 1, "modifications": 1, "created_time": 1, "people": 1,
            "deploymentType": 1, "modelVersion": 1, "objectVersion": 1, "securityLevel": 1
        })

    def find_alias_repeat(self, alias, table_id):
        """
             Finds if the alias exist in other data table.

        :param alias: str, represents the alias of the data dictionary
        :param table_id: str, represents id of the data dictionary.
        :return True or False
        """
        alias_exist = True
        if self.collection.find_one({"alias": alias, "_id": {"$ne": ObjectId(table_id)}}) is None:
            alias_exist = False
        return alias_exist

    def update_row_fields(self, table_id, column, data):
        """
            Updates the row in the column parameter position with the data parameter.

        :param table_id: str, represents id of the data dictionary.
        :param data: json. Row information instance of Field_data_table entity.
        :param column: str. Field position.
        """
        self.collection.update_one({"_id": ObjectId(str(table_id))}, {"$set": {"fields." + str(column): data}})

    def update_all_fields(self, table_id, data):
        """
            Updates all the fields for a data table with the data parameter.

        :param table_id: str, represents id of the data dictionary.
        :param data: Array. Array of instances of Field_data_table entity.
        """
        self.collection.update_one({"_id": ObjectId(str(table_id))}, {"$set": {"fields": data}})

    def update_origin_tables(self, table_id, data):
        """
            Updates the property origin_tables for a data table with the data parameter.

        :param table_id: str, represents id of the data dictionary.
        :param data: Array.
        """
        self.collection.update_one({"_id": ObjectId(str(table_id))}, {"$set": {"origin_tables": data}})

    def same_naming(self, naming):
        """
            Finds one naming in all data tables.

        :param naming: str. string that represent a naming.
        :return An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data table documents,
                If the query's result is one data table document returns a bson, otherwise return a No
        """
        return self.collection.find({
            "Fields": {"$elemMatch": {"naming.naming": naming}}
        })

    def distinct_origin_namings(self, table_id):
        """
            Finds the distinct values for a specified field in the data table
            which id is equal to the table_id parameter.
            and returns the results in an array.

        :return An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data table documents,
                If the query's result is one data table document returns a bson, otherwise return a No
        """
        return self.collection.distinct("fields.originNamings.table_name", {"_id": ObjectId(str(table_id).strip())})

    def distinct_origin_tables(self, table_id):
        """
            Finds the distinct values for a specified field in the data table
            which id is equal to the table_id parameter.
            and returns the results in an array.

        :return An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data table documents,
                If the query's result is one data table document returns a bson, otherwise return a No
        """
        return self.collection.distinct("origin_tables.originName", {"_id": ObjectId(str(table_id).strip())})

    def find_all(self):
        """
            Finds all the data tables with its basic information in the data table collection.

        :return An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data table documents,
                If the query's result is one data table document returns a bson, otherwise return a No
        """
        return self.collection.find({}, {"_id": 1, "master_name": 1, "alias": 1, "baseName": 1,
                                         "observationField": 1, "uuaaMaster": 1, "periodicity": 1, "origin_tables": 1, "master_path": 1})

    def update_check_status(self, table_id, user, check, column, comments):
        """
            Updates property check of the data table which id is equal to table_id parameter.
            The property check is used for approving the data dictionary by the reviewer.

        :param table_id: str, represents id of the data table
        :param user: str, represents id of the reviewer
        :param check: str, Expects NOK or OK.
        :param column: str, column of the fields
        :param comments: array, comments by the reviewer.
        """
        self.collection.update_one({"_id": ObjectId(str(table_id).strip())}, {"$set": {
            "fields." + str(column) + ".check.status": check, "fields." + str(column) + ".check.user": user,
            "fields." + str(column) + ".check.comments": comments}})

    def update_modifications(self, table_id, modification, column):
        """
            Updates the property modification by the reviewer.
            The property modifications saved information related to who edit a data table and the date.

        :param table_id: str. Represents id of the dictionary.
        :param modification: array. Modifications by an user.
        :param column: str. Position in the fields.
        """
        self.collection.update_one({"_id": ObjectId(str(table_id))},
                                   {"$set": {"fields."+str(column)+".modification": modification}})

    def find_all_origin(self):
        """
            Finds all data tables available to be data source for another data table.

        :return An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data table documents,
                If the query's result is one data table document returns a bson, otherwise return a No
        """
        return self.collection.find({"stateTable": {"$ne": "D"}},
                                    {"fields": 0, "origin_tables": 0, "master_comment": 0,
                                     "information_group_level_1": 0, "information_group_level_2": 0})

    def find_other_tables(self, table_id):
        """
            Finds all data tables excepts for the data table which parameter table_id is equal to property id.

        :param table_id: str. Represents id of the dictionary.
        :return An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data table documents,
                If the query's result is one data table document returns a bson, otherwise return a No
        """
        return self.collection.find({"_id": {"$ne": ObjectId(str(table_id).strip())}})

    def find_same_naming_other_data_table(self, data_table_id, naming):
        """
            Finds all data tables expects for data table which parameter table_id is equal to property id and
            the data tables contains the exact naming parameter in their fields the naming.

        :param data_table_id: str, string that represent a table id
        :param naming: str, string that represent a field to get
        :return An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data table documents,
                If the query's result is one data table document returns a bson, otherwise return a No
        """
        return self.collection.find({
            "_id": {"$ne": ObjectId(str(data_table_id).strip())}, "fields": {"$elemMatch": {"naming.naming": naming}}
        })

    def find_regex_naming_other_data_table(self, data_table_id, naming):
        """
            Finds all data tables expects for data table which parameter table_id is equal to property id and
            the data tables contains part or exact naming parameter in their fields.

        :param data_table_id: str. Represents id of the dictionary.
        :param naming: str. Represents the naming information.
        :return An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data table documents,
                If the query's result is one data table document returns a bson, otherwise return a No
        """
        return self.collection.find({
            "_id": {"$ne": ObjectId(str(data_table_id).strip())}, "fields": {"$elemMatch": {"naming.naming": {"$regex": naming}}}
        }, {"fields.$": 1, "alias": 1})

    def find_inside_naming(self, data_table_id, naming):
        """
            Finds all data tables that contains the exact naming parameter in their fields.

        :param data_table_id: str. Represents id of the dictionary.
        :param naming: str. Represents the naming information.
        :return An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data table documents,
                If the query's result is one data table document returns a bson, otherwise return a No
        """
        return self.collection.find(
            {"$and": [{"_id": ObjectId(str(data_table_id).strip())}, 
                      {"fields": {"$elemMatch": {"naming.naming": naming.strip().lower()}}}]},
            {"fields.$": 1, "master_name": 1})

    def find_master_name(self, master_name):
        """
            Finds one data table which parameter master_name is equal to the property master_name.

        :param master_name: str. Represents master_name of the data table.
        :return An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data table documents,
                If the query's result is one data table document returns a bson, otherwise return a No
        """
        return self.collection.find_one({"master_name": master_name.strip().lower()}, {"fields": 0})

    def find_all_without_fields(self):
        """
            Finds all data tables with the basic information.

        :return An array of bson, bson or None,
                An array of bson that contains the query's result when its more than one data table documents,
                If the query's result is one data table document returns a bson, otherwise return a None
        """
        return self.collection.find({}, {"_id": 1, "master_name": 1, "baseName": 1, "observationField": 1,
                                         "uuaaMaster": 1, "alias": 1, "user": 1, "stateTable": 1})

    def find_by_id(self, data_table_id):
        """
            Finds one data table by the property id with the basic information.

        :param data_table_id: str. Represents id of the dictionary.
        :return Bson or None.
        """
        return self.collection.find_one(
            {"_id": ObjectId(str(data_table_id).strip())},
            {"_id": 1, "master_name": 1, "baseName": 1, "observationField": 1,
             "uuaaMaster": 1, "alias": 1, "user": 1, "stateTable": 1})

    def find_user_tables_state(self, user_id, state):
        """
            Finds all data tables which had been created by the user parameter and had the state table parameter.

        :param user_id: str, string that represent a user id
        :param state: str, string that represent a table state
        :return: bson, dict that represent user tables data in one state
        """
        return self.collection.find({
            "$and": [{"user": ObjectId(str(user_id))}, {"stateTable": state.strip().upper()}]
        }, {
             "master_name": 1, "stateTable": 1,
             "baseName": 1, "alias": 1, "fields": 1
        })

    def find_rol_tables_state(self, state):
        """
            Finds all data tables by state table property.

        :param state: str, string that represent a table state
        :return: bson, dict that represent a tables in one state
        """
        return self.collection.find({
            "stateTable": state.strip().upper()
        }, {
            "master_name": 1, "stateTable": 1,
            "baseName": 1, "alias": 1, "fields": 1
        })

    def update_modifications_audit(self, data_table_id, modifications):
        """
            Updates the modifications property for the data table which id is equal to data_table_id parameter.

        :param data_table_id: str. Represents id of the dictionary.
        :param modifications: array. Modifications made by users.
        """
        self.collection.update_one({"_id": ObjectId(str(data_table_id))}, {"$set": {"modifications": modifications}})

    def update_people(self, data_table_id, people):
        """
            Updates the property people.
            The property people saved information related to the people who edits a data table.

        :param data_table_id: str. Represents id of the dictionary.
        :param people: array. People who changed the data table.
        """
        self.collection.update_one({"_id": ObjectId(str(data_table_id).strip())}, {"$set": {"people": people}})
    
    def find_data_tables_state(self, state):
        """
            Finds all data tables by stateTable.

        :param state: str, string that represent a table state
        :return: bson, dict that represent the data dictionary in one state
        """
        return self.collection.find({"stateTable": state}, {"master_name": 1, "baseName": 1, "user": 1, "alias": 1, "stateTable": 1, "uuaaMaster": 1, "observationField": 1, "fields": 1, "modifications": 1})


    def sent_to(self, data_table_id, state):
        """
            Updates the property stateTable for the data dictionary which table_id parameter is equal to
            id property in data dictionary collection.

        :param table_id: str, string that represent a table id.
        :param state: str, string that represent data dictionary state.
        """
        self.collection.update_one({"_id": ObjectId(str(data_table_id).strip())}, {"$set": {"stateTable": state}})
    
    def dt_count(self):
        """
            Return the estimated count of the number of data tables, documents in this collection.

        :return: number, result of the estimated count
        """
        return self.collection.estimated_document_count()

    def reset_all_naming_origins(self, data_table_id):
        """
            Return the estimated count of the number of data tables, documents in this collection.

        :return: number, result of the estimated count
        """
        return self.collection.update_many({"_id": ObjectId(str(data_table_id).strip())}, {"$set": { "fields.$[elem].originNamings" : []}}, 
                                           upsert = False, array_filters = [{ "elem.originNamings": {"$exists": True}}])