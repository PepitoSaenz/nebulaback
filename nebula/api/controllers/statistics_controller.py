from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

import json

from nebula.api.services.statistics_service import *

statistics_service = Statistics_service()


def find_state_table(request, table_id):
    """
        Returns the general statistics for the project, which _id is equal to the parameter project_id given.
        :param request: HttpRequest
        :param table_id: str, string that represent a project id
        :return: HttpResponse, table states
    """
    if request.method == "GET":
        response = JsonResponse(statistics_service.find_table_state(table_id),
                                safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def find_general_status(request, where, code):
    """Find a project or use case global state

    :param request:HttpRequest
    :param where:str, string that represent where need to check
    :param code: str, string that represent id to check
    :return:HttpResponse, global statistic
    """
    if request.method == "GET":
        response = JsonResponse(statistics_service.find_general_status(
            code, where, "dd"),
                                safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def find_general_status_dt(request, where, code):
    """Find a project or use case global state

    :param request:HttpRequest
    :param where:str, string that represent where need to check
    :param code: str, string that represent id to check
    :return:HttpResponse, global statistic
    """
    if request.method == "GET":
        response = JsonResponse(statistics_service.find_general_status(
            code, where, "dt"),
                                safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def find_between_days(request, code, group, start_date, end_date):
    """Find how many field was proposed between two dates by a user or team

    :param request:HttpRequest
    :param code: str, string that represent id to check
    :param group: str, string that represent where find (users or teams)
    :param start_date: str, string that represent a star date
    :param end_date: str, string that represent a end data
    :return: HttpResponse, json with statistic group
    """
    if request.method == "GET":
        response = JsonResponse(statistics_service.find_proposer_dates(
            group, code, start_date, end_date),
                                safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def get_main_statistics(request):
    """
        Returns the general statistics for the project, which _id is equal to the parameter project_id given.
        :param request: HttpRequest
        :param table_id: str, string that represent a project id
        :return: HttpResponse, table states
    """
    if request.method == "GET":
        response = JsonResponse(statistics_service.main_statistics(),
                                safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def get_main_statistics_dt(request):
    """
        Returns the general statistics for the project, which _id is equal to the parameter project_id given.
        :param request: HttpRequest
        :param table_id: str, string that represent a project id
        :return: HttpResponse, table states
    """
    if request.method == "GET":
        response = JsonResponse(statistics_service.main_statistics_dt(),
                                safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def get_tables_by_project(request):
    """
        Returns the general statistics for a project tables.
        :param request: HttpRequest
        :return: HttpResponse, tables per project
    """
    if request.method == "GET":
        try:
            response = JsonResponse(statistics_service.tables_by_project(),
                                    safe=False)
        except Exception as e:
            context = {'status': '500', 'reason': str(e)}
            response = HttpResponse(json.dumps(context),
                                    content_type='application/json')
            response.status_code = 500


def get_uuaa_statistics(request, uuaa, type, loc):
    """
        Returns the general statistics for the project, which _id is equal to the parameter project_id given.
        :param request: HttpRequest
        :param table_id: str, string that represent a project id
        :return: HttpResponse, table states
    """
    if request.method == "GET":
        response = JsonResponse(statistics_service.duplicated_namings_uuaa(
            uuaa, type, loc),
                                safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def get_tables_by_state_and_project(request):
    """
        Returns a project tables and states.
        :param request: HttpRequest
        :return: HttpResponse, tables per project
    """
    if request.method == "GET":
        try:
            response = JsonResponse(
                statistics_service.tables_by_state_and_project(), safe=False)
        except Exception as e:
            print("Excepcion: ", e)
            context = {'status': '500', 'reason': str(e)}
            response = HttpResponse(json.dumps(context),
                                    content_type='application/json')
            response.status_code = 500


def get_proyects_statistics(request):
    """
        Returns the general statistics for the project, which _id is equal to the parameter project_id given.
        :param request: HttpRequest
        :param table_id: str, string that represent a project id
        :return: HttpResponse, table states
    """
    if request.method == "GET":
        response = JsonResponse(statistics_service.number_all_proyects(),
                                safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def get_tables_by_uuaa(request):
    """
        Returns a project tables and states.
        :param request: HttpRequest
        :return: HttpResponse, tables per project
    """
    if request.method == "GET":
        try:
            response = JsonResponse(statistics_service.tables_by_uuaa(),
                                    safe=False)
        except Exception as e:
            print("Excepcion: ", e)
            context = {'status': '500', 'reason': str(e)}
            response = HttpResponse(json.dumps(context),
                                    content_type='application/json')
            response.status_code = 500


def get_proyects_statistics_dt(request):
    """
        Returns the general statistics for the project, which _id is equal to the parameter project_id given.
        :param request: HttpRequest
        :param table_id: str, string that represent a project id
        :return: HttpResponse, table states
    """
    if request.method == "GET":
        response = JsonResponse(statistics_service.number_all_proyects_dt(),
                                safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def get_general_statistics(request):
    """
        Returns the general statistics of Nebula
        :param request: HttpRequest
        :param table_id: str, string that represent a project id
        :return: HttpResponse, table states
    """
    if request.method == "GET":
        response = JsonResponse(statistics_service.general_statistics(),
                                safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def get_usedtables_in_datatables(request):
    """
        Returns the general statistics for the project, which _id is equal to the parameter project_id given.
        :param request: HttpRequest
        :param table_id: str, string that represent a project id
        :return: HttpResponse, table states
    """
    if request.method == "GET":
        #arr = ["concol11", "concol14", "ddadiportd", "ddaefser", "ddasfcas", "ddasfcon", 'ddasfopp', "ddasfvis", "hdabgbcs", "hdabgcli",
        #       "hdaca131", "hdacalot", "hdacaope", "hdacapcd", "hdacapch","hdacapdd","hdacapdh","hdacnps2","hdahamfl","hdahavrt","hdaknctc","hdakndis","hdalscomle","hdamccli","hdamccon","hdamccrd","hdamcefs","hdamclog","hdamcmol","hdamcpan","hdamcvrf","hdape001","hdape002","hdape003","hdape053","hdape094","hdapeinh","hdapemar","hdapm022","hdapmfm9006","hdaprodu","hdaqgcac","hdaqgfle","hdaqghco","hdarccon","hdarcctd","hdarcvlr","hdarvca2","hdarvcad","hdarvcar","hdarvcas","hdarvcfm","hdarvfm02","hdarvfm07","hdarvodmp","hdarvodpr","hdarvpr5","hdarvpr6","hdarvreg","hdasmofi","hdatc050","hdatpbas","hdaubfac","hdaubmae","hdaugconso","hdauggrb","hdauggrd","hdauggre","hdauggrf","hdaugpreap","hdaugr01","hdaugs0652","hdaugs0654","hipcol11","hipcol14","ldamk1968","ldamk201","ldape213","ldapemtr","odaficon","sdafiin6","sdarvcifcom","sdarvoricons","sdarvorihip","sdarvoritarj","sdarvrir","sdarvvarcom","tjacol11","tjacol14","udaesfiem","udaesresin","udafiadq","udaficb","udaficli","udafipro","udafirenva","udahacanx8","udahaconx8","udahafoesp","udahrlda","udaic8750","udaicsab","udaicsam","udamkcam","udape1829","udape1831","udape1992","udapebaven","udapecuo","udapefalle","udapefidup","udapegeo","udapehuell","udapeintrgpo","udapeprleg","udapesi1","udapestf","udapeven","udappsflyer","udaqrkhro","udarvbaext","udarvews","udarvsctg","udatcatms","udaxccom","udaxcvd","utapepatit"]
        arr = [
            "concol11", "concol14", "ddaefser", "ddasfcas", "ddasfcon",
            "ddasfopp", "ddasfvis", "hdabgbcs", "hdabgcli", "hdaca131",
            "hdacalot", "hdacaope", "hdacapcd", "hdacapch", "hdacapdd",
            "hdacapdh", "hdacnps2", "hdahamfl", "hdaknctc", "hdakndis",
            "hdamccon", "hdamccrd", "hdamcefs", "hdamclog", "hdamcmol",
            "hdamcpan", "hdamcvrf", "hdape001", "hdape002", "hdape003",
            "hdape053", "hdape094", "hdapeinh", "hdapemar", "hdapm022",
            "hdapmfm9006", "hdaprodu", "hdaqgcac", "hdaqgfle", "hdaqghco",
            "hdarccon", "hdarvca2", "hdarvcad", "hdarvcar", "hdarvcas",
            "hdarvcfm", "hdarvfm02", "hdarvfm07", "hdarvodmp", "hdarvodpr",
            "hdarvpr5", "hdarvpr6", "hdarvreg", "hdasmofi", "hdatc050",
            "hdaugr01", "hipcol11", "hipcol14", "ldamk1968", "ldamk201",
            "ldape213", "ldapemtr", "odaficon", "sdafiin6", "sdarvcifcom",
            "sdarvoricons", "sdarvorihip", "sdarvoritarj", "sdarvrir",
            "sdarvvarcom", "tjacol11", "tjacol14", "udaesfiem", "udaesresin",
            "udafiadq", "udaficb", "udaficli", "udafipro", "udahrlda",
            "udaic8750", "udaicsab", "udaicsam", "udamkcam", "udape1829",
            "udape1831", "udape1992", "udapecuo", "udapefalle", "udapefidup",
            "udapegeo", "udapehuell", "udapeintrgpo", "udapesi1", "udapestf",
            "udapeven", "udappsflyer", "udaqrkhro", "udarvbaext", "udarvews",
            "udarvsctg", "udatcatms", "udaxccom", "udaxcvd"
        ]
        secArr = [
            "qdarvfm02", "plcmolrcomprastarjet", "plcmolracteconoindex",
            "plcmolrcomprasacteco", "mgksanlclcontglobloc", "plcsanabttrainaa",
            "plcplmcomfuerzaventa", "plcbtqsfaccount", "plcsanabttrainso",
            "mgkcogbranch", "mgksanintervecontlcl", "plcbtqsfasset",
            "plctycsfuser", "plctycsfbranch", "plctycsfusrbrnch",
            "mlcctkampmodrating", "plcmoltransofi", "mlcctkampmodindividu",
            "mgkmollclreceiptmove", "mgkmollclloanmove",
            "mgkmollclloandetmove", "plcctkorihipoteca", "plcctkoriconsumo",
            "plcctkoritarjetas", "plcsanabtpredictaa", "mgksanrelnshpcontlcl",
            "pgkpfmreceiptssudets", "pgkpfmloancontradets",
            "plcsanabtpredictso", "plcctkcubocontratos", "mlcsaneomcontract",
            "mlcsaneomassetsliab", "mlcsaneomoutofbalit",
            "mlcsaneomdomesticint", "mgkbtqeomeconinform", "mgkgugassets",
            "mgkgugvaluations", "mgkbtqeomcustomer", "mgkdeoinapprtcontsit",
            "mgkdeoinapprtcustsit", "mgkrasriskanalytpeop",
            "mgkslbmeomownedsec", "mgksageomlocalgroup", "mlcsageomsegment",
            "mgkgugguarantees", "plcdeoprovisionbase", "plwikjslrep",
            "plcbtqpersonaskpis", "mgkdeowriteoffcont", "mgkctkcustratingatrb",
            "mgkrasriskancontgnte", "plcctdinfoadq", "plcmolinfomediopago",
            "plcpadinfobaseprod", "plcsagbasegment"
        ]
        response = JsonResponse(
            statistics_service.get_usedtables_in_datatables(arr,secArr), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response
