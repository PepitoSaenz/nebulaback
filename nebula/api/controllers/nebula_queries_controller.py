from django.http import HttpResponse, JsonResponse
from nebula.api.services.nebula_queries_service import *

nebula_queries_service = Nebula_queries_service()


def _convert_cursor_to_json(cursor):
    """Return the cursor in a str format.
    it go down to json checking that doesn't exist objectId's to send

        :param cursor: A bson
        :return: array, a string with the cursor information.
        """
    res = []
    for i in cursor:
        for j in i:
            if type(i[j]) is ObjectId:
                i[j] = str(i[j])
            elif type(i[j]) is list:
                for k in range(len(i[j])):
                    if type(i[j][k]) is ObjectId:
                        i[j][k] = str(i[j][k])
                    if type(i[j][k]) is dict:
                        for p in i[j][k]:
                            if type(i[j][k][p]) is list:
                                for w in range(len(i[j][k][p])):
                                    for t in i[j][k][p][w]:
                                        if type(i[j][k][p][w][t]) is ObjectId:
                                            i[j][k][p][w][t] = str(i[j][k][p][w][t])
                            elif type(i[j][k][p]) is ObjectId:
                                i[j][k][p] = str(i[j][k][p])
        res.append(i)
    return res


def find_global_query(request, where, value):
    """Find a global query where group naming, legacy and uuaas in one query

    :param request: HttpsRequest
    :param where: str, string that represent where find
    :param value: str, string that represent value to find
    :return:HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(_convert_cursor_to_json(nebula_queries_service.find_global_query(where, value)), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response
