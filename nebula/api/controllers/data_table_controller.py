from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json

from nebula.api.services.data_table_service import *
data_table_service = Data_table_service()


def _convert_cursor_to_json(cursor):
    """
    Return the cursor in a str format to send a petition
    it check all object id's that exists a change it
    :param cursor: A bson
    :return: array, a strings with the cursor information.
    """
    res = []
    for i in cursor:
        for j in i:
            if type(i[j]) is ObjectId:
                i[j] = str(i[j])
            elif type(i[j]) is list:
                for k in range(len(i[j])):
                    if type(i[j][k]) is ObjectId:
                        i[j][k] = str(i[j][k])
                    if type(i[j][k]) is dict:
                        for p in i[j][k]:
                            if type(i[j][k][p]) is list:
                                for w in range(len(i[j][k][p])):
                                    for t in i[j][k][p][w]:
                                        if type(i[j][k][p][w][t]) is ObjectId:
                                            i[j][k][p][w][t] = str(i[j][k][p][w][t])
                            elif type(i[j][k][p]) is ObjectId:
                                i[j][k][p] = str(i[j][k][p])
        res.append(i)
    return res


@csrf_exempt
def object_head(request, table_id):
    """find or add/update a table head:
        A head are routes, perimeters, etc..

    :param request: HttpRequest
    :param table_id: str, a string that represent the table id
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(data_table_service.find_head(table_id), safe=False)
    elif request.method == "POST":
            try:
                data_table_service.update_head(table_id, json.loads(request.body))
                response = HttpResponse(status=200)
            except Exception as e:
                response = HttpResponse(json.dumps({'status': '500', 'reason': str(e)}), content_type='application/json')
                response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def change_state(request, table_id):
    """Change a table state to pass a other phase:
        Example: it can change to governance to architecture or architecture to governance

        :param request: HttpRequest
        :param table_id: str, a string that represents the table id.
        :return: HttpResponse
        """
    if request.method == "PUT":
        try:
            data_table_service.send_to(table_id, json.loads(request.body))
            response = HttpResponse(status=200)
        except Exception as e:
            traceback.print_exc()
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    elif request.method == "POST":
        if request.body:
            data_table_service.free_change_state(table_id, json.loads(request.body))
            response = HttpResponse(status=200)
        else: 
            response = HttpResponse(status=500)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def generate_excels(request, table_id, phase):
    """Generate excels to a new import data

    :param request: HttpRequest
    :param table_id: str, string that represents a table id
    :param phase: str, string that represent a phase to generate
    :return: HttpResponse, a excel to the data to import
    """
    if request.method == "GET":
        try:
            response = JsonResponse(data_table_service.generate_excel(table_id, phase), safe=False)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def fields_data_table(request, table_id):
    """find or add/update a table head:
        A head are routes, perimeters, etc..

    :param request: HttpRequest
    :param table_id: str, a string that represent the table id
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(data_table_service.find_fields(table_id), safe=False)
    elif request.method == "POST":
            try:
                data_table_service.update_fields(table_id, json.loads(request.body))
                response = HttpResponse(status=200)
            except Exception as e:
                context = {
                    'status': '500', 'reason': str(e)
                }
                response = HttpResponse(json.dumps(context), content_type='application/json')
                response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def fields_actions_data_table(request, table_id):
    """
    :param request: HttpRequest
    :param table_id: str, a string that represent the table id
    :return: HttpResponse
    """
    if request.method == "POST":
        try:
            response = JsonResponse(data_table_service.fields_actions(table_id, json.loads(request.body)), safe=False)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def validate_origin_tables(request, table_id):
    """
       :param request: HttpRequest
       :param table_id: str, a string that represent the table id
       :return: HttpResponse
    """
    if request.method == "GET":
        try:
            response = JsonResponse(data_table_service.validate_origin_tables(table_id), safe=False)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_origin_tables(request, alias, datio_name, uuaa_raw, uuaa_master, logic):
    """

        :param request: HttpRequest
        :param alias: str
        :param datio_name: str
        :param uuaa: str
        :param logic: str
        :return: HttpResponse
    """
    if request.method == "GET":
        try:
            response = JsonResponse(data_table_service.find_origin_tables(alias, datio_name, uuaa_raw, uuaa_master, logic), safe=False)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_origin_data_tables(request, alias, datio_name, uuaa, logic):
    """

        :param request: HttpRequest
        :param alias: str
        :param datio_name: str
        :param uuaa: str
        :param logic: str
        :return: HttpResponse
    """
    if request.method == "GET":
        try:
            response = JsonResponse(data_table_service.find_origin_data_tables(alias.strip(), datio_name.strip(), uuaa.strip(), logic.strip()), safe=False)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def update_validation(request, table_id):
    """Request to update VoBo product owner.

        :param request: HttpRequest
        :param table_id: str, a string that represents the table id.
        :return: HttpResponse
        """
    if request.method == "POST":
        try:
            data_table_service.update_validation_po(table_id, json.loads(request.body))
            response = HttpResponse(status=200)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_similarities(request, table_id, type):
    """Request to compare with others data tables.

        :param request: HttpRequest
        :param type: str
        :param table_id: str
        :return: HttpResponse
        """
    if request.method == "GET":
        try:
            response = JsonResponse(data_table_service.find_similarities(table_id, type), safe=False)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def update_origins(request, table_id, category):
    """Request to compare with others data tables.

        :param request: HttpRequest
        :param category: str
        :param table_id: str
        :return: HttpResponse
        """
    if request.method == "PUT":
        try:
            data_table_service.update_information(table_id, category, json.loads(request.body))
            response = HttpResponse(status=200)
        except Exception as e:
            if str(e) == "list index out of range":
                e = "La cantidad de namings del tablon es menor a los namings que se cargan."
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def find_inside_tablon(request, table_id, suffix, naming, logic, desc, state):
    """

    :param request:
    :param table_id:
    :param suffix:
    :param naming:
    :param logic:
    :param desc:
    :param state:
    :return:
    """
    if request.method == "GET":
        try:
            response = JsonResponse(_convert_cursor_to_json(data_table_service.find_inside_data_table(table_id, suffix, naming, logic, desc, state)), safe=False)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

def find_data_tables_state(request, state):
    """
        Returns all the data tables given a state

    :param request: HttpRequest
    :param state_id: str, a string that represents the sprint id.
    :param state: str.
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(
            _convert_cursor_to_json(data_table_service.find_data_tables_state_only(state)), safe=False
        )
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def generate_excels_todo(request):
    """Generate excels to a new import data

    :param request: HttpRequest
    :param table_id: str, string that represents a table id
    :param phase: str, string that represent a phase to generate
    :return: HttpResponse, a excel to the data to import
    """
    if request.method == "GET":
        try:
            response = JsonResponse(data_table_service.generate_excel_todo(), safe=False)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def generate_excels_todo(request, table_id):
    """Generate excels to a new import data

    :param request: HttpRequest
    :param table_id: str, string that represents a table id
    :param phase: str, string that represent a phase to generate
    :return: HttpResponse, a excel to the data to import
    """
    if request.method == "GET":
        try:
            response = JsonResponse(data_table_service._reset_all_naming_origins(table_id), safe=False)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def clean_columns(request, table_id):
    if request.method == "GET":
        try:
            response = JsonResponse(data_table_service._clean_columns(table_id), safe=False)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

