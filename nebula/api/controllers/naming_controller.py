from django.http import HttpResponse, JsonResponse
from nebula.api.services.field_service import *
from nebula.api.services.suffix_service import *
from nebula.api.services.word_service import *
from nebula.api.services.global_search_service import *

import json

field_service = Field_service()
suffix_service = Suffix_service()
word_service = Word_service()
global_search_service=Global_search_service()


def _convert_cursor_to_json(cursor):
    """Returns the cursor in a str format.

    :param cursor: A bson
    :return: array, a string with the cursor information.
    """
    res = []
    for i in cursor:
        res.append(i)
    return res


def find_hierarchy(request):
    """Find all hierarchy defines

    :param request: HttpRequest
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(field_service.find_hierarchy(),safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def find_functional_level(request, level):
    """Find all field with a functional levels defined

    :param request: HttpRequest
    :param level: str, string that represent a level to find
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(field_service.find_functional_level(level),safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def machine_global_query(request, naming, suffix, description):
    """Do a smart query with some specific conditions

    :param request: HttpRequest
    :param naming: str, string that represent a naming
    :param suffix: str, string that represent a suffix
    :param description: str, string that represent a description field
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(
            _convert_cursor_to_json(global_search_service.raw_search(naming, suffix, description)), safe=False
        )
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def global_query(request, naming, logic, description, type, level):
    """Do a query by specific filters

    :param request: HttpRequest
    :param naming: str, string that represent a naming
    :param logic: str, string that represent a logic name
    :param description: str, string that represent a description name
    :param type: str, string that represent a suffix
    :return:
    """
    if request.method == "GET":
        response = JsonResponse(field_service.find_global_query(naming.strip(), logic.strip(), description.strip(), type.strip(), level.strip()), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def find_exactly_naming(request, naming):
    """Find a exactly naming in a repository

    :param request: HttpRequest
    :param naming: str, string that represent a naming
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(
            field_service.find_exactly_naming(naming), safe=False
        )
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def find_all_suffix(request):
    """Find all possible suffix that can be use

    :param request: HttpRequest
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(
            _convert_cursor_to_json(suffix_service.find_all()), safe=False
        )
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def find_all_word_information(request, word, abbreviation, word_spanish, synonymous, type):
    """Find a word information as the synonymous and abbreviation

    :param request: HttpRequest
    :param word: str, string that represent a word to find
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(word_service.find_all_information(word, abbreviation, word_spanish, synonymous, type), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def find_naming_table(request, naming):
    """Find a exactly naming in all tables
    it get naming and in where is it, what tables get it

    :param request: HttpRequest
    :param naming: str, string that represent a naming
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(field_service.find_naming_other_table(naming), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def export_namings(request):
    """
        Generates namings excel similar to Sophia export 

    :param request: HttpRequest
    :return: HttpResponse, a excel to the data to export
    """
    if request.method == "POST":
        try:
            resu = field_service.generate_naming_export(json.loads(request.body))
            response = JsonResponse(resu, safe=False)
            response.status_code = 200
        except Exception as e:
            context = {'status': '500', 'reason': str(e)}
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

def export_namings_limit(request, limit):
    """
    :param naming: str, string that represent a naming
    :return: HttpResponse
    """
    if request.method == "GET":
        try:
            response = JsonResponse(field_service.generate_extra_export(limit, "limit"), safe=False)
        except Exception as e:
            context = {'status': '500', 'reason': str(e)}
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

def export_namings_skip(request, skip):
    """
    :param naming: str, string that represent a naming
    :return: HttpResponse
    """
    if request.method == "GET":
        try:
            response = JsonResponse(field_service.generate_extra_export(skip, "skip"), safe=False)
        except Exception as e:
            context = {'status': '500', 'reason': str(e)}
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response
