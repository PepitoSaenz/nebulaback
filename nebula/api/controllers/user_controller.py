from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json

from nebula.api.services.user_service import *
from nebula.api.services.team_service import *


user_service = User_service()
team_service = Team_service()


def _convert_cursor_to_json(cursor):
    """Return the cursor in a str format.

        :param cursor: A bson
        :return: array, a string with the cursor information.
        """
    res = []
    for i in cursor:
        for j in i:
            if type(i[j]) is ObjectId:
                i[j] = str(i[j])
            elif type(i[j]) is list:
                for k in range(len(i[j])):
                    if type(i[j][k]) is ObjectId:
                        i[j][k] = str(i[j][k])
                    if type(i[j][k]) is dict:
                        for p in i[j][k]:
                            if type(i[j][k][p]) is list:
                                for w in range(len(i[j][k][p])):
                                    for t in i[j][k][p][w]:
                                        if type(i[j][k][p][w][t]) is ObjectId:
                                            i[j][k][p][w][t] = str(i[j][k][p][w][t])
        res.append(i)
    return res


def _conver_json(json):
    """

    :param json:
    :return:
    """
    for j in json:
        if type(json[j]) is ObjectId:
            json[j] = str(json[j])
        elif type(json[j]) is list:
            for k in range(len(json[j])):
                if type(json[j][k]) is ObjectId:
                    json[j][k] = str(json[j][k])
    return json


@csrf_exempt
def login(request):
    """Check that one user can access with they credentials

    :param request: HttpRequest, credentials to log in, emails and password
    :return: HttpResponse, data to construct a user information
    """
    if request.method == "POST":
        try:
            response = JsonResponse(_conver_json(user_service.login(json.loads(request.body))),safe=False)
        except:
            response = HttpResponse(status=500)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def pending_users(request, user_id):
    """find public data to specific user

        :param request: HttpRequest
        :param user_id: str, string that represent a user id
        :return:HttpResponse, json with user public data
        """
    if request.method == "GET":
        response = JsonResponse(user_service.find_pending_users(user_id), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def user_update_changes(request, user_id):
    """find public data to specific user

        :param request: HttpRequest
        :param user_id: str, string that represent a user id
        :return:HttpResponse, json with user public data
        """
    if request.method == "POST":
        try:
            user_service.update_user_data(user_id, json.loads(request.body))
            response = HttpResponse(status=200)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def get_all_users(request):
    """
            Returns the list of all registered users.
        :param request: HttpRequest
        :return: HttpResponse, json with user public data
    """
    if request.method == "POST":
        response = JsonResponse(user_service.get_all_users(json.loads(request.body)), safe=False)
    else:
        response = HttpResponse(status=500)

    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def find_user_email(request, email):
    """find public data to specific user

        :param request: HttpRequest
        :param email: str
        :return:HttpResponse, json with user public data
    """
    if request.method == "GET":
        response = JsonResponse(user_service.find_by_email(email), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def users(request):
    """Find all user names or insert one user

    :param request: HttpRequest, json that contains data user to insert
    :return: HttpResponse, json to all user names
    """
    if request.method == "GET":
        response = JsonResponse(_convert_cursor_to_json(user_service.find_all_names()), safe=False)
    elif request.method == "POST":
        try:
            user_service.insert_one(json.loads(request.body))
            response = HttpResponse(status=200)
        except:
            response = HttpResponse(status=500)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def teams(request):
    """Insert a team or consult all teams

    :param request: HttpRequest, json to team data
    :return:HttpResponse, json with team data
    """
    if request.method == "GET":
        response = JsonResponse(team_service.find_all(), safe=False)
    elif request.method == "POST":
        try:
            team_service.insert_one(json.loads(request.body))
            response = HttpResponse(status=200)
        except:
            response = HttpResponse(status=500)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def teams_actions(request, team_id):
    """Find or update one team

        :param request: HttpRequest, json with data to update
        :param team_id: str, string that represent team id
        :return:HttpResponse, json with data from one team
        """
    if request.method == "GET":
        response = JsonResponse(team_service.find_one(team_id), safe=False)
    elif request.method == "PUT":
        team_service.update_one(team_id, json.loads(request.body))
        response = HttpResponse(status=200)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def countries(request):
    """Find all available countries

    :param request:HttpRequest
    :return:HttpsResponse, json with available countries
    """
    response = JsonResponse(team_service.countries(), safe=False)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def product_owner_projects(request, user_type, user_id):
    """Find all associate projects to one product owner

        :param request: HttpRequest
        :param user_type: str, string that represent a prodcto owner rol (PO or SPO)
        :param user_id: str, string that represent a product owner id
        :return: HttpResponse, json with project associate
        """
    if request.method == "GET":
        response = JsonResponse(
            _convert_cursor_to_json(user_service.find_projects_users(user_type, user_id)), safe=False
        )
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def tables_state(request, user_id, state):
    """Find all tables that one user has associate depending to they rol

        :param request: HttpRequest
        :param user_id: str, string that represent a user id
        :param state: str, string that represent a user rol
        :return:HttpResponse, json with table associate
        """
    if request.method == "GET":
        response = JsonResponse(
            _convert_cursor_to_json(user_service.table_state(user_id, state)), safe=False
        )
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def data_table_state(request, user_id, state):
    """Find all data tables that one user has associate depending to they rol

        :param request: HttpRequest
        :param user_id: str, string that represent a user id
        :param state: str, string that represent a user rol
        :return:HttpResponse, json with table associate
        """
    if request.method == "GET":
        response = JsonResponse(
            _convert_cursor_to_json(user_service.data_table_state(user_id, state)), safe=False
        )
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def team_tables_state(request, team_id, state):
    """Find all tables associate to one team with one state (ingest, governance, architecture, etc..)

        :param request: HttpRequest
        :param team_id: str, string that represent team id
        :param state: str, string that represent a state to check
        :return:HttpResponse, Json with table associate to one team a state
        """
    if request.method == "GET":
        response = JsonResponse(
            _convert_cursor_to_json(team_service.find_state_tables(team_id, state)), safe=False
        )
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def team_data_tables_state(request, team_id, state):
    """Find all data tables associate to one team with one state (ingest, governance, architecture, etc..)

            :param request: HttpRequest
            :param team_id: str, string that represent team id
            :param state: str, string that represent a state to check
            :return:HttpResponse, Json with table associate to one team a state
            """
    if request.method == "GET":
        response = JsonResponse(
            _convert_cursor_to_json(team_service.find_state_data_tables(team_id, state)), safe=False
        )
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def rol(request, rol):
    """Find all user associate to one rol

    :param request: HttpRequest
    :param rol: str, string that represent a rol to check
    :return:HttpResponse, Json with user with that rol
    """
    if request.method == "GET":
        response = JsonResponse(
            _convert_cursor_to_json(user_service.find_by_rol(rol)), safe=False
        )
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def find_parnerts(request, group, code):
    """Find all users in the same team

    :param request: HttpRequest
    :param group: str, string that represent a group to check (user or team)
    :param code: str, string that represent a id to check
    :return: HttpResponse, Json with parnerts data
    """
    if request.method == "GET":
        response = JsonResponse(user_service.user_project(group, code), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

def find_user_vobos(request, user_id):
    """Find the user projects available to vobo

    :param request: HttpRequest
    :param user_id: str, string that represent user id
    :return: HttpResponse, json that has project id's and names associate to one user
    """
    if request.method == "GET":
        response = JsonResponse(user_service.find_vobos(user_id), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def find_user_projects(request, user_id):
    """Find projects associate to one user

    :param request: HttpRequest
    :param user_id: str, string that represent user id
    :return: HttpResponse, json that has project id's and names associate to one user
    """
    if request.method == "GET":
        response = JsonResponse(user_service.find_projects(user_id), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def user_changes(request):
    """

    :param request: HttpRequest
    :return: HttpResponse
    """
    if request.method == "POST":
        try:
            response = JsonResponse(user_service.change_password(json.loads(request.body)), safe=False)
        except:
            response = HttpResponse(status=500)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def create_user(request, user_id):
    """find public data to specific user

        :param request: HttpRequest
        :param user_id: str, string that represent a user id
        :return:HttpResponse, json with user public data
        """
    if request.method == "POST":
        response = JsonResponse(user_service.create_user(user_id, json.loads(request.body)), safe=False)
        response = HttpResponse(status=200)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def user_actions(request, user_id):
    """find public data to specific user

        :param request: HttpRequest
        :param user_id: str, string that represent a user id
        :return:HttpResponse, json with user public data
        """
    if request.method == "GET":
        response = JsonResponse(
            _conver_json(user_service.find_one(user_id)), safe=False
        )
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def users_roles(request):
    """Gets the user role options including the natural name for them.
        ie. rol: G, name: Gobierno

        :param request: HttpRequest
        :return: HttpResponse, json with user public data
    """
    if request.method == "GET":
        response = JsonResponse(user_service.users_roles(), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response
