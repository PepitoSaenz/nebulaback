import traceback
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

import json

from nebula.api.services.functional_map_service import *

functional_map_service = Functional_map_service()


def _convert_cursor_to_json(cursor):
    """
        Returns the cursor in a str format to send petition.
        Checks all object id's that exists a changed to str.
    :param cursor: A bson
    :return: array, a strings with the cursor information.
    """
    res = []
    for i in cursor:
        for j in i:
            if type(i[j]) is ObjectId:
                i[j] = str(i[j])
            elif type(i[j]) is list:
                for k in range(len(i[j])):
                    if type(i[j][k]) is ObjectId:
                        i[j][k] = str(i[j][k])
                    if type(i[j][k]) is dict:
                        for p in i[j][k]:
                            if type(i[j][k][p]) is list:
                                for w in range(len(i[j][k][p])):
                                    for t in i[j][k][p][w]:
                                        if type(i[j][k][p][w][t]) is ObjectId:
                                            i[j][k][p][w][t] = str(i[j][k][p][w][t])
                            elif type(i[j][k][p]) is ObjectId:
                                i[j][k][p] = str(i[j][k][p])
        res.append(i)
    return res

@csrf_exempt
def query_master_search_props(request):
    """
        Finds and retrieves raw uuaas based on some factors.

    :param request: HttpRequest
    :param uuaa_name: str, string that represent a table id
    :param app_name: str, string that represent a fase to check
    :param origin_name: str, string that represent a naming to find
    :return: HttpResponse, a fields with meet the conditions
    """
    if request.method == "GET":
        response = JsonResponse(
            functional_map_service.query_master_search_props(), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def query_new_uuaa_master(request, uuaa_name, system_name, data_source, desc, level1, level2, country):
    """
        Finds and retrieves raw uuaas based on some factors.

    :param request: HttpRequest
    :param uuaa_name: str, string that represent a table id
    :param app_name: str, string that represent a fase to check
    :param origin_name: str, string that represent a naming to find
    :return: HttpResponse, a fields with meet the conditions
    """
    if request.method == "GET":
        response = JsonResponse(_convert_cursor_to_json(
            functional_map_service.query_master_uuaas(uuaa_name, system_name, data_source, desc, level1, level2, country)), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def query_uuaa_master(request, uuaa_name, desc, desc_eng, data_source, country, deploy_country, level1, level2):
    """
        Finds and retrieves raw uuaas based on some factors.

    :param request: HttpRequest
    :param uuaa_name: str, string that represent a table id
    :param app_name: str, string that represent a fase to check
    :param origin_name: str, string that represent a naming to find
    :return: HttpResponse, a fields with meet the conditions
    """
    if request.method == "GET":
        response = JsonResponse(_convert_cursor_to_json(
            functional_map_service.query_uuaas(uuaa_name, desc, desc_eng, data_source, country, deploy_country, level1, level2)), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def query_all_uuaa_master(request):
    """
        Finds and retrieves all master uuaas.

    :param request: HttpRequest
    :param uuaa_name: str, string that represent a table id
    :param app_name: str, string that represent a fase to check
    :param origin_name: str, string that represent a naming to find
    :return: HttpResponse, a fields with meet the conditions
    """
    if request.method == "GET":
        response = JsonResponse(_convert_cursor_to_json(
            functional_map_service.query_uuaas("", "", "", "", "", "", "", "")), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def query_all_uuaa_master_full(request):
    if request.method == "GET":
        response = JsonResponse(_convert_cursor_to_json(
            functional_map_service.get_uuaa_fullname()), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def get_uuaa(request, uuaa_id):
    """
        Updates all data dictionary object information.

    :param request: HttpRequest
    :param uuaa_id: str, string that represent a uuaa id
    :return: HttpResponse
    """
    if request.method == "GET":
        try:
            response = HttpResponse(json.dumps(
                functional_map_service.get_uuaa(uuaa_id)), content_type='application/json')
            response.status_code = 200
        except Exception as e:
            context = { 'status': '500', 'reason': str(e) }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def get_property_options(request, property_name):
    """
        Updates all data dictionary object information.

    :param request: HttpRequest
    :param uuaa_id: str, string that represent a uuaa id
    :return: HttpResponse
    """
    if request.method == "GET":
        try:
            response = HttpResponse(json.dumps(
                functional_map_service.get_property_options(property_name)), content_type='application/json')
            response.status_code = 200
        except Exception as e:
            context = { 'status': '500', 'reason': str(e) }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def new_uuaa(request):
    """
        Updates all data dictionary object information.

    :param request: HttpRequest
    :param uuaa_id: str, string that represent a uuaa id
    :return: HttpResponse
    """
    if request.method == "POST":
        try:
            response = HttpResponse(json.dumps(
                functional_map_service.new_uuaa(json.loads(request.body))), content_type='application/json')
            response.status_code = 200
        except Exception as e:
            context = { 'status': '500', 'reason': str(e) }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def update_uuaa(request, uuaa_id):
    """
        Updates all data dictionary object information.

    :param request: HttpRequest
    :param uuaa_id: str, string that represent a uuaa id
    :return: HttpResponse
    """
    if request.method == "PUT":
        try:
            response = HttpResponse(json.dumps(
                functional_map_service.update_uuaa(uuaa_id, json.loads(request.body))), content_type='application/json')
            response.status_code = 200
        except Exception as e:
            traceback.print_exc()
            context = { 'status': '500', 'reason': str(e) }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def update_object(request):
    """
        Updates the uuaa object information.

    :param request: HttpRequest
    :param uuaa_id: str, string that represent a uuaa id
    :return: HttpResponse
    """
    if request.method == "POST":
        try:
            response = HttpResponse(json.dumps(
                functional_map_service.update_object(json.loads(request.body))), content_type='application/json')
            response.status_code = 200
        except Exception as e:
            context = { 'status': '500', 'reason': str(e) }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def get_ambitos(request):
    """
        Retrieves the list of ambitos that the uuaas can get

    :param request: HttpRequest
    :param uuaa_id: str, string that represent a uuaa id
    :return: HttpResponse
    """
    if request.method == "GET":
        try:
            response = HttpResponse(json.dumps(
                functional_map_service.get_ambitos()), content_type='application/json')
            response.status_code = 200
        except Exception as e:
            response = HttpResponse(json.dumps({'status': '500', 'reason': str(e)}), content_type='application/json')
            response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def report_tables(request, param):
    """
        Generates the report of the tables with or without proper data sources

    :param request: HttpRequest
    :param uuaa_id: str, string that represent a uuaa id
    :return: HttpResponse
    """
    if request.method == "GET":
        try:
            response = HttpResponse(json.dumps(
                functional_map_service.report_tables(param)), content_type='application/json')
            response.status_code = 200
        except Exception as e:
            response = HttpResponse(json.dumps({'status': '500', 'reason': str(e)}), content_type='application/json')
            response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response


