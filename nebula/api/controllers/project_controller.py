from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
import traceback
from bson import ObjectId

from nebula.api.services.project_service import Project_service
from nebula.api.services.functional_map_service import Functional_map_service
from nebula.api.services.raw_functional_map_service import Raw_functional_map_service
from nebula.api.services.use_case_service import Use_case_service
from nebula.api.services.backlog_service import Backlog_service
from nebula.api.services.requests_service import Requests_service
from nebula.api.services.data_table_service import Data_table_service

project_service = Project_service()
functional_map_service = Functional_map_service()
raw_functional_map_service = Raw_functional_map_service()
use_case_service = Use_case_service()
backlog_service = Backlog_service()
requests_service = Requests_service()
data_tables_service = Data_table_service()


def _convert_cursor_to_json(cursor):
    """
    Return the cursor in a str format to send a petition,
    it go down checking that doesn't exist objectId to send
    :param cursor: A bson
    :return: array, a strings with the cursor information.
    """
    res = []
    for i in cursor:
        for j in i:
            if type(i[j]) is ObjectId:
                i[j] = str(i[j])
            elif type(i[j]) is list:
                for k in range(len(i[j])):
                    if type(i[j][k]) is ObjectId:
                        i[j][k] = str(i[j][k])
        res.append(i)
    return res


def _conver_dd_cursor(cursor):
    res = []
    for i in cursor:
        for j in i:
            if type(i[j]) is ObjectId:
                i[j] = str(i[j])
            elif type(i[j]) is list:
                for k in range(len(i[j])):
                    if type(i[j][k]) is ObjectId:
                        i[j][k] = str(i[j][k])
                    if type(i[j][k]) is dict:
                        for p in i[j][k]:
                            if type(i[j][k][p]) is list:
                                for w in range(len(i[j][k][p])):
                                    for t in i[j][k][p][w]:
                                        if type(i[j][k][p][w][t]) is ObjectId:
                                            i[j][k][p][w][t] = str(i[j][k][p][w][t])
                            elif type(i[j][k][p]) is ObjectId:
                                i[j][k][p] = str(i[j][k][p])
        res.append(i)
    return res


def _conver_json(json):
    """Check a json can be send as a petition and transform to do it

    :param json: json, that reprensent a data to send
    :return:json, that will be transform to be send
    """
    for j in json:
        if type(json[j]) is ObjectId:
            json[j] = str(json[j])
        elif type(json[j]) is list:
            for k in range(len(json[j])):
                if type(json[j][k]) is ObjectId:
                    json[j][k] = str(json[j][k])
    return json


def find_project_VoBo(request, user_id):
    """

    :param request:
    :param user_id:
    :return:
    """
    if request.method == 'GET':
        try:
            response = JsonResponse(project_service.find_project_vobo(user_id), safe=False)
        except Exception as e:
            response = HttpResponse(json.dumps({'status': '500', 'reason': str(e)}), content_type='application/json')

    response["Access-Control-Allow-Origin"] = "*"
    return response

def check_vobo_in_projects(request, user_id):
    """

    :param request:
    :param user_id:
    :return:
    """
    if request.method == 'GET':
        response = JsonResponse(project_service._check_vobo_in_projects(user_id), safe=False)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def project(request):
    """Insert or get a project

    :param request: HttpRequest, can be a json that contains a data to save
    :return:HttpResponse, can be a project to get
    """
    if request.method == "GET":
        try:
            response = JsonResponse(project_service.find_all(), safe=False)
            response.status_code = 200
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    elif request.method == 'POST':
        project_service.insert_one(json.loads(request.body))
        response = HttpResponse(status=200)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_project_to_rol(request, rol, rol_id):
    """ Find all project that one user can see as productOwner

        :param request: HttpRequest
        :param rol: str, string that represent rol to check
        :param rol_id: str, string that represent product Owner id
        :return:HttpResponse, projects that can see
        """
    if request.method == "GET":
        response = JsonResponse(
            _convert_cursor_to_json(project_service.find_to_rol(rol, rol_id)), safe=False
        )
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def one_project(request, project_id):
    """Find or update a specific project

        :param request: HttpRequest
        :param project_id: str, string that represent a project id
        :return:HttpResponse, can be all data from one project
        """
    if request.method == 'GET':
        response = JsonResponse(project_service.find_one(project_id), safe=False)
    elif request.method == 'PUT':
        project_service.update_one(project_id, json.loads(request.body))
        response = HttpResponse(status=200)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def action_in_project(request, project_id, action):
    """Add, remove or find data from one project

        :param request:HttpRequest
        :param project_id: str, string that represent a project id
        :param action: str, string that represent a action to do (add, remove, what get)
        :return:HttpResponse, can be a data get it
        """
    if request.method == "PUT":
        project_service.action_in_project(project_id, action, json.loads(request.body))
        response = HttpResponse(status=200)
    elif request.method == "GET":
        response = JsonResponse(
            project_service.find_actions_projects(project_id, action), safe=False
        )
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_functional_map(request, project_id):
    """Find uuaa's from an id

        :param request: HttpRequest
        :param project_id: str, string that represent a project id
        :return:
        """
    if request.method == "GET":
        response = JsonResponse(
            functional_map_service.find_uuaas_project(project_id), safe=False
        )
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_functional_map_data_table(request, project_id):
    """Find uuaa's from an id

        :param request: HttpRequest
        :param project_id: str, string that represent a project id
        :return:
        """
    if request.method == "GET":
        response = JsonResponse(
            functional_map_service.find_uuaas_data_table(project_id), safe=False
        )
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_use_cases_project(request, project_id):
    """Get all use case that has one project

    :param request: HttpsRequest
    :param project_id: str, string that represent project id
    :return:HttpResponse, all use case associate to a project
    """
    if request.method == "GET":
        response = JsonResponse(_convert_cursor_to_json(project_service.use_cases_project(project_id)), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_use_case_backlogs(request, use_case_id):
    """Find backlogs associated to one use case.

        :param request: HttpRequest,
        :param use_case_id: str, string that represent use case id
        :return: HttpResponse, all tables associate an one useCase
        """
    if request.method == "GET":
        try:
            response = JsonResponse(use_case_service.find_use_case_backlogs(use_case_id), safe=False)
        except Exception as e:
            traceback.print_exc()
            print(e)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_use_case_data_tables(request, use_case_id):
    """Find backlogs associated to one use case.

        :param request: HttpRequest,
        :param use_case_id: str, string that represent use case id
        :return: HttpResponse, all tables associate an one useCase
        """
    if request.method == "GET":
        response = JsonResponse(use_case_service.find_use_case_data_tables(use_case_id), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def static_atributes_backlog(request, attributes):
    """ Find the periodicity, origin system and type file options for the document Backlog.

    :param request: HttpRequest
    :param attributes: str, string which is periodicity, originSystem or typeFile.
    :return: HttpResponse,
    """
    if request.method == "GET":
        try:
            response = JsonResponse(_convert_cursor_to_json(backlog_service.static_atributes(attributes)), safe=False)
        except:
            response = JsonResponse(backlog_service.static_atributes(attributes), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def global_backlog_query(request, id_case, search_type, base_name, alias, origin_system, observation_field, uuaa_raw, uuaa_master):
    """Find a backlog with some specific conditions

    :param request: HttpRequest
    :param base_name: str, string that represent a table base name
    :param origin_system: str, string that reprensent a table origin system
    :param observation_field: str, string that represent a table observation
    :return:HttpResponse, backlogs that has the specific conditions
    """
    if request.method == "GET":
        tmp = backlog_service.global_backlog_query(id_case, search_type, base_name, alias, origin_system, observation_field, uuaa_raw, uuaa_master)
        try:
            response = JsonResponse(_conver_dd_cursor(tmp), safe=False)
        except:
            response = JsonResponse(tmp, safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_use_case_datadictionaries_to_vobo(request, use_case_id):
    """Request to send a approved or removed backlog

        :param request:HttpRequest, get an action to do
        :param backlog_id: str, string that represent a backlog id
        :return:HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(_conver_dd_cursor(use_case_service.find_use_case_dictionaries_vobo(use_case_id)), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_use_case_data_tables_vobo(request, use_case_id):
    """Request to send a approved or removed backlog

        :param request:HttpRequest, get an action to do
        :param backlog_id: str, string that represent a backlog id
        :return:HttpResponse
    """
    if request.method == "GET":
        try:
            response = JsonResponse(_conver_dd_cursor(use_case_service.find_use_case_data_tables_vobo(use_case_id)), safe=False)
        except Exception as e:
            context = {'status': '500', 'reason': str(e)}
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_backlog_requests(request, project_id, use_case_name, backlog_name, action):
    """ Find backlogs with the parameters

    :param request: HttpRequest, get an action to do
    :param project_id: str, string that represent a project id.
    :param use_case_name: str, string that represent the use case name.
    :param table_name: str, string that represent the table name.
    :param action: str, string that represent the type of the requests. ADD: aggregation, DEL: Remove, UPD:Update
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(requests_service.find_backlogs_requests(project_id, use_case_name, backlog_name, action), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_data_tables_requests(request, project_id, use_case_name, data_name, action):
    """ Find backlogs with the parameters

    :param request: HttpRequest, get an action to do
    :param project_id: str, string that represent a project id.
    :param use_case_name: str, string that represent the use case name.
    :param table_name: str, string that represent the table name.
    :param action: str, string that represent the type of the requests. ADD: aggregation, DEL: Remove, UPD:Update
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(requests_service.find_data_tables_requests(project_id, use_case_name, data_name, action), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def requests_use_cases(request):
    """ Find all the use cases's pending requests.
    :param request: HttpRequest
    :return: HttpResponse, Array with all the pending use cases.
    """
    if request.method == "GET":
        response = JsonResponse(requests_service.find_all_use_cases_approved(), safe=False)
    elif request.method == "PUT":
        try:
            response = JsonResponse(requests_service.create_new_use_case(json.loads(request.body)), safe=False)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    elif request.method == "POST":
        try:
            response = JsonResponse(requests_service.update_use_case(json.loads(request.body)), safe=False)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def requests_backlogs(request):
    """ Find all the backlogs's pending requests.

    :param request: HttpRequest
    :return: HttpResponse, Array with all the pending backlogs.
    """
    if request.method == "GET":
        response = JsonResponse(requests_service.find_all_backlogs_approved(), safe=False)
    elif request.method == "POST":
        try:
            requests_service.update_backlog_use_case(json.loads(request.body))
            response = HttpResponse(status=200)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def tickets_backlogs(request):
    """ Find all the backlogs's pending requests.

    :param request: HttpRequest
    :return: HttpResponse, Array with all the pending backlogs.
    """
    if request.method == "GET":
        response = JsonResponse(requests_service.find_all_tickets_backlogs(),safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def tickets_use_cases(request):
    """ Find all the backlogs's pending requests.

    :param request: HttpRequest
    :return: HttpResponse, Array with all the pending backlogs.
    """
    if request.method == "GET":
        response = JsonResponse(requests_service.find_all_tickets_use_cases(), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def tickets_data_tables(request):
    """ Find all the backlogs's pending requests.

    :param request: HttpRequest
    :return: HttpResponse, Array with all the pending backlogs.
    """
    if request.method == "GET":
        response = JsonResponse(requests_service.find_all_tickets_data_tables(), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def update_request(request, request_id):
    """ Update the state for a request.
    :param request: HttpRequest
    :return: HttpResponse
    """
    if request.method == "POST":
        try:
            requests_service.update_request(request_id, json.loads(request.body))
            response = HttpResponse(status=200)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def update_request_data_table(request, request_id):
    """ Update the state for a request.
    :param request: HttpRequest
    :return: HttpResponse
    """
    try:
        if request.method == "POST":
                requests_service.update_request_data_table(request_id, json.loads(request.body))
                response = HttpResponse(status=200)
        elif request.method == "GET":
                response = JsonResponse(requests_service.find_request_by_id(request_id), safe=True)
    except Exception as e:
        context = {
            'status': '500', 'reason': str(e)
        }
        response = HttpResponse(json.dumps(context), content_type='application/json')
        response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def use_case_actions(request, use_case_id):
    """ Find one use case

    :param request: HttpRequest
    :param use_case_id: str, string
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(_conver_json(use_case_service.find_use_case(use_case_id)), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def backlog_actions(request, backlog_id):
    """ Find one use case

    :param request: HttpRequest
    :param use_case_id: str, string
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(_conver_json(backlog_service.find_one(backlog_id)), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def project_request_options(request):
    """ Find all projects request options.

    :param request: HttpRequest
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(requests_service.find_projects_with_requests(), safe=False)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def project_request_options_data_tables(request):
    """ Find all projects request options.

    :param request: HttpRequest
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(requests_service.find_projects_with_requests_data_tables(), safe=False)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def project_tickets_options(request):
    """ Find all projects request options.

    :param request: HttpRequest
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(requests_service.find_projects_with_tickets(),safe=False)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_use_case_by_query(request, project_id, use_case_name, backlog_name, tablon_name):
    """
    :param request: HttpRequest
    :return: HttpResponse
    """
    if request.method == "GET":
        try:
            response = JsonResponse(
                project_service.find_use_case_backlog_tablon(project_id, use_case_name, backlog_name, tablon_name), safe=False
            )
            response.status_code = 200
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_backlog_name(request, use_case_id, backlog_name, request_state, state):
    """

    :param request: HttpRequest
    :param use_case_id: str
    :param backlog_name: str
    :return: HttpResponse
    """
    if request.method == "GET":
        try:
            response = JsonResponse(
                use_case_service.find_backlog_name_state(use_case_id, backlog_name, request_state, state),safe=False
            )
            response.status_code = 200
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_data_table_name(request, use_case_id, data_table_name, request_state, state):
    """

    :param request: HttpRequest
    :param use_case_id: str
    :param backlog_name: str
    :return: HttpResponse
    """
    if request.method == "GET":
        try:
            response = JsonResponse(
                use_case_service.find_data_tables_name_state(use_case_id, data_table_name, request_state,state), safe=False
            )
            response.status_code = 200
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_backlog_tickets(request, project_id, use_case_name, backlog_name, action, status_req):
    """ Find backlogs with the parameters

    :param request: HttpRequest, get an action to do
    :param project_id: str, string that represent a project id.
    :param use_case_name: str, string that represent the use case name.
    :param table_name: str, string that represent the table name.
    :param action: str, string that represent the type of the requests. ADD: aggregation, DEL: Remove, UPD:Update
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(requests_service.find_backlogs_tickets(project_id, use_case_name, backlog_name, action, status_req), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_data_tables_tickets(request, project_id, use_case_name, data_name, action, status_req):
    """ Find backlogs with the parameters

    :param request: HttpRequest, get an action to do
    :param project_id: str, string that represent a project id.
    :param use_case_name: str, string that represent the use case name.
    :param table_name: str, string that represent the table name.
    :param action: str, string that represent the type of the requests. ADD: aggregation, DEL: Remove, UPD:Update
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(requests_service.find_data_table_tickets(project_id, use_case_name, data_name, action, status_req), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def requests_data_tables(request):
    """ Find all the backlogs's pending requests.

    :param request: HttpRequest
    :return: HttpResponse, Array with all the pending backlogs.
    """
    if request.method == "GET":
        response = JsonResponse(requests_service.find_all_data_tables_approved(), safe=False)
    elif request.method == "POST":
        try:
            response = JsonResponse(requests_service.request_data_table(json.loads(request.body)), safe=False)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response


def find_data_tables(request, master_name, alias, description, uuaa):
    """

    :param request:
    :param master_name:
    :param alias:
    :param description:
    :param uuaa:
    :return:
    """
    if request.method == "GET":
        response = JsonResponse(data_tables_service.find_all_data_tables(master_name, alias, description, uuaa), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def find_data_table_use_case(request, use_case_id, master_name, request_state, state):
    """

    :param request:
    :param use_case_id:
    :param master_name:
    :param request_state:
    :param state:
    :return:
    """
    if request.method == "GET":
        response = JsonResponse(use_case_service.find_data_table(use_case_id, master_name, request_state, state), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def find_team_projects(request, project_id):
    """

    :param request:
    :param project_id:
    :return:
    """
    if request.method == "GET":
        response = JsonResponse(project_service.find_project_teams(project_id), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def update_VoBos(request, project_id):
    """

    :param request:
    :param project_id:
    :return:
    """
    if request.method == "POST":
        try:
            project_service.update_vobo(project_id, json.loads(request.body))
            response = HttpResponse(status=200)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def find_excels_files(request, project_id, use_case_id):
    """

    :param request:
    :param project_id:
    :param use_case_id:
    :return:
    """
    if request.method == "GET":
        try:
            response = JsonResponse(project_service.generate_excels(project_id, use_case_id), safe=False)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def update_table_from_file(request):
    """

    :param request:
    :return:
    """
    if request.method == "POST":
        try:
            backlog_service.upload_data_dictionary(json.loads(request.body))
            response = HttpResponse(status=200)
        except Exception as e:
            print(traceback.format_exc())
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def update_table_from_file_new(request):
    """

    :param request:
    :return:
    """
    if request.method == "POST":
        try:
            response = JsonResponse(backlog_service.upload_data_dictionary_new(json.loads(request.body)), safe=False)
        except Exception as e:
            traceback.print_exc()
            traceback.format_exc()
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def update_data_table_from_file(request):
    """

    :param request:
    :return:
    """
    if request.method == "POST":
        try:
            response = JsonResponse(use_case_service.upload_data_table(json.loads(request.body)), safe=False)
            #response = HttpResponse(status=200)
        except Exception as e:
            traceback.print_exc()
            response = HttpResponse(json.dumps({'status': '500', 'reason': str(e)}), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def get_tables_by_filter(request, table_name, alias, project_id, table_state, logic_name, table_desc, backlog_name, base_type):
    """
    Return a list of tables that have this physical name or alias
    :param request: HttpRequest
    :param table_name: str, that represent the physical name of the table
    :param alias: str, that represent the alias of the table
    :param base_type: str,
    :return: HttpResponse, a list whit tables
    """
    if request.method == "GET":
        response = JsonResponse(project_service.find_table_by_filter(table_name, alias, project_id, table_state, logic_name, table_desc, backlog_name, base_type), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def upload_functional_map(request):
    """Find uuaa's from an id

        :param request: HttpRequest
        :param project_id: str, string that represent a project id
        :return:
        """
    if request.method == "POST":
        try:
            functional_map_service.upload_uuaa(json.loads(request.body))
            response = HttpResponse(status=200)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def remove_functional_map(request):
    """Find uuaa's from an id

        :param request: HttpRequest
        :param project_id: str, string that represent a project id
        :return:
        """
    if request.method == "POST":
        try:
            functional_map_service.remove_uuaa(json.loads(request.body))
            response = HttpResponse(status=200)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def upload_raw_functional_map(request):
    """

    :param request:
    :return:
    """
    if request.method == "POST":
        try:
            raw_functional_map_service.upload_uuaas(json.loads(request.body))
            response = HttpResponse(status=200)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def check_backlog_basename(request, baseName):
    """

    :param request:
    :return:
    """
    if request.method == "GET":
        try:
            response = JsonResponse(backlog_service.check_backlog_basename(baseName), safe=False)
        except Exception as e:
            context = {'status': '500', 'reason': str(e)}
            response = HttpResponse(json.dumps(context), content_type='application/json')
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def update_vobo_project(request, project_id):
    """
        Generates namings excel similar to Sophia export 

    :param request: HttpRequest
    :return: HttpResponse, a excel to the data to export
    """
    if request.method == "POST":
        try:
            resu = project_service._update_vobo_to_project(project_id, json.loads(request.body))
            response = JsonResponse(resu, safe=False)
            response.status_code = 200
        except Exception as e:
            context = {'status': '500', 'reason': str(e)}
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def export_data(request):
    """
        Generates namings excel similar to Sophia export 

    :param request: HttpRequest
    :return: HttpResponse, a excel to the data to export
    """
    if request.method == "POST":
        try:
            resu = project_service.generate_data_export(json.loads(request.body))
            response = JsonResponse(resu, safe=False)
            response.status_code = 200
        except Exception as e:
            context = {'status': '500', 'reason': str(e)}
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def project_update_changes(request, user_id):
    """
        Updates all project data for an admin role
    :param request: HttpRequest
    :return: HttpResponse, a excel to the data to export
    """
    if request.method == "POST":
        try:
            response = JsonResponse(project_service.update_project_data(user_id, json.loads(request.body)), safe=False)
            response.status_code = 200
        except Exception as e:
            response = HttpResponse(json.dumps({'status': '500', 'reason': str(e)}), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def create_project(request, user_id):
    """
        Creates a project from scratch with an use case integrated
    :param request: HttpRequest
    :return: HttpResponse, a excel to the data to export
    """
    if request.method == "POST":
        try:
            response = JsonResponse(project_service.create_project(user_id, json.loads(request.body)), safe=False)
            response.status_code = 200
        except Exception as e:
            response = HttpResponse(json.dumps({'status': '500', 'reason': str(e)}), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response