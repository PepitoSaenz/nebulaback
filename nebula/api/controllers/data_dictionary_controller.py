from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

import json

from nebula.api.services.data_dictionary_service import *
from nebula.api.services.legacy_service import *
import traceback

data_dictionary_service = Data_dictionary_service()
legacy_service = Legacy_service()


def _convert_cursor_to_json(cursor):
    """
        Returns the cursor in a str format to send petition.
        Checks all object id's that exists a changed to str.
    :param cursor: A bson
    :return: array, a strings with the cursor information.
    """
    res = []
    for i in cursor:
        for j in i:
            if type(i[j]) is ObjectId:
                i[j] = str(i[j])
            elif type(i[j]) is list:
                for k in range(len(i[j])):
                    if type(i[j][k]) is ObjectId:
                        i[j][k] = str(i[j][k])
                    if type(i[j][k]) is dict:
                        for p in i[j][k]:
                            if type(i[j][k][p]) is list:
                                for w in range(len(i[j][k][p])):
                                    for t in i[j][k][p][w]:
                                        if type(i[j][k][p][w][t]) is ObjectId:
                                            i[j][k][p][w][t] = str(i[j][k][p][w][t])
                            elif type(i[j][k][p]) is ObjectId:
                                i[j][k][p] = str(i[j][k][p])
        res.append(i)
    return res


def available_to_governance(request, user_id):
    """
        Finds all the tables available for naming proposing.

    :param request: HttpRequest.
    :param user_id: str, a string that represents the user id.
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(
            _convert_cursor_to_json(data_dictionary_service.available_to_governance(user_id)), safe=False
        )
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def table(request):
    """
        Creates a new data dictionary.

    :param request: HttpRequest
    :return: HttpResponse
    """
    if request.method == "POST":
        try:
            response = JsonResponse(
                data_dictionary_service.create_table(json.loads(request.body)),safe=False
            )
        except Exception as e:
            traceback.print_exc()
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def update_old_tables(request, table_id):
    """Updates old tables with the raw info so its easier to calculate, without the fieldsRaw

    :param request: HttpRequest
    :return: HttpResponse
    """
    if request.method == "POST":
        try:
            response = JsonResponse(
                data_dictionary_service.updateOldTables(table_id),safe=False
            )
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def update_field(request, fase, table_id, user_id):
    """
        Updates a raw or master phase for the dictionary.

    :param request: HttpRequest
    :param fase: str,
    :param table_id: str, a string that represents the table id.
    :param user_id: str, a string that represents the user id.
    :return: HttpResponse
    """
    if request.method == "POST":
        try:
            data_dictionary_service.update_fase_field(
                fase, table_id, user_id, json.loads(request.body)
            )
            response = HttpResponse(status=200)
        except Exception as e:
            traceback.print_exc()
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def update_validation(request, table_id):
    """
        Validate the data dictionary for the reviewer.

    :param request: HttpRequest
    :param table_id: str, a string that represents the table id.
    :return: HttpResponse
    """
    if request.method == "POST":
        try:
            data_dictionary_service.update_validation_po(table_id, json.loads(request.body))
            response = HttpResponse(status=200)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def generate_new_fase(request, table_id):
    """
        Creates a raw or master phase for the dictionary.

    :param request: HttpsRequest
    :param table_id: str, a string that represents the table id
    :return: HttpResponse
    """
    if request.method == "PUT":
        try:
            data_dictionary_service.generate_new_fase(table_id, json.loads(request.body))
            response = HttpResponse(status=200)
        except Exception as e:
            traceback.print_exc()
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
        
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def find_tables(request, state_id, state):
    """
        Returns all the tables for a sprint given.

    :param request: HttpRequest
    :param state_id: str, a string that represents the sprint id.
    :param state: str.
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(
            _convert_cursor_to_json(data_dictionary_service.find_tables(state_id, state)), safe=False
        )
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

def find_tables_state(request, state):
    """
        Returns all the tables given a state

    :param request: HttpRequest
    :param state_id: str, a string that represents the sprint id.
    :param state: str.
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(
            _convert_cursor_to_json(data_dictionary_service.find_tables_state_only(state)), safe=False
        )
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def field(request, table_id):
    """
        Does an action from a specific field:
        blocker, un-blocker, insert and delete.

    :param request: HttpRequest
    :param table_id: str, a string that represents the table id
    :return: HttpResponse
    """
    if request.method == "PUT":
        try:
            data_dictionary_service.action_field(table_id, json.loads(request.body))
            response = HttpResponse(status=200)
        except Exception as e :
            traceback.print_exc()
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def change_state(request, table_id):
    """
        Changes a table state to pass a other phase.
        Example: it can change to governance to architecture or architecture to governance.

        :param request: HttpRequest
        :param table_id: str, a string that represents the table id.
        :return: HttpResponse
        """
    if request.method == "PUT":
        try:
            response = HttpResponse(status=200)
            data_dictionary_service.send_to(table_id, json.loads(request.body))
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    elif request.method == "POST":
        if request.body:
            data_dictionary_service.free_change_state(table_id, json.loads(request.body))
            response = HttpResponse(status=200)
        else: 
            response = HttpResponse(status=500)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def insert_legacy(request, table_id):
    """
        Adds legacy name, description, type and length to a tables fields by excel structure.

    :param request: HttpRequest
    :param table_id: str, a string that represents the table id
    :return: HttpResponse
    """
    if request.method == "PUT":
        try:
            message = data_dictionary_service.update_legacy_fields(table_id, json.loads(request.body))
            if (message == ""):
                response = HttpResponse(status=200)
            else: 
                context = {
                    'status': '206', 'reason': str(message)
                }
                response = HttpResponse(json.dumps(context), content_type='application/json')
                response.status_code = 206
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def update_legacy(request, table_id):
    """
        Adds legacy name, description, type and length to a tables fields
        by the user with functional profile.

    :param request: HttpRequest
    :param table_id: str, a string that represents the table id
    :return: HttpResponse
    """
    if request.method == "PUT":
        try:
            data_dictionary_service.upload_legacy_functional(table_id, json.loads(request.body))
            response = HttpResponse(status=200)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def update_load_all_fields(request, table_id):
    """
        Adds legacy name, description, type and length to a tables fields
        by the user with functional profile.

    :param request: HttpRequest
    :param table_id: str, a string that represents the table id
    :return: HttpResponse
    """
    if request.method == "PUT":
        try:
            data_dictionary_service.update_load_all_fields(table_id, json.loads(request.body))
            response = HttpResponse(status=200)
        except Exception as e:
            traceback.print_exc()
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

def legacy(request, legacy):
    """
        Finds a field from their legacy information.

    :param request: HttpRequest
    :param legacy: str, a string that represent the legacy
    :return: HttpResponse with possible legacy fields
    """
    if request.method == "GET":
        response = JsonResponse(legacy_service.legacy_query(legacy), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_head(request, table_id, state):
    """
        Updates or finds a table object.

    :param request: HttpRequest
    :param table_id: str, a string that represent the table id
    :param state: str, a string that represent a fase to find or add/update
    :return: HttpResponse
    """
    try:
        if request.method == "GET":
            response = JsonResponse(
                data_dictionary_service.find_head(table_id, state), safe=False
            )
        elif request.method == "PUT":
            data_dictionary_service.generate_body(table_id, state, json.loads(request.body))
            response = HttpResponse(status=200)
        else:
            response = HttpResponse(status=500)
    except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500

    response["Access-Control-Allow-Origin"] = "*"
    return response


def comment_return_reasons(request):
    """
        Finds all reason to classify a field state

    :param request: HttpRequest
    :return: HttpResponse, with all classification
    """
    if request.method == "GET":
        response = JsonResponse(_convert_cursor_to_json(data_dictionary_service.comment_governance_return()),
                                safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def find_field(request, table_id, field):
    """
        Finds a specific phase to a table.

    :param request: HttpRequest
    :param table_id: str, string that represents table id
    :param field: str, string that represents a fase
    :return: HttpResponse
    """
    if request.method == "GET":
        try:
            response = JsonResponse(_convert_cursor_to_json(data_dictionary_service.find_field(table_id, field)),
                safe=False)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def find_blocker_fields(request, table_id, field):
    """
        Finds all blocker field of a phase table.

    :param request: HttpRequest
    :param table_id: str, string that represents a table id
    :param field: str, string that represents a phase
    :return: HttpResponse, a blocker fields
    """
    if request.method == "GET":
        response = JsonResponse(data_dictionary_service.find_blocker_field(table_id, field), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


def find_in_table(request, table_id, fase, naming, logic, desc, state, legacy, descLegacy, suffix, comment_state):
    """
        Finds fields by the params information.

    :param request: HttpRequest
    :param table_id: str, string that represent a table id
    :param fase: str, string that represent a fase to check
    :param naming: str, string that represent a naming to find
    :param logic: str, string that represent a logic name to find
    :param desc: str, string that represent a description field to find
    :return: HttpResponse, a fields with meet the conditions
    """
    if request.method == "GET":
        response = JsonResponse(_convert_cursor_to_json(
            data_dictionary_service.find_in_table(table_id, fase, naming, logic, desc,
                                                  state, legacy, descLegacy, suffix, comment_state)), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def update_object_head(request, table_id):
    """
        Updates all data dictionary object information.

    :param request: HttpRequest
    :param table_id: str, string that represent a table id
    :return: HttpResponse
    """
    if request.method == "PUT":
        try:
            data_dictionary_service.update_head(table_id, json.loads(request.body))
            response = HttpResponse(status=200)
        except Exception as e:
            response = HttpResponse(json.dumps({'status': '500', 'reason': str(e)}), content_type='application/json')
            response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def update_object_functional(request, table_id):
    """
        Updates data dictionary object only with the functional information.

    :param request: HttpRequest
    :param table_id: str, string that represent a table id
    :return: HttpResponse
    """
    if request.method == "PUT":
        try:
            data_dictionary_service.update_head_functional(table_id, json.loads(request.body))
            response = HttpResponse(status=200)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response


def find_uuaa_data_dictionary(request, table_id):
    """
        Finds all uuaas options for a data dictionary.

    :param request: HttpRequest
    :param table_id: str, string that represent a table id
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(data_dictionary_service.find_uuaa_data_dictionary(table_id), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

def find_no_ok_fields(request, table_id, fase):
    """
        Finds all the fields which are pending for approving

    :param request: HttpRequest
    :param table_id: str.
    :param fase: str.
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(data_dictionary_service.find_no_ok_fields(table_id, fase), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def generate_excels(request, table_id, fase):
    """
        Generates excels to a upload to Sophia.

    :param request: HttpRequest
    :param table_id: str, string that represents a table id
    :param fase: str, string that represent a fase to generate
    :return: HttpResponse, a excel to the data to import
    """
    if request.method == "GET":
        try:
            resu = data_dictionary_service.generate_excel(table_id, fase)
            response = JsonResponse(resu, safe=False)
            response.status_code = 200
        except Exception as e:
            traceback.print_exc()
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def create_table(request):
    """
        Creates a new data dictionary with the functional information.

    :param request: HttpRequest
    :return: HttpResponse
    """
    if request.method == "PUT":
        try:
            response = JsonResponse(
                data_dictionary_service.create_table_functional(json.loads(request.body)),safe=False
            )
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def fields_functional(request, table_id):
    """
        Finds/updatesthe fields for a phase to the data dictionary.

    :param request: HttpRequest
    :param table_id: str, string that represents table id
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(_convert_cursor_to_json(data_dictionary_service.find_fields_functional(table_id)),
                                safe=False)
    elif request.method == "POST":
        try:
            data_dictionary_service.update_fields_functional(table_id, json.loads(request.body))
            response = HttpResponse(status=200)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def get_uuaas_raw(request, uuaa_name, app_name, level1, level2):
    """
        Finds and retrieves raw uuaas based on some factors.

    :param request: HttpRequest
    :param uuaa_name: str, string that represent a table id
    :param app_name: str, string that represent a fase to check
    :param origin_name: str, string that represent a naming to find
    :return: HttpResponse, a fields with meet the conditions
    """
    if request.method == "GET":
        response = JsonResponse(_convert_cursor_to_json(
            data_dictionary_service.find_uuaas_raw(uuaa_name, app_name, level1, level2)), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def get_all_uuaas_raw(request):
    """
        Finds and retrieves raw uuaas based on some factors.

    :param request: HttpRequest
    :param uuaa_name: str, string that represent a table id
    :param app_name: str, string that represent a fase to check
    :param origin_name: str, string that represent a naming to find
    :return: HttpResponse, a fields with meet the conditions
    """
    if request.method == "GET":
        response = JsonResponse(_convert_cursor_to_json(
            data_dictionary_service.find_uuaas_raw("", "", "", "")), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def get_uuaas_master(request, uuaa_name, scope_name, level2, level3):
    """
        Finds and retrieves raw uuaas based on some factors.

    :param request: HttpRequest
    :param uuaa_name: str, string that represent a table id
    :param app_name: str, string that represent a fase to check
    :param origin_name: str, string that represent a naming to find
    :return: HttpResponse, a fields with meet the conditions
    """
    if request.method == "GET":
        response = JsonResponse(_convert_cursor_to_json(
            data_dictionary_service.find_uuaas_master(uuaa_name, scope_name, level2, level3)), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def get_raw_app_options(request):
    """
        Finds and retrieves raw uuaas based on some factors.

    :param request: HttpRequest
    :param uuaa_name: name of the property in the DB
    :return: HttpResponse, a fields with meet the conditions
    """
    if request.method == "GET":
        try:
            response = HttpResponse(json.dumps(
                data_dictionary_service.find_raw_property_options("app")), content_type='application/json')
            response.status_code = 200
        except Exception as e:
            context = {'status': '500', 'reason': str(e)}
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def get_raw_origin_options(request):
    """
        Finds and retrieves raw uuaas based on some factors.

    :param request: HttpRequest
    :param uuaa_name: name of the property in the DB
    :return: HttpResponse, a fields with meet the conditions
    """
    if request.method == "GET":
        try:
            response = HttpResponse(json.dumps(
                data_dictionary_service.find_raw_property_options("originSystem")), content_type='application/json')
            response.status_code = 200
        except Exception as e:
            context = {'status': '500', 'reason': str(e)}
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def get_master_level_options(request):
    """
        Finds and retrieves raw uuaas based on some factors.

    :param request: HttpRequest
    :param uuaa_name: name of the property in the DB
    :return: HttpResponse, a fields with meet the conditions
    """
    if request.method == "GET":
        try:
            response = HttpResponse(json.dumps(
                data_dictionary_service.find_master_level_options()), content_type='application/json')
            response.status_code = 200
        except Exception as e:
            context = {'status': '500', 'reason': str(e)}
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def update_uuaa_raw(request, uuaa_id):
    """
        Updates all data dictionary object information.

    :param request: HttpRequest
    :param uuaa_id: str, string that represent a uuaa id
    :return: HttpResponse
    """
    if request.method == "PUT":
        try:
            response = HttpResponse(json.dumps(
                data_dictionary_service.update_uuaa_raw(uuaa_id, json.loads(request.body))), content_type='application/json')
            response.status_code = 200
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def new_uuaa_raw(request):
    """
        Updates all data dictionary object information.

    :param request: HttpRequest
    :param uuaa_id: str, string that represent a uuaa id
    :return: HttpResponse
    """
    if request.method == "POST":
        try:
            response = HttpResponse(json.dumps(
                data_dictionary_service.new_uuaa_raw(json.loads(request.body))), content_type='application/json')
            response.status_code = 200
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def update_uuaa_master(request, uuaa_id):
    """
        Updates all data dictionary object information.

    :param request: HttpRequest
    :param uuaa_id: str, string that represent a uuaa id
    :return: HttpResponse
    """
    if request.method == "PUT":
        try:
            response = HttpResponse(json.dumps(
                data_dictionary_service.update_uuaa_master(uuaa_id, json.loads(request.body))), content_type='application/json')
            response.status_code = 200
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def new_uuaa_master(request):
    """
        Updates all data dictionary object information.

    :param request: HttpRequest
    :param uuaa_id: str, string that represent a uuaa id
    :return: HttpResponse
    """
    if request.method == "POST":
        try:
            response = HttpResponse(json.dumps(
                data_dictionary_service.new_uuaa_master(json.loads(request.body))), content_type='application/json')
            response.status_code = 200
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def get_raw_uuaa_dd_options(request):
    """
        Finds and retrieves raw uuaas based on some factors.

    :param request: HttpRequest
    :param uuaa_name: name of the property in the DB
    :return: HttpResponse, a fields with meet the conditions
    """
    if request.method == "GET":
        try:
            response = HttpResponse(json.dumps(
                data_dictionary_service.find_dd_property_options("uuaaRaw")), content_type='application/json')
            response.status_code = 200
        except Exception as e:
            context = {'status': '500', 'reason': str(e)}
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def get_master_uuaa_dd_options(request):
    """
        Finds and retrieves raw uuaas based on some factors.

    :param request: HttpRequest
    :param uuaa_name: name of the property in the DB
    :return: HttpResponse, a fields with meet the conditions
    """
    if request.method == "GET":
        try:
            response = HttpResponse(json.dumps(
                data_dictionary_service.find_dd_property_options("uuaaMaster")), content_type='application/json')
            response.status_code = 200
        except Exception as e:
            context = {'status': '500', 'reason': str(e)}
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def get_property_options(request, property_name):
    """Gets the distinct values for a given property name inside the Data Dictionary collection.

    :param request: HttpRequest
    :param property_name: name of the property in the DB
    :return: HttpResponse, json array with the distinct values
    """
    if request.method == "GET":
        try:
            response = HttpResponse(json.dumps(
                data_dictionary_service.find_dd_property_options("uuaaMaster")), content_type='application/json')
            response.status_code = 200
        except Exception as e:
            response = HttpResponse(json.dumps({'status': '500', 'reason': str(e)}), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def delete_raw(request, table_id):
    """Modifies only master data, including removing the master phase and the master fields

    :param request: HttpRequest
    :param table_id: str, string that represents a table id
    :return: HttpResponse, a excel to the data to import
    """
    if request.method == "POST":
        try:
            response = JsonResponse(data_dictionary_service.delete_raw(table_id, request.body), safe=False)
        except Exception as e:
            context = {'status': '500', 'reason': str(e)}
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def update_master(request, table_id):
    """Modifies only master data, including removing the master phase and the master fields

    :param request: HttpRequest
    :param table_id: str, string that represents a table id
    :return: HttpResponse, a excel to the data to import
    """
    if request.method == "POST":
        try:
            response = JsonResponse(data_dictionary_service.update_master(table_id, request.body), safe=False)
        except Exception as e:
            context = {'status': '500', 'reason': str(e)}
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def get_uuaa_raw(request, uuaa_id):
    """
        Updates all data dictionary object information.

    :param request: HttpRequest
    :param uuaa_id: str, string that represent a uuaa id
    :return: HttpResponse
    """
    if request.method == "GET":
        try:
            response = HttpResponse(json.dumps(
                data_dictionary_service.get_uuaa(uuaa_id)), content_type='application/json')
            response.status_code = 200
        except Exception as e:
            context = { 'status': '500', 'reason': str(e) }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def get_clean_null_data_dictionary(request, table_id):
    """
        Updates all data dictionary object information.

    :param request: HttpRequest
    :param uuaa_id: str, string that represent a uuaa id
    :return: HttpResponse
    """
    if request.method == "GET":
        try:
            response = HttpResponse(json.dumps(
                data_dictionary_service.clean_null_values_data_dictionary(table_id)), content_type='application/json')
            response.status_code = 200
        except Exception as e:
            context = { 'status': '500', 'reason': str(e) }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response

