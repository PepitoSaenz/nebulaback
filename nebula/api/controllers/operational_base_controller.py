from django.http import HttpResponse, JsonResponse
from nebula.api.services.operational_base_service import *
from django.views.decorators.csrf import csrf_exempt
import json
import traceback

operational_base_service = Operational_base_service()


@csrf_exempt
def create_operational_base(request):
    """Request to post a new table.

    :param request: HttpRequest
    :return: HttpResponse
    """
    if request.method == "POST":
        try:
            response = JsonResponse(
                operational_base_service.create_data_table(json.loads(request.body)), safe=False
            )
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def operational_object(request, operational_id):
    """

    :param request:
    :param operational_id:
    :return:
    """
    if request.method == "GET":
        response = JsonResponse(operational_base_service.find_object(operational_id), safe=False)
    elif request.method == "PUT":
        try:
            print(1)
            response = JsonResponse(operational_base_service.update_object(operational_id, json.loads(request.body)), safe=False)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def operational_fields(request, operational_id):
    """

    :param request:
    :param operational_id:
    :return:
    """
    if request.method == "GET":
        response = JsonResponse(operational_base_service.find_fields(operational_id), safe=False)
    elif request.method == "PUT":
        try:
            response = JsonResponse(operational_base_service.update_fields(operational_id, json.loads(request.body)), safe=False)
        except Exception as e:
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def field_action(request, operational_id, action):
    """

    :param request:
    :param operational_id:
    :return:
    """
    try:
        if request.method == "PUT":
            operational_base_service.update_field_action(operational_id, json.loads(request.body), action)
            response = HttpResponse(status=200)
        else:
            response = HttpResponse(status=500)
    except Exception as e:
        context = {
            'status': '500', 'reason': str(e)
        }
        response = HttpResponse(json.dumps(context), content_type='application/json')
        response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_operational_bases(request, logic, desc, name, uuaa):
    """

    :param request:
    :param logic:
    :param desc:
    :param name:
    :param uuaa:
    :return:
    """
    try:
        if request.method == "GET":
            response = JsonResponse(operational_base_service.find_by_filter(logic, desc, name, uuaa), safe=False)
        else:
            response = HttpResponse(status=500)
    except Exception as e:
        context = {
            'status': '500', 'reason': str(e)
        }
        response = HttpResponse(json.dumps(context), content_type='application/json')
        response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def generate_excels(request, operational_id):
    """

    :param request:
    :param operational_id:
    :return:
    """
    try:
        if request.method == "GET":
            response = JsonResponse(operational_base_service.generate_excels(operational_id), safe=False)
        else:
            response = HttpResponse(status=500)
    except Exception as e:
        traceback.print_exc()
        context = {
            'status': '500', 'reason': str(e)
        }
        response = HttpResponse(json.dumps(context), content_type='application/json')
        response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def update_operational_base(request, operational_id):
    """

    :param request:
    :param operational_id:
    :return:
    """
    try:
        if request.method == "PUT":
            operational_base_service.update_structute(operational_id, json.loads(request.body))
            response = HttpResponse(status=200)
        else:
            response = HttpResponse(status=500)
    except Exception as e:
        context = {
            'status': '500', 'reason': str(e)
        }
        response = HttpResponse(json.dumps(context), content_type='application/json')
        response.status_code = 500
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def create_operational_base_file(request):
    """

    :param request:
    :return:
    """
    if request.method == "POST":
        try:
            operational_base_service.upload_operational_base(json.loads(request.body))
            response = HttpResponse(status=200)
        except Exception as e:
            traceback.print_exc()
            context = {
                'status': '500', 'reason': str(e)
            }
            response = HttpResponse(json.dumps(context), content_type='application/json')
            response.status_code = 500
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_by_state(request, state, project_id):
    """

    :param request:
    :param state:
    :return:
    """
    if request.method == "GET":
        response = JsonResponse(operational_base_service.find_by_state(state, project_id), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def update_validation_vobo(request, operational_id):
    """

    :param request:
    :param operational_id:
    :return:
    """
    if request.method == "PUT":
        response = JsonResponse(operational_base_service.update_validation_vobo(operational_id, json.loads(request.body)), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response

@csrf_exempt
def load_settings(request):
    """

    :param request:
    :param operational_id:
    :return:
    """
    if request.method == "GET":
        response = JsonResponse(operational_base_service.get_load_settings(), safe=False)
    elif request.method == "POST":
        response = JsonResponse(operational_base_service.update_load_settings(json.loads(request.body)), safe=False)
    elif request.method == "PUT":
        response = JsonResponse(operational_base_service.default_load_settings(json.loads(request.body)), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response
