from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from nebula.api.services.acl_validator_service import Acl_service


acl_service = Acl_service()


@csrf_exempt
def find_all_alhambra(request, env):
    """
        Finds all ACLs information in the live or work environment.

    :param request: HttpRequest.
    :param env: str,
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(acl_service.find_all_alhambra(env), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_all_groups(request, env):
    """
        Finds all the groups for ACLs in the live or work environment.

    :param request: HttpRequest.
    :param env: str
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(acl_service.find_all_groups(env), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_routes_by_group(request, env, group):
    """
        Finds the route for a group in the environment work or live.

    :param request: HttpRequest.
    :param env: str
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(acl_service.find_routes_by_group(env, group), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_all_relationship_between_uuaas(request, env):
    """
        Finds all the relationship between uuaas raw-master in the environment work or live.

    :param request: HttpRequest.
    :param env: str
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(acl_service.find_all_relationship(env), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def find_relationship_uuaa(request, env, uuaa):
    """
        Finds the relationship between the uuaa master and the uuaas raw.

    :param request: HttpRequest
    :param env: str.
    :param uuaa: str.
    :return: HttpResponse
    """
    if request.method == "GET":
        response = JsonResponse(acl_service.find_uuaa_relationship(env, uuaa), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def folders_validation(request):
    """
        Validates if the folders for an ACL excel exists.

    :param request: HttpRequest
    :return: HttpResponse
    """
    if request.method == "POST":
        response = JsonResponse(acl_service.folder_validation(json.loads(request.body)), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def acl_validation(request, env):
    """
        Validates an ACL excel.

    :param request: HttpRequest
    :param env: str. Expects work or live.
    :return: HttpResponse
    """
    if request.method == "POST":
        response = JsonResponse(acl_service.acl_validation(env, json.loads(request.body)), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response


@csrf_exempt
def accounts_validation(request, env):
    """
         Validates if the groups for an ACL excel exists.

    :param request: HttpRequest
    :param env: str.
    :return: HttpResponse
    """
    if request.method == "POST":
        response = JsonResponse(acl_service.acl_accounts_validation(env, json.loads(request.body)), safe=False)
    else:
        response = HttpResponse(status=500)
    response["Access-Control-Allow-Origin"] = "*"
    return response
