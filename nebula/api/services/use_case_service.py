from nebula.api.entities.use_case import Use_case

from nebula.api.persistence.use_case_repository import Use_case_repository
from nebula.api.persistence.backlog_repository import Backlog_repository
from nebula.api.persistence.data_table_repository import Data_table_repository
from nebula.api.persistence.requests_repository import Requests_repository
from nebula.api.persistence.user_repository import User_repository
from nebula.api.persistence.project_repository import Project_repository
from nebula.api.persistence.data_dictionary_repository import Data_dictionary_repository

from nebula.api.services.data_table_service import Data_table_service
from nebula.api.services.data_dictionary_service import Data_dictionary_service

import datetime
from bson import ObjectId


class Use_case_service(object):

    def __init__(self):

        self.use_case_repository = Use_case_repository()
        self.backlog_repository = Backlog_repository()
        self.data_dictionary_repository = Data_dictionary_repository()
        self.request_repository = Requests_repository()
        self.data_table_repository = Data_table_repository()
        self.user_repository = User_repository()
        self.project_repository = Project_repository()
        self.values = {"G": "Gobierno", "A": "Arquitectura", "N": "En propuesta namings", "I": "Para ingestar",
                       "IN": "Ingestada en desarrollo", "Q": "Calidad", "M": "Mallas", "P": "Produccion",
                       "ungoverned": "En backlog", "PA": "Pendiente aprobación backlog", "RN": "Revisión Negocio",
                       "R": "Revisión solicitud", "D": "Descartado", "F": "Gobierno funcional"}
        self.data_dictionary_service = Data_dictionary_service()
        self.data_table_service = Data_table_service()

    def add_one(self, use_case):
        """Insert an use case

        :param use_case: json, dictionary that represent a data to insert
        :return: str, string that represent a object id asignate to the new use case insert
        """
        use_case_str = Use_case({}).data
        use_case_str["name"] = use_case["name"].strip().upper()
        use_case_str["description"] = use_case["description"]
        tmp = [int(item) for item in use_case["startDate"].split('-')]
        tmp2 = [int(item) for item in use_case["finishDate"].split('-')]
        use_case["startDate"] = datetime.datetime(tmp[0], tmp[1], tmp[2])
        use_case["finishDate"] = datetime.datetime(tmp2[0], tmp2[1], tmp2[2])
        ids = self.use_case_repository.insert_one(use_case)
        use_case["_id"] = str(ids)
        return use_case

    def update_tables(self, use_case_id, tables, user_id):
        """Insert a new backlog create with an use case

        :param use_case_id:
        :param tables:
        :param user_id:
        :return:
        """
        user_id = self.user_repository.find_one(user_id)
        if user_id is None:
            raise ValueError("Usuario no autorizado para realizar cambios.")
        user_id = user_id["_id"]

        use_case_bd = self.use_case_repository.find_one(use_case_id)
        if use_case_bd is None:
            raise ValueError("Caso de uso no registrado.")

        temp = [{"user": ObjectId(str(user_id)),
                 "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]

        for i in use_case_bd["modifications"]:
            temp.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]})
        self.use_case_repository.update_tables(use_case_id, tables)
        self.use_case_repository.update_modifications(use_case_id, temp[:20])

    def update_tablones(self, use_case_id, tables, user_id):
        """Insert a new backlog create with an use case

        :param use_case_id:
        :param tables:
        :param user_id:
        :return:
        """
        user_id = self.user_repository.find_one(user_id)
        if user_id is None:
            raise ValueError("Usuario no autorizado para realizar cambios.")
        user_id = user_id["_id"]

        use_case_bd = self.use_case_repository.find_one(use_case_id)
        if use_case_bd is None:
            raise ValueError("Caso de uso no registrado.")

        temp = [{"user": ObjectId(str(user_id)),
                 "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]

        for i in use_case_bd["modifications"]:
            temp.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]})
        self.use_case_repository.update_tablones(use_case_id, tables)
        self.use_case_repository.update_modifications(use_case_id, temp[:20])

    def find_use_case_backlogs(self, use_case_id):
        """Get all table backlogs associate to an use case

        :param use_case_id: str, string that represent an use case id
        :return: array, contains backlogs id's
        """
        result = []
        for j in self.use_case_repository.find_one(use_case_id)["tables"]:
            tmp_backlog = self.backlog_repository.find_one(str(j))
            try:
                tmp_backlog["_id"] = str(tmp_backlog["_id"])
                tmp_backlog["idTable"] = str(tmp_backlog["idTable"])
                tmp_backlog["productOwner"] = str(tmp_backlog["productOwner"])
                tmp_backlog["stateTable"] = ""
                tmp_backlog["viewStateTable"] = ""
                if len(tmp_backlog["idTable"]) > 0:
                    tmp_backlog["stateTable"] = self.data_dictionary_repository.find_one(tmp_backlog["idTable"])["stateTable"]
                    tmp_backlog["viewStateTable"] = self.values[tmp_backlog["stateTable"]]
                
                try:
                    tmp_backlog["modifications"] = {
                        "user_name": self.user_repository.find_one(str(tmp_backlog["modifications"][0]["user"]))["name"],
                        "date": tmp_backlog["modifications"][0]["date"]}
                except Exception as e:
                    tmp_backlog["modifications"] = {"user_name": "N/A", "date": "N/A"}

                requests = self.request_repository.find_any_query({"$and": [{"target": "B"}, {"action": "Add"},
                    {"idUsecase": ObjectId(str(use_case_id))}, {"idBacklog": ObjectId(str(j))}]})
                try:
                    tmp_backlog["request_date"] = requests[0]["created_time"].strftime("%Y-%m-%d")
                    tmp_backlog["request_date"] = tmp_backlog["request_date"]
                except Exception:
                    tmp_backlog["request_date"] = ''

                result.append(tmp_backlog)
            except Exception as e:
                print(e)
                print("BACKLOG: " + str(j))

        return result

    def find_use_case_data_tables(self, use_case_id):
        """Get all tables associate to an use case

        :param use_case_id: str, string that represent an use case id
        :return: array, contains backlogs id's
        """
        result = []
        tmp = self.use_case_repository.find_one(use_case_id)
        if tmp is not None:
            if len(tmp["modifications"]) != 0:
                tmp["modifications"] = {
                    "user_name": self.user_repository.find_one(str(tmp["modifications"][0]["user"]))["name"],
                    "date": tmp["modifications"][0]["date"]}
            else:
                tmp["modifications"] = {"user_name": "N/A", "date": "N/A"}

            for j in tmp["tablones"]:
                tmp_backlog = self.data_table_service.find_head(str(j))
                tmp_backlog["originSystem"] = "HDFS-Parquet"
                tmp_backlog["viewStateTable"] = self.values[tmp_backlog["stateTable"]]

                requests = self.request_repository.find_any_query({"$and": [{"target": "T"}, {"action": "Add"},
                        {"idUsecase": ObjectId(str(use_case_id))}, {"idBacklog": ObjectId(str(j))}]})
                try:
                    tmp_backlog["tableRequest"] = requests[0]["stateRequest"]
                    tmp_backlog["request_date"] = requests[0]["created_time"].strftime("%Y-%m-%d")
                except Exception as e:
                    tmp_backlog["request_date"] = ''
                result.append(tmp_backlog)

        return result

    def find_use_case(self, use_case_id):
        """Get a specific use case

        :param use_case_id: str, string that represent an use case id
        :return: json, dictionary with use case data
        """
        tmp_use_case = self.use_case_repository.find_one(use_case_id)
        tmp_use_case["_id"] = str(tmp_use_case["_id"])
        if tmp_use_case["startDate"] != "":
            tmp_use_case["startDate"] = tmp_use_case["startDate"].strftime("%Y-%m-%d")
        if tmp_use_case["finishDate"] != "":
            tmp_use_case["finishDate"] = tmp_use_case["finishDate"].strftime("%Y-%m-%d")
        if len(tmp_use_case["modifications"]) != 0:
            tmp_use_case["modifications"] = {
                "user_name": self.user_repository.find_one(str(tmp_use_case["modifications"][0]["user"]))["name"],
                "date": tmp_use_case["modifications"][0]["date"]}
        else:
            tmp_use_case["modifications"] = {"user_name": "N/A", "date": "N/A"}
        return tmp_use_case

    def remove_table(self, table_id, user_id):
        """Remove all reference from one table to all use case

        :param table_id: str, string that represent a table id
        :param user_id: str,
        :return:
        """
        self.use_case_repository.remove_all_tables(table_id, user_id)

    def contains_backlog(self, use_case_id, backlog_id):
        """Check if an use case contains or not a table backlog

        :param use_case_id: str, string that represent an use case id
        :param backlog_id: str, string that represent an backlog id
        :return: boolean, if an use case contains or not a backlog
        """
        return self.use_case_repository.contains_backlog(use_case_id, backlog_id)

    def update_one(self, use_case_id, use_case, user_id):
        """Update use case data

        :param use_case_id: str, string that represent a use case id
        :param use_case: json, dictionary with new data to update an use case
        :param user_id: str,
        :return:
        """
        user_id = self.user_repository.find_one(user_id)
        if user_id is None:
            raise ValueError("Usuario no autorizado para realizar cambios.")
        user_id = user_id["_id"]

        use_case_db = self.use_case_repository.find_one(use_case_id)
        if use_case_db is None:
            raise ValueError("Caso de uso no registrado.")

        temp = [{"user": ObjectId(str(user_id["_id"])),
                 "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]
        for i in use_case_db["modifications"]:
            temp.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]
            })

        if use_case["action"] == "head":
            self.use_case_repository.update_head(use_case_id, use_case)
        elif use_case["action"] == "tables":
            self.use_case_repository.update_tables(use_case_id, use_case)

        self.use_case_repository.update_modifications(use_case_id, temp[:20])

    def update_state(self, use_case_id, state, user_id):
        """

        :param use_case_id:
        :param state:
        :param user_id:
        :return:
        """
        user_id = self.user_repository.find_one(user_id)
        if user_id is None:
            raise ValueError("Usuario no autorizado para realizar cambios.")
        user_id = user_id["_id"]

        use_case_bd = self.use_case_repository.find_one(use_case_id)
        if use_case_bd is None:
            raise ValueError("Caso de uso no registrado.")

        temp = [{"user": ObjectId(str(user_id)),
                 "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]

        for i in use_case_bd["modifications"]:
            temp.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]
            })
        self.use_case_repository.update_state_request(use_case_id, state)
        self.use_case_repository.update_modifications(use_case_id, temp[:20])

    def find_use_case_dictionaries_vobo(self, use_case_id):
        """

        :param use_case_id: str,
        :return:
        """
        data_dictionaries = []
        try:
            tables = self.use_case_repository.find_one(use_case_id)["tables"]
            if tables is not None:
                for i in tables:
                    temp_dd = self.data_dictionary_repository.find_to_check(str(i), "RN")
                    if temp_dd is not None:
                        head = self.data_dictionary_service.find_head(str(temp_dd["_id"]), "FieldReview")
                        head["fieldsRaw"] = temp_dd["fieldsRaw"] if len(temp_dd["fieldsRaw"]) > 0 else temp_dd["fieldsMaster"]  #Para modificar a donde apunta en el caso de las tablas que no tengan Raw
                        head["baseName"] = temp_dd["baseName"]
                        data_dictionaries.append(head)

            return data_dictionaries
        except Exception as e:
            print(e, "--------> VoBo")
            raise ValueError(e)

    def find_use_case_data_tables_vobo(self, use_case_id):
        """

        :param use_case_id: str,
        :return:
        """
        data_tables = []
        try:
            tables = self.use_case_repository.find_one(use_case_id)["tablones"]
            if tables is not None:
                for i in tables:
                    head = self.data_table_service.find_head(str(i))
                    if head is not None and head["stateTable"] == "RN":
                        head["_id"] = str(i)
                        head["fields"] = self.data_table_service.find_fields(str(i))
                        data_tables.append(head)
            return data_tables
        except Exception as e:
            print(e, "--------> DT VoBo")
            raise ValueError(e)

    def find_by_query(self, use_case_name, backlog_name):
        """

        :param use_case_name: str,
        :param backlog_name: str
        :return:
        """
        use_cases = self.use_case_repository.find_all()
        query = []
        for i in use_cases:
            if use_case_name == "" or use_case_name in i["name"]:
                tables = []
                for j in i["tables"]:
                    tmp_backlog = self.backlog_repository.find_one(j)
                    if backlog_name == "" or backlog_name in tmp_backlog["baseName"]:
                        query.append(tmp_backlog)
                use_cases[i]["tables"] = tables
                query.append(i)
        return query

    def find_backlog_name_state(self, use_case_id, backlog_name, request_state, state):
        """

        :param use_case_id: str
        :param backlog_name: str
        :param request_state: str
        :param state: str
        :return:
        """
        backlog_name = backlog_name.strip()
        state = state.strip()
        use_case = self.use_case_repository.find_one(use_case_id.strip())
        tables = []
        

        for j in use_case["tables"]:
            tmp_backlog = self.backlog_repository.find_one(str(j))
            tmp_backlog["_id"]=str(tmp_backlog["_id"])
            tmp_backlog["idTable"] = str(tmp_backlog["idTable"])
            tmp_backlog["stateTable"] = ""
            tmp_backlog["viewStateTable"] = ""
            if len(tmp_backlog["idTable"]) > 0:
                tmp_backlog["stateTable"] = self.data_dictionary_repository.find_one(tmp_backlog["idTable"])[
                    "stateTable"]
                tmp_backlog["viewStateTable"] = self.values[tmp_backlog["stateTable"]]
            tmp_backlog["productOwner"] = str(tmp_backlog["productOwner"])
            requests = self.request_repository.find_any_query({"$and": [{"target": "B"}, {"action": "Add"},
                                                                        {"idUsecase": ObjectId(str(use_case_id))},
                                                                        {"idBacklog": ObjectId(str(j))}]})
            if requests.count() == 0:
                tmp_backlog["request_date"] = ''
            else:
                tmp_backlog["request_date"] = requests[0]["created_time"].strftime("%Y-%m-%d")
            tmp_mod_arr = []
            if tmp_backlog["modifications"] != "":
                for mod in tmp_backlog["modifications"]:
                    tmp_mod = {"user": str(mod["user"]),  "date": mod["date"]}
                    tmp_mod_arr.append(tmp_mod)
            tmp_backlog["modifications"] = tmp_mod_arr
            tables.append(tmp_backlog)

        if backlog_name != "":
            tables = self._find_backlog_name(backlog_name,tables)
        if request_state != "":
            tables = self._find_request_state(request_state,tables)
        if state != "":
            tables = self._find_state_backlog(state,tables)
        use_case["tables"] = tables
        use_case["tablones"] = self.find_use_case_data_tables(use_case_id)
        use_case["startDate"] = use_case["startDate"].strftime("%Y-%m-%d")
        use_case["finishDate"] = use_case["finishDate"].strftime("%Y-%m-%d")
        use_case["_id"] = str(use_case["_id"])

        tmp_mod_arr = []
        if use_case["modifications"] != "":
            for mod in use_case["modifications"]:
                tmp_mod = {"user": str(mod["user"]),  "date": mod["date"]}
                tmp_mod_arr.append(tmp_mod)
        use_case["modifications"] = tmp_mod_arr
        return use_case

    def find_data_tables_name_state(self, use_case_id, data_table_name, request_state, state):
        """

        :param use_case_id:
        :param data_table_name:
        :param request_state:
        :param state:
        :return:
        """
        data_table_name = data_table_name.strip()
        state = state.strip()
        use_case = self.use_case_repository.find_one(use_case_id.strip())
        tables = []
        try:
            for j in use_case["tablones"]:
                tmp_backlog = self.data_table_repository.find_head(str(j))
                tmp_backlog["_id"] = str(j)
                tmp_backlog["user"] = str(tmp_backlog["user"])
                tmp_backlog["project_owner"] = str(tmp_backlog["project_owner"])
                tmp_backlog["viewStateTable"] = ""
                tmp_backlog["viewStateTable"] = self.values[tmp_backlog["stateTable"]]

                tmp_backlog["request_date"] = ''
                tmp_backlog["tableRequest"] = ''

                requests = self.request_repository.find_any_query(
                    {"$and": [{"target": "T"}, {"idUsecase": ObjectId(str(use_case_id))}, {"idBacklog": ObjectId(str(j))}]})

                if requests.count() == 0:
                    tmp_backlog["tableRequest"] = "A"
                    tmp_backlog["request_date"] = ''
                else:
                    tmp_backlog["tableRequest"] = requests[0]["stateRequest"]
                    tmp_backlog["request_date"] = requests[0]["created_time"].strftime("%Y-%m-%d")
                """
                tmp_mod_arr = []
                if tmp_backlog["modifications"] != "":
                    for mod in tmp_backlog["modifications"]:
                        tmp_mod = {"user": str(mod["user"]),  "date": mod["date"]}
                        tmp_mod_arr.append(tmp_mod)
                """

                tmp_backlog["modifications"] = {"date": "N/A", "user_name": "N/A"}

                tmp_mod_arr = []
                if tmp_backlog["origin_tables"] != "":
                    for mod in tmp_backlog["origin_tables"]:
                        tmp_mod = {
                            "_id": str(mod["_id"]),
                            "originSystem": mod["originSystem"],
                            "originName": mod["originName"],
                            "originRoute": mod["originRoute"],
                            "originType": mod["originType"]
                        }
                        tmp_mod_arr.append(tmp_mod)
                tmp_backlog["origin_tables"] = tmp_mod_arr
                if tmp_backlog["people"] != "":
                    del tmp_backlog["people"]
                tables.append(tmp_backlog)
        except Exception as e:
            print(e)

        if data_table_name != "":
            tables = self._find_by_property(tables, data_table_name, "master_name")
        if request_state != "":
            tables = self._find_by_property(tables, request_state, "tableRequest")
        if state != "":
            tables = self._find_by_property(tables, state, "stateTable")

        use_case["tablones"] = tables
        use_case["tables"] = self.find_use_case_backlogs(use_case_id)
        use_case["startDate"] = use_case["startDate"].strftime("%Y-%m-%d")
        use_case["finishDate"] = use_case["finishDate"].strftime("%Y-%m-%d")
        use_case["_id"] = str(use_case["_id"])
        tmp_mod_arr = []
        if use_case["modifications"] != "":
            for mod in use_case["modifications"]:
                tmp_mod = {"user": str(mod["user"]),  "date": mod["date"]}
                tmp_mod_arr.append(tmp_mod)
        use_case["modifications"] = tmp_mod_arr
        return use_case

    def _find_by_property(self, tables, value, property):
        """

        :param tables:
        :param value:
        :param property:
        :return:
        """
        res = []
        for i in tables:
            if value.strip().lower() in i[property].strip().lower():
                res.append(i)
        return res

    def _find_backlog_name(self,backlog_name,tables):
        """

        :param backlog_name:
        :return:
        """
        tmp_backlog=[]
        for i in tables:
            if backlog_name.strip().lower() in i["baseName"].strip().lower():
                tmp_backlog.append(i)
        return tmp_backlog

    def _find_request_state(self,request_state,tables):
        """

        :param request_state:
        :param tables:
        :return:
        """
        tmp_backlog=[]
        for i in tables:
            if request_state.strip().lower() in i["tableRequest"].strip().lower():
                tmp_backlog.append(i)
        return tmp_backlog

    def _find_state_backlog(self,state,tables):
        """

        :param state:
        :param tables:
        :return:
        """
        tmp_backlog = []
        for i in tables:
            if state == "B":
                if i["stateTable"] == "":
                    tmp_backlog.append(i)
            else:
                if state.strip().lower() in i["stateTable"].strip().lower():
                    tmp_backlog.append(i)
        return tmp_backlog

    def find_data_table(self, use_case_id, master_name, request_state, state):
        """

        :param use_case_id:
        :param master_name:
        :param request_state:
        :param state:
        :return:
        """
        result = []
        tmp_use_case = self.use_case_repository.find_one(use_case_id)
        if tmp_use_case is not None:
            for i in tmp_use_case["tablones"]:
                tmp_data_table = self.data_table_repository.find_head(str(i))
                tmp_data_table["_id"] = str(i)
                tmp_data_table["user"] = self.user_repository.find_one(str(tmp_data_table["user"]))["email"]
                tmp_data_table["project_owner"] = self.project_repository.find_one(str(tmp_data_table["project_owner"]))["name"]
                result.append(tmp_data_table)

            if len(master_name.strip()) != 0:
                result = self.__filter_by_property(result, master_name, "master_name")
            if len(state.strip()) != 0:
                result = self.__filter_by_property(result, state, "stateTable")
            if len(request_state.strip()) != 0:
                result = self.__filter_state_request_data_table(result, request_state, use_case_id)

        return result

    def __filter_by_property(self, array, value, property):
        """

        :param array:
        :param value:
        :param property:
        :return:
        """
        res = []
        for i in array:
            if value in i[property]:
                res.append(i)
        return res

    def __filter_state_request_data_table(self, array, value, case_id):
        """

        :param array:
        :param value:
        :return:
        """
        res = []
        for i in array:
            temp_req = self.request_repository.find_any_query({"$and": [{"target": "T"}, {"idUsecase": ObjectId(case_id)}, {"idBacklog": ObjectId(i["_id"])}, {"stateRequest": value}]})
            if temp_req.count() != 0:
                res.append(i)

        return res
    



    def upload_data_table(self, data):
        """
            Loads a data table info from a sophia excel file
        :param data:
        :return:
        """
        if data["new"] == True:
            data_table_id = self.data_table_service.new_upload_data_table(data, data["user"], data["project_id"])
            
            flag = True
            temp_tablones = self.use_case_repository.find_one(str(data["use_case_id"]))["tablones"]
            for i in temp_tablones:
                if str(i) == data_table_id:
                    flag = False
            if flag:
                temp_tablones.append(ObjectId(str(data_table_id)))
                self.use_case_repository.update_tablones(str(data["use_case_id"]), temp_tablones)
            return data_table_id
        else:
            project_id = data["project_id"]
            project = self.project_repository.find_one(str(project_id))
            if project is None:
                raise ValueError("El proyecto no se encuentra registrado.")

            use_case_id = data["use_case_id"]
            use_case = self.use_case_repository.find_one(str(use_case_id))
            if use_case is None:
                raise ValueError("El caso de uso no se encuentra registrado.")

            user_id = data["user"]
            user = self.user_repository.find_one(str(user_id))
            if user is None:
                raise ValueError("El usuario no se encuentra registrado.")

            data_table_id = self.data_table_service.upload_data_table(data["data_table"], user_id, project_id)
            temp_tablones = use_case["tablones"]
            flag = True
            for i in temp_tablones:
                if str(i) == data_table_id:
                    flag = False

            temp = [{"user": ObjectId(str(user_id)),
                    "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]
            """
            for i in use_case_id["modifications"]:
                temp.append({
                    "user": ObjectId(str(i["user"])),
                    "date": i["date"]
                })
            self.use_case_repository.update_modifications(use_case_id, temp[:20])
            """

            if flag:
                temp_tablones.append(ObjectId(str(data_table_id)))
                self.use_case_repository.update_tablones(str(use_case_id), temp_tablones)
