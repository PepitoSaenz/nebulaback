from nebula.api.persistence.raw_functional_map_repository import *
from nebula.api.persistence.project_repository import *
from nebula.api.persistence.user_repository import *
from datetime import datetime

class Raw_functional_map_service(object):

    """
        A class used to represent a Raw functional map services.

        Attributes
             -------
            raw_functional_map_repository : Raw_functional_map_repository
                Reference to Raw functional map repository class to interact with database
            project_repository: Project_repository
                Reference to project repository class to interact with database
    """

    def __init__(self):
        """

        """
        self.raw_functional_map_repository = Raw_functional_map_repository()
        self.project_repository = Project_repository()
        self.user_repository = User_repository()

    def find_uuaas_project(self,project_id):
        """Find all uuaa's to one project

        :param project_id: str, String that reference to project id
        :return: Array of uuaa's
        """
        uuaas = self.raw_functional_map_repository.find_all()
        res = []
        for j in uuaas:
            res.append(j["uuaa"])
        return sorted(res,key=str.lower)

    def find_uuaas_data_table(self, project_id):
        """Find all uuaa's to one project

        :param project_id: str, String that reference to project id
        :return: Array of uuaa's
        """
        project = self.project_repository.find_one(project_id)
        res = []
        if project is not None:
            uuaas = self.raw_functional_map_repository.find_all()
            for j in uuaas:
                res.append(j["uuaa"])
        return sorted(res, key=str.lower)
    
    def find_uuaas_information(self,project_id):
        """Find all uuaa's to one project

        :param project_id: str, String that reference to project id
        :return: Array of uuaa's
        """
        uuaas = self.raw_functional_map_repository.find_all()
        res = []
        for j in uuaas:
            res.append(j["uuaa"])
        return sorted(res,key=str.lower)
    
    def remove_uuaa(self,data):
        """Delete an uuaa from the collection

        :param data: json with information of the uuaa to delete
        :return:
        """
        '''uuaa = data["uuaa"]
        if len(uuaa) == 4:
            uuaa = uuaa[1:-1]
        repository_uuaa = self.functional_map_repository.find_uuaa(str(uuaa))       
        if repository_uuaa is None:
            raise ValueError("La UUAA no se encuentra registrada.")
        self.functional_map_repository.remove_uuaa(uuaa)'''
        
    def upload_uuaas(self,data):
        """

        :param information:
        :return:
        """
        user = self.user_repository.find_one(data["user"])
        if user is None:
            raise ValueError("Usuario inválido para esa operación.")

        uuaa = data["uuaa"]
        newUuaa = {}
        newUuaa["uuaa"] = uuaa
        newUuaa["app"] = data["app"]
        newUuaa["originSystem"] = data["originSystem"]
        newUuaa["description"] = data["description"]
        newUuaa["insertDate"] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        newUuaa["userId"] = user["_id"]
        
        raw_uuaa = self.raw_functional_map_repository.find_uuaa(uuaa)
        if raw_uuaa is None:
            self.raw_functional_map_repository.insert_uuaa(newUuaa)
        else:
            self.raw_functional_map_repository.update_uuaa(newUuaa)
    
        