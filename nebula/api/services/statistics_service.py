from nebula.api.persistence.data_dictionary_repository import *
from nebula.api.persistence.data_table_repository import *
from nebula.api.persistence.field_repository import *
from nebula.api.persistence.project_repository import *
from nebula.api.persistence.use_case_repository import *
from nebula.api.persistence.user_repository import *
from nebula.api.persistence.backlog_repository import *
from nebula.api.persistence.team_repository import *

import datetime


class Statistics_service(object):

    """
   A class used to represent a statistics services.

   Attributes
          -------
          project_repository : Project_repository
              Reference to Project repository class to interact with database
          user_repository: User_repository
              Reference to User repository class to interact with database
          data_dictionary_repository: Data_dictionary_repository
              Reference to Data dictionary repository class to interact with database
          use_case_repository: Use_case_repository
              Reference to Use case repository class to interact with database
          team_repository: Team_repository
              Reference to Team repository class to interact with database
          state_tables: Array
              State predefine in a field table
          values: dict
              State predefine for a table state

  """

    def __init__(self):
        """

        """
        self.data_dictionary_repository = Data_dictionary_repository()
        self.data_table_repository = Data_table_repository()
        self.project_repository = Project_repository()
        self.use_case_repository = Use_case_repository()
        self.user_repository = User_repository()
        self.backlog_repository = Backlog_repository()
        self.team_repository = Team_repository()
        self.naming_repository = Field_repository()
        self.state_tables = ["FieldReview","fieldsRaw","fieldsMaster"]
        self.values = {"G":"Gobierno", "A":"Arquitectura", "N":"En propuesta namings", "I": "Para ingestar",
                       "IN":"Ingestada en desarrollo","Q": "Calidad", "M": "Mallas", "P": "Produccion",
                       "ungoverned": "En backlog", "PA": "Pendiente aprobación backlog","RN":"Revisión Negocio",
                       "F": "Gobierno funcional", "R": "Revisión solicitud", "D": "Descartado"}

    def close_user_field(self,user_id):
        pass

    def find_table_state(self,table_id):
        """Get all fields state in a table as a percent

        :param table_id: str, string that represent a table id
        :return: array of jsons, with a percent in every fase
        """
        res = {}
        table = self.data_dictionary_repository.find_one(table_id)
        if table["stateTable"] in ["IN", "I", "Q", "M", "P"]:
            res["raw"]=[{"name": self.values[table["stateTable"]], "y": 100}]
            res["master"]=[{"name": self.values[table["stateTable"]], "y": 100}]
        elif table["stateTable"] == "N":
           res = self._find_namings_statistics(table)
        elif table["stateTable"] == "G":
           users_ids = []
           users=self.user_repository.find_by_rol("G")
           for i in users:
               users_ids.append(i["_id"])
           res = self._find_governance_statistics(table, users_ids)
        elif table["stateTable"] == "A":
            users_ids = []
            users = self.user_repository.find_by_rol("A")
            for i in users:
                users_ids.append(i["_id"])
            res= self._find_architecture_statistics(table,users_ids)
        return res

    def _count_phase(self,phase, table):
        """Get all namings that are proposed in a table

        :param phase: str, string that reference a phase to check (FieldReview, raw or master)
        :param table: json, dictionary that contains all table data
        :return: array of json that contains a number of proposed field
        """
        res={}
        try:
            if len(table[phase]) > 0:
                contF = 0
                totalF = len(table[phase])
                for i in table[phase]:
                    if i["naming"]["naming"] != "empty" and i["logic"] != "empty" and i["description"] != "empty" and i["legacy"]["legacy"] != "empty":
                        contF += 1
                res[phase] = [{"name": "Propuestos", "y": contF},{"name": "Sin proponer", "y": totalF - contF}]
            else:
                res[phase] = [{"name": "Propuestos", "y": 0},{"name": "Sin proponer", "y": 0}]
        except:
            res[phase] = [{"name": "Propuestos", "y": 0},{"name": "Sin proponer", "y": 0}]
        return res

    def _find_namings_statistics(self, table):
        """count how many fields was proposed in all table fase

        :param table: json, dictionary that contains all table data
        :return: array of dict
        """
        res = []
        res.append(self._count_phase("FieldReview",table))
        res.append(self._count_phase("fieldsRaw", table))
        res.append(self._count_phase("fieldsMaster", table))
        return res

    def _find_modificacion(self,phase,users,table):
        """Get how many fields was proposed in a table by an user

        :param phase: str, string that represent a fase to check
        :param users: str, string that represent a user id
        :param table: json, dictionary with table data
        :return: Array of jsons
        """
        res = {}
        try:
            if len(table[phase]) > 0:
                count=0
                total=len(table[phase])
                for i in table[phase]:
                    try:
                        if len(i["modification"])>0:
                            for j in i["modification"]:
                                if j["user"] in users and j["reason"]=="Ok":
                                    count+=1
                            res[phase] = [{"name": "Revisados", "y": count}, {"name": "Sin revisar", "y": total - count}]
                    except:
                        res[phase] = [{"name": "Revisados", "y": 0}, {"name": "Sin revisar", "y": total}]
            else:
                res[phase] = [{"name": "Revisados", "y": 0}, {"name": "Sin revisar", "y": 0}]
        except:
            res[phase]=[{"name": "Revisados", "y": 0},{"name": "Sin revisar", "y": 0}]
        return res

    def _find_governance_statistics(self, table, users):
        """count how many fields was comment in all table fase

        :param table: json, dictionary that contains all table data
        :param users: str, string that represent a user id
        :return: array of dict
        """
        res = []
        res.append(self._find_modificacion("FieldReview", users, table))
        res.append(self._find_modificacion("fieldsRaw",users,table))
        res.append(self._find_modificacion("fieldsMaster", users, table))
        return res

    def _find_architecture_statistics(self,table, users):
        """count how many fields was architecture comment in all table fase

        :param table: json, dictionary that contains all table data
        :param users: str, string that represent a user id
        :return: array of dict
        """
        res = []
        res.append(self._find_modificacion("FieldReview", users, table))
        res.append(self._find_modificacion("fieldsRaw", users, table))
        res.append(self._find_modificacion("fieldsMaster", users, table))
        return res

    def find_general_status(self,code,where,typo):
        """Get a global statistics in project or use case

        :param code: str, string that represent a id
        :param where: str, string that represent where find
        :return: Array of json
        """
        res = []
        if where == "project": 
            if typo == "dd":
                res = self._project_state(code)
            if typo == "dt":
                res = self._project_state_dt(code)
        elif where == "use_case": res = self._use_case_state(code)
        return res

    def _project_state(self,project_id):
        """Get a global statistic that how is it a project

        :param project_id: str, string that represent a project id
        :return: Array of jsons, with a global status in a project
        """
        #pipeline remove and transform data thath only need to calculate a statistic
        #Join with a datadictionary with backlog and project
        pipeline = [
            { '$match': { '_id': ObjectId(project_id)}},
            { '$lookup': { 'from': 'useCase', 'localField': 'useCases',
                    'foreignField': '_id','as': 'use_cases_projects'}},
            { '$unwind': { 'path': '$use_cases_projects'}},
            { '$project': { 'tables': '$use_cases_projects.tables'}},
            { '$unwind': { 'path': '$tables'}},
            { '$group': { '_id': '$tables' }},
            { '$lookup': { 'from': 'backlog', 'localField': '_id',
                    'foreignField': '_id', 'as': 'backlogs'}},
            { '$unwind': { 'path': '$backlogs'}},
            { '$lookup': { 'from': 'dataDictionary', 'localField': 'backlogs.idTable',
                    'foreignField': '_id', 'as': 'dataDictionary_backlogs'}},
            { '$unwind': { 'path': '$dataDictionary_backlogs'}},
            { '$project': { 'backlogIdTable': '$backlogs.idTable', 'backlogTableRequest': '$backlogs.tableRequest',
                    'dataDictionaryState': '$dataDictionary_backlogs.stateTable'}}
        ]
        result = self.project_repository.generate_aggreation(pipeline)
        res = []
        tmp=dict()
        for i in result:
            if i["backlogIdTable"] == "" and (i["backlogTableRequest"] == "" or i["backlogTableRequest"] == "P") :
                if "PA" in tmp:
                    tmp["PA"]+=1
                else:
                    tmp["PA"]=1
            elif i["backlogIdTable"] == "" and i["backlogTableRequest"]== "A":
                if "ungoverned" in tmp:
                    tmp["ungoverned"]+=1
                else:
                    tmp["ungoverned"]=1
            elif i["backlogIdTable"] != "":
                if i["dataDictionaryState"] in tmp:
                    tmp[i["dataDictionaryState"]]+=1
                else:
                    tmp[i["dataDictionaryState"]]=1
        for key in tmp:
            res.append({"name":self.values[key],"y": tmp[key]})
        return res
    
    def _project_state_dt(self,project_id):
        """Get a global statistic that how is it a project FOR DATA TABLES

        :param project_id: str, string that represent a project id
        :return: Array of jsons, with a global status in a project
        """
        #pipeline remove and transform data thath only need to calculate a statistic
        #Join with a datadictionary with backlog and project
        pipeline = [
            { '$match': { '_id': ObjectId(project_id)}},
            { '$lookup': { 'from': 'useCase', 'localField': 'useCases',
                    'foreignField': '_id','as': 'use_cases_projects'}},
            { '$unwind': { 'path': '$use_cases_projects'}},
            { '$project': { 'tablones': '$use_cases_projects.tablones'}},
            { '$unwind': { 'path': '$tablones'}},
            { '$group': { '_id': '$tablones' }},

            { '$lookup': { 'from': 'dataTable', 'localField': '_id',
                    'foreignField': '_id', 'as': 'dataTables'}},
            { '$unwind': { 'path': '$dataTables'}},

            { '$project': {'dataTableState': '$dataTables.stateTable'}}
        ]
        result = self.project_repository.generate_aggreation(pipeline)
        res = []
        tmp=dict()
        for i in result:
            if i["dataTableState"] in tmp:
                tmp[i["dataTableState"]]+=1
            else:
                tmp[i["dataTableState"]]=1
        for key in tmp:
            res.append({"name":self.values[key],"y": tmp[key]})
        return res

    def _use_case_state(self,use_case_id):
        """Get a global statistic that how is it a use case

        :param use_case_id: str, string that represent a use case id
        :return: Array of jsons, with a global status in an use case
        """
        #change to Tables in production
        tables = self.use_case_repository.find_one(use_case_id)["tables"]
        #trasnform data to calculate data
        #use case join with data dictionary
        pipeline = [
            {"$match":{"_id":{"$in":tables}}},
            {'$lookup': {'from': 'dataDictionary', 'localField': 'idTable', 'foreignField': '_id', 'as': 'tableCheck'}},
            {'$project': {'state': '$tableCheck.stateTable', '_id': 0}},
            {'$group': {'_id': {'state': '$state'}, 'count': {'$sum': 1}}},
            {'$group': {'_id': None, 'total': {'$sum': '$count'}, 'state': {'$push': {'state': '$_id.state', 'count': '$count'}}}},
            {'$unwind': {'path': '$state'}},
            {'$project': {'_id': 0, 'state': '$state.state', 'percentage': {'$multiply': '$state.count'}}}
        ]
        res = []
        tmp = self.backlog_repository.generate_aggregate(pipeline)
        for i in tmp:
            if i["state"] == []:
                state = "ungoverned"
            else:
                state = i["state"][0]
            res.append({"name":self.values[state],"y":i["percentage"]})
        return res

    def find_proposer_dates(self,group,code,start_date,end_date):
        """Get how many fields was proposed between two days by users or teams

        :param group: str, string that represent what going to check
        :param code: str, string that represent a id
        :param start_date: str, string that represent a star date
        :param end_date: str, string that represent a end date
        :return:
        """
        res = []
        if group == "users":
            res = self._find_user_proposer(code,start_date,end_date)
        elif group == "teams":
            res = self._find_team_proposer(code,start_date,end_date)
        return self._order_keys(self._add_null_dates(res,start_date,end_date))

    def _add_null_dates(self,data,start_date,end_date):
        """Add days where any field was proposed by need to check statistics

        :param data: array, contains dates to check
        :param start_date: str, string that represent a start date
        :param end_date: str, string that represent a end date
        :return: array of dates
        """
        start_date = datetime.datetime.strptime(start_date, "%Y-%m-%d")
        end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d")
        date = start_date
        while start_date <= date <= end_date:
            if (not date in data) and date.weekday()!=6 and date.weekday()!=5:
                data[date] = 0
            date += datetime.timedelta(days=1)
        return data

    def _order_keys(self,data):
        """Order an array

        :param data: array
        :return: array
        """
        return sorted(data.items())

    def _find_user_proposer(self,user_id,start_date,end_date):
        """Get all fields proposed by a user between two days

        :param user_id: str, string that represent a user id
        :param start_date: str, string that represent a start day
        :param end_date: str, string that represent a end day
        :return: Array of json, with how many fields was proposed by day
        """
        start_date = datetime.datetime.strptime(start_date,"%Y-%m-%d")
        end_date = datetime.datetime.strptime(end_date, "%Y-%m-%d") + datetime.timedelta(days=1)
        pipeline = [
            {'$match': {'FieldReview': {'$elemMatch': {'modification': {'$elemMatch': {'startDate': {'$gte': start_date, '$lte': end_date}}}}}}},
            {'$match': {'FieldReview': {'$elemMatch': {'modification': {'$elemMatch': {'user': ObjectId(user_id)}}}}}},
            {'$project': {'modification': '$FieldReview.modification', '_id': 0}},
            {'$match': {'modification': {'$elemMatch': {'$elemMatch': {'user': ObjectId(user_id)}}}}},
            {'$unwind': {'path': '$modification'}}
        ]
        data = self.data_dictionary_repository.generate_aggreation(pipeline)
        res = {}
        for i in data:
            date = {}
            for j in i["modification"]:
                date[str(j["startDate"]).split()[0]] = 1
            for j in date:
                j = datetime.datetime.strptime(j,"%Y-%m-%d")
                if j in res:
                    res[j] += 1
                else:
                    res[j] = 1
        return res

    def _find_team_proposer(self,team_id,start_date,end_date):
        """Get all fields proposed by a team between two days

        :param team_id: str, string that represent a team id
        :param start_date: str, string that represent a start day
        :param end_date: str, string that represent a end day
        :return: Array of json, with how many fields was proposed by day
        """
        users = self.user_repository.find_team(team_id)
        res = {}
        for i in users:
            tmp = self._find_user_proposer(i["_id"],start_date,end_date)
            for j in tmp:
                if j in res:
                    res[j] += tmp[j]
                else:
                    res[j] = tmp[j]
        return res


    def main_statistics(self):
        """Get a global statistic that how is it a project

        :param project_id: str, string that represent a project id
        :return: Array of jsons, with a global status in a project
        """
        res = []

        res = self._get_tables_stats()

        return res
    
    def _get_tables_stats(self):
        """Get a global statistic that how is it a project

        :param project_id: str, string that represent a project id
        :return: Array of jsons, with a global status in a project
        """
        res = []
        proyects = self.project_repository.find_all()

        for pro in proyects:
            if pro["name"] != 'Unassigned':
                tmp = self._project_state(str(pro['_id']))
                for val in tmp:
                    flag = 0
                    for cos in res:
                        if "name" in cos and cos["name"] == val["name"]:
                            cos["y"] += val["y"]
                            flag = 1
                    if flag == 0:
                        res.append(val)

        return res
    
    def main_statistics_dt(self):
        """Get a global statistic that how is it a project

        :param project_id: str, string that represent a project id
        :return: Array of jsons, with a global status in a project
        """
        res = []
        proyects = self.project_repository.find_all()

        for pro in proyects:
            if pro["name"] != 'Unassigned':
                tmp = self._project_state_dt(str(pro['_id']))
                for val in tmp:
                    flag = 0
                    for cos in res:
                        if "name" in cos and cos["name"] == val["name"]:
                            cos["y"] += val["y"]
                            flag = 1
                    if flag == 0:
                        res.append(val)

        return res
    
    def tables_by_project(self):
        """Get for each project how many tables it has

        :param:
        :return: Array of jsons
        """
        res=[]
        pipeline = [
                #{"$lookup":{"from" : "useCase","localField" : "useCases","foreignField" : "_id","as" : "usecase"}},
                #{"$unwind": {"path": "$usecase"}},
                #{"$unwind": {"path": "$usecase.tables"}},
                #{'$group':{'_id': {'table': '$name'},  'y': {'$sum': 1}}}
                {"$lookup":{"from" : "useCase","localField" : "useCases","foreignField" : "_id","as" : "usecase"}},
                {"$unwind": {"path": "$usecase", "preserveNullAndEmptyArrays": True}},
                {"$lookup":{"from" : "dataDictionary","localField" : "usecase.tables","foreignField" : "_id","as" : "information"}},
                {"$unwind": {"path": "$information", "preserveNullAndEmptyArrays": True}},
                {'$project': {'name': '$name','usecase':'$usecase.name', 'table': '$information._id', 'information.stateTable': 1}},
                {'$group':{'_id': {'project': '$name'},  'y': {'$sum': 1}}}
                ]
        data = self.project_repository.generate_aggreation(pipeline)
        for i in data:
            line = {}
            line["name"] = i["_id"]["project"]
            line["y"] = i["y"]
            res.append(line)
        return res
    
    def tables_by_state_and_project(self):
        """Get for each project how many tables it has and in what state they are

        :param:
        :return: Array of jsons
        """
        res=[]
        pipeline = [
                {"$lookup":{"from" : "useCase","localField" : "useCases","foreignField" : "_id","as" : "usecase"}},
                {"$unwind": {"path": "$usecase", "preserveNullAndEmptyArrays": True}},
                {"$lookup":{"from" : "dataDictionary","localField" : "usecase.tables","foreignField" : "_id","as" : "information"}},
                {"$unwind": {"path": "$information", "preserveNullAndEmptyArrays": True}},
                {'$project': {'name': '$name','usecase':'$usecase.name', 'table': '$information._id', 'information.stateTable': 1}},
                {'$group':{'_id': {'tableState':'$information.stateTable'},  'y': {'$sum': 1}}}
                ]
        data = self.project_repository.generate_aggreation(pipeline)
        for i in data:
            line = {}
            '''if "project" in i["_id"]:
                line["name"] = i["_id"]["project"]'''
            if "tableState" in i["_id"]:
                line["name"] = i["_id"]["tableState"]
            line["y"] = i["y"]
            res.append(line)
        return res
    
    def tables_by_uuaa(self):
        """Get for each project how many tables it has and in what state they are

        :param:
        :return: Array of jsons
        """
        res=[]
        pipeline = [
                {'$group':{'_id': {'name': '$uuaaMaster'},  'y': {'$sum': 1}}}
                ]
        pipelineRaw = [
                {'$group':{'_id': {'name': '$uuaaRaw'},  'y': {'$sum': 1}}}
                ]
        data = self.data_dictionary_repository.generate_aggreation(pipeline)
        dataRaw = self.data_dictionary_repository.generate_aggreation(pipelineRaw)
        for i in data:
            line = {}
            if "name" in i["_id"]:
                line["name"] = i["_id"]["name"]
            line["y"] = i["y"]
            res.append(line)
        for j in dataRaw:
            line = {}
            if "name" in j["_id"]:
                line["name"] = j["_id"]["name"]
            line["y"] = j["y"]
            res.append(line)
        return res

    def duplicated_namings_uuaa(self, uuaa, typeF, loc):
        """Get a global statistic that how is it a project

        :param uuaa: str, string that represent the uuaa to search
        :param typeF: str, string raw or master that defines where to look the fields
        :return: Array of jsons, with a global status in a project
        """
        res = []
        propName = "uuaaRaw" 
        fieldName = "FieldReview"

        if loc == "raw": 
            fieldName = "fieldsRaw" 
            propName = "uuaaRaw" 
        if loc == "master": 
            fieldName = "fieldsMaster"
            propName = "uuaaMaster"

        data = self.data_dictionary_repository.find_dd_property(propName, uuaa)
        bad_tables = self.use_case_repository.find_one("5d8412b7e7d05611848562cd")["tables"] #use case undefined

        for tab in data:
            if not tab["_id"] in bad_tables:
                try:
                    for naming in tab[fieldName]:
                        flago = 0
                        for nam in res:
                            if nam["name"] == naming["naming"]["naming"]:
                                nam["y"] += 1
                                flago = 1
                        if flago == 0:
                            res.append({"name": naming["naming"]["naming"], "y": 1})
                except Exception:
                    print(tab["alias"] + " does not have attribute " + propName)

        return res

    def number_all_proyects(self):
        """Get a global statistic that how is it a project

        :param uuaa: str, string that represent the uuaa to search
        :param typeF: str, string raw or master that defines where to look the fields
        :return: Array of jsons, with a global status in a project
        """
        res = []
        proyects = self.project_repository.find_all()

        for pro in proyects:
            if pro["name"] != 'Unassigned':
                res.append({"name": pro["name"], "y": self.numbertables_proyect( str(pro['_id']) )})
                
        return res
    
    def number_all_proyects_dt(self):
        """Get a global statistic that how is it a project

        :param uuaa: str, string that represent the uuaa to search
        :param typeF: str, string raw or master that defines where to look the fields
        :return: Array of jsons, with a global status in a project
        """
        res = []
        proyects = self.project_repository.find_all()

        for pro in proyects:
            if pro["name"] != 'Unassigned':
                res.append({"name": pro["name"], "y": self.numbertables_proyect_dt( str(pro['_id']) )})
                
        return res

    def numbertables_proyect(self, id_pro):
        pipeline = [
            { '$match': { '_id': ObjectId(id_pro)}},
            { '$lookup': { 'from': 'useCase', 'localField': 'useCases',
                    'foreignField': '_id','as': 'use_cases_projects'}},
            { '$unwind': { 'path': '$use_cases_projects'}},
            { '$project': { 'tables': '$use_cases_projects.tables'}},
            { '$unwind': { 'path': '$tables'}},
            { '$group': { '_id': '$tables' }},
            { '$lookup': { 'from': 'backlog', 'localField': '_id',
                    'foreignField': '_id', 'as': 'backlogs'}},
            { '$unwind': { 'path': '$backlogs'}},
            { '$lookup': { 'from': 'dataDictionary', 'localField': 'backlogs.idTable',
                    'foreignField': '_id', 'as': 'dataDictionary_backlogs'}},
            { '$unwind': { 'path': '$dataDictionary_backlogs'}},
            { '$project': { 'backlogIdTable': '$backlogs.idTable', 'backlogTableRequest': '$backlogs.tableRequest',
                    'dataDictionaryState': '$dataDictionary_backlogs.stateTable'}}
        ]
        result = self.project_repository.generate_aggreation(pipeline)
        return len(list(result))
    
    def numbertables_proyect_dt(self, id_pro):
        pipeline = [
            { '$match': { '_id': ObjectId(id_pro)}},
            { '$lookup': { 'from': 'useCase', 'localField': 'useCases',
                    'foreignField': '_id','as': 'use_cases_projects'}},
            { '$unwind': { 'path': '$use_cases_projects'}},
            { '$project': { 'tablones': '$use_cases_projects.tablones'}},
            { '$unwind': { 'path': '$tablones'}},
            { '$group': { '_id': '$tablones' }},

            { '$lookup': { 'from': 'dataTable', 'localField': '_id',
                    'foreignField': '_id', 'as': 'dataTables'}},
            { '$unwind': { 'path': '$dataTables'}},

            { '$project': {'dataTableState': '$dataTables.stateTable'}}
        ]
        result = self.project_repository.generate_aggreation(pipeline)
        return len(list(result))

    def use_tables_by_usecase(self):
        """Get the amount of tables used for each use case

        :param:
        :return: Array of jsons
        """
        use_cases = self.use_case_repository.find_all()
        res = []
        for uc in use_cases:
            pipeline = [
                {"$match":{"_id":{"$in":uc["_id"]}}},
                {'$lookup': {'from': 'dataDictionary', 'localField': 'tables', 'foreignField': '_id', 'as': 'usecase_table'}},
                {'$project': {'state': '$tableCheck.stateTable', '_id': 0}},
                {'$group': {'_id': {'state': '$state'}, 'count': {'$sum': 1}}},
                {'$group': {'_id': None, 'total': {'$sum': '$count'}, 'state': {'$push': {'state': '$_id.state', 'count': '$count'}}}},
                {'$unwind': {'path': '$state'}},
                {'$project': {'_id': 0, 'state': '$state.state', 'percentage': {'$multiply': '$state.count'}}}
            ]


        data = self.project_repository.generate_aggreation(pipeline)
        for i in data:
            line = {}
            line["name"] = i["_id"]["project"]
            line["y"] = i["y"]
            res.append(line)
        
        #change to Tables in production
        tables = self.use_case_repository.find_one(use_cases["_id"])["tables"]
        #trasnform data to calculate data
        #use case join with data dictionary
        pipeline = [
            {"$match":{"_id":{"$in":tables}}},
            {'$lookup': {'from': 'dataDictionary', 'localField': 'idTable', 'foreignField': '_id', 'as': 'tableCheck'}},
            {'$project': {'state': '$tableCheck.stateTable', '_id': 0}},
            {'$group': {'_id': {'state': '$state'}, 'count': {'$sum': 1}}},
            {'$group': {'_id': None, 'total': {'$sum': '$count'}, 'state': {'$push': {'state': '$_id.state', 'count': '$count'}}}},
            {'$unwind': {'path': '$state'}},
            {'$project': {'_id': 0, 'state': '$state.state', 'percentage': {'$multiply': '$state.count'}}}
        ]
        
        tmp = self.backlog_repository.generate_aggregate(pipeline)
        for i in tmp:
            if i["state"] == []:
                state = "ungoverned"
            else:
                state = i["state"][0]
            res.append({"name":self.values[state],"y":i["percentage"]})
        return res

    def general_statistics(self):
        res = {
            "dataDictionary": self.data_dictionary_repository.dd_count(),
            "dataTable": self.data_table_repository.dt_count(),
            "namings": "{:,}".format(self.naming_repository.namings_count()).replace(",", "."),
            "projects": self.project_repository.project_count(),
            "useCase": self.use_case_repository.use_case_count()
        }

        return res
    
    def get_usedtables_in_datatables(self, search_alias_arr, secArr):
        print("started")
        resu = []
        search_id_arr = []
        errors = []
        founds = []
        routes = []
        try:
            for alias in search_alias_arr:
                #print("alias: " + alias)
                tmpId = self.data_dictionary_repository.find_dd_property("alias", alias)
                if tmpId.count() == 0:
                    #print("tabla not found")
                    errors.append( {"error": "Tabla no encontrada en Nebula", "alias": alias} )
                else:
                    #print("id: " + str(tmpId[0]["_id"]))
                    search_id_arr.append( {"alias": alias, "_id": str(tmpId[0]["_id"])} )
                    routes.append(
                        {"alias": alias, "routeRaw": tmpId[0]["raw_route"], "routeMaster": tmpId[0]["master_route"]}
                    )
        except Exception as e:
            print(e)

        #print(search_id_arr)
        all_dts = self.data_table_repository.find_all()
        #print(all_dts.count())
        for datat in all_dts:
            #print("Tablon " + datat["alias"] )
            if datat["alias"] in secArr:
                print(datat["master_path"])
            for origin in datat["origin_tables"]:
                for search in search_id_arr:
                    if str(origin["_id"]) == search["_id"]:
                        #print("found " + str(origin["originName"] + " in thing: " + datat["alias"]))
                        founds.append( {"success": "Tablon "+ datat["alias"] + " posee:: " + str(origin["originName"]), "Tabla Origen": str(origin["originName"]),
                                        "Tablon": datat["alias"], "Nombre Tablon": datat["master_name"], "Ruta Tablon": datat["master_path"]} )

        resu.append(founds)
        resu.append(errors)
        resu.append(routes)
        return resu