from nebula.api.persistence.field_repository import *
from nebula.api.persistence.suffix_repository import *
from nebula.api.persistence.word_repository import *

import re


class Global_search_service(object):

    def __init__(self):
        self.field_repository = Field_repository()
        self.suffix_repository = Suffix_repository()
        self.word_repository = Word_repository()

    #Anteriormente tildes
    """
    Reemplaza las tildes lo utiliza clear
    """
    def _replace_accent_mark(self, text):
        replacements = (
            ("á", "a"),
            ("é", "e"),
            ("í", "i"),
            ("ó", "o"),
            ("ú", "u"),
            ("ü", "u")
        )
        for a, b in replacements:
            text = text.replace(a, b)
        return text

    #Anteriormente limpieza
    """
         Quita espacios, simbolos, conectores, las tildes(replace_accent) todo en minus
    """
    def _clear(self, word):
        symbols = re.compile('([.]|[,]|[/]|[\]|[;]|[(]|[)]|[-]|[_]|["]|[\']|[:])')
        connects = re.compile(
            '( que | cual | por | a | e | y | u | o |s | un | una | unos | unas | de | la | los | las | lo | es | son | el | un | una | unos | unas )')
        space = re.compile('\s+')

        result_word = word.lower()
        result_word = self._replace_accent_mark(result_word)
        result_word = symbols.sub(' ', result_word)
        result_word = connects.sub(' ', result_word)
        result_word = space.sub(' ', result_word)
        result_word = result_word.split(' ')
        try:
            result_word.remove("")
        except:
            pass
        return result_word

    #Anteriormente comparacion
    """
        Peso del resultado de la contencion de la descripcion y logico name con el lagacy. De no haber match legacy vs logical name, description by letter 
    """
    def _first_words_comparison(self, legacy, description):
        desc = list(set(description))
        match_word_sum = 0
        total_sum_by_letter = 0
        for i in range(len(legacy)):
            if (legacy[i] in desc):
                match_word_sum += 1
            else:
                for j in range(len(desc)):
                    sum_by_letter = self._third_words_comparison(legacy[i], desc[j])
                    total_sum_by_letter +=  sum_by_letter
        return (match_word_sum + total_sum_by_letter) + ((match_word_sum + total_sum_by_letter) / len(desc))

    #Anteriormente comparacion2
    """
        Peso del resultado de la contencion de la descripcion y logico name con el legacy
    """
    def _second_words_comparison(self, legacy, description):
        match_word_sum = 0
        for i in range(len(legacy)):
            if (legacy[i] in list(set(description))):
                match_word_sum += 1
            else:
                pass
        return match_word_sum + (match_word_sum / len(description))

    #Anteriormente comparacion3
    """
     Compara las palabras del legacy vs la descripcion, si hay un oprocetanje corelacion mayor al 60 % lo retorna.
    """
    def _third_words_comparison(self, legacy, description):

        if len(legacy) > len(description):
            long = (legacy)
            short = (description)
        else:
            short = (legacy)
            long = (description)

        if len(short) / len(long) < 0.6:
            pass
        else:
            new_short=short
            for i in range(len(long) - len(short) + 1):
                match_by_letter_total_sum = 0
                for j in range(len(new_short)):
                    if new_short[j] == long[j]:
                        match_by_letter_total_sum +=1
                    else:
                        pass
                new_short= "." + new_short
                result_total_sum = match_by_letter_total_sum / len(long)
                if  result_total_sum >= 0.6:
                    return result_total_sum
                else:
                    pass

            match_by_letter_total_sum = 0

            for i in range((len(short) * 10 - len(long) * 6) // 10):
                long = long + "."
                for j in range(len(new_short)):
                    if new_short[j] == long[j]:
                        match_by_letter_total_sum +=1
                    else:
                        pass
                new_short = "." + new_short
                result_total_sum = match_by_letter_total_sum / len(long)
                if result_total_sum >= 0.6:
                    return result_total_sum
                else:
                    pass

            match_by_letter_total_sum = 0

            for i in range((len(short) * 10 - len(long) * 6) // 10):
                short = short[1:]
                for j in range(len(short)):
                    if short[j] == long[j]:
                        match_by_letter_total_sum += 1
                    else:
                        pass
                    result_total_sum = match_by_letter_total_sum / len(long)
                    if result_total_sum >= 0.6:
                        return result_total_sum
                    else:
                        pass
        return 0

    """
    Busca las abreviaciones por palabra que contiene el naming.
    """
    def _abbreviations(self, type_list):
        type_list = self._clear(type_list)
        suffix = ["id", "type", "per", "date", "amount", "desc","name", "number"]
        abbreviations = []
        for word in type_list:
            if (word in suffix):
                pass
            else:
                abbreviations.append(word)
                query = self.word_repository.find_by_long_name(word)
                if (query != None):
                    abbreviations.append(query["abbreviation"].lower())
        return abbreviations

    # Búsqueda normal
    """
    Busqueda sin filtro , no se tiene en cuenta los namings. 
    """
    def normal_search(self, legacy, logical_name, description):
        logical = self._clear(logical_name)
        descript = self._clear(description)
        comparation_1 = self._first_words_comparison(legacy, logical)
        comparation_2 = self._first_words_comparison(legacy, descript)
        join_Logical_name_description = self._clear(logical_name + " " + description)
        comparation_3 = self._second_words_comparison(legacy, join_Logical_name_description)
        return comparation_1 + comparation_2 + comparation_3

    # Búsqueda normal + teniendo en cuenta el naming
    """
    busqeuda teniendo cuenta los namings y abreviaciones
    """
    def filter_naming_search(self, legacy, logical_name, description, naming, abbreviations):
        normal_search_result = self.normal_search(legacy, logical_name, description)
        naming = self._clear(naming)
        naming_match_result = self._second_words_comparison(abbreviations, naming)
        return normal_search_result + naming_match_result

    def raw_search(self,naming,suffix,description):
        query=self._find_suffix_global_field(suffix)
        result = []
        legacy = self._clear(description)
        best_field = []
        final_sum = 0
        abbreviations = self._abbreviations(naming)
        for word in legacy:
            if ("ñ" in word):
                legacy.append(re.subn("ñ", "ni", word)[0])
        for field in query:
            # Si es free_field
            if (field["naming"]["naming"].startswith("free_field") or field["naming"]["naming"] == ""
                    or field["logic"] == "" or field["description"] == ""):
                pass
            # Si no es campo libre
            else:
                # Si tipo es vacío, osea no se quiere buscar palabras en el naming
                if (naming == ""):
                    final_sum = self.normal_search(legacy, field["logic"], field["description"])
                # Si hay palabras que se quieren buscar en el naming
                else:
                    final_sum = self.filter_naming_search(legacy, field["logic"], field["description"],
                                                     field["naming"]["naming"], abbreviations)
            if final_sum == 0:
                pass
            else:
                best_field.append((field, final_sum))

        if (len(best_field) > 0):
            best_field.sort(key=lambda tup: tup[1], reverse=True)
        for f in best_field:
            result.append(f[0])
        return result

    def _find_suffix_global_field(self, suffix):
        querySuffix=self.suffix_repository.find_one(suffix)
        if querySuffix != None:
            query={"naming.suffix": querySuffix["_id"]}
        else:
            query={}
        return self.field_repository.find_any_query(query)

    def convert_cursor_to_json(self,cursor):
        res = []
        for i in cursor:
            res.append(i)
        return res
