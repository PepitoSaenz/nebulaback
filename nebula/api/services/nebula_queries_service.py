from nebula.api.services.field_service import *
from nebula.api.services.legacy_service import *

from nebula.api.persistence.functional_map_repository import *
from nebula.api.persistence.data_dictionary_repository import *
import re


class Nebula_queries_service():

    """
       A class used to represent a Nebula queries services.

       Attributes
           -------
           field_service : Field_service
               Reference to Field repository class to interact with a database
           legacy_service: Legacy_service
               Reference to legacy repository class to interact with a database
           functional_map_repository: Functional_map_repository
               Reference to Functional map repository class to interact with a database
            data_dictionary_repository: Data_dictionary_repository
                Reference to Data dictionary repository class to interact with a database
    """

    def __init__(self):
        self.field_service = Field_service()
        self.legacy_service = Legacy_service()
        self.functional_map_repository = Functional_map_repository()
        self.data_dictionary_repository = Data_dictionary_repository()

    def find_global_query(self, where, value):
        """Get a specific data to find naming, legacy or uuaa's

        :param where: str, string that represent where find
        :param value: str, string that represent a value to find
        :return: Array of Json's
        """
        res = []
        if where == "naming":
            res = [self.field_service.find_exactly_naming(value)]
        elif where == "legacy":
            res = self.legacy_service.legacy_query(value)
        elif where == "uuaa":
            res = self.find_tables_functional_map(value)
        return res

    def find_tables_functional_map(self, value):
        """Get tables that has a specific uuaa or functional level

        :param value: str, string that represent a value to find
        :return: cursor, array of dict with tables data
        """
        tmp = self.functional_map_repository.find_by_levels(value)["uuaa"]
        return self.data_dictionary_repository.generate_aggreation([
            {"$match": {"uuaaMaster": re.compile(tmp,re.IGNORECASE)}},
            {"$project": {"alias": 1, "raw_name": 1, "master_name": 1}}
        ])
