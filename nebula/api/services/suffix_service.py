from nebula.api.persistence.suffix_repository import *


class Suffix_service(object):

    """
    A class used to represent a suffix services.

    Attributes
          -------
          suffix_repository : Suffix_repository
              Reference to Suffix repository class to interact with database
    """

    def __init__(self):
        self.suffix_repository = Suffix_repository()

    def find_all(self):
        """Get all suffix

        :return: cursor, with all suffix data
        """
        return self.suffix_repository.find_all()