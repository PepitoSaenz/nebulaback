from nebula.api.persistence.field_repository import *
from nebula.api.persistence.suffix_repository import *
from nebula.api.persistence.word_repository import *
from nebula.api.persistence.data_dictionary_repository import *

from django.views.decorators.csrf import csrf_exempt

import re
from unicodedata import normalize


class Field_service(object):

    """
        A class used to represent a Functional map services.

        Attributes
             -------
            field_repository : Field_repository
                Reference to Field repository class to interact with database
            suffix_repository: Suffix_repository
                Reference to Suffix repository class to interact with database
            word_repository: Word_repository
                Reference to Word repository class to interact with database
            data_dictionary_repository: Data_dictionary_repository
                Reference to Data dictionary repository class to interact with database
            fields: Array
                phases pre-defines to check
    """

    def __init__(self):
        """

        """
        self.field_repository = Field_repository()
        self.suffix_repository = Suffix_repository()
        self.word_repository = Word_repository()
        self.data_dictionary_repository = Data_dictionary_repository()
        self.fields = ["FieldReview", "fieldsRaw", "fieldsMaster"]
        self.export_props = {
            "ID NEBULA": "_id",
            "FIELD CODE ID": "code",
            "GLOBAL NAMING FIELD": "naming",
            "GLOBAL MODEL LOGICAL FORMAT (DOMAIN + LENGTH)": "governanceFormat",
            "HIERARCHY": "level",
            "LOGICAL NAME CODE": "codeLogic",
            "LOGICAL NAME OF THE FIELD (SPA)": "logic",
            "FIELD DESCRIPTION (SPA)": "originalDesc",
            "LOGICAL NAME OF THE FIELD (ENG)": "logicEnglish",
            "FIELD DESCRIPTION (ENG)": "descEnglish",
            "FUNCTIONAL GROUPING OF PHYSICAL FIELDS (LEVEL 1)": "funtionalLevel1",
            "FUNCTIONAL GROUPING OF PHYSICAL FIELDS (LEVEL 2)": "functionalLevel2",
            "TAGS (PHYSICAL FIELDS)": "",
            "RELATED FIELDS": "",
            "COUNTRY/GROUP": "",
            "SECURITY CLASS": "",
            "SECURITY LABEL": "",
            "SECURITY STATUS": "",
            "DATE OF REGISTRATION": "created_time",
            "DATE OF LAST MODIFICATION": "modification",
            "NAMING API CATALOG GLOBAL - FORMAT": "",
            "NUMBER OF COUNTRIES USING": "countriesUse",
            "COLOMBIAN FORMAT": "colombianFormat",
            "NAMING DOCUMENT MANAGER - FORMAT": "",
            "USER": ""
        }
        self.new_export_props = {
            "FIELD CODE ID": "field_code_id",
            "GLOBAL NAMING FIELD": "naming",
            "HIERARCHY": "hierarchy",
            "LOGICAL NAME CODE": "logical_code_id",
            "LOGICAL NAME OF THE FIELD (ENG)": "logical_name_eng",
            "LOGICAL NAME OF THE FIELD (SPA)": "logical_name_spa",
            "FIELD DESCRIPTION (ENG)": "field_description_eng",
            "FIELD DESCRIPTION (SPA)": "field_description_spa",
            "GLOBAL MODEL LOGICAL FORMAT (DOMAIN + LENGTH)": "logical_format",
            "NUMBER OF COUNTRIES USING": "number_countries_using",
            "COLOMBIA - MARK OF USE": "colombia_mark",
            "COLOMBIA - FORMAT HDFS_AVRO": "colombia_format_avro",
            "COLOMBIA - FORMAT HDFS_PARQUET": "colombia_format_parquet",
            "SECURITY CLASS": "security_class",
            "SECURITY LABEL": "security_label",
            "SECURITY STATUS": "security_status",
            "FUNCTIONAL GROUPING OF PHYSICAL FIELDS (LEVEL 1)": "functional_grouping_level_1",
            "FUNCTIONAL GROUPING OF PHYSICAL FIELDS (LEVEL 2)": "functional_grouping_level_2",
            "DATE OF REGISTRATION": "date_of_registration",
            "DATE OF LAST MODIFICATION": "date_of_last_modification",
        }
        


    def find_functional_level(self,level):
        """Get all different functional levels (1 or 2)

        :param level: str, string that represent a level to find
        :return: Array
        """
        return self._find_functional_levels_1() if int(level) == 1 else self._find_functional_levels_2()

    def _find_functional_levels_1(self):
        """Get all functional level 1

        :return: Array
        """
        return self.field_repository.find_functional_levels_1()

    def _find_functional_levels_2(self):
        """Get all functional level 2

        :return: Array
        """
        return self.field_repository.find_functional_levels_2()

    def find_hierarchy(self):
        """Get all hierarchies

        :return: Array
        """
        return self.field_repository.find_hierarchy()

    @staticmethod
    def _structure_text(text):
        """Check that all structure hasn't special character and is in uppercase

        :param text: str, String that represent a text to check
        :return: str, string transformed
        """
        if len(text) == 0:
            return text
        text_without_tildes = re.sub(r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+", r"\1", normalize("NFD", text), 0, re.I)
        arr = text_without_tildes.split()
        remove_special_characters = [re.sub('[^A-Za-z0-9]+', '', i).strip().upper() for i in arr]
        res = remove_special_characters[0]
        for i in range(1, len(remove_special_characters)):
            res += " "+remove_special_characters[i]
        return res

    def find_global_query(self, naming, logic, desc, type, level):
        """Do a global query to find a naming from filter defines
        A query its building dynamic, depend what data has it

        :param naming: str, String that reference a naming
        :param logic: str, string that reference a logic name
        :param desc: str, string that reference a naming description
        :param type: str, String that reference a naming type
        :return: Array of json, result that has this filters
        """
        res = []
        query = []
        if naming.strip() == '' and logic.strip() == '' and desc.strip() == '' and type.strip() == '' and level.strip() == '':
            tmp = self.field_repository.find_all_limit(20000)
            for i in tmp:
                res.append(i)
        else:   
            if len(naming) != 0:
                split_naming = naming.split("_")
                temp_suffix = self.suffix_repository.find_one(split_naming[-1])
                if temp_suffix is not None:
                    if len(type) == 0:
                        type = split_naming[-1]
                        split_naming[-1] = ""

                naming_search = []
                for j in split_naming:
                    temp_or = []
                    if len(j) != 0:
                        temp_or.append({"naming.naming": {"$regex": j.lower()}})
                        short = self.word_repository.find_long_or_abbreviation(j)
                        if short is not None and len(short["abbreviation"]) != 0:
                            temp_or.append({"naming.naming": {"$regex": short["abbreviation"].lower()+"_"}})

                    if len(temp_or) == 1:
                        naming_search.append({"naming.naming": {"$regex": j.lower()}})
                    elif len(temp_or) == 2:
                        naming_search.append({"$or": temp_or})

                query.append({"$or": naming_search})

            if len(logic) != 0:
                transform = self._structure_text(logic)
                words = transform.split()
                words_query = []
                for j in words:
                    words_query.append({"logic": {"$regex": j.upper()}})
                if len(words_query) != 1:
                    query.append({"$and": words_query})
                else:
                    query.append(words_query[0])

            if len(desc) != 0:
                transform = self._structure_text(desc)
                words = transform.split()
                words_query = []
                for j in words:
                    words_query.append({"description": {"$regex": j.upper()}})
                if len(words_query) != 1:
                    query.append({"$and": words_query})
                else:
                    query.append(words_query[0])

            if len(type) != 0:
                temp_suffix = self.suffix_repository.find_one(type)
                if temp_suffix is not None:
                    query.append({"naming.suffix": temp_suffix["_id"]})
            
            if len(level) != 0:
                query.append({"level": level })
                    
            tmp = self.field_repository.find_global_query({"$and": query})

            if len(naming) != 0:
                res = self._sort_result(tmp, naming, type)
            else:
                for i in tmp:
                    res.append(i)
        return res

    def _sort_result(self, result, naming, type):
        """

        :param result:
        :param naming:
        :param type:
        :return:
        """
        split_naming = naming.split("_")
        temp_suffix = self.suffix_repository.find_one(split_naming[-1])
        if temp_suffix is not None:
            type = split_naming[-1]
            split_naming = split_naming[:-1]

        dict_result = {}
        for j in range(0, len(split_naming)+1):
            dict_result[j] = []

        temp_naming = "_".join(split_naming) + "_" + type
        naming_exact = self.field_repository.find_exactly_naming(temp_naming)

        if naming_exact is not None:
            dict_result[0].append(naming_exact)

        res = []
        for i in result:
            if temp_naming in i["naming"]["naming"]:
                if naming_exact is not None and i["naming"]["naming"] != naming_exact["naming"]["naming"]:
                    dict_result[0].append(i)
                elif naming_exact is None:
                    dict_result[0].append(i)
            else:
                cont = 0
                for j in range(0, len(split_naming)):
                    short = self.word_repository.find_long_or_abbreviation(split_naming[j])
                    if short is not None and len(short["abbreviation"]) != 0:
                        if split_naming[j].lower() in i["naming"]["naming"].lower() or short["abbreviation"].lower() in i["naming"]["naming"].lower():
                            cont += 1
                    elif split_naming[j].lower() in i["naming"]["naming"].lower():
                        cont += 1

                if cont != 0:
                    if naming_exact is not None and i["naming"]["naming"] != naming_exact["naming"]["naming"]:
                        dict_result[cont].append(i)
                    elif naming_exact is None:
                        dict_result[cont].append(i)

        res += dict_result[0]
        for k in range(3, 0, -1):
            if k in dict_result:
                res += dict_result[k]
        return res

    def find_exactly_naming(self, naming):
        """Get one exactly data of a naming

        :param naming: str, String that reference a naming
        :return:json, dictionary of naming data
        """
        return self.field_repository.find_exactly_naming(naming)

    def find_naming_other_table(self,naming):
        """Get naming that has in other table
        Its generate from a pipeline define check all data dictionaries
        and get ony data that we need

        :param naming: str, String that represent a naming
        :return: Array
        """
        return list(self.data_dictionary_repository.generate_aggreation([
            {"$match": {"FieldReview":{"$elemMatch":{"naming.naming":naming}}}},
            {"$unwind": {"path": "$FieldReview"}},
            {"$match": {"FieldReview.naming.naming": naming}},
            {'$project': {"naming": "$FieldReview.naming.naming", "logic":"$FieldReview.logic",
                          "description": "$FieldReview.description", "table": '$alias', "_id":0}},
            {"$group": {"_id": {"logic": "$logic", "description": "$description"}, "sum": {"$sum": 1},
                        "tables": {"$push": "$table"}}}]))

    def generate_naming_export(self, body):
        """
            Generates a report for an excel file based on query search parameters.

        :param body: json, contains the query data 
        :return: Array
        """
        export = []
        namings = self.find_global_query(body["naming"].strip(), body["logic"].strip(), body["desc"].strip(), body["type"].strip(), body["level"].strip())
        
        for namein in namings:
            tmp = {}
            for prop in self.new_export_props.keys():
                if self.new_export_props[prop] == "naming":
                    try:
                        tmp[prop] = namein["naming"]["naming"]
                    except Exception: 
                        tmp[prop] = ""
                else:
                    try:
                        tmp[prop] = namein[self.new_export_props[prop]]
                    except Exception: 
                        try:
                            tmp[prop] = namein[self.export_props[prop]]
                        except Exception: 
                            tmp[prop] = ""
            export.append(tmp)
        
        return export

    def generate_extra_export(self, amount, type):
        """
            Generates a report for an excel file based on query search parameters.

        :param amount: string, amount of value to limit or skip in the search
        :param type: string, type of search to do, can be 'limit' or 'skip'
        :return: Array
        """
        export = []
        namings = []

        if type == "limit":
            namings = self.field_repository.find_all_limit(amount)
        
        if type == "skip":
            namings = self.field_repository.find_all_skip(amount)
        
        for namein in namings:
            tmp = {}
            for prop in self.new_export_props.keys():
                if self.new_export_props[prop] == "naming":
                    try:
                        tmp[prop] = namein["naming"]["naming"]
                    except Exception: 
                        tmp[prop] = ""
                else:
                    try:
                        tmp[prop] = namein[self.new_export_props[prop]]
                    except Exception: 
                        try:
                            tmp[prop] = namein[self.export_props[prop]]
                        except Exception: 
                            tmp[prop] = ""
            export.append(tmp)
            
        return export

            
    
        
        
        
        
        
        
        
