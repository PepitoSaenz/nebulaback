from nebula.api.persistence.acl_validator_repository import Acl_repository
from nebula.api.services.functional_map_service import Functional_map_service
import re


class Acl_service(object):

    """
        A class used to represent a ACL validator services.

        Attributes
             -------
            acl_repository : Acl_repository
                Reference to ACL repository class to interact with database
    """

    def __init__(self):
        """

        """
        self.acl_repository = Acl_repository()
        self.uuaa_service = Functional_map_service()

    def find_all_alhambra(self, env):
        """Get all alhambra

        :return: Array
        """
        res = []
        for i in self.acl_repository.find_all_alhambra(env):
            res.append(i)
        return res

    def find_all_groups(self, env):
        """

        :return:
        """
        res = []
        for i in self.acl_repository.find_all_groups(env):
            res.append(i)
        return res

    def find_routes_by_group(self, env, group):
        """

        :param env:
        :param group:
        :return:
        """
        res = []
        for i in self.acl_repository.find_route_by_group(env, group):
            res.append(i["route"])
        return res

    def _structure_response(self, array):
        """

        :param array:
        :return:
        """
        res = []
        for j in array:
            res.append({"uuaa": j, "relations": array[j]})
        return res

    def find_all_relationship(self, env):
        """
            Find all the relationship between master uuaa in the functional map and raw uuaa.
        :return:
        """
        alhambra_joins = {}
        query = {"$and": [{"group": {"$regex": ".*DACOW_SE_SP.*"}}, {"route": {"$regex": ".*in/staging/ratransmit.*"}}]}
        if env == "live":
            query = {"$and": [{"group": {"$regex": ".*DACOL_SE_SP.*"}}, {"route": {"$regex": ".*in/staging/ratransmit.*"}}]}
        result = self.acl_repository.find_any_query(env, query)
        for i in result:
            uuaaMaster = str(i["group"]).upper()[11::]
            uuaaRaw = str(i["route"]).lower()
            if len(uuaaRaw.split("/")[-1]) == 0:
                uuaaRaw = uuaaRaw.split("/")[-2]
            else:
                uuaaRaw = uuaaRaw.split("/")[-1]
            if uuaaMaster not in alhambra_joins:
                alhambra_joins[uuaaMaster] = [uuaaRaw.upper()]
            else:
                if uuaaRaw.upper() not in alhambra_joins[uuaaMaster]:
                    alhambra_joins[uuaaMaster].append(uuaaRaw.upper())
        return self._structure_response(alhambra_joins)

    def find_uuaa_relationship(self, env, uuaa):
        """
            Find raw uuaas associated to parameter master uuaa.
        :param env:
        :param uuaa:
        :return:
        """
        alhambra_joins = {}
        query = {"$and": [{"group": {"$regex": ".*DACOW_SE_SP"+str(uuaa).upper()+".*"}}, {"route": {"$regex": ".*in/staging/ratransmit.*"}}]}
        if env == "live":
            query = {
                "$and": [{"group": {"$regex": ".*DACOL_SE_SP"+str(uuaa).upper()+".*"}}, {"route": {"$regex": ".*in/staging/ratransmit.*"}}]}
        result = self.acl_repository.find_any_query(env, query)
        for i in result:
            uuaa_master = str(i["group"]).upper()[11::]
            uuaa_raw = str(i["route"]).lower()
            if len(uuaa_raw.split("/")[-1]) == 0:
                uuaa_raw = uuaa_raw.split("/")[-2]
            else:
                uuaa_raw = uuaa_raw.split("/")[-1]
            if uuaa_master not in alhambra_joins:
                alhambra_joins[uuaa_master] = [uuaa_raw.upper()]
            else:
                if uuaa_raw.upper() not in alhambra_joins[uuaa_master]:
                    alhambra_joins[uuaa_master].append(uuaa_raw.upper())
        return self._structure_response(alhambra_joins)

    def __find_uuaa_relationship_dict(self, env, uuaa):
        """
            Find raw uuaas associated to parameter master uuaa.
        :param env:
        :param uuaa:
        :return:
        """
        alhambra_joins = {}
        query = {"$and": [{"group": {"$regex": ".*DACOW_SE_SP"+str(uuaa).upper()+".*"}}, {"route": {"$regex": ".*in/staging/ratransmit.*"}}]}
        if env == "live":
            query = {
                "$and": [{"group": {"$regex": ".*DACOL_SE_SP"+str(uuaa).upper()+".*"}}, {"route": {"$regex": ".*in/staging/ratransmit.*"}}]}
        result = self.acl_repository.find_any_query(env, query)
        for i in result:
            uuaa_master = str(i["group"]).upper()[11::]
            uuaa_raw = str(i["route"]).lower()
            if len(uuaa_raw.split("/")[-1]) == 0:
                uuaa_raw = uuaa_raw.split("/")[-2]
            else:
                uuaa_raw = uuaa_raw.split("/")[-1]
            if uuaa_master not in alhambra_joins:
                alhambra_joins[uuaa_master] = [uuaa_raw.upper()]
            else:
                if uuaa_raw.upper() not in alhambra_joins[uuaa_master]:
                    alhambra_joins[uuaa_master].append(uuaa_raw.upper())
        return alhambra_joins

    def folder_validation(self, request):
        """

        :param request:
        :return:
        """
        res = {}

        work = request["work"]
        for k in work:
            route = k
            if "#" in k:

                level = self.uuaa_service.find_level_uua(route.split("/")[3].strip().upper())
                if len(level) != 0:
                    route = route.replace("/#/", level)
                else:
                    route = route.replace("/#/", "/")
            alhambra = self.acl_repository.find_any_query("work", {"route": {"$regex": ".*"+route+".*"}})
            if alhambra.count() > 0:
                temp = []
                for j in alhambra:
                    temp.append("Grupo: "+j["group"]+" Ruta: "+j["route"]+" Permisos: "+j["permissions"])
                if route not in res:
                    res[route] = {"route": route, "work": {"inWork": 1, "comments": temp}}
                else:
                    res[route]["work"] = {"inWork": 1, "comments": temp}
            else:
                if route not in res:
                    res[route] = {"route": route, "work": {"inWork": 0, "comments": ["No existen registros para la ruta en Alhambra Work."]}}
                else:
                    res[route]["work"] = {"inWork": 0, "comments": ["No existen registros para la ruta en Alhambra Work."]}

        live = request["live"]
        for k in live:
            route = k
            if "#" in k:
                level = self.uuaa_service.find_level_uua(route.split("/")[3].strip().upper())
                if len(level) != 0:
                    route = route.replace("/#/", level)
                else:
                    route = route.replace("/#/", "/")
            alhambra = self.acl_repository.find_any_query("live", {"route": {"$regex": ".*" + route + ".*"}})
            if alhambra.count() > 0:
                temp = []
                for j in alhambra:
                    temp.append("Grupo: " + j["group"] + " Ruta: " + j["route"] + " Permisos: " + j["permissions"])
                if route not in res:
                    res[route] = {"route": route, "live": {"inLive": 1, "comments": temp}}
                else:
                    res[route]["live"] = {"inLive": 1, "comments": temp}
            else:
                if route not in res:
                    res[route] = {"route": route, "live": {"inLive": 0, "comments": ["No existen registros para la ruta en Alhambra Live."]}}
                else:
                    res[route]["live"] = {"inLive": 0, "comments": ["No existen registros para la ruta en Alhambra Live."]}
        result = []
        for i in res:
            result.append(res[i])
        return result

    def acl_validation(self, env, request):
        """
            e.g:
             DACOL_US_VBOX4||FOLDER/pr/HDFS/CO/data/raw/cpeh/data||R
             DACOL_US_MNCCEG||https://daas.live.co.ether.igrupobbva/argos-front/dcos-userland||R
        :param env:
        :param request:
        :return:
        """
        res = {}
        if env == "work":
            work = request["work"]
            for i in work:
                tmp_group = i.strip().split("||")[0]
                tmp_route = i.strip().split("||")[1]
                tmp_perm = i.strip().split("||")[2]
                tmp_err = self._validation_route_work(tmp_group, tmp_route, tmp_perm)
                tmp_struc = {
                    "route": tmp_route,
                    "access": tmp_perm,
                    "isOk": 0,
                    "comments": tmp_err
                }
                if len(tmp_err) == 0:
                    tmp_struc["isOk"] = 1
                    tmp_struc["comments"] = []
                if tmp_group not in res:
                    res[tmp_group] = {"group": tmp_group, "routes": [tmp_struc]}
                else:
                    res[tmp_group]["routes"].append(tmp_struc)
        elif env == "live":
            live = request["live"]
            for i in live:
                tmp_group = i.strip().split("||")[0]
                tmp_route = i.strip().split("||")[1]
                tmp_perm = i.strip().split("||")[2]
                tmp_err = self._validation_route_live(tmp_group, tmp_route, tmp_perm)
                tmp_struc = {
                    "route": tmp_route,
                    "access": tmp_perm,
                    "isOk": 0,
                    "comments": tmp_err
                }
                if len(tmp_err) == 0:
                    tmp_struc["isOk"] = 1
                    tmp_struc["comments"] = []

                if tmp_group not in res:
                    res[tmp_group] = {"group": tmp_group, "routes": [tmp_struc]}
                else:
                    res[tmp_group]["routes"].append(tmp_struc)
        result = []
        for i in res:
            result.append(res[i])
        return result

    def _validation_route_work(self, group, route, perm):
        """

        :param gruop:
        :param route:
        :param perm:
        :return:
        """
        errors = []
        flag = True
        if route.split("/")[1] != "de" and not "daas.work" in route and flag:
            flag = False
            errors.append("No es una ruta adecuada para ambiente work (FOLDER/de/) " + route)

        if "DACOW_US_" in group and "FOLDER" not in route.upper() and not "daas.work" in route and flag:
            flag = False
            errors.append("No es una ruta adecuada para ambiente work (daas.work) " + route)

        if flag and not self._validate_group(group):
            errors.append("No es un grupo adecuado para work "+group)
            flag = False

        if flag:

            tmp = self.acl_repository.find_route_group_permissions("work", group, route, perm)
            tmp_1 = self.acl_repository.find_route_group("work", group, route)
            tmp_2 = self.acl_repository.find_any_query("work", {"route": route})

            if tmp_2.count() == 0:
                errors.append("La ruta no se encuentra en Alhambra Work.")

            elif tmp_1.count() == 0 and tmp.count() == 0:
                errors.append("La ruta no se ha vinculado al grupo en Alhambra Work.")

            elif tmp.count() == 0 and tmp_1.count() != 0:
                errors.append("Los permisos de la ruta no concuerdan con los permisos de Alhambra Work.")

            if "DACOW_SE_" in group:
                if "schemas" in route:
                    if perm != "R":
                        errors.append("Validar los permisos de la ruta, solo se permite lectura (R) para esquemas.")
                else:
                    if perm != "RWX":
                        errors.append("Validar los permisos de la ruta, se espera RWX (todos los permisos).")

            elif "DEV_" in group:
                if "schemas" in route or "refusals" in route:
                    if perm != "R":
                        errors.append("Validar los permisos de la ruta, solo se permite lectura (R) para esquemas.")
                else:
                    if perm != "RWX":
                        errors.append("Validar los permisos de la ruta, se espera RWX (todos los permisos).")

            elif "ARQ_CO" in group:
                    if perm != "R":
                        errors.append("Validar los permisos de la ruta, solo se permite lectura (R) para esquemas.")
                    else:
                        if perm != "RWX":
                            errors.append("Validar los permisos de la ruta, se espera RWX (todos los permisos).")

            elif "DACOW_US_MN" in group:
                if perm != "R":
                    errors.append("Validar los permisos de la ruta, solo se permite lectura (R) para esquemas.")

            if "DACOW_SE_SP" in group:
                temp = self.__find_uuaa_relationship_dict("work", group.upper()[11::])
                temp = temp[group.upper()[11::]]
                route_uuaa_raw = str(route).lower().split("/")
                uuaa_raw = ""
                for i in route_uuaa_raw:
                    if len(i) == 4 and i != "data" and i != "hdfs" and (i[0] == "c" or i[0] == "k"):
                        uuaa_raw = i
                if uuaa_raw.upper() not in temp:
                    errors.append("No existe la relación de las uuaas "+uuaa_raw.upper()+" / "+group.upper()[11::])
        return errors

    def _validate_group(self, group):
        profiles_work = ["DACOW_SE_", "DEV_", "ARQ_CO", "DACOW_US_"]
        flag_group = False
        for k in profiles_work:
            if k in group:
                flag_group = True
        return flag_group

    def _validate_group_live(self, group):
        profiles_live = ["DACOL_SE_SP", "DACOL_SE_EG", "DACOL_US_LS", "ARQ_CO", "DACOL_US_MN", "DACOL_US_VBOX4", "DACOL_US_DW"]
        flag_group = False
        for k in profiles_live:
            if k in group:
                flag_group = True
        return flag_group

    def _validation_route_live(self, group, route, perm):
        """

        :param gruop:
        :param route:
        :param perm:
        :return:
        """
        errors = []
        flag = True
        if route.split("/")[1] != "pr" and not "daas.live" in route and flag:
            flag = False
            errors.append("No es una ruta adecuada para ambiente live (FOLDER/pr/) " + route)

        if ("DACOL_US_MN" in group or "DACOL_US_DW" in group) and "FOLDER" not in route.upper() and not "daas.live" in route and flag:
            flag = False
            errors.append("No es una ruta adecuada para ambiente live (daas.live) " + route)

        if flag and not self._validate_group_live(group):
            errors.append("No es un grupo adecuado para live " + group)
            flag = False

        if flag:

            tmp = self.acl_repository.find_route_group_permissions("live", group, route, perm)
            tmp_1 = self.acl_repository.find_route_group("live", group, route)
            tmp_2 = self.acl_repository.find_any_query("live", {"route": route})

            if tmp_2.count() == 0:
                errors.append("La ruta no se encuentra en Alhambra Live.")

            elif tmp_1.count() == 0 and tmp.count() == 0:
                errors.append("La ruta no se ha vinculado al grupo en Alhambra Live.")

            elif tmp.count() == 0 and tmp_1.count() != 0:
                errors.append("Los permisos de la ruta no concuerdan con los permisos de Alhambra Live.")

            if "DACOL_SE_SP" in group:
                if perm != "RWX":
                    errors.append("Validar los permisos de la ruta, se espera RWX (todos los permisos).")

            if "DACOL_US_LS" in group:
                if perm != "R":
                    errors.append("Validar los permisos de la ruta, solo se permite lectura (R) para esquemas.")

            if "ARQ_CO" in group:
                if perm != "R":
                        errors.append("Validar los permisos de la ruta, solo se permite lectura (R) para esquemas.")
                else:
                    if "schemas" not in route:
                        errors.append("Validar la ruta para el perfil ARQ_CO. Solo rutas de esquemas permitidas.")

            if "DACOL_US_VBOX4" in group:
                if perm != "R":
                    errors.append("Validar los permisos de la ruta, solo se permite lectura (R) para esquemas.")

            if "DACOL_SE_SP" in group:
                temp = self.__find_uuaa_relationship_dict("live", group.upper()[11::])
                temp = temp[group.upper()[11::]]
                route_uuaa_raw = str(route).lower().split("/")
                uuaa_raw = ""
                for i in route_uuaa_raw:
                    if len(i) == 4 and i != "data" and i != "hdfs" and (i[0] == "c" or i[0] == "k"):
                        uuaa_raw = i
                if uuaa_raw.upper() not in temp:
                    errors.append(
                        "No existe la relación de las uuaas " + uuaa_raw.upper() + " / " + group.upper()[11::])

        return errors

    def acl_accounts_validation(self, env, request):
        """
            {"$and":[{"group":"DACOW_US_FDEV6"}, {"users":{"$elemMatch":{"mail":"deborah.gabisson.contractor@bbva.com"}}}]}
        :param env:
        :return:
        """
        users = request["users"]
        user_sheet = []
        for j in users:
            mail = j.strip().split("||")[1]
            user_sheet.append(mail)
        accounts = request["accounts"]
        res = {}
        for i in accounts:
            errors = []
            tmp = i.strip().split("||")
            mail = tmp[1]
            group = tmp[2]

            group_query = self.acl_repository.find_one_group(group, env)
            user_query = self.acl_repository.find_user_in_group(env, group, mail)

            if group_query is None:
                errors.append("No se encuentra el grupo en alhambra "+env+".")
            else:
                if user_query:
                    errors.append("Se encuentra el usuario vinculado al grupo.")
                else:
                    if mail in user_sheet:
                        errors.append("No se encuentra vinculado al grupo, pero se encuentra en la hoja de users.")
                    else:
                        errors.append("No se encuentra vinculado al grupo y no esta en la hoja de users.")

            res[i] = {
                "group": group,
                "user": mail,
                "comments": errors
            }

        result = []
        for i in res:
            result.append(res[i])
        return result
