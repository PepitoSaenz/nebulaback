from nebula.api.entities.project import Project

from nebula.api.persistence.project_repository import *
from nebula.api.persistence.use_case_repository import *
from nebula.api.persistence.backlog_repository import *
from nebula.api.persistence.data_dictionary_repository import *
from nebula.api.persistence.requests_repository import *
from nebula.api.persistence.team_repository import *
from nebula.api.persistence.user_repository import User_repository
from nebula.api.persistence.data_table_repository import Data_table_repository

from nebula.api.services.data_dictionary_service import Data_dictionary_service
from nebula.api.services.data_table_service import Data_table_service
import datetime


class Project_service(object):

    """
        A class used to represent a projects services.

        Attributes
            -------
            project_repository : Project_repository
                Reference to Project repository class to interact with a database
            use_case_repository: Use_case_repository
                Reference to Use case repository class to interact with a database
            backlog_repository: Backlog_repository
                Reference to backlog repository class to interact with a database
    """

    def __init__(self):
        self.project_repository = Project_repository()
        self.use_case_repository = Use_case_repository()
        self.backlog_repository = Backlog_repository()
        self.request_repository = Requests_repository()
        self.team_repository = Team_repository()
        self.user_repository = User_repository()
        self.data_dict_service = Data_dictionary_service()
        self.data_dictionary_repository = Data_dictionary_repository()
        self.data_table_service = Data_table_service()
        self.data_table_repository = Data_table_repository()
        self.values = {"G": "Gobierno", "A": "Arquitectura", "N": "En propuesta namings", "I": "Para ingestar",
                       "IN": "Ingestada en desarrollo", "Q": "Calidad", "M": "Mallas", "P": "Prduccion",
                       "ungoverned": "En backlog", "PA": "Pendiente aprobación backlog", "RN": "Revisión Negocio",
                       "R": "Revisión solicitud", "D": "Descartado", "F": "Gobierno funcional"}

    def find_all(self):
        """Get all projects

        :return: cursor, array of json with all projects
        """
        result = []
        temp = self.project_repository.find_all()
        for j in temp:
            j["_id"] = str(j["_id"])
            temp_use = []
            for k in j["useCases"]:
                temp_use.append(str(k))
            j["useCases"] = temp_use

            if "VoBo" in j:
                try:
                    temp_vobo = []
                    for k in j["VoBo"]:
                        temp_vobo.append(str(k))
                    j["VoBo"] = temp_vobo
                except:
                    j["VoBo"] =str(j["VoBo"])
            
            if "teams" in j:
                try:
                    temp_teams = []
                    for k in j["teams"]:
                        temp_teams.append(str(k))
                    j["teams"] = temp_teams
                except:
                    j["teams"] =str(j["teams"])
            
            try:
                if len(j["modifications"]) > 0:
                    j["modifications"] = {
                        "user_name": self.user_repository.find_one(str(j["modifications"][0]["user"])) ["name"],
                        "date": j["modifications"][0]["date"]
                    }
                else:
                    j["modifications"] = {"user_name": "N/A sin", "date": "N/A"}
            except Exception as e:
                j["modifications"] = {"user_name": "N/A", "date": "N/A"}
                
            result.append(j)
        return result

    def insert_one(self, project):
        """Insert one project

        :param project: json, dictionary with a project data to insert
        :return:
        """
        count = self.project_repository.find_any_query({
            "countries": {"$in": project["countries"]}, "name": project["name"]
        }).count()

        if count == 0:

            project_str = Project({}).data
            project_str["name"] = project["name"]
            project_str["shortDesc"] = project["shortDesc"]
            project_str["countries"] = project["countries"]

            teams = []
            for i in project["teams"]:
                teams.append(ObjectId(str(i)))
            project_str["teams"] = teams

            owners = []
            for u in project["owners"]:
                owners.append(ObjectId(str(u)))
            project_str["owners"] = owners

            vobo = []
            if "VoBo" in project:
                for t in project["VoBo"]:
                    vobo.append(ObjectId(str(t)))

            project_str["VoBo"] = vobo
            self.project_repository.insert_one(project_str)

        else:
            raise ValueError("El proyecto ya se encuentra registrado.")

    def find_to_rol(self, rol, user_id):
        """Get projects from user rol

        :param rol: str, string that represent a user rol
        :param user_id: str, string that represent a user id
        :return: Array of json, with projects data
        """
        res = []
        if rol == "PO":
            res = self._find_product_owner(user_id)
        elif rol == "SPO":
            res = self.find_all()
        return res

    def _find_product_owner(self,product_owner_id):
        """Get all projects associate to a product owner

        :param product_owner_id: str, sring that reprj["modifications"][-1]esent a product owner id
        :return: Array of json, with projects names and id's
        """
        projects = self.project_repository.find_all()
        res = []
        for i in projects:
            if product_owner_id in i["owners"]:
                res.append({
                    "_id": i["_id"], "name": i["name"]
                })
        return res

    def update_one(self, project_id, project):
        """
            Update project data

        :param project_id: str, string that represent project's id
        :param project: json,
        :return:
        """
        project_bd = self.project_repository.find_one(project_id)
        if project_bd is None:
            raise ValueError("El proyecto no se encuentra registrado.")

        if "user_id" not in project:
            raise ValueError("Usuario no autorizado para realizar cambios.")

        user_id = self.user_repository.find_one(project["user_id"])
        if user_id is None:
            raise ValueError("Usuario no autorizado para realizar cambios.")

        count = self.project_repository.find_any_query({
            "countries": {"$in": project["countries"]}, "name": project["name"]
        }).count()
        if count <= 0:
            del project_bd["_id"]
            teams = []
            for p in project["teams"]:
                teams.append(ObjectId(str(p)))

            owners = []
            for j in project["owners"]:
                owners.append(ObjectId(str(j)))

            vobo = []
            if "VoBo" in project:
                for t in project["VoBo"]:
                    vobo.append(ObjectId(str(t)))

            project_bd["name"] = project["name"]
            project_bd["shortDesc"] = project["shortDesc"]
            project_bd["teams"] = teams
            project_bd["owners"] = owners
            project_bd["VoBo"] = vobo

            temp = [{"user": ObjectId(str(user_id["_id"])),
                     "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]
            for i in project_bd["modifications"]:
                temp.append({
                    "user": ObjectId(str(i["user"])),
                    "date": i["date"]})
            project_bd["modifications"] = temp[:20]
            self.project_repository.update_one(project_id, project_bd)
        else:
            raise ValueError("El proyecto ya se encuentra registrado.")

    def find_one(self, project_id):
        """Get one specific project data

        :param project_id: str, string that represent a project id
        :return: Json, dictionary with project data
        """
        project = self.project_repository.find_one(project_id)
        project["_id"] = str(project["_id"])
        if len(project["modifications"]) > 0:
            project["modifications"] = {
                "user_name": self.user_repository.find_one(str(project["modifications"][0]["user"]))["name"],
                "date": project["modifications"][0]["date"]}
        else:
            project["modifications"] = {"user_name": "N/A", "date": "N/A"}
        return project

    def use_cases_project(self, project_id):
        """Get all use case associate to a project

        :param project_id: str, string that represent a project id
        :return: Array of json, with use case data
        """
        try:
            project = self.project_repository.find_one(project_id)
            res = []
            for i in project["useCases"]:
                use_case = self.use_case_repository.find_one(str(i))
                if len(use_case["modifications"]) != 0:
                    use_case["modifications"] = {
                        "user_name": self.user_repository.find_one(str(use_case["modifications"][0]["user"]))["name"],
                        "date": use_case["modifications"][0]["date"]}
                else:
                    project["modifications"] = {"user_name": "N/A", "date": "N/A"}

                if use_case["startDate"] != "":
                    use_case["startDate"] = use_case["startDate"].strftime("%Y-%m-%d")
                if use_case["finishDate"] != "":
                    use_case["finishDate"] = use_case["finishDate"].strftime("%Y-%m-%d")
                requests = self.request_repository.find_any_query({"target": "U", "action": "Add",
                  "idProject": ObjectId(project_id), "idUsecase": ObjectId(str(i))})

                try:
                    use_case["request_date"] = requests[0]["created_time"].strftime("%Y-%m-%d")
                except Exception as e:
                    use_case["request_date"] = ''

                res.append(use_case)
            return res
        except Exception as e:
            print(e)

    def action_in_project(self, project_id, action, data):
        """Check what action to do in a project if add or remove a use case

        :param project_id: str, string that represent a project id
        :param action: str, string that represent that action do
        :param data: json, dictionary with data to interact
        :return:
        """
        if action == "add":
            self._insert_data_to_project(project_id, data)
        elif action == "remove":
            self._remove_data_to_project(project_id, data)

    def _insert_data_to_project(self, project_id, data):
        """Update/insert data to insert something

        :param project_id: str, string that represent a project id
        :param data: json, dictionary with that to insert
        :return:
        """
        project_bd = self.project_repository.find_one(project_id)
        if project_bd is None:
            raise ValueError("El proyecto no se encuentra registrado.")

        if "user_id" not in data:
            raise ValueError("Usuario no autorizado para realizar cambios.")

        user_id = self.user_repository.find_one(data["user_id"])
        if user_id is None:
            raise ValueError("Usuario no autorizado para realizar cambios.")

        temp = [{"user": ObjectId(str(user_id["_id"])),
                 "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]

        for i in project_bd["modifications"]:
            temp.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]
            })
        project_bd["modifications"] = temp[:20]
        self.project_repository.update_modifications(project_id,  project_bd["modifications"])
        self.project_repository.insert_data_to_project(project_id, data["field"], data["data"])
    
    def _update_vobo_to_project(self, project_id, data):
        """
            Updates VoBo array of one Project with an user id data
        :param project_id: str, string that represent a project id
        :param data: json, dictionary with data to update
        :return:
        """
        if data["action"] == "add":
            self.project_repository.add_vobo(project_id, data["_id"])
        elif data["action"] == "remove":
            self.project_repository.remove_vobo(project_id, data["_id"])
        result = []

        for vobo in self.project_repository.find_one(project_id)["VoBo"]:
            result.append(str(vobo))

        return result

    def _check_vobo_in_projects(self, user_id):
        """
            Updates VoBo array of one Project with an user id data
        :param project_id: str, string that represent a project id
        :param data: json, dictionary with data to update
        :return:
        """
        all_projects = self.project_repository.find_all()
        result = []

        for proj in all_projects:
            try:
                if ObjectId(str(user_id)) in proj["VoBo"]:
                    result.append(proj["name"])
            except Exception as e:
                print(proj["name"])

        return result

    def _remove_data_to_project(self, project_id, data):
        """Remove any data into a project

        :param project_id: str, string that represent a project id
        :param data: json, dictionary with check to remove
        :return:
        """
        project_bd = self.project_repository.find_one(project_id)
        if project_bd is None:
            raise ValueError("El proyecto no se encuentra registrado.")

        if "user_id" not in data:
            raise ValueError("Usuario no autorizado para realizar cambios.")

        user_id = self.user_repository.find_one(data["user_id"])
        if user_id is None:
            raise ValueError("Usuario no autorizado para realizar cambios.")

        temp = [{"user": ObjectId(str(user_id["_id"])),
                 "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]
        for i in project_bd["modifications"]:
            temp.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]
            })
        project_bd["modifications"] = temp[:20]
        self.project_repository.update_modifications(project_id, project_bd["modifications"])
        self.project_repository.remove_data_to_project(project_id, data["field"], data["data"])

    def find_actions_projects(self, project_id, action):
        """Check what action to do in a project if find a use case data or backlog data

        :param project_id: str, string that represent a project id
        :param action: str, string that represent what to do
        :return: Array of json, with data that want
        """
        res = []
        if action == "useCases":
            res = self._find_use_cases(project_id)
        elif action == "backlogs":
            res = self._find_backlogs(project_id)
        return res

    def _find_use_cases(self, project_id):
        """Get use case data associate to a project

        :param project_id: str, string that represent a project id
        :return: Array of json, with use case data
        """
        use_cases = self.project_repository.find_project_use_cases(project_id)
        res = []
        for i in use_cases["useCases"]:
            tmp_use_cases = self.use_case_repository.find_one(i)
            res.append({
                "_id": str(tmp_use_cases["_id"]), "name": tmp_use_cases["name"], "tables": tmp_use_cases["tables"]
            })
        return res

    def _find_backlogs(self, project_id):
        """Get backlog data associate to a project

        :param project_id: str, string that represent a project id
        :return: Array of json, with backlog data
        """
        tables = self._find_use_cases(project_id)
        res = []
        for i in tables:
            for j in i["tables"]:
                tmp_backlog = self.backlog_repository.find_one(j)
                res.append({
                    "idProject": project_id, "projectName": self.project_repository.find_name(project_id)["name"],
                    "idUseCase": i["_id"], "useCase": i["name"], "backlogName": tmp_backlog["baseName"], "idBacklog": str(tmp_backlog["_id"])
                })
        return res

    def find_use_case_backlog_tablon(self, project_id, use_case_name, backlog_name, tablon_name):
        """

        :param project_id:
        :param use_case_name:
        :param backlog_name:
        :param tablon_name:
        :return:
        """
        use_case_name = use_case_name.strip()
        backlog_name = backlog_name.strip()
        tablon_name = tablon_name.strip()

        use_cases = self.use_cases_project(project_id)
        query = []
        if use_case_name != "":
            for i in use_cases:
                if use_case_name.lower() in i["name"].lower():
                    if len(i["modifications"]) > 0:
                        i["modifications"] = {
                            "user_name": self.user_repository.find_one(str(i["modifications"][0]["user"]))["name"],
                            "date": i["modifications"][0]["date"]}
                    else:
                        i["modifications"] = {"user_name": "N/A", "date": "N/A"}
                    query.append(i)
        else:
            query = use_cases

        result = []
        if backlog_name != "":
            for p in query:
                tables = []
                tmp_all_tables = self._structure_all_backlogs(str(p["_id"]), p["tables"])
                for j in tmp_all_tables:
                    if backlog_name.lower() in j["baseName"].lower():
                        tables.append(j)
                p["tables"] = tables
        else:
            for p in query:
                p["tables"] = self._structure_all_backlogs(str(p["_id"]), p["tables"])

        if tablon_name != "":
            for p in query:
                data_tables = []
                tmp_all_data_tables = self._structute_all_data_tables(str(p["_id"]), p["tablones"])
                for j in tmp_all_data_tables:
                    if tablon_name.lower() in j["baseName"].lower():
                        data_tables.append(j)
                p["tablones"] = data_tables
        else:
            for p in query:
                p["tablones"] = self._structute_all_data_tables(str(p["_id"]), p["tablones"])

        for k in query:
            k["_id"] = str(k["_id"])
            result.append(k)
        print(result)
        return result

    def _structute_all_data_tables(self, use_case_id, query):
        """

        :param use_case_id:
        :param query:
        :return:
        """
        result = []
        for j in query:
            tmp_backlog = self.data_table_repository.find_by_id(str(j))
            tmp_backlog["_id"] = str(j)
            tmp_backlog["user"] = str(tmp_backlog["user"])
            tmp_backlog["viewStateTable"] = ""
            tmp_backlog["viewStateTable"] = self.values[tmp_backlog["stateTable"]]
            requests = self.request_repository.find_any_query({"$and": [{"target": "T"},
                                                                        {"idUsecase": ObjectId(str(use_case_id))},
                                                                        {"idBacklog": ObjectId(str(j))}]})
            tmp_backlog["request_state"] = requests[0]["stateRequest"]
            if requests.count() == 0:
                tmp_backlog["request_date"] = ''
            else:
                tmp_backlog["request_date"] = requests[0]["created_time"].strftime("%Y-%m-%d")

            tmp_backlog["origin_tables"] = []
            result.append(tmp_backlog)
        return result

    def _structure_all_backlogs(self, use_case_id, query):
        """
        :return:
        """
        result = []
        for o in query:
            tmp_backlog = self.backlog_repository.find_one(str(o))
            if len(tmp_backlog["modifications"]) > 0:
                tmp_backlog["modifications"] = {
                    "user_name": self.user_repository.find_one(str(tmp_backlog["modifications"][0]["user"]))["name"],
                    "date": tmp_backlog["modifications"][0]["date"]}
            else:
                tmp_backlog["modifications"] = {"user_name": "N/A", "date": "N/A"}
            tmp_backlog["_id"] = str(tmp_backlog["_id"])
            tmp_backlog["idTable"] = str(tmp_backlog["idTable"])
            tmp_backlog["stateTable"] = ""
            tmp_backlog["viewStateTable"] = ""
            if len(tmp_backlog["idTable"]) > 0:
                tmp_backlog["stateTable"] = self.data_dictionary_repository.find_one(tmp_backlog["idTable"])[
                    "stateTable"]
                tmp_backlog["viewStateTable"] = self.values[tmp_backlog["stateTable"]]
            tmp_backlog["productOwner"] = str(tmp_backlog["productOwner"])
            requests = self.request_repository.find_any_query({"$and": [{"target": "B"}, {"action": "Add"},
                                                                        {"idUsecase": ObjectId(str(use_case_id))},
                                                                        {"idBacklog": ObjectId(str(o))}]})
            if requests.count() == 0:
                tmp_backlog["request_date"] = ''
            else:
                tmp_backlog["request_date"] = requests[0]["created_time"].strftime("%Y-%m-%d")
            result.append(tmp_backlog)
        return result

    def find_project_vobo(self, user_id):
        """

        :param user_id:
        :return:
        """
        res = []
        for j in self.project_repository.find_vobo(user_id):
            tmp_cases = []
            for case in j["useCases"]:
                tmp_cases.append(str(case))
            try:
                if len(j["modifications"]) > 0:
                    j["modifications"] = {
                        "user_name": self.user_repository.find_one(str(j["modifications"][0]["user"]))["name"],
                        "date": j["modifications"][0]["date"]}
                else:
                    j["modifications"] = {"user_name": "N/A", "date": "N/A"}
            except Exception as e:
                print(e)
                j["modifications"] = {"user_name": "N/A", "date": "N/A"}

            j["_id"] = str(j["_id"])
            j["useCases"] = tmp_cases
            res.append(j)
        return res

    def find_project_teams(self, project_id):
        """

        :param project_id:
        :return:
        """
        teams = []
        project = self.project_repository.find_one(project_id)
        if project is not None:
            for j in project["teams"]:
                tmp = self.team_repository.find_one(str(j))
                if tmp is not None:
                    teams.append({"_id": str(tmp["_id"]), "name": tmp["name"]})
        return teams

    def update_vobo(self, project_id, project):
        """

        :param project_id:
        :return:
        """
        project_bd = self.project_repository.find_one(project_id)
        if project_bd is None:
            raise ValueError("El proyecto no se encuentra registrado.")

        if "user_id" not in project:
            raise ValueError("Usuario no autorizado para realizar cambios.")

        user_id = self.user_repository.find_one(project["user_id"])
        if user_id is None:
            raise ValueError("Usuario no autorizado para realizar cambios.")

        self.project_repository.add_vobo(project_id, project["VoBo"])

        temp = [{"user": ObjectId(str(user_id["_id"])),
                 "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]

        for i in project_bd["modifications"]:
            temp.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]
            })
        project_bd["modifications"] = temp[:20]
        self.project_repository.update_modifications(project_id, project_bd["modifications"])

    def generate_excels(self, project_id, use_case_id):
        """

        :return:
        """
        try:
            if len(project_id.strip()) == 0 or len(use_case_id.strip()) == 0:
                res = self.generate_excels_all()
            else:
                if len(use_case_id.strip()) == 0:
                    res = self.generate_excels_project_use_case(project_id.strip(), use_case_id.strip())
                else:
                    res = self.generate_excels_project(project_id.strip())
            return res
        except Exception as e:
            raise ValueError(e)

    def generate_excels_all(self):
        """
            Object / Fields excel files
        :return:
        """
        result_object_raw = []
        result_object_master = []
        result_fields_raw = []
        result_fields_master = []

        for i in self.project_repository.find_all():
            for j in i["useCases"]:
                temp = self.use_case_repository.find_one(str(j))
                for k in temp["tables"]:
                    table_temp = self.backlog_repository.find_one(str(k))
                    if table_temp is not None and str(table_temp["idTable"]) != "":
                        temp_files = self.data_dict_service.generate_excel_backlog(str(table_temp["idTable"]), "Object")
                        if len(temp_files[0]) != 0:
                            temp_files[0]["PROJECT"] = i["name"]
                            temp_files[0]["USE CASE"] = temp["name"]
                            result_object_raw.append(temp_files[0])

                        if len(temp_files[1]) != 0:
                            temp_files[1]["PROJECT"] = i["name"]
                            temp_files[1]["USE CASE"] = temp["name"]
                            result_object_master.append(temp_files[1])

                        temp_fields = self.data_dict_service.generate_excel_backlog(str(table_temp["idTable"]), "Fields")
                        if len(temp_fields[0]) != 0:
                            result_fields_raw += temp_fields[0]
                        if len(temp_fields[1]) != 0:
                            result_fields_master += temp_fields[1]

        res = {"object": {"raw": result_object_raw, "master": result_object_master},
               "fields": {"raw": result_fields_raw, "master": result_fields_master}}
        return res

    def generate_excels_project(self, project_id):
        """

        :return:
        """
        result_object_raw = []
        result_object_master = []
        result_fields_raw = []
        result_fields_master = []

        project = self.project_repository.find_one(project_id)
        if project is not None:
            for j in project["useCases"]:
                temp = self.use_case_repository.find_one(str(j))
                for k in temp["tables"]:
                    table_temp = self.backlog_repository.find_one(str(k))
                    if table_temp is not None and str(table_temp["idTable"]) != "":
                        temp_files = self.data_dict_service.generate_excel_backlog(str(table_temp["idTable"]), "Object")
                        if len(temp_files[0]) != 0:
                            temp_files[0]["USE CASE"] = temp["name"]
                            result_object_raw.append(temp_files[0])

                        if len(temp_files[1]) != 0:
                            temp_files[1]["USE CASE"] = temp["name"]
                            result_object_master.append(temp_files[1])

                        temp_fields = self.data_dict_service.generate_excel_backlog(str(table_temp["idTable"]), "Fields")
                        if len(temp_fields[0]) != 0:
                            result_fields_raw += temp_fields[0]

                        if len(temp_fields[1]) != 0:
                            result_fields_master += temp_fields[1]

        res = {"object": {"raw": result_object_raw, "master": result_object_master},
               "fields": {"raw": result_fields_raw, "master": result_fields_master}}
        return res

    def generate_excels_project_use_case(self, project_id, use_case_id):
        """

        :param project_id:
        :param use_case_id:
        :return:
        """
        result_object_raw = []
        result_object_master = []
        result_fields_raw = []
        result_fields_master = []

        project = self.project_repository.find_one(project_id)
        if project is not None:
            temp = self.use_case_repository.find_one(str(use_case_id))
            for k in temp["tables"]:
                table_temp = self.backlog_repository.find_one(str(k))
                if table_temp is not None and str(table_temp["idTable"]) != "":
                    temp_files = self.data_dict_service.generate_excel_backlog(str(table_temp["idTable"]), "Object")
                    if len(temp_files[0]) != 0:
                        result_object_raw.append(temp_files[0])

                    if len(temp_files[1]) != 0:
                        result_object_master.append(temp_files[1])

                    temp_fields = self.data_dict_service.generate_excel_backlog(str(table_temp["idTable"]), "Fields")
                    if len(temp_fields[0]) != 0:
                        result_fields_raw += temp_fields[0]

                    if len(temp_fields[1]) != 0:
                        result_fields_master += temp_fields[1]

        res = {"object": {"raw": result_object_raw, "master": result_object_master},
               "fields": {"raw": result_fields_raw, "master": result_fields_master}}
        return res

    def find_table_by_filter(self, table_name, alias, project_id, table_state, logic_name, table_desc, backlog_name, base_type):
        """
        Return a list of tables that have this physical name or alias
        :param table_name: str, that represents the physical name of the table
        :param alias: str, that represents the alias of the table
        :param project_id: ObjectId, that represents the identifier of the project
        :param table_state: str, represents the state of the table
        :param logic_name: str, represents the base name of the table
        :param table_desc: str, represents the observation field of the table
        :param base_type: str,
        :return: a list with tables
        """
        if base_type.strip().lower() == "dd":
            tables = self.data_dict_service.find_table_by_filter(table_name, alias, project_id, table_state, logic_name, table_desc, backlog_name)
        elif base_type.strip().lower() == "dt":
            tables = self.data_table_service.find_table_by_filter(table_name, alias, project_id, table_state, logic_name, table_desc)
        else:
            tables_1 = self.data_dict_service.find_table_by_filter(table_name, alias, project_id, table_state, logic_name, table_desc, backlog_name)
            tables_2 = self.data_table_service.find_table_by_filter(table_name, alias, project_id, table_state, logic_name, table_desc)
            tables = tables_1 + tables_2
        return tables


    def generate_naming_export(self, body):
        """
            Generates a report for an excel file based on query search parameters.

        :param body: json, contains the query data 
        :return: Array
        """
        export = []
        query_data = self.find_table_by_filter(body["table_name"].strip(), body["alias"].strip(), body["project_id"].strip(), 
                                               body["table_state"].strip(), body["logic_name"].strip(), body["table_desc"].strip(), 
                                               body["backlog_name"].strip(), body["base_type"].strip())
        
        for single in query_data:
            tmp = {
                "Proyectos": str(single["project_name"]),
                "Nombre Logico": single["baseName"],
                "Nombre Raw": single["raw_name"],
                "UUAA Raw": single["uuaaRaw"],
                "Nombre Master": single["master_name"],
                "UUAA Master": single["uuaaMaster"],
                "Nombre Legacy": single["legacy_name"],
                "Alias": single["alias"],
                "Tipo": "Tabla" if single["type"] == "T" else "Tablon",
                "Descripcion": single["observationField"],
            }
            export.append(tmp)
            
        return export

    def update_project_data(self, user_id, body):
        """
            Updates all sent data to one user
        :param user_id:
        :param user_data:
        :return:
        """
        admin = self.user_repository.find_one(user_id)
        if admin is not None and admin["rol"] == "A":
            old_project_data = self.project_repository.find_one(body["_id"])
            tmp_teams = []
            for team in body["teams"]:
                tmp_teams.append(ObjectId(team))
            tmp_vobos = []
            for vobo in body["VoBo"]:
                tmp_vobos.append(ObjectId(vobo))
            tmp_case = []
            for case in body["useCases"]:
                tmp_case.append(ObjectId(case))

            proj_data = body
            proj_data["teams"] = tmp_teams
            proj_data["VoBo"] = tmp_vobos
            proj_data["useCases"] = tmp_case
            proj_data["modification"] = { "old_data": str(old_project_data), "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}

            del proj_data["_id"]
            self.project_repository.update_project_data(old_project_data["_id"], proj_data)
        else:
            raise ValueError("You don't have permission to access to this resource.")
    
    def create_project(self, user_id, body):
        """
            Creates a project from scratch with an use case integrated
        :param user_id:
        :param user_data:
        :return:
        """
        print(body)
        admin = self.user_repository.find_one(user_id)
        if admin is not None and admin["rol"] == "A": 
            tmp_teams = []
            for team in body["teams"]:
                tmp_teams.append(ObjectId(team))
            tmp_vobos = []
            for vobo in body["VoBo"]:
                tmp_vobos.append(ObjectId(vobo))
            
            new_use_case_data = {
                "name": body["name"].upper(),
                "description": body["shortDesc"],
                "tables": [],
                "tablones": [],
                "startDate": "",
                "finishDate": "",
                "modifications":[{"action": "create", "user": ObjectId(user_id), "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}],
                "created_time": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            }
            new_use_case_id = self.use_case_repository.insert_one(new_use_case_data)
            print(new_use_case_id)

            body["useCases"].append(ObjectId(new_use_case_id))
            body["teams"] = tmp_teams
            body["VoBo"] = tmp_vobos
            body["modifications"] = {"action": "create", "user": ObjectId(user_id), "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
            body["created_time"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

            self.project_repository.insert_one(body)
        else:
            raise ValueError("You don't have permission to access to this resource.")

