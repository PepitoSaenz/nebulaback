from nebula.api.services.field_service import *

from nebula.api.persistence.data_table_repository import *
from nebula.api.persistence.backlog_repository import *
from nebula.api.persistence.use_case_repository import *
from nebula.api.persistence.project_repository import *
from nebula.api.persistence.user_repository import *
from nebula.api.persistence.functional_map_repository import *
from nebula.api.persistence.team_repository import *
from nebula.api.persistence.excels_repository import *
from nebula.api.persistence.requests_repository import *

from nebula.api.entities.field_data_table import *
from nebula.api.entities.object_data_table import *

import ast
import datetime
import traceback
import json



class Data_table_service(object):

    """
        A class used to represent a Functional map services.

        Attributes
             -------
            backlog_repository : Backlog_repository
                Reference to Backlog repository class to interact with database
            user_repository: User_repository
                Reference to User repository class to interact with database
            project_repository: Project_repository
                Reference to Project repository class to interact with database
            use_case_repository: Use_case_repository
                Reference to Use case repository class to interact with database
            data_dictionary_repository: Data_dictionary_repository
                Reference to Data dictionary repository class to interact with database
            field_repository: Field_repository
                Reference to Field repository class to interact with database
            functional_map_repository: Functional_map_repository
                Reference to Functional map repository class to interact with database
            team_repository: Team_repository
                Reference to Team repository class to interact with database
            field_service: field_service
                Reference to Field repository class to interact with database tih they services
            excels_repository: Excels_repository
                Reference to Excel repository class to interact with database
            file: Dict
                Format of extend files
            state: Dict
                Correct user format to value of 0 and 1
    """

    def __init__(self):
        self.data_dictionary_repository = Data_dictionary_repository()
        self.backlog_repository = Backlog_repository()
        self.user_repository = User_repository()
        self.project_repository = Project_repository()
        self.use_case_repository = Use_case_repository()
        self.field_repository = Field_repository()
        self.functional_map_repository = Functional_map_repository()
        self.team_repository = Team_repository()
        self.field_service = Field_service()
        self.excels_repository = Excels_repository()
        self.suffix_repository = Suffix_repository()
        self.data_table_repository = Data_table_repository()
        self.request_repository = Requests_repository()
        self.file = {"fixed": ".txt", "csv": ".csv"}
        self.state = {0: "NO", 1: "YES"}
        self.yes_no_1_0 = {"YES": 1, "NO": 0, "SI": 1, "": 0, 1: 1, 0: 0}
        self.values = {"G": "Gobierno", "A": "Arquitectura", "N": "En propuesta namings", "I": "Para ingestar",
                       "IN": "Ingestada en desarrollo", "Q": "Calidad", "M": "Mallas", "P": "Produccion",
                       "ungoverned": "En backlog", "PA": "Pendiente aprobación backlog", "RN": "Revisión Negocio",
                       "F": "Gobniero funcional", "R": "Revisión solicitud", "D": "Descartado"}
        self.type_lengths = {"TIME": 12, "CLOB": 3999, "DATE": 10, "TIMESTAMP": 26, "INT64": 255, 
                            "NUMERIC SHORT": 10, "NUMERIC LARGE": 255}

        self.sophia_props_object = {
            "COUNTRY OF THE DATA SOURCE": "",
            "PHYSICAL NAME OBJECT": "master_name",
            "LOGICAL NAME OBJECT": "baseName",
            "DESCRIPTION OBJECT": "observationField",
            "INFORMATION GROUP LEVEL 1": "information_group_level_1",
            "INFORMATION GROUP LEVEL 2": "information_group_level_2",
            "PERIMETER": "perimeter",
            "INFORMATION LEVEL": "information_level",
            "DATA SOURCE (DS)": "data_source",
            "MODEL VERSION": "modelVersion",
            "DEPLOYMENT TYPE": "deploymentType",
            "SECURITY LEVEL": "securityLevel",
            "TECHNICAL RESPONSIBLE": "",
            "STORAGE TYPE": "",
            "STORAGE ZONE": "",
            "OBJECT TYPE": "",
            "DATA PHYSICAL PATH": "master_path",
            "SYSTEM CODE/UUAA": "uuaaMaster",
            "PARTITIONS": "partitions",
            "FREQUENCY": "periodicity",
            "TIME REQUIREMENT": "",
            "LOADING TYPE": "loading_type",
            "CURRENT DEPTH": "current_depth",
            "REQUIRED DEPTH": "required_depth",
            "ESTIMATED VOLUME OF RECORDS": "estimated_volume_records",
            "STORAGE TYPE OF SOURCE OBJECT": "",
            "PHYSICAL NAME OF SOURCE OBJECT": "origin_tables",
            "MAILBOX SOURCE TABLE": "",
            "SOURCE PATH": "",
            "SCHEMA PATH": "",
            "SOURCE FILE TYPE": "",
            "SOURCE FILE DELIMITER": "",
            "TARGET FILE TYPE": "target_file_type",
            "TARGET FILE DELIMITER": "target_file_delimiter",
            "VALIDATED BY DATA ARCHITECT": "",
            "TAGS": "alias",
            "MARK OF TACTICAL OBJECT": "tacticalObject" }
        
        self.sophia_props_outnaming = {
            "ID": "",
            "COUNTRY": "",
            "PHYSICAL NAME OBJECT": "",
            "STORAGE TYPE": "",
            "STORAGE ZONE": "",
            "DEPLOYMENT TYPE": "",
            "PHYSICAL NAME FIELD": "naming",
            "LOGICAL NAME FIELD": "",
            "LOGICAL NAME FIELD (SPA)": "logic",
            "SIMPLE FIELD DESCRIPTION": "",
            "SIMPLE FIELD DESCRIPTION (SPA)": "description",
            "LEVEL": "",
            "COMPLEX STRUCTURE": "",
            "TECHNICAL COMMENTS": "operation",
            "CATALOG": "catalogue",
            "SECURITY CLASS": "",
            "SECURITY LABEL": "",
            "TOKENIZED AT DATA SOURCE": "tds",
            "TDS": "tds",
            "TOKENIZED FIELD": "",
            "LOCALE": "",
            "DATA TYPE": "dataType",
            "FORMAT": "format",
            "LOGICAL FORMAT": "logicalFormat",
            "KEY": "key",
            "MANDATORY": "mandatory",
            "DEFAULT VALUE": "default",
            "PHYSICAL NAME OF SOURCE OBJECT": "originNamingsSources",
            "SOURCE FIELD": "originNamings",
            "DATA TYPE OF SOURCE FIELD": "",
            "FORMAT OF SOURCE FIELD": "",
            "LENGTH OF SOURCE FIELD": "length",
            "TAGS": "",
            "FIELD POSITION IN THE OBJECT": "column",
            "GENERATED FIELD": "",
            "TOKENIZATION TYPE": "tokenization",
            "REGISTRATION DATE": ""
        }


    def send_to(self, table_id, data):
        """Change a data dictionary state

        :param table_id: str, String that reference a table id
        :param data:json, dictionary with data to check (from and to)
        :return:
        """
        user_id = self.user_repository.find_one(data["user_id"])
        if user_id is None:
            raise ValueError("Permisos denegados para el usuario.")

        temp = self.data_table_repository.find_head(table_id)
        if temp is not None and len(temp["origin_tables"]) == 0 and (temp["uuaaMaster"] != "KCRT"):
            raise ValueError("No hay tablas de origen para el tablón.")

        flag = True
        for k in temp["people"]:
            if str(user_id["_id"]) == str(k["user"]):
                flag = False
        if flag:
            temp["people"].append(
                {"user": ObjectId(str(user_id["_id"])), "rol": user_id["rol"]})

        user_id = user_id["_id"]

        if data["from"] == "governance" and data["to"] == "ingesta":
            self._change_state(table_id, "N", "G")
        elif data["from"] == "ingesta" and data["to"] == "review":
            self._change_state(table_id, "R", "N")
        elif data["from"] == "review" and data["to"] == "governance":
            self._change_state(table_id, "G", "R")
        # elif data["from"] == "ingesta" and data["to"] == "governance": self._change_state_to_governance(table_id,"G","N")
        elif data["from"] == "ingesta" and data["to"] == "governance":
            self._change_state(table_id, "G", "N")
        elif data["from"] == "governance" and data["to"] == "business":
            self._change_state(table_id, "RN", "G")
        elif data["from"] == "business" and data["to"] == "governance":
            self._change_state(table_id, "G", "RN")
        elif data["from"] == "architecture" and data["to"] == "governance":
            self._change_state(table_id, "G", "A")
        elif data["from"] == "architecture" and data["to"] == "ingesta":
            self._change_state(table_id, "I", "A")
        elif data["from"] == "ingesta" and data["to"] == "architecture":
            self._change_state(table_id, "A", "I")
        elif data["from"] == "ingesta" and data["to"] == "ingesta-table":
            self._change_state(table_id, "IN", "I")
        elif data["from"] == "ingesta-table" and data["to"] == "ingesta":
            self._change_state(table_id, "I", "IN")
        elif data["from"] == "ingest-table" and data["to"] == "meshes":
            self._change_state(table_id, "M", "IN")
        elif data["from"] == "meshes" and data["to"] == "ingest-table":
            self._change_state(table_id, "IN", "M")
        elif data["from"] == "meshes" and data["to"] == "qualified":
            self._change_state(table_id, "Q", "M")
        elif data["from"] == "qualified" and data["to"] == "meshes":
            self._change_state(table_id, "Q", "M")
        elif data["from"] == "qualified" and data["to"] == "production":
            self._change_state(table_id, "P", "Q")
        elif data["from"] == "production" and data["to"] == "qualified":
            self._change_state(table_id, "P", "Q")

        if "ingestOwner" in data:
            self.data_dictionary_repository.update_field(
                table_id, {"ingestOwner": data["ingestOwner"]})

        temp_mod = [{"user": ObjectId(str(user_id)),
                     "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]

        for i in temp["modifications"]:
            temp_mod.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]})

        self.data_table_repository.update_modifications_audit(
            table_id, temp_mod[:10])
        self.data_table_repository.update_people(table_id, temp["people"])

    def free_change_state(self, table_id, body):
        """Change a data dictionary state

        :param table_id: str, String that reference a table id
        :param new: str, string that reference new state
        :param old: str, string that reference a old state
        :return:
        """
        self.data_table_repository.sent_to(table_id, body["newState"])

    def _change_state_to_governance(self, table_id, new, old):
        """Change a data table state

        :param table_id: str, String that reference a table id
        :param new: str, string that reference new state
        :param old: str, string that reference a old state
        :return:
        """
        if new != old:
            req_temp = self.request_repository.find_request_data_table(
                table_id)
            flag = True
            for t in req_temp:
                if t["stateRequest"] == "P" or t["stateRequest"] == "D":
                    t["stateRequest"] == "A"
                    t["comment"] == "<<Aprobado Automatico>>"
                    #flag = False
            if flag:
                self.data_table_repository.change_state_table(table_id, new)
            else:
                print(
                    "La solicitud del tablón no ha sido aceptada por el Request Manager del DH. ")
                raise ValueError(
                    "La solicitud del tablón no ha sido aceptada por el Request Manager del DH. ")

    def _change_state(self, table_id, new, old):
        """Change a data dictionary state

        :param table_id: str, String that reference a table id
        :param new: str, string that reference new state
        :param old: str, string that reference a old state
        :return:
        """
        if new != old:
            self.data_table_repository.change_state_table(table_id, new)
        else:
            raise ValueError("Can't be pass to this state")

    def fields_actions(self, table_id, data):
        """

        :param table_id:
        :param data:
        :return:
        """
        user_id = self.user_repository.find_one(data["user_id"])
        if user_id is None:
            raise ValueError("Permisos denegados para el usuario.")

        data_dic = self.data_table_repository.find_one(table_id)
        if data_dic is None:
            raise ValueError("La tabla no se encuentra registrada.")

        flag = True
        for k in data_dic["people"]:
            if str(user_id["_id"]) == str(k["user"]):
                flag = False
        if flag:
            data_dic["people"].append(
                {"user": ObjectId(str(user_id["_id"])), "rol": user_id["rol"]})

        user_id = user_id["_id"]
        temp = self.data_table_repository.find_head(table_id)
        # if temp is not None and len(temp["origin_tables"]) == 0:
        #raise ValueError("No hay tablas de origen para el tablón.")
        temp_mod = [{"user": ObjectId(str(user_id)),
                     "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]

        for i in temp["modifications"]:
            temp_mod.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]})

        if data["action"] == "insert":
            self._insert_field(table_id, data)
        elif data["action"] == "substract":
            self._substract_field(table_id, data)

        self.data_table_repository.update_modifications_audit(
            table_id, temp_mod[:10])
        self.data_table_repository.update_people(table_id, temp["people"])
        return self.find_fields(table_id)

    def _insert_field(self, table_id, data):
        """

        :param table_id:
        :param data:
        :return:
        """
        table = self.data_table_repository.find_one(table_id)
        res = []
        for i in table["fields"]:
            if i["column"] == data["column"]:
                res.append(ast.literal_eval(
                    str(Field_data_table(column=data["column"]))))
                i["column"] += 1
            elif i["column"] > data["column"]:
                i["column"] += 1
            res.append(i)

        table["fields"] = res
        if data["column"] == len(table["fields"]):
            res.append(ast.literal_eval(
                str(Field_data_table(column=data["column"]))))
        table["fields"] = res

        self.data_table_repository.update_all_fields(table_id, res)

    def _substract_field(self, table_id, data):
        """

        :param table_id:
        :param data:
        :return:
        """
        table = self.data_table_repository.find_one(table_id)
        res = []
        for i in table["fields"]:
            if i["column"] > data["column"]:
                i["column"] -= 1
                res.append(i)
            elif i["column"] < data["column"]:
                res.append(i)
        table["fields"] = res
        self.data_table_repository.update_all_fields(table_id, res)
    
    def update_datasourceinfo_head(self, data):
        """Updates the Data Source and Information Levels 1 & 2 of a DD head.
        Created for future development.
        
        :param data: json, dictionary with data to update
        :return: json, same input data with the added info
        """
        tmp = self.functional_map_repository.find_uuaa(data["uuaaMaster"].upper()[1:])
        data["data_source"] = tmp["data_source"] if "data_source" in tmp else tmp["system_name"] if "system_name" in tmp else ""
        data["information_group_level_1"] = tmp["information_level_1"] if "information_level_1" in tmp else ""
        data["information_group_level_2"] = tmp["information_level_2"] if  "information_level_2" in tmp else ""

        return data

    def create_table(self, data, user_id):
        """Create a new data dictionary

        :param data: json, dictionary with initial Data dictionary data
        :param user_id: str,
        :return: Json, with Data dictionary id
        """
        new_data_table = Object_data_table().data
        if not self.data_table_repository.find_table_by_alias(data["alias"]) and not self.data_dictionary_repository.find_table_by_alias(data["alias"]):
            new_data_table["fields"] = [ast.literal_eval(
                str(Field_data_table(column=i))) for i in range(int(data["numFields"]))]
            new_data_table["master_name"] = data["master_name"].strip().lower()
            new_data_table["baseName"] = data["baseName"].strip().upper()
            new_data_table["observationField"] = data["observationField"].strip()
            new_data_table["uuaaMaster"] = data["uuaaMaster"].strip().upper()
            new_data_table["alias"] = data["alias"].strip().lower()
            new_data_table["periodicity"] = data["periodicity"].strip().upper()
            new_data_table["tacticalObject"] = data["tacticalObject"].strip().upper()
            new_data_table["user"] = ObjectId(user_id.strip())
            new_data_table["project_owner"] = ObjectId(data["project_owner"].strip())
            new_data_table["origin_tables"] = data["origin_tables"]
            new_data_table["deploymentType"] = ""
            new_data_table["modelVersion"] = ""
            new_data_table["objectVersion"] = ""
            new_data_table["people"] = []

            return self._insert_data_table(new_data_table)
        else:
            raise ValueError(
                "Ya existe una tabla o tablón bajo el alias {0}.".format(data["alias"]))

    def _insert_data_table(self, new_data_table):
        """
            CREA LAS RUTAS DE UN TABLON NUEVO
        :param new_data_table:
        :return: _id
        """
        new_data_table["master_path"] = "/data/master/" + new_data_table["uuaaMaster"].lower() + "/data/" + new_data_table["master_name"]
        new_data_table = self.update_datasourceinfo_head(new_data_table)

        if "_id" in new_data_table:
            del new_data_table["_id"]
        return self.data_table_repository.insert_one(new_data_table)

    def find_head(self, table_id):
        """

        :param table_id:
        :return:
        """
        information = self.data_table_repository.find_head(table_id)
        if information is not None:
            information["_id"] = str(information["_id"])
            information["user"] = self.user_repository.find_one(information["user"])[
                "name"]
            information["project_owner"] = self.project_repository.find_one(
                information["project_owner"])["name"]
            if len(information["modifications"]) != 0:
                information["modifications"] = {
                    "user_name": self.user_repository.find_one(str(information["modifications"][0]["user"]))["name"],
                    "date": information["modifications"][0]["date"]}
            else:
                information["modifications"] = {"user_name": "N/A", "date": "N/A"}
            temp = []
            for i in information["origin_tables"]:
                i["_id"] = str(i["_id"])
                temp.append(i)
            information["origin_tables"] = temp
            if len(information["people"]) != 0:
                tmpPeople = []
                for p in information["people"]:
                    tmp = {
                        "user": str(p["user"]),
                        "rol": p["rol"]}
                    tmpPeople.append(tmp)
                information["people"] = tmpPeople
        else:
            information = None

        return information

    def update_head(self, data_table_id, data):
        """
            ACTUALIZA EL OBJETO DE UN TABLON 
        :param data_table_id:
        :param data:
        :return:
        """
        data_dic = self.data_table_repository.find_one(data_table_id)
        if data_dic is None:
            raise ValueError("El tablón no se encuentra registrada.")

        user_id = self.user_repository.find_one(data["user_id"])
        if user_id is None:
            raise ValueError("Permisos denegados para el usuario.")

        flag = True
        for k in data_dic["people"]:
            if str(user_id["_id"]) == str(k["user"]):
                flag = False
        if flag:
            data_dic["people"].append(
                {"user": ObjectId(str(user_id["_id"])), "rol": user_id["rol"]})

        user_id = user_id["_id"]

        temp_mod = [{"user": ObjectId(str(user_id)),
                     "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]

        for i in data_dic["modifications"]:
            temp_mod.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]})

        if data_dic["stateTable"] == "N" or data_dic["stateTable"] == "G" or data_dic["stateTable"] == "R" or data["complete"]:

            if not self.data_table_repository.find_alias_repeat(data["alias"], data_table_id) and not self.data_dictionary_repository.find_table_by_alias(data["alias"]):
                if "_id" in data:
                    del data["_id"]
                if "modifications" in data:
                    del data["modifications"]
                if "created_time" in data:
                    del data["created_time"]
                if "user_id" in data:
                    del data["user_id"]
                data["alias"] = data["alias"]
                desc_table = data["master_name"][2:].lower()

                tmp = self.functional_map_repository.find_uuaa(data["uuaaMaster"].upper()[1:])
                data["data_source"] = tmp["data_source"] if "data_source" in tmp else tmp["system_name"] if "system_name" in tmp else ""
                data["information_group_level_1"] = tmp["information_level_1"] if "information_level_1" in tmp else ""
                data["information_group_level_2"] = tmp["information_level_2"] if  "information_level_2" in tmp else ""

                #Actualiza la ruta de origen SI ESTA VACIA
                if len(data["master_path"].strip()) == 0:
                    data["master_path"] = "/data/master/" + data["uuaaMaster"].lower() + "/data/" + data["master_name"]
                temp = []
                for i in data["origin_tables"]:

                    if i["_id"] == str(data_table_id).strip():
                        i["originName"] = data["master_name"]
                        i["originRoute"] = data["master_path"]

                    i["_id"] = ObjectId(str(i["_id"]))
                    temp.append(i)
                data["origin_tables"] = temp
                self.data_table_repository.update_field(data_table_id, data)
                self.data_table_repository.update_modifications_audit(
                    data_table_id, temp_mod[:10])
                self.data_table_repository.update_people(
                    data_table_id, data_dic["people"])
            else:
                raise ValueError(
                    "Una tabla bajo el alias {0} ya existe.".format(data["alias"]))
        else:
            raise ValueError("Tablón cerrado, no es posible su edición")

    def find_fields(self, table_id):
        """

        :param table_id:
        :return:
        """
        information = self.data_table_repository.find_one(table_id)["fields"]
        for i in information:
            try:
                i["check"]["user"] = str(i["check"]["user"])
            except:
                pass
            for j in i["modification"]:
                try:
                    j["user"] = str(j["user"])
                    j["name"] = self.user_repository.find_one(j["user"])[
                        "name"]
                except:
                    j["user"] = str(j["user"])
                    j["name"] = "Unassigned"
            temp = []
            for h in i["originNamings"]:
                h["table_id"] = str(h["table_id"])
                temp.append(h)
            i["originNamings"] = temp
        return information

    def update_fields(self, data_table_id, data):
        """

        :param data_table_id:
        :return:
        """
        data_dic = self.data_table_repository.find_one(data_table_id)
        if data_dic is None:
            raise ValueError("El tablón no se encuentra registrada.")
        user_repo = self.user_repository.find_one(data["user"])
        if user_repo is None:
            raise ValueError("El usuario no es válido.")

        flag = True
        for k in data_dic["people"]:
            if data["user"] == str(k["user"]):
                flag = False
        if flag:
            data_dic["people"].append(
                {"user": ObjectId(user_repo["_id"]), "rol": user_repo["rol"]})

        if data_dic["stateTable"] == "N" or data_dic["stateTable"] == "G" or data_dic["stateTable"] == "R":
            if data["action"] == "notSave":
                self._save_field(data_table_id, data["row"])
            elif data["action"] == "row":
                user_id = data["user"]
                self._update_field(data_table_id, user_id, data["row"])
                self.data_table_repository.update_people(
                    data_table_id, data_dic["people"])
        else:
            raise ValueError("El tablón no se encuentra registrada.")

    def _naming_state(self, table_id, naming, logical_name, description):
        """Check what state has to be assigned to one naming
                GL: Is global, belong to global naming repository
                GC: Global and common, belong to global naming repository and is in other table
                PS: Proposed and simple, only is proposed in one data dictionary
                PC: proposed and common, isn't a global naming repository but is proposed in other table

                :param table_id: str, String that reference a table id
                :param naming: str, String that reference a naming
                :return: str, state of a naming
                """
        global_naming = self.field_repository.find_exactly_naming(naming)
        local_naming = self.data_dictionary_repository.find_naming(
            table_id, naming).count()
        if global_naming is not None and local_naming == 0:
            res = "GL"
        elif global_naming is not None and local_naming > 0:
            res = "GC"
        elif global_naming is None and local_naming == 0:
            res = "PS"
        else:
            res = "PC"
        return res

    def _generate_data_owner_data(self, data, naming_state):
        """Cross a naming with a excels define with data owner data

        :param data: json, dictionary with data to check and add
        :param naming_state: str, string that reference a naming state
        :return: json, dictionary with new cross data
        """
        check = True
        if naming_state == "GL" or naming_state == "GC":
            owner_data = self.field_repository.find_naming_data_owner_information(
                data["naming"]["naming"])
            try:
                if "dataOwnerInformation" in owner_data:
                    data["domain"] = owner_data["dataOwnerInformation"][0]["domain"]
                    data["subdomain"] = owner_data["dataOwnerInformation"][0]["subdomain"]
                    data["ownership"] = owner_data["dataOwnerInformation"][0]["ownership"]
                    data["conceptualEntity"] = owner_data["dataOwnerInformation"][0]["conceptualEntity"]
                    data["operationalEntity"] = owner_data["dataOwnerInformation"][0]["operationalEntity"]
                    check = False
            except:
                data["domain"] = "Unassigned"
                data["subdomain"] = ""
                data["ownership"] = ""
                data["conceptualEntity"] = ""
                data["operationalEntity"] = ""
        if check:
            data["domain"] = "Unassigned"
            data["subdomain"] = ""
            data["ownership"] = ""
            data["conceptualEntity"] = ""
            data["operationalEntity"] = ""
        return data

    def _change_common_field(self, naming, new_state):
        """Change naming state with a proposed naming is in other table

        :param naming: str, String that reference a naming
        :param new_state: str, string with naming to check
        :return:
        """
        tables = self.data_table_repository.same_naming(naming)
        for i in tables:
            for j in i["Fields"]:
                try:
                    if j["naming"]["naming"] == naming and j["modification"][0]["state"] == "PS":
                        j["modification"][0]["state"] = new_state
                        self.data_table_repository.update_row_fields(
                            str(i["_id"]), str(j["column"]), j)
                        break
                except Exception as e:
                    print(e)
                    continue

    def _save_field(self, table_id, data):
        """

        :param table_id:
        :param data:
        :return:
        """
        namings_origin = []
        for h in data["originNamings"]:
            h["table_id"] = ObjectId(str(h["table_id"]))
            namings_origin.append(h)
        data["originNamings"] = namings_origin
        naming_state = self._naming_state(
            table_id, data["naming"]["naming"], data["logic"], data["description"])
        if len(data["modification"]) > 0:
            data["modification"][0]["state"] = naming_state
        if naming_state == "PC":
            self._change_common_field(data["naming"]["naming"], naming_state)
        data = self._generate_data_owner_data(data, naming_state)
        self.data_table_repository.update_row_fields(
            table_id, data["column"], data)

    def _update_field(self, table_id, user_id, data):
        """

        :param table_id:
        :param user_id:
        :param data:
        :return:
        """
        data_dic = self.data_table_repository.find_one(table_id)
        if data_dic is None:
            raise ValueError("La tabla no se encuentra registrada.")

        user_repo = self.user_repository.find_one(user_id)
        if user_repo is None:
            raise ValueError("El usuario no es válido.")

        flag = True
        for k in data_dic["people"]:
            if user_id == str(k["user"]):
                flag = False
        if flag:
            data_dic["people"].append(
                {"user": ObjectId(user_repo["_id"]), "rol": user_repo["rol"]})

        for i in data:
            self._save_field(table_id, i)
        self.data_table_repository.update_people(table_id, data_dic["people"])

    def generate_excel(self, table_id, phase):
        """Generate excel to download by phase

        :param table_id:
        :param phase:
        :return:
        """
        res = []
        if phase == "Object":
            res = self.generate_excel_object(table_id)
        elif phase == "Fields":
            res = self.generate_excel_fields(table_id)
        elif phase == "VoBo":
            res = self.generate_excel_fields_vobo(table_id)
        return res

    def generate_excel_object(self, table_id):
        """

        :param table_id:
        :return:
        """
        table = self.data_table_repository.find_one(table_id)
        res = []
        volume_records = 0
        if int(table["estimated_volume_records"]) != 0:
            volume_records = int(table["estimated_volume_records"])

        current_depth = str(int(table["current_depth"])) + " months"
        if table["current_depth"] == 1:
            current_depth = str(int(table["current_depth"])) + " month"

        required_depth = str(int(table["required_depth"])) + " months"
        if int(table["required_depth"]) == 1:
            required_depth = str(int(table["required_depth"])) + " month"

        tactical = "NO"
        if str(table["tacticalObject"]) == "1" or str(table["tacticalObject"]) == "YES":
            tactical = "YES"
        
        try:
            security_level = table["securityLevel"]
        except:
            security_level = ""

        origin_tables_temp = []
        routes_origin_temp = []
        zone_origin_temp = []
        types_origin_temp = []
        for i in table["origin_tables"]:
            if i["originSystem"] not in origin_tables_temp:
                origin_tables_temp.append(i["originName"])
            if i["originSystem"] not in routes_origin_temp:
                routes_origin_temp.append(i["originRoute"])
            if i["originSystem"] not in zone_origin_temp:
                zone_origin_temp.append(i["originSystem"])
                if "HDFS-Avro" in i["originSystem"] and "Avro" not in types_origin_temp:
                    types_origin_temp.append("Avro")
                elif "HDFS-Parquet" in i["originSystem"] and "Parquet" not in types_origin_temp:
                    types_origin_temp.append("Parquet")
        origin_tables = ";".join(origin_tables_temp)
        routes_origin_tables = ";".join(routes_origin_temp)
        zone_origin_tables = ";".join(zone_origin_temp)
        types_origin_tables = ";".join(types_origin_temp)

        try:
            res.append({
                "COUNTRY OF THE DATA SOURCE": "Colombia",
                "PHYSICAL NAME OBJECT": table["master_name"],
                "LOGICAL NAME OBJECT": table["baseName"],
                "DESCRIPTION OBJECT": table["observationField"],
                "INFORMATION GROUP LEVEL 1": table["information_group_level_1"],
                "INFORMATION GROUP LEVEL 2": table["information_group_level_2"],
                "PERIMETER": table["perimeter"],
                "INFORMATION LEVEL": table["information_level"],
                "DATA SOURCE (DS)": table["data_source"],
                "SECURITY LEVEL": security_level,
                "ENCRYPTION AT REST": "",
                "DEPLOYMENT TYPE": table["deploymentType"],
                "TECHNICAL RESPONSIBLE": "datahub.co.group@bbva.com",
                "MODEL VERSION": table["modelVersion"],
                "OBJECT VERSION": table["objectVersion"],
                "STORAGE TYPE": "HDFS-Parquet",
                "STORAGE ZONE": "MASTERDATA",
                "OBJECT TYPE": "File",
                "DATA PHYSICAL PATH": table["master_path"],
                "SYSTEM CODE/UUAA": table["uuaaMaster"],
                "PARTITIONS": table["partitions"],
                #"FREQUENCY": self.excels_repository.find_one_periodicity(table["periodicity"])["state"],
                "TIME REQUIREMENT": "",
                #"LOADING TYPE": table["loading_type"],
                "CURRENT DEPTH": current_depth,
                "REQUIRED DEPTH": required_depth,
                "ESTIMATED VOLUME OF RECORDS": volume_records,
                "STORAGE TYPE OF SOURCE OBJECT": zone_origin_tables,
                "PHYSICAL NAME OF SOURCE OBJECT": origin_tables,
                "MAILBOX SOURCE TABLE": "",
                "SOURCE PATH": routes_origin_tables,
                "SCHEMA PATH": "",
                "SOURCE FILE TYPE": types_origin_tables,
                "SOURCE FILE DELIMITER": "",
                "TARGET FILE TYPE": table["target_file_type"],
                "TARGET FILE DELIMITER": table["target_file_delimiter"],
                "VALIDATED BY DATA ARCHITECT": "NO",
                "TAGS": table["alias"].lower(),
                "MARK OF TACTICAL OBJECT": tactical
            })
        except Exception as e:
            print(e, "Fallo-----------------------")
            raise ValueError(
                "Error generando alguno de los campos en el objeto.")
        return res

    def generate_excel_fields(self, table_id):
        """

        :param table_id:
        :return:
        """
        table = self.data_table_repository.find_one(table_id)
        res = []
        index = 1
        values = {0: "NO", 1: "YES"}
        for i in table["fields"]:
            domain = "Unassigned"
            subdomain = ""
            ownership = ""
            operationalEntity = ""
            log_eng = ""
            desc_eng = ""
            log_spn = i["logic"]
            desc_spn = i["description"]
            naming = self.field_repository.find_naming_all_data(
                i["naming"]["naming"])
            if naming is not None:
                log_eng = naming["logicEnglish"]
                desc_eng = naming["descEnglish"]
                log_spn = naming["logic"]
                desc_spn = naming["originalDesc"]
                naming = self.field_repository.find_naming_all(
                    "operational_audit_insert_date")
                if "dataOwnerInformation" in naming:
                    domain = naming["dataOwnerInformation"][0]["domain"]
                    if len(domain.strip()) == 0:
                        domain = "Unassigned"
                    subdomain = naming["dataOwnerInformation"][0]["subdomain"]
                    ownership = naming["dataOwnerInformation"][0]["ownership"]
                    operationalEntity = naming["dataOwnerInformation"][0]["operationalEntity"]

            tokenization = ""
            default = i["default"]
            format = i["format"]

            try:
                if i["tokenization"] == "empty":
                    tokenization = ""
                else:
                    tokenization = i["tokenization"]
            except:
                tokenization = ""

            if i["default"] == "empty":
                default = ""
            elif len(i["originNamings"]) != 0:
                default = ""

            if i["format"] == "empty":
                format = ""

            temp_sources = []
            temp_namings = []
            temp_types = []
            temp_format = []
            for u in i["originNamings"]:
                if u["table_name"] not in temp_sources:
                    temp_sources.append(u["table_name"])
                if u["naming"] not in temp_namings:
                    temp_namings.append(u["naming"])
                if "type" in u and u["type"] not in temp_types:
                    temp_types.append(u["type"])
                if "format" in u and u["format"] not in temp_format:
                    temp_format.append(u["format"])

            generate_field = "NO"
            table_source = ";".join(temp_sources)
            source_field = ";".join(temp_namings)
            data_type_sources = ";".join(temp_types)
            format_data_type = ";".join(temp_format)

            if len(i["originNamings"]) == 0:
                if table["uuaaMaster"] == "KCTD":
                    table_source = ""
                    source_field = ""
                elif table["uuaaMaster"] == "KCRT":
                    table_source = "Reference Model"
                    source_field = "Reference Model"
                else:
                    generate_field = "YES"
                    table_source = "Calculated"
                    source_field = "Calculated"

            data_type = i["dataType"].lower()
            if i["dataType"] == "DECIMAL":
                data_type = i["logicalFormat"].strip()
            if "DECIMAL" in i["dataType"].upper():
                data_type = "decimal"

            tds = "NO"
            if i["tds"] == 1 or i["tds"] == "YES":
                tds = "YES"

            try:
                res.append({
                    "COUNTRY": "Colombia",
                    "PHYSICAL NAME OBJECT": table["master_name"],
                    "STORAGE TYPE": "HDFS-Parquet",
                    "STORAGE ZONE": "MASTERDATA",
                    "DEPLOYMENT TYPE": table["deploymentType"],
                    "PHYSICAL NAME FIELD": i["naming"]["naming"].strip().lower(),
                    "LOGICAL NAME FIELD": log_eng,
                    "LOGICAL NAME FIELD (SPA)": log_spn.strip(),
                    "SIMPLE FIELD DESCRIPTION": desc_eng,
                    "SIMPLE FIELD DESCRIPTION (SPA)": desc_spn,
                    "LEVEL": "",
                    "COMPLEX STRUCTURE": "",
                    "TECHNICAL COMMENTS": "",
                    "CATALOG": i["catalogue"],
                    "TOKENIZED AT DATA SOURCE": "",
                    "LOCALE": "",
                    "DATA TYPE": data_type,
                    "FORMAT": format,
                    "LOGICAL FORMAT": i["logicalFormat"].strip(),
                    "KEY": values[int(i["key"])],
                    "MANDATORY": values[int(i["mandatory"])],
                    "DEFAULT VALUE": default,
                    "PHYSICAL NAME OF SOURCE OBJECT": table_source,
                    "SOURCE FIELD": source_field,
                    "DATA TYPE OF SOURCE FIELD": data_type_sources,
                    "FORMAT OF SOURCE FIELD": format_data_type,
                    "LENGTH OF SOURCE FIELD": i["length"],
                    "TAGS": table["alias"].lower(),
                    "FIELD POSITION IN THE OBJECT": index,
                    "GENERATED FIELD": generate_field,
                    "TOKENIZATION TYPE": tokenization,
                    # "COUNTRY OF THE DATA OWNER": "Colombia",
                    # "OPERATIONAL ENTITY DOMAIN": domain,
                    # "OPERATIONAL ENTITY SUBDOMAIN": subdomain,
                    # "OPERATIONAL ENTITY OWNERSHIP": ownership,
                    # "OPERATIONAL ENTITY SPA": operationalEntity,
                    # "TDS": tds
                })
                index += 1
            except Exception as e:
                print(e, "Fallo-----------------------")
                raise ValueError(
                    "Error generando alguno de los campos en los fields.")
        return res

    def generate_excel_fields_vobo(self, table_id):
        """

        :param table_id:
        :return:
        """
        table = self.data_table_repository.find_one(table_id)
        res = []
        index = 1
        values = {0: "NO", 1: "YES"}
        for i in table["fields"]:
            domain = "Unassigned"
            subdomain = ""
            ownership = ""
            operationalEntity = ""
            log_eng = ""
            desc_eng = ""
            log_spn = i["logic"]
            desc_spn = i["description"]
            naming = self.field_repository.find_naming_all_data(
                i["naming"]["naming"])
            if naming is not None:
                log_eng = naming["logicEnglish"]
                desc_eng = naming["descEnglish"]
                log_spn = naming["logic"]
                desc_spn = naming["originalDesc"]
                naming = self.field_repository.find_naming_all(
                    "operational_audit_insert_date")
                if "dataOwnerInformation" in naming:
                    domain = naming["dataOwnerInformation"][0]["domain"]
                    if len(domain.strip()) == 0:
                        domain = "Unassigned"
                    subdomain = naming["dataOwnerInformation"][0]["subdomain"]
                    ownership = naming["dataOwnerInformation"][0]["ownership"]
                    operationalEntity = naming["dataOwnerInformation"][0]["operationalEntity"]

            tokenization = ""
            default = i["default"]
            format = i["format"]

            try:
                if i["tokenization"] == "empty":
                    tokenization = ""
                else:
                    tokenization = i["tokenization"]
            except:
                tokenization = ""

            if i["default"] == "empty":
                default = ""
            elif len(i["originNamings"]) != 0:
                default = ""

            if i["format"] == "empty":
                format = ""

            temp_sources = []
            temp_namings = []
            temp_types = []
            temp_format = []
            for u in i["originNamings"]:
                if u["table_name"] not in temp_sources:
                    temp_sources.append(u["table_name"])
                if u["naming"] not in temp_namings:
                    temp_namings.append(u["naming"])
                if "type" in u and u["type"] not in temp_types:
                    temp_types.append(u["type"])
                if "format" in u and u["format"] not in temp_format:
                    temp_format.append(u["format"])

            generate_field = "NO"
            table_source = ";".join(temp_sources)
            source_field = ";".join(temp_namings)
            data_type_sources = ";".join(temp_types)
            format_data_type = ";".join(temp_format)

            if len(i["originNamings"]) == 0:
                if table["uuaaMaster"] == "KCTD":
                    table_source = ""
                    source_field = ""
                else:
                    generate_field = "YES"
                    table_source = "Calculated"
                    source_field = "Calculated"

            data_type = i["dataType"].lower()
            if i["dataType"] == "DECIMAL":
                data_type = i["logicalFormat"].strip()

            tds = "NO"
            if i["tds"] == 1 or i["tds"] == "YES":
                tds = "YES"

            try:
                res.append({
                    "COUNTRY": "Colombia",
                    "PHYSICAL NAME OBJECT": table["master_name"],
                    "STORAGE TYPE": "HDFS-Parquet",
                    "STORAGE ZONE": "MASTERDATA",
                    "DEPLOYMENT TYPE": table["deploymentType"],
                    "PHYSICAL NAME FIELD": i["naming"]["naming"].strip().lower(),
                    "LOGICAL NAME FIELD": log_eng,
                    "LOGICAL NAME FIELD (SPA)": log_spn.strip(),
                    "SIMPLE FIELD DESCRIPTION": desc_eng,
                    "SIMPLE FIELD DESCRIPTION (SPA)": desc_spn,
                    "LEVEL": "",
                    "COMPLEX STRUCTURE": "",
                    "TECHNICAL COMMENTS": "",
                    "CATALOG": i["catalogue"],
                    "TOKENIZED AT DATA SOURCE": "",
                    "LOCALE": "",
                    "DATA TYPE": data_type,
                    "FORMAT": format,
                    "LOGICAL FORMAT": i["logicalFormat"].strip(),
                    "KEY": values[int(i["key"])],
                    "MANDATORY": values[int(i["mandatory"])],
                    "DEFAULT VALUE": default,
                    "PHYSICAL NAME OF SOURCE OBJECT": table_source,
                    "SOURCE FIELD": source_field,
                    "DATA TYPE OF SOURCE FIELD": data_type_sources,
                    "FORMAT OF SOURCE FIELD": format_data_type,
                    "LENGTH OF SOURCE FIELD": "",
                    "TAGS": table["alias"].lower(),
                    "FIELD POSITION IN THE OBJECT": index,
                    "GENERATED FIELD": generate_field,
                    "TOKENIZATION TYPE": tokenization,
                    "VoBo CHECK": str(i["check"]["status"]),
                    "VoBo COMMENTS": str(i["check"]["comments"])
                    # "COUNTRY OF THE DATA OWNER": "Colombia",
                    # "OPERATIONAL ENTITY DOMAIN": domain,
                    # "OPERATIONAL ENTITY SUBDOMAIN": subdomain,
                    # "OPERATIONAL ENTITY OWNERSHIP": ownership,
                    # "OPERATIONAL ENTITY SPA": operationalEntity,
                    # "TDS": tds
                })
                index += 1
            except Exception as e:
                print(e, "Fallo-----------------------")
                raise ValueError(
                    "Error generando alguno de los campos en los fields.")
        return res

    def validate_origin_tables(self, table_id):
        """
            Validate the use of all origin tables in the namings proposal.
        :param table_id:
        :return:
        """
        errors = []
        origin_namings = self.data_table_repository.distinct_origin_namings(
            table_id)
        origin_tables = self.data_table_repository.distinct_origin_tables(
            table_id)
        if len(origin_tables) == 0:
            errors.append("No hay tablas de origen para el tablón.")
        else:
            for i in origin_tables:
                if i not in origin_namings:
                    errors.append("El origen "+str(i) +
                                  " no se usa en propuesta de ningún naming.")
        return errors

    def find_all_data_tables(self, name, alias, desc, uuaa):
        """

        :param name:
        :param alias:
        :param desc:
        :param uuaa:
        :return:
        """
        result = []
        name = name.strip()
        alias = alias.strip()
        desc = desc.strip()
        uuaa = uuaa.strip()
        res = self.data_table_repository.find_all()
        if len(name) != 0:
            res = self.__filter_by_property(res, name, "master_name")
        if len(alias) != 0:
            res = self.__filter_by_property(res, alias, "alias")
        if len(desc) != 0:
            res = self.__filter_by_property(res, desc, "observationField")
        if len(uuaa) != 0:
            res = self.__filter_by_property(res, uuaa, "uuaaMaster")
        for i in res:
            i["_id"] = str(i["_id"])
            result.append(i)
        return result

    def __filter_by_property(self, array, value, property):
        """

        :param array:
        :param value:
        :param property:
        :return:
        """
        res = []
        for i in array:
            try:
                if value.lower() in i[property].lower():
                    res.append(i)
            except:
                pass
        return res

    def __filter_by_state_table(self, array, state_table):
        """

        :param query:
        :param state_table:
        :return:
        """
        res = []
        for i in array:
            if state_table.lower() == i["stateTable"].lower():
                res.append(i)
        return res

    def find_origin_tables(self, alias, datio_name, uuaa_raw, uuaa_master, logic):
        """

        :return:
        """
        result = []
        res = self.data_dictionary_repository.find_origin_data_tables()
        if len(datio_name.strip()) != 0:
            res = self.__filter_by_property(res, datio_name, "raw_name")
            res = self.__filter_by_property(res, datio_name, "master_name")
        if len(alias.strip()) != 0:
            res = self.__filter_by_property(res, alias, "alias")
        if len(logic.strip()) != 0:
            res = self.__filter_by_property(res, logic, "baseName")
        if len(uuaa_raw.strip()) != 0:
            res = self.__filter_by_property(res, uuaa_raw, "uuaaRaw")
        if len(uuaa_master.strip()) != 0:
            res = self.__filter_by_property(res, uuaa_master, "uuaaMaster")

        for i in res:
            i["_id"] = str(i["_id"])
            i["user"] = self.user_repository.find_one(str(i["user"]))["email"]
            result.append(i)
        return result

    def __filter_by_property2(self, array, value, property1, property2):
        """

        :param array:
        :param value:
        :param property:
        :return:
        """
        res = []
        for i in array:
            if value.lower() in i[property1].lower() or value.lower() in i[property2].lower():
                res.append(i)
        return res

    def update_validation_po(self, table_id, data):
        """

        :param table_id:
        :param data:
        :return:
        """
        data_dic = self.data_table_repository.find_one(table_id)
        if data_dic is None:
            raise ValueError("La tabla no se encuentra registrada.")

        user_repo = self.user_repository.find_one(str(data["user"]))
        if user_repo is None:
            raise ValueError("El usuario no es válido.")

        flag = True
        for k in data_dic["people"]:
            if str(data["user"]) == str(k["user"]):
                flag = False
        if flag:
            data_dic["people"].append(
                {"user": ObjectId(user_repo["_id"]), "rol": user_repo["rol"]})

        try:
            temp = self.data_table_repository.find_one(table_id)["fields"]
            modi = temp[data["column"]]["modification"]
            modification = []
            modification_temp = {
                "user": ObjectId(str(data["user"])),
                "startDate": datetime.datetime.now(),
                "state": modi[0]["state"],
                "stateValidation": "YES"
            }
            modification.append(modification_temp)
            for j in range(0, 4):
                if len(modi) > j:
                    modification.append(modi[j])
            self.data_table_repository.update_modifications(
                table_id, modification, data["column"])
        except Exception as e:
            print(e, "NO se ingreso la modificación...")

        self.data_table_repository.update_check_status(
            table_id, data["user"], data["status"], data["column"], data["comments"])
        self.data_table_repository.update_people(table_id, data_dic["people"])

    def find_origin_data_tables(self, alias, datio_name, uuaa, logic):
        """

        :return:
        """
        result = []
        res = self.data_table_repository.find_all_origin()
        if len(datio_name) != 0:
            res = self.__filter_by_property(res, datio_name, "master_name")
        if len(alias) != 0:
            res = self.__filter_by_property(res, alias, "alias")
        if len(logic) != 0:
            res = self.__filter_by_property(res, logic, "baseName")
        if len(uuaa) != 0:
            res = self.__filter_by_property(res, uuaa, "uuaaMaster")
        for i in res:
            i["_id"] = str(i["_id"])
            i["user"] = self.user_repository.find_one(str(i["user"]))["email"]
            i["project_owner"] = self.project_repository.find_one(
                str(i["project_owner"]))["name"]
            if len(i["modifications"]) != 0:
                i["modifications"] = {
                    "user_name": self.user_repository.find_one(str(i["modifications"][0]["user"]))["name"],
                    "date": i["modifications"][0]["date"]}
            else:
                i["modifications"] = {"user_name": "N/A", "date": "N/A"}
            if len(i["people"]) != 0:
                tmpPeople = []
                for p in i["people"]:
                    tmp = {
                        "user": str(p["user"]),
                        "rol": p["rol"]}
                    tmpPeople.append(tmp)
                i["people"] = tmpPeople
            result.append(i)
        return result

    def find_similarities(self, table_id, type):
        """

        :param table_id:
        :param type:
        :return:
        """
        result = {}
        if type.strip() == "fields":
            result = self._compare_fields_with_data_tables(table_id)
        elif type.strip() == "object":
            result = self._compare_object_with_data_tables(table_id)
        return result

    def _compare_fields_with_data_tables(self, table_id):
        """

        :param table_id:
        :return:
        """
        result = {}
        namings_count = {}
        repeat_namings = []
        data_table = self.data_table_repository.find_one(table_id)

        if data_table["stateTable"] == "N":
            raise ValueError("El tablón se encuentra en propuesta de namings.")

        for i in data_table["fields"]:
            query1 = self.data_table_repository.find_same_naming_other_data_table(
                table_id, i["naming"]["naming"])
            query2 = self.data_table_repository.find_regex_naming_other_data_table(
                table_id, i["naming"]["naming"])

            if query1.count() != 0:
                if i["naming"]["naming"] not in ["partition_data_year_id", "partition_data_month_id", "partition_data_day_id", "operational_audit_insert_date"]:
                    result[i["naming"]["naming"]] = []
                    for j in query1:
                        repeat_namings.append(i["naming"]["naming"])
                        result[i["naming"]["naming"]].append(
                            {"source": j["alias"], "type": "EQUAL", "_id": str(j["_id"])})
                        if j["alias"] not in namings_count:
                            namings_count[j["alias"]] = 1
                        else:
                            namings_count[j["alias"]] += 1

            if query2.count() != 0:
                for k in query2:
                    for p in k["fields"]:
                        if i["naming"]["naming"] not in ["partition_data_year_id", "partition_data_month_id", "partition_data_day_id", "operational_audit_insert_date"]:
                            if p["naming"]["naming"] not in repeat_namings:
                                repeat_namings.append(p["naming"]["naming"])
                                result[i["naming"]["naming"]].append(
                                    {"source": k["alias"], "type": "INSIDE", "_id": str(k["_id"])})
                                if k["alias"] not in namings_count:
                                    namings_count[k["alias"]] = 1
                                else:
                                    namings_count[k["alias"]] += 1

        final_result = {}
        final_result["table"] = []
        for l in result:
            final_result["table"].append({"naming": l, "sources": result[l]})

        final_result["percentage"] = []
        for l in namings_count:
            final_result["percentage"].append({"source": l,
                                               "percentage": (namings_count[l] * 100) / len(data_table["fields"])})
        return final_result

    def _compare_object_with_data_tables(self, table_id):
        """

        :param table_id:
        :return:
        """
        result = {}
        tables_count = {}
        table_origin = {}

        data_table = self.data_table_repository.find_head(table_id)
        if data_table["stateTable"] == "N":
            raise ValueError("El tablón se encuentra en propuesta de namings.")

        others_data_tables = self.data_table_repository.find_other_tables(
            table_id)
        for j in data_table["origin_tables"]:
            table_origin[j["originName"]] = []
            for i in others_data_tables:
                for k in i["origin_tables"]:
                    if str(j["_id"]).strip().lower() == str(k["_id"]).strip().lower() or \
                            str(j["originName"]).strip().lower() == str(k["originName"]).strip().lower():
                        table_origin[j["originName"]].append(
                            {"alias": i["alias"], "_id": str(i["_id"])})
                        if i["alias"] in tables_count:
                            tables_count[i["alias"]] = +1
                        else:
                            tables_count[i["alias"]] = 1

        result["table"] = []
        for l in table_origin:
            result["table"].append({"name": l, "sources": table_origin[l]})

        result["percentage"] = []
        for l in tables_count:
            result["percentage"].append({"name": l,
                                         "percentage": (tables_count[l]*100)/len(data_table["origin_tables"])})
        return result
    

    def new_upload_data_table(self, data, user_id, project_owner):
        """
            Creates or just loads a data table without calculations from a sophia file
        :param data: dict
        :param user_id: id of the user creating it
        :param project_owner: id of the project which this data table belongs to
        :return: new data table id
        """
        data_table = self.data_table_repository.find_one_by_alias(data["alias"])
        if data_table is None:
            data_table = {}
        
        if data["load_type"] == "full" or data["load_type"] == "object":
            data_table = self.new_create_structure_head(data["newData"])
        if data["load_type"] == "full" or data["load_type"] == "fields":
            data_table["fields"] = self.new_create_structure_fields(data["newData"])
        
        data_table["user"] = ObjectId(str(user_id))
        data_table["project_owner"] = ObjectId(str(project_owner))
        try:
            data_table["modifications"] = self.add_modification(data_table["modifications"], {"user": data_table["user"], "type": "LOAD HEAD", "details": "LoadType: " + data["load_type"]})
        except Exception as e:
            traceback.print_exc()

        #if not self.data_dictionary_repository.find_table_by_alias(data_table["alias"]):
        data_dic = self.data_table_repository.find_one_by_alias(data_table["alias"])
        if not data_dic:
            data_table_id = self.data_table_repository.insert_one(data_table)
        else:
            data_table_id = data_dic["_id"]
            self.data_table_repository.update_field(str(data_table_id), data_table)
        #else:
        #   raise ValueError("Ya existe una TABLA con ese mismo alias {0}.".format(data_table["alias"]))
        

        return str(data_table_id)


    def clean_new_head(self, data):
        """
            Cleans some data of a new data table object previously loaded
        :param data: dict
        :return: object cleaned, dict
        """
        #Integer types
        data["current_depth"] = 0 if len(str(data["current_depth"]).strip()) == 0 else int(str(data["current_depth"]).strip().split(" ")[0])  
        data["required_depth"] = 0 if len(str(data["required_depth"]).strip()) == 0 else int(str(data["required_depth"]).strip().split(" ")[0])  
        data["estimated_volume_records"] = 0 if len(str(data["estimated_volume_records"]).strip()) == 0 else int(str(data["estimated_volume_records"]).strip().split(" ")[0])  

        #Loading type in lower case
        data["loading_type"] = data["loading_type"].lower() 

        #Proper spanish periodicity based on the database if needed
        temp_per = self.excels_repository.find_state_periodicity(data["periodicity"].upper())
        if temp_per is not None:
            data["periodicity"] = temp_per["periodicity"]
        
        #Origins cleaning
        origin_tables = []
        for origin_name in data["origin_tables"].split(";"):
            if origin_name.strip() == "":
                raise ValueError("No se encuentra cargada la tabla de origen {0}.".format(origin_name))
            else:
                origin_obj = self.get_origin_data_from_repos(origin_name)
                if origin_obj is not None:
                    origin_tables.append({
                        "_id": ObjectId(str(origin_obj["data"]["_id"])),
                        "originSystem": "HDFS-Avro" if origin_obj["type"] == "TR" else "HDFS-Parquet",
                        "originName": origin_obj["data"]["raw_name"] if origin_obj["type"] == "TR" else origin_obj["data"]["master_name"],
                        "originRoute": origin_obj["data"]["raw_route"] if origin_obj["type"] == "TR" else origin_obj["data"]["master_route"] if origin_obj["type"] == "TM" else origin_obj["data"]["master_path"],
                        "originType": "DT" if origin_obj["type"] == "DT" else "T", 
                    })
        data["origin_tables"] = origin_tables

        #Final comment
        data["master_comment"] = "Carga mediante archivos: " + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        return data

    def get_origin_data_from_repos(self, origin_name):
        """
            Creates a new data table head object from data to load
        :param data: dict
        :return:
        """
        temp_tablon = self.data_table_repository.find_master_name(origin_name)
        temp_dd_master = self.data_dictionary_repository.find_master_name(origin_name)
        temp_dd_raw = self.data_dictionary_repository.find_raw_name(origin_name)

        if temp_tablon is not None:
            return {"type": "DT", "data": temp_tablon}
        if temp_dd_master is not None:
            return {"type": "TM", "data": temp_dd_master}
        if temp_dd_raw is not None:
            return {"type": "TR", "data": temp_dd_raw}
        
        return None

    def new_create_structure_head(self, data):
        """
            Creates a new data table head object from data to load
        :param data: dict
        :return:
        """
        object_data = data[0][0] #to change
        new_data_table = Object_data_table({}).data

        for key in object_data:
            try:
                new_data_table[self.sophia_props_object[key]] = object_data[key].strip()
            except Exception: pass
                
        del new_data_table[""]
        new_data_table = self.clean_new_head(new_data_table)
        
        return new_data_table
    

    def load_naming(self, field):
        """
            Loads naming data from the Database to fill in to the proper naming structure inside a single field
        :param data: dict
        :return: object cleaned, dict
        """
        new_naming = Naming({}).data
        global_naming = self.field_repository.find_exactly_naming(field["naming"].strip().lower())

        if global_naming is not None:
            new_naming["naming"] = global_naming["naming"]["naming"]
            new_naming["isGlobal"] = "Y"
            new_naming["code"] = global_naming["code"]
            new_naming["suffix"] = global_naming["naming"]["suffix"]
            new_naming["Words"] = global_naming["naming"]["Words"]
            new_naming["hierarchy"] = global_naming["level"]

            field["logic"] = global_naming["logic"]
            field["description"] = global_naming["originalDesc"].replace("~", "\n")

            if "dataOwnerInformation" in global_naming and len(global_naming["dataOwnerInformation"]) != 0:
                field["domain"] = global_naming["dataOwnerInformation"][0]["domain"]
                if len(field["domain"]) == 0:
                    field["domain"] = "Unassigned"
                field["subdomain"] = global_naming["dataOwnerInformation"][0]["subdomain"]
                field["ownership"] = global_naming["dataOwnerInformation"][0]["ownership"]
                field["conceptualEntity"] = global_naming["dataOwnerInformation"][0]["conceptualEntity"]
                field["operationalEntity"] = global_naming["dataOwnerInformation"][0]["operationalEntity"]
        else:
            new_naming["naming"] = field["naming"]
            suffix_id = self.suffix_repository.find_one(field["naming"].strip().split("_")[-1])
            new_naming["suffix"] = suffix_id["_id"]

            field["logic"] = field["logic"].strip()
            field["description"] = field["description"].strip()

        field["naming"] = new_naming
        return field

    def clean_new_field_types(self, field):
        """
            Cleans data types of a new data field previously loaded
        :param field: dict
        :return: field cleaned, dict
        """
        field["dataType"] = field["dataType"].strip().upper()
        try:
            field["length"] = self.type_lengths[field["logicalFormat"].strip()]
        except Exception:
            try:
                field["length"] = int(field["logicalFormat"].strip()[:-1].strip().split("(")[1])  
            except Exception:
                field["length"] = 1

        if "DECIMAL" in field["dataType"]:
            field["dataType"] = "DECIMAL"
            field["length"] = int(field["logicalFormat"].strip().split("(")[1][:-1].split(",")[0])
        elif "INT32" in field["dataType"] or "INT" in field["dataType"]:
            field["dataType"] = "INT32"
                   
        return field

    def add_origins_new_field(self, field):
        """
            Adds origin namings data to a single field
        :param data: dict
        :return: field cleaned, dict
        """
        origin_namings = []
        for tmp_origin_name in field["originNamingsSources"].split(";"):
            tmp_origin_obj = self.get_origin_data_from_repos(tmp_origin_name.strip().lower())

            if tmp_origin_obj is not None:
                if tmp_origin_obj["type"] == "DT":
                    for tmp_naming in field["originNamings"].split(";"):
                        query_naming = self.data_table_repository.find_inside_naming(str(tmp_origin_obj["data"]["_id"]), tmp_naming)
                        try:
                            origin_namings.append({
                                "table_id": ObjectId(str(tmp_origin_obj["data"]["_id"])),
                                "table_name": tmp_origin_name,
                                "naming": tmp_naming,
                                "type": query_naming[0]["fields"][0]["dataType"],
                                "format": query_naming[0]["fields"][0]["format"],
                                "originType": "DT"})
                        except Exception: 
                            print("El naming de origen {0} no se encuentra en Nebula.".format(tmp_naming))
                else:
                    for tmp_naming in field["originNamings"].split(";"):
                        query_phase = "fieldsMaster" if tmp_origin_obj["type"] == "TM" else "fieldsRaw"
                        query_naming = self.data_dictionary_repository.find_inside_naming(str(tmp_origin_obj["data"]["_id"]), query_phase, tmp_naming)
                        try:
                            origin_namings.append({
                                "table_id": ObjectId(str(tmp_origin_obj["data"]["_id"])),
                                "table_name": tmp_origin_name,
                                "naming": tmp_naming,
                                "type": query_naming[0][query_phase][0]["destinationType"],
                                "format": query_naming[0][query_phase][0]["format"],
                                "originType": "T"})
                        except Exception: 
                            print("El naming de origen {0} no se encuentra en Nebula.".format(tmp_naming))

            elif not (tmp_origin_name.strip().lower() == "calculated" or tmp_origin_name.strip().lower() == "-" or tmp_origin_name.strip().lower() == ""):
                print("El origen {0} no se encuentra en la columna de origenes del objeto.".format(tmp_origin_name))

        field["originNamings"] = origin_namings
        del field["originNamingsSources"]
        return field

    def clean_new_field(self, field, user_id):
        """
            Cleans some data of a new data table object previously loaded
        :param data: dict
        :return: object cleaned, dict
        """
        print(field)
        #Column
        field["column"] = int(field["column"]) - 1

        #Load naming from DB and clean data types
        field = self.load_naming(field)
        field = self.clean_new_field_types(field)
        field = self.add_origins_new_field(field)

        #Clean natural words to 1 or 0
        try:
            field["key"] = self.yes_no_1_0[str(field["key"]).strip().upper()]
            field["mandatory"] = self.yes_no_1_0[str(field["mandatory"]).strip().upper()]
            field["tds"] = self.yes_no_1_0[str(field["tds"]).strip().upper()]
        except Exception: pass

        #Naming Validation flag
        state = "GL" if field["naming"]["isGlobal"] == "Y" else "PS"
        field["modification"].append({"user": ObjectId(user_id), "startDate": datetime.datetime.now(), "state": state, "stateValidation": "YES"})

        return field

    def sort_new_fields(self, array):
        """
            Sorts the fields based on their column number
        :param array: json array
        :return: array, with sorted fields
        """
        dirty = True
        while dirty:
            dirty = False
            for pos in range(0, len(array)):
                if array[pos]["column"] != pos:
                    dirty = True
                    array[array[pos]["column"]], array[pos] = array[pos], array[array[pos]["column"]]
        
        return array
        

    def new_create_structure_fields(self, data):
        """
            Updates the modification array for a data dictionary with the given data for
        :param old_mod: json array, current/old modifications array to be worked on
        :param new_mod: json, new modification data
        :return: array, with the updates values for direct replacement with the original
        """
        fields_data = data[1] #to change
        new_fields = []

        for naming in fields_data:
            new_tmp_field = Field_data_table({}).data
            for key in naming:
                try:
                    new_tmp_field[self.sophia_props_outnaming[key]] = str(naming[key]).strip()
                except Exception as e:
                    print("error " + str(e))

            new_tmp_field = self.clean_new_field(new_tmp_field, "5c2f81db5862a60969ae84f2")
            new_fields.append(new_tmp_field)

        new_fields = self.sort_new_fields(new_fields)
        return new_fields


    def add_modification(self, old_mod, new_mod):
        """
            Updates the modification array for a data dictionary with the given data for
        auditory reasons. Also 'cleans' the variable by removing any other variable thats not an array
        :param old_mod: json array, current/old modifications array to be worked on
        :param new_mod: json, new modification data
        :return: array, with the updates values for direct replacement with the original
        """
        temp_mod = { "user": new_mod["user"],
                     "type": new_mod["type"],
                     "details": new_mod["details"],
                     "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}

        if not "date" in old_mod: 
            if len(old_mod) == 10:
                old_mod.pop(0)
        else:
            old_mod = []
    
        old_mod.append(temp_mod)
        return old_mod

    def upload_data_table(self, data, user_id, project_owner):
        """
        :param data: dict
        :param user_id: str
        :param project_owner: str
        :return:
        """
        try:
            user_repo = self.user_repository.find_one(user_id)
            if user_repo is None:
                raise ValueError("El usuario no es válido.")
    
            alias = data["object"][33].strip().lower()
            data_table = self._create_structure_head(data, alias)
            data_table["fields"] = self._create_structure_fields(
                data, data_table["origin_tables"], user_id)

            data_table["user"] = ObjectId(str(user_id))
            data_table["project_owner"] = ObjectId(str(project_owner))

            if not self.data_table_repository.find_table_by_alias(alias) and not self.data_dictionary_repository.find_table_by_alias(alias):
                data_table["people"] = [{"user": ObjectId(
                    user_id), "rol": self.user_repository.find_one(user_id)["rol"]}]
                data_table_id = self.data_table_repository.insert_one(data_table)
            else:
                data_dic = self.data_table_repository.find_one_by_alias(alias)
                data_table_id = data_dic["_id"]
                flag = True
    
                for k in data_dic["people"]:
                    if user_id == str(k["user"]):
                        flag = False
                if flag:
                    data_dic["people"].append(
                        {"user": ObjectId(user_repo["_id"]), "rol": user_repo["rol"]})
    
                self.data_table_repository.update_field(
                    str(data_table_id), data_table)
                self.data_table_repository.update_people(
                    data_table_id, data_dic["people"])
        except Exception as e:
            traceback.print_exc()
        return str(data_table_id)

    def _create_structure_head(self, data, alias):
        """

        :param data:
        :return:
        """
        data_table = Object_data_table({}).data
        data_table["alias"] = alias
        data_table["master_name"] = data["object"][2].strip().lower()
        data_table["baseName"] = data["object"][3].strip().upper()
        data_table["observationField"] = data["object"][4].strip()
        data_table["perimeter"] = data["object"][7].strip()
        data_table["information_level"] = data["object"][8].strip()
        data_table["master_path"] = data["object"][14].strip().lower()
        data_table["uuaaMaster"] = data["object"][15].strip().upper()
        tmp = self.functional_map_repository.find_uuaa(
            data_table["uuaaMaster"][1:])
        data_table["data_source"] = tmp["data_source"] if "data_source" in tmp else ""
        data_table["information_group_level_1"] = tmp["domain"] if "domain" in tmp else tmp["level1"] if "level1" in tmp else ""
        data_table["information_group_level_2"] = tmp["level2"] if "level2" in tmp else tmp["full_name"] if "full_name" in tmp else ""
        
        data_table["partitions"] = data["object"][16].strip()

        temp_per = self.excels_repository.find_state_periodicity(
            data["object"][17].strip().upper())
        if temp_per is not None:
            data_table["periodicity"] = temp_per["periodicity"]
        else:
            data_table["periodicity"] = data["object"][17].strip().upper()
        data_table["loading_type"] = data["object"][19].strip().lower()

        if len(str(data["object"][20]).strip()) != 0:
            data_table["current_depth"] = int(
                str(data["object"][20]).strip().split(" ")[0])

        if len(str(data["object"][21]).strip()) != 0:
            data_table["required_depth"] = int(
                str(data["object"][21]).strip().split(" ")[0])

        if len(str(data["object"][22]).strip()) != 0:
            data_table["estimated_volume_records"] = int(
                str(data["object"][22]).strip())

        data_table["target_file_type"] = data["object"][30].strip()
        data_table["target_file_delimiter"] = data["object"][31].strip()
        data_table["tacticalObject"] = data["object"][34].strip()
        origin_tables = []
        origins_temp = data["object"][24].strip().split(";")
        #routes_temp = data["object"][26].strip().split(";")
        for i in origins_temp:
            if i.strip() == "":
                i = "--none--"
            
            temp_tablon = self.data_table_repository.find_master_name(i)
            temp_dd_master = self.data_dictionary_repository.find_master_name(
                i)
            temp_dd_raw = self.data_dictionary_repository.find_raw_name(i)
            
            if temp_tablon is None and temp_dd_raw is None and temp_dd_master is None and i != "--none--":
                raise ValueError(
                    "No se encuentra cargada la tabla de origen {0}.".format(i))

            if temp_tablon is not None:
                """
                if temp_tablon["master_path"] not in routes_temp:
                    raise ValueError(
                        "El tablon {0} en Nebula, con la ruta {1} no se encuentra en columna de rutas de origen.".format(
                            i, temp_tablon["master_path"]))
                """
                origin_tables.append({
                    "_id": ObjectId(str(temp_tablon["_id"])),
                    "originSystem": "HDFS-Parquet",
                    "originName": temp_tablon["master_name"],
                    "originRoute": temp_tablon["master_path"],
                    "originType": "DT"
                })

            if temp_dd_master is not None:
                """
                if temp_dd_master["master_route"] not in routes_temp:
                    raise ValueError(
                        "La tabla {0} en Nebula, con la ruta {1} no se encuentra en columna de rutas de origen.".format(
                            i, temp_dd_master["master_route"]))
                """
                origin_tables.append({
                    "_id": ObjectId(str(temp_dd_master["_id"])),
                    "originSystem": "HDFS-Parquet",
                    "originName": temp_dd_master["master_name"],
                    "originRoute": temp_dd_master["master_route"],
                    "originType": "T"
                })

            if temp_dd_raw is not None:
                """
                if temp_dd_raw["raw_route"] not in routes_temp:
                    raise ValueError(
                        "La tabla {0} en Nebula, con la ruta {1} no se encuentra en columna de rutas de origen.".format(
                            i, temp_dd_raw["raw_route"]))
                """

                origin_tables.append({
                    "_id": ObjectId(str(temp_dd_raw["_id"])),
                    "originSystem": "HDFS-Avro",
                    "originName": temp_dd_raw["raw_name"],
                    "originRoute": temp_dd_raw["raw_route"],
                    "originType": "T"
                })

        data_table["origin_tables"] = origin_tables
        data_table["master_comment"] = "Carga mediante archivos."
        return data_table

    def _create_structure_fields(self, data, origin_tables, user):
        """

        :param data: dict
        :param origin_tables: array
        :return:
        """
        fields = []
        for j in range(0, len(data["fields"])):
            temp_field = Field_data_table({}).data
            naming = self.field_repository.find_exactly_naming(
                data["fields"][j][5].strip().lower())
            state = "PS"
            if naming is not None:
                state = "GL"
                temp_field["naming"]["naming"] = naming["naming"]["naming"]
                temp_field["naming"]["suffix"] = naming["naming"]["suffix"]
                temp_field["naming"]["Words"] = naming["naming"]["Words"]
                temp_field["naming"]["isGlobal"] = "Y"
                temp_field["naming"]["code"] = naming["code"]
                temp_field["naming"]["hierarchy"] = naming["level"]

                temp_field["logic"] = naming["logic"]
                temp_field["description"] = naming["originalDesc"].replace(
                    "~", "\n")

                if "dataOwnerInformation" in naming and len(naming["dataOwnerInformation"]) != 0:
                    temp_field["domain"] = naming["dataOwnerInformation"][0]["domain"]
                    if len(temp_field["domain"]) == 0:
                        temp_field["domain"] = "Unassigned"
                    temp_field["subdomain"] = naming["dataOwnerInformation"][0]["subdomain"]
                    temp_field["ownership"] = naming["dataOwnerInformation"][0]["ownership"]
                    temp_field["conceptualEntity"] = naming["dataOwnerInformation"][0]["conceptualEntity"]
                    temp_field["operationalEntity"] = naming["dataOwnerInformation"][0]["operationalEntity"]
            else:
                suffix_id = self.suffix_repository.find_one(
                    data["fields"][j][5].strip().split("_")[-1])
                if suffix_id is None:
                    raise ValueError("Suffix " + data["fields"][j][5].strip().split(
                        "_")[-1] + "not found..." + data["fields"][j][5])

                temp_field["naming"]["naming"] = data["fields"][j][5].strip().lower()
                temp_field["naming"]["suffix"] = suffix_id["_id"]
                temp_field["logic"] = data["fields"][j][7].strip()
                temp_field["description"] = data["fields"][j][8].strip()

            if len(data["fields"][j][10].strip()) != 0:
                temp_field["operation"] = data["fields"][j][10].strip()

            temp_field["catalogue"] = data["fields"][j][11].strip()
            data_type = data["fields"][j][13].strip()
            if "STRING" in data_type.upper():
                if "TIME" == data["fields"][j][15].strip():
                    temp_field["length"]=12
                elif "CLOB" == data["fields"][j][15].strip():
                    temp_field["length"]=3999
                elif "DATE" == data["fields"][j][15].strip():
                    temp_field["length"] = 10
                elif "TIMESTAMP" == data["fields"][j][15].strip():
                    temp_field["length"] = 26
                else:
                    temp_field["length"] = int(data["fields"][j][15].strip()[
                                               :-1].strip().split("(")[1])
                temp_field["dataType"] = "STRING"
                temp_field["logicalFormat"] = data["fields"][j][15].strip()
            elif "DECIMAL" in data_type.upper():
                temp_field["length"] = int(
                    data["fields"][j][15].strip().split("(")[1][:-1].split(",")[0])
                temp_field["dataType"] = "DECIMAL"
                temp_field["logicalFormat"] = data["fields"][j][15].strip()
            elif "TIMESTAMP_MILLIS" in data_type.upper():
                temp_field["length"] = 26
                temp_field["dataType"] = "TIMESTAMP_MILLIS"
                temp_field["logicalFormat"] = data["fields"][j][15].strip()
            elif "INT64" in data_type.upper():
                temp_field["length"] = 255
                temp_field["dataType"] = "INT64"
                temp_field["logicalFormat"] = data["fields"][j][15].strip()
            elif "INT32" in data_type.upper():
                if data["fields"][j][15].strip().upper() == "NUMERIC SHORT":
                    temp_field["length"] = 10
                    temp_field["dataType"] = "INT32"
                    temp_field["logicalFormat"] = data["fields"][j][15].strip()
                elif data["fields"][j][15].strip().upper() == "NUMERIC LARGE":
                    temp_field["length"] = 255
                    temp_field["dataType"] = "INT32"
                    temp_field["logicalFormat"] = data["fields"][j][15].strip()
            elif "DATE" in data_type.upper():
                temp_field["length"] = 10
                temp_field["dataType"] = "DATE"
                temp_field["logicalFormat"] = data["fields"][j][15].strip()
                
            if len(data["fields"][j][14].strip()) != 0:
                temp_field["format"] = data["fields"][j][14].strip()

            temp_field["key"] = self.yes_no_1_0[data["fields"]
                                                [j][16].strip().upper()]
            temp_field["mandatory"] = self.yes_no_1_0[data["fields"]
                                                      [j][17].strip().upper()]

            if len(data["fields"][j][18].strip()) != 0:
                temp_field["default"] = data["fields"][j][18].strip()

            origin_namings = []
            temp_tables = data["fields"][j][19].strip().split(";")
            temp_namings = data["fields"][j][20].strip().split(";")
            temp_types = data["fields"][j][21].strip().split(";")
            temp_formats = data["fields"][j][22].strip().split(";")

            for k in temp_tables:
                flag = False
                for l in origin_tables:
                    if l["originName"].strip().lower() == k.strip().lower():
                        flag = True

                        if l["originType"] == "DT":
                            for b in temp_namings:
                                query_naming = self.data_table_repository.find_inside_naming(
                                    str(l["_id"]), b)
                                if query_naming.count() != 0:
                                    destiny = query_naming[0]["fields"][0]["dataType"]
                                    if destiny not in temp_types:
                                        print("El tipo de dato {0} no se encuentra para el naming {1} del tablón {2}.".format(
                                            destiny, b, k))

                                    format = query_naming[0]["fields"][0]["format"]
                                    if format != "empty" and format not in temp_formats:
                                        print("El formato {0} no se encuentra para el naming {1} del tablón {2}.".format(
                                            format, b, k))

                                    if format == "empty":
                                        format = ""

                                    origin_namings.append({"table_id": ObjectId(str(l["_id"])),
                                                           "table_name": query_naming[0]["master_name"],
                                                           "naming": b.strip().lower(),
                                                           "type": destiny,
                                                           "format": format,
                                                           "originType": "DT"})
                        else:
                            for b in temp_namings:
                                phase = "fieldsMaster"
                                if l["originSystem"] == "HDFS-Avro":
                                    phase = "fieldsRaw"
                                query_naming = self.data_dictionary_repository.find_inside_naming(
                                    str(l["_id"]), phase, b)
                                if query_naming.count() != 0:
                                    destiny = query_naming[0][phase][0]["destinationType"]
                                    if destiny.lower() not in temp_types:
                                        print("El tipo de dato {0} no se encuentra para el naming {1} de la tabla {2}.".format(
                                            destiny, b, k))

                                    format = query_naming[0][phase][0]["format"]
                                    if format != "empty" and format not in temp_formats:
                                        print("El formato {0} no se encuentra para el naming {1} de la tabla {2}.".format(
                                            format, b, k))

                                    if format == "empty":
                                        format = ""

                                    if phase == "fieldsRaw":
                                        table_name = query_naming[0]["raw_name"]
                                    else:
                                        table_name = query_naming[0]["master_name"]

                                    origin_namings.append({"table_id": ObjectId(str(l["_id"])),
                                                           "table_name": table_name,
                                                           "naming": b.strip().lower(),
                                                           "type": destiny,
                                                           "format": format,
                                                           "originType": "T"})

                if not flag and not (k.strip().lower() == "calculated" or k.strip().lower() == "-" or k.strip().lower() == ""):
                    raise ValueError("El origen {0} no se encuentra en la columna de origenes del objeto.".format(k))

            temp_field["originNamings"] = origin_namings
            temp_field["column"] = j
            if len(data["fields"][j][27].strip()) != 0:
                temp_field["tokenization"] = data["fields"][j][27].strip()
            temp_field["tds"] = self.yes_no_1_0[data["fields"]
                                                [j][34].strip().upper()]
            temp_field["modification"].append({"user": ObjectId(user), "startDate": datetime.datetime.now(),
                                               "state": state, "stateValidation": "YES"})
            fields.append(temp_field)
        return fields

    def update_information(self, table_id, category, information):
        """

        :param table_id:
        :param category:
        :param information:
        :return:
        """
        if category.strip().lower() == "origins":
            self._upload_origin(table_id, information)
        elif category.strip().lower() == "structure":
            self._upload_structure(table_id, information)
        else:
            raise ValueError("No se reconoce el tipo de archivo.")

    def _upload_structure(self, table_id, information):
        """

        :param table_id:
        :param information:
        :return:        new_tmp_field = Field_data_table({}).data

        """
        data_table = self.data_table_repository.find_one(table_id)
        if data_table is None:
            raise ValueError("El tablón no se encuentra registrado.")

        isGlobal = False
        if "K" in data_table["uuaaMaster"].upper():
            isGlobal = True

        user_id = self.user_repository.find_one(information["user_id"])
        if user_id is None:
            raise ValueError("Permisos denegados para el usuario.")

        flag = True
        for k in data_table["people"]:
            if str(user_id["_id"]) == str(k["user"]):
                flag = False
        if flag:
            data_table["people"].append(
                {"user": ObjectId(str(user_id["_id"])), "rol": user_id["rol"]})

        user_id = user_id["_id"]

        temp_mod = [{"user": ObjectId(str(user_id)),
                     "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]

        for i in data_table["modifications"]:
            temp_mod.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]})

        validation_types = {
            "STRING": {"format": "empty", "logical": "ALPHANUMERIC", "suffix": ["id", "name", "desc", "type"]},
            "INT": {"format": "empty", "logical": "NUMERIC SHORT", "suffix": ["number"]},
            "INT32": {"format": "empty", "logical": "NUMERIC LARGE", "suffix": ["number"]},
            "DECIMAL": {"format": "empty", "logical": "DECIMAL", "suffix": ["number", "per", "amount"]},
            "INT64": {"format": "empty", "logical": "NUMERIC BIG", "suffix": ["number"]},
            "DATE": {"format": "yyyy-MM-dd", "logical": "DATE", "suffix": ["date"]},
            "TIMESTAMP": {"format": "yyyy-MM-dd HH:mm:ss.SSSSSS", "logical": "TIMESTAMP", "suffix": ["date"]}}

        temp_fields = data_table["fields"]
        information = information["fields"]
        
        new_temp_fields = []

        for j in range(0, len(information)):
            namings = [i["naming"]["naming"] for i in temp_fields]

            if len(information[j]["naming"].strip().lower()) != 0:
                new_naming = Field_data_table({}).data
                naming = self.field_repository.find_exactly_naming(information[j]["naming"].strip().lower())
                suffix = information[j]["naming"].strip().lower().split("_")[-1]
                state_naming = "PS"
                new_naming["column"] = int(j)
                
                if naming is not None:
                    new_naming["naming"]["naming"] = naming["naming"]["naming"]
                    new_naming["naming"]["suffix"] = naming["naming"]["suffix"]
                    new_naming["naming"]["Words"] = naming["naming"]["Words"]
                    new_naming["naming"]["isGlobal"] = "Y"
                    new_naming["naming"]["code"] = naming["code"]
                    new_naming["naming"]["hierarchy"] = naming["level"]
                    new_naming["logic"] = naming["logic"]
                    new_naming["description"] = naming["originalDesc"].replace("~", "\n")
                    state_naming = "GL"
                    
                    if "dataOwnerInformation" in naming and len(naming["dataOwnerInformation"]) != 0:
                        new_naming["domain"] = naming["dataOwnerInformation"][0]["domain"]
                        if len(new_naming["domain"]) == 0:
                            new_naming["domain"] = "Unassigned"
                        new_naming["subdomain"] = naming["dataOwnerInformation"][0]["subdomain"]
                        new_naming["ownership"] = naming["dataOwnerInformation"][0]["ownership"]
                        new_naming["conceptualEntity"] = naming["dataOwnerInformation"][0]["conceptualEntity"]
                        new_naming["operationalEntity"] = naming["dataOwnerInformation"][0]["operationalEntity"]
                else:
                    suffix_id = self.suffix_repository.find_one(suffix)
                    if suffix_id is None:
                        raise ValueError("Suffix " + information[j]["naming"].strip().split("_")[-1] + "not found..." + information[j]["naming"])

                    new_naming["naming"]["naming"] = information[j]["naming"].strip().lower()
                    new_naming["naming"]["suffix"] = suffix_id["_id"]

                    try:
                        new_naming["logic"] = information[j]["logic"].strip()
                        new_naming["description"] = information[j]["description"].strip()
                    except Exception as e:
                        raise ValueError("El naming " + information[j]["naming"] + " en la posición " + str(int(j)+2) +
                                         " no es global, es necesario informar el nombre lógico y descripció.")

                new_naming["key"] = int(information[j]["key"])
                new_naming["mandatory"] = int(information[j]["mandatory"])
                new_naming["length"] = int(information[j]["length"])
                new_naming["direct"] = int(information[j]["direct"])
                new_naming["operation"] = str(information[j]["operation"])
                new_naming["catalogue"] = "N/A"
                
                if len(str(information[j]["catalogue"])) != 0:
                    new_naming["catalogue"] = information[j]["catalogue"]

                if information[j]["data_type"].strip().upper() in validation_types:
                    if not isGlobal:
                        if suffix not in validation_types[information[j]["data_type"].strip().upper()]["suffix"]:
                            raise ValueError("El naming {0} en la posición {1} no tiene un sufijo válido para el tipo de dato {2}.".format(
                                information[j]["naming"].strip().lower(), j+2, information[j]["data_type"].strip().upper()))

                    new_naming["dataType"] = information[j]["data_type"].strip().upper()
                    new_naming["logicalFormat"] = validation_types[information[j]["data_type"].strip().upper()]["logical"]
                    new_naming["format"] = validation_types[information[j]["data_type"].strip().upper()]["format"]
                    
                    if "INT" in information[j]["data_type"].strip().upper():
                        if int(information[j]["length"]) > 4:
                            new_naming["logicalFormat"] = "NUMERIC LARGE"

                    if "DECIMAL" in information[j]["data_type"].strip().upper():
                        new_naming["logicalFormat"] = "DECIMAL("+str(int(information[j]["decimals"])+int(information[j]["integers"]))+","+str(int(information[j]["decimals"])) + ")"
                        new_naming["format"] = "("+str(int(information[j]["decimals"])+int(information[j]["integers"]))+","+str(int(information[j]["decimals"])) + ")"

                    if "STRING" in information[j]["data_type"].strip().upper():
                        new_naming["logicalFormat"] = "ALPHANUMERIC("+str(int(information[j]["length"])) + ")"

                else:
                    new_naming["dataType"] = "empty"
                    new_naming["logicalFormat"] = "empty"
                    new_naming["format"] = "empty"
                temp_mod_fi = new_naming["modification"]
                temp_up_fi = [{"stateValidation": "NO", "startDate": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "state": state_naming, "user": ObjectId(str(user_id))}]

                new_naming["modification"] = temp_up_fi + temp_mod_fi[:4]
                new_temp_fields.append(new_naming)

        self.data_table_repository.update_all_fields(str(table_id), new_temp_fields)
        self.data_table_repository.update_modifications_audit(table_id, temp_mod[:10])
        self.data_table_repository.update_people(table_id, data_table["people"])
        
    def _clean_origins_upload(self, table_id):
        """

        :param table_id:
        :param information:
        :return:
        """
        data_table = self.data_table_repository.find_one(table_id)

    def _upload_origin_dos(self, table_id, data):
        """
        :param table_id:
        :param information:
        :return:
        """
        data_table = self.data_table_repository.find_one(table_id)
        new_origin_tables = []
        set_origin = {}

        temp_fields = data_table["fields"]
        
        
    def _upload_origin(self, table_id, information):
        """

        :param table_id:
        :param information:
        :return:
        """
        #Clean origin from namings
        self.data_table_repository.reset_all_naming_origins(table_id)   

        data_table = self.data_table_repository.find_one(table_id)
        if data_table is None:
            raise ValueError("El tablón no se encuentra registrado.")

        user_id = self.user_repository.find_one(information["user_id"])
        if user_id is None:
            raise ValueError("Permisos denegados para el usuario.")
        user_id = user_id["_id"]
        
        data_table["modifications"] = self.add_modification(data_table["modifications"], {"user": user_id, "type": "LOAD ORIGINS", "details": "Plantilla usuario"})

        #temp_origin_tables = data_table["origin_tables"]
        temp_origin_tables = []
        temp_fields = data_table["fields"]
        set_origin = {}
        
        #Prepares set_origin array with old origins
        """
        for k in temp_origin_tables:
            if k["originName"].strip().lower()[6:] not in set_origin:
                set_origin[k["originName"].strip().lower()[6:]] = k["originName"].strip().lower()
            else:
                if set_origin[k["originName"].strip().lower()[6:]] != k["originName"].strip().lower():
                    raise ValueError("El origen {0} se encuentra mapeado desde fase RAW y MASTER.".format(k["originName"].strip().lower()))
        """
                  
        #Ciclo para recorrer los origenes a cargar
        for j in information["fields"]: 
            flag = False
            if not flag: #errores
                if 0 > j["field_position"] or j["field_position"] > len(temp_fields):
                    raise ValueError("La posicion {0} es negativo o supera la longitud {1} de los campos del tablon.".format(
                        j["field_position"], len(temp_fields)))

                if j["type"].strip().upper() not in ["TR", "TM", "T"]:
                    raise ValueError("No se conoce el valor {0} como tipo de origen.".format(j["type"]))

                if j["type"].strip().upper() == "TR":

                    set_namings = [i["naming"] + i["table_name"]
                                   for i in temp_fields[int(j["field_position"]) - 1]["originNamings"]]
                    if j["naming"].strip().lower() + j["table"].strip().lower() not in set_namings:

                        temp_table_id = self.data_dictionary_repository.find_raw_name(
                            j["table"].strip().lower())
                        if temp_table_id is None:
                            raise ValueError("La tabla {0} no se encuentra en Nebula.".format(
                                j["table"].strip().lower()))

                        if temp_table_id["raw_name"][6:] in set_origin and set_origin[temp_table_id["raw_name"][6:]] != temp_table_id["raw_name"]:
                            raise ValueError("El origen {0} se encuentra mapeado en ambas fases, por favor elegir solo una.".format(temp_table_id["raw_name"]))
                        if not temp_table_id["raw_name"][6:] in set_origin:
                            set_origin[temp_table_id["raw_name"][6:]] = temp_table_id["raw_name"]
                            temp_origin_tables.append({
                                "_id": ObjectId(str(temp_table_id["_id"])),
                                "originSystem": "HDFS-Avro",
                                "originName": temp_table_id["raw_name"],
                                "originRoute": temp_table_id["raw_route"],
                                "originType": "T"
                            })

                        query_naming = self.data_dictionary_repository.find_inside_naming(
                            str(temp_table_id["_id"]), "fieldsRaw", j["naming"].strip().lower())
                        if query_naming.count() == 0:
                            raise ValueError("La tabla {0} no cuenta con el naming {1} en Nebula.".format(
                                j["table"].strip().lower(), j["naming"].strip().lower()))

                        destiny = query_naming[0]["fieldsRaw"][0]["destinationType"].upper(
                        )
                        format = query_naming[0]["fieldsRaw"][0]["format"]

                        temp_fields[int(j["field_position"]) - 1]["originNamings"].append({
                            "table_id": ObjectId(str(temp_table_id["_id"])),
                            "table_name": temp_table_id["raw_name"],
                            "naming": j["naming"].strip().lower(),
                            "type": destiny,
                            "format": format,
                            "originType": "T"
                        })

                if j["type"].strip().upper() == "TM":

                    set_namings = [i["naming"] + i["table_name"]
                                   for i in temp_fields[int(j["field_position"]) - 1]["originNamings"]]
                    if j["naming"].strip().lower() + j["table"].strip().lower() not in set_namings:

                        temp_table_id = self.data_dictionary_repository.find_master_name(
                            j["table"].strip().lower())
                        if temp_table_id is None:
                            raise ValueError("La tabla {0} no se encuentra en Nebula.".format(
                                j["table"].strip().lower()))

                        if temp_table_id["master_name"][6:] in set_origin and set_origin[temp_table_id["master_name"][6:]] != temp_table_id["master_name"]:
                            raise ValueError("El origen {0} se encuentra mapeado desde fase RAW y MASTER.".format(
                                temp_table_id["master_name"]))

                        if not temp_table_id["master_name"][6:] in set_origin:
                            set_origin[temp_table_id["master_name"][6:]] = temp_table_id["master_name"]
                            temp_origin_tables.append({
                                "_id": ObjectId(str(temp_table_id["_id"])),
                                "originSystem": "HDFS-Parquet",
                                "originName": temp_table_id["master_name"],
                                "originRoute": temp_table_id["master_route"],
                                "originType": "T"
                            })
                        else:
                            print("duplicado " + temp_table_id["master_name"])

                        query_naming = self.data_dictionary_repository.find_inside_naming(
                            str(temp_table_id["_id"]), "fieldsMaster", j["naming"].strip().lower())
                        if query_naming.count() == 0:
                            raise ValueError("La tabla {0} no cuenta con el naming {1} en Nebula.".format(
                                j["table"].strip().lower(), j["naming"].strip().lower()))

                        destiny = query_naming[0]["fieldsMaster"][0]["destinationType"].upper(
                        )
                        format = query_naming[0]["fieldsMaster"][0]["format"]

                        temp_fields[int(j["field_position"]) - 1]["originNamings"].append({
                            "table_id": ObjectId(str(temp_table_id["_id"])),
                            "table_name": temp_table_id["master_name"],
                            "naming": j["naming"].strip().lower(),
                            "type": destiny,
                            "format": format,
                            "originType": "T"
                        })

                if j["type"].strip().upper() == "T":

                    set_namings = [i["naming"] + i["table_name"]for i in temp_fields[int(j["field_position"]) - 1]["originNamings"]]
                    
                    if j["naming"].strip().lower() + j["table"].strip().lower() not in set_namings:

                        temp_table_id = self.data_table_repository.find_master_name(
                            j["table"].strip().lower())
                        if temp_table_id is None:
                            raise ValueError("El tablón {0} no se encuentra en Nebula.".format(
                                j["table"].strip().lower()))

                        if temp_table_id["master_name"][6:] in set_origin and set_origin[temp_table_id["master_name"][6:]] != temp_table_id["master_name"]:
                            raise ValueError("El origen {0} se encuentra mapeado desde fase RAW y MASTER.".format(temp_table_id["master_name"]))

                        if not temp_table_id["master_name"][6:] in set_origin:
                            set_origin[temp_table_id["master_name"][6:]] = temp_table_id["master_name"]
                            temp_origin_tables.append({
                                "_id": ObjectId(str(temp_table_id["_id"])),
                                "originSystem": "HDFS-Parquet",
                                "originName": temp_table_id["master_name"],
                                "originRoute": temp_table_id["master_path"],
                                "originType": "DT"
                            })
                        else:
                            print("duplicado " + temp_table_id["master_name"])
                            
                        query_naming = self.data_table_repository.find_inside_naming(
                            str(temp_table_id["_id"]), j["naming"].strip().lower())
                        if query_naming.count() == 0:
                            raise ValueError("El tablón {0} no cuenta con el naming {1} en Nebula.".format(
                                j["table"].strip().lower(), j["naming"].strip().lower()))

                        destiny = query_naming[0]["fields"][0]["dataType"].upper(
                        )
                        format = query_naming[0]["fields"][0]["format"]

                        temp_fields[int(j["field_position"]) - 1]["originNamings"].append({
                            "table_id": ObjectId(str(temp_table_id["_id"])),
                            "table_name": temp_table_id["master_name"],
                            "naming": j["naming"].strip().lower(),
                            "type": destiny,
                            "format": format,
                            "originType": "DT"
                        })
                     
        self.data_table_repository.update_all_fields(str(table_id), temp_fields)
        self.data_table_repository.update_origin_tables(str(table_id), temp_origin_tables)
        self.data_table_repository.update_modifications_audit(table_id, data_table["modifications"])
        #self.data_table_repository.update_people(table_id, data_table["people"])

    def find_table_by_filter(self, table_name, alias, project_id, table_state, logic_name, table_desc):
        """

        :param table_name:
        :param alias:
        :param project_id:
        :param table_state:
        :param logic_name:
        :param table_desc:
        :return:
        """
        table_name = str(table_name).strip()
        alias = str(alias).strip()
        table_state = str(table_state).strip()
        logic_name = str(logic_name).strip()
        table_desc = str(table_desc).strip()
        tables = []

        if len(str(project_id).strip()) == 0:
            tables = self.data_table_repository.find_all_without_fields()
        else:
            use_cases = self.project_repository.find_project_use_cases(
                str(project_id).strip())
            for case in use_cases["useCases"]:
                casetmp = self.use_case_repository.find_one(case)
                for table_id in casetmp["tablones"]:
                    table = self.data_table_repository.find_by_id(
                        str(table_id))
                    if table not in tables:
                        tables.append(table)

        if len(table_name) != 0:
            tables = self.__filter_by_property(
                tables, table_name, "master_name")
        if len(alias) != 0:
            tables = self.__filter_by_property(tables, alias, "alias")
        if len(table_state) != 0:
            tables = self.__filter_by_state_table(tables, table_state)
        if len(logic_name) != 0:
            tables = self.__filter_by_property(tables, logic_name, "baseName")
        if len(table_desc) != 0:
            tables = self.__filter_by_property(
                tables, table_desc, "observationField")
        tables_query = []
        for table in tables:
            if table is not None:
                table["legacy_name"] = "---"
                table["raw_name"] = "---"
                table["uuaaRaw"] = "---"
                table["stateTable"] = self._define_table_state(
                    table["stateTable"])
                table["type"] = "DT"
                table["_id"] = str(table["_id"])
                try:
                    email = self.user_repository.find_one(str(table["user"]))
                    table["user"] = email["email"]
                except Exception as e:
                    table["user"] = "Unassigned"

                try:
                    table["project_name"] = self._get_table_project(
                        str(table["_id"]))
                except Exception as e:
                    table["project_name"] = "Unassigned"
                tables_query.append(table)
        return tables_query

    def _define_table_state(self, state):
        """
        Return the name of the state of the table
        :param state: str, a letter that represents the state of the table
        :return: str, the name of the state
        """
        final_state = ""
        states = {"I": "Listo para ingestar", "IN": "Ingestado", "M": "Mallas", "P": "Producción", "Q": "Calidad",
                  "G": "Gobierno", "A": "Arquitectura", "N": "En propuesta", "RN": "VoBo Negocio", "D": "Descartado", "R": "Revisión Solicitud"}
        if state in states:
            final_state = states[state]
        return final_state

    def _get_table_project(self, table_id):
        """
        Get the project name of the table
        :param table_id: ObjectId, identifier of the table
        :return: str, name of the project
        """
        project_name = ""
        table_use_case = None
        uses_cases = self.use_case_repository.find_all()
        break_use_case = False
        for useCase in uses_cases:
            case_tmp = self.use_case_repository.find_one(useCase["_id"])
            if break_use_case:
                break
            for table in case_tmp["tablones"]:
                if str(table_id) == str(table):
                    table_use_case = case_tmp
                    break_use_case = True
                    break
        projects = self.project_repository.find_all()
        break_project = False
        for project in projects:
            if break_project:
                break
            for use_case in project["useCases"]:
                if str(table_use_case["_id"]) == str(use_case):
                    project_name = project["name"]
                    break_project = True
                    break
        return project_name

    def _find_naming_state(self, query, state):
        """Check what namings are Ok or noOk in a comment

        :param query: dictionary, with data to check if state conditions its the same
        :param state: str, condition to check
        :return: dictionary, new data with condition ok
        """
        res = []
        for i in query:
            if len(i["comments"]) == 0:
                if state == "NOK":
                    res.append(i)
            else:
                if state == "NOK":
                    if i["comments"][len(i["comments"])-1]["reasonReturn"] != "OK" and i["comments"][len(i["comments"])-1]["reasonReturn"] != "OBSERVACIÓN":
                        res.append(i)
                else:
                    if i["comments"][len(i["comments"])-1]["reasonReturn"] == state:
                        res.append(i)
        return res

    def _find_logic(self, query, logic):
        """Check what naming has the same logic name

        :param query: dictionary, with data to check if logic name its the same
        :param logic: str, condition to check
        :return: dictionary, new data with condition ok
        """
        res = []
        logic_transform = self.field_service._structure_text(logic)
        for i in query:
            if logic_transform in i["logic"]:
                res.append(i)
        return res

    def _find_desc(self, query, desc):
        """Check what naming has the same description or part of them

        :param query: dictionary, with data to check if description naming its the same or part of them
        :param logic: str, condition to check
        :return: dictionary, new data with condition ok
        """
        res = []
        desc_transform = self.field_service._structure_text(desc)
        for i in query:
            if desc_transform in self.field_service._structure_text(i["description"]):
                res.append(i)
        return res

    def _find_naming(self, query, naming):
        """
        Check what naming has the same naming of part of them

        :param query: dictionary, with data to check if a naming its the same of part of them
        :param naming: str, condition to check
        :return: dictionary, new data with condition ok
        """
        res = []
        for field in query:
            if naming in field["naming"]["naming"]:
                res.append(field)
        return res

    def _find_suffix_name(self, query, suffix):
        """
        Get fields with especific suffix
        :param query: dictionary, with data to check if a naming its the same of part of them
        :param suffix: str, represent the suffix
        :return: list of fields
        """
        res = []
        for field in query:
            if field["naming"]["naming"].endswith("_"+suffix):
                res.append(field)
        return res

    def find_inside_data_table(self, data_table_id, suffix, naming, logic, desc, state):
        """

        :param data_table_id:
        :param suffix:
        :param naming:
        :param logic:
        :param desc:
        :param state:
        :return:
        """
        query = self.data_table_repository.find_one(str(data_table_id))
        if query is None:
            raise ValueError("")
        fields = query["fields"]

        if len(state.strip()) != 0:
            fields = self._find_naming_state(fields, state)
        if len(logic.strip()) != 0:
            fields = self._find_logic(fields, logic)
        if len(desc.strip()) != 0:
            fields = self._find_desc(fields, desc)
        if len(naming.strip()) != 0:
            fields = self._find_naming(fields, naming)
        if len(suffix.strip()) != 0:
            fields = self._find_suffix_name(fields, suffix)

        return fields

    def find_data_tables_state_only(self, state):
        """
            Finds all data tables in a specific state only

        :param state: str, String that reference a state to get it
        :return: Array of json, contains id's and name of data dictionaries tables
        """
        data_tables = self.data_table_repository.find_data_tables_state(state)
        resTables = []
        for table in data_tables:
            try:
                if table is not None:
                    try:
                        table["fieldsLength"] = len(table["fields"])
                        del table["fields"]
                    except:
                        table["fieldsLength"] = 0
                    
                    try:
                        table["last_mod"] = table["modifications"][0]["date"]
                        del table["modifications"]
                    except:
                        table["last_mod"] = "1999-01-01 00:00:00"

                resTables.append(table)
            except Exception as e:
                print(table["alias"] + e)
        try:
            resTables.sort(key=lambda x: datetime.datetime.strptime(x['last_mod'], '%Y-%m-%d %H:%M:%S'))
        except Exception as e:
            print(e)

        return resTables[::-1]

    def get_dt_excel(self, id_pro):
        pipeline = [
            {'$match': {'_id': ObjectId(id_pro)}},
            {'$lookup': {'from': 'useCase', 'localField': 'useCases',
                         'foreignField': '_id', 'as': 'use_cases_projects'}},
            {'$unwind': {'path': '$use_cases_projects'}},
            {'$project': {'tablones': '$use_cases_projects.tablones'}},
            {'$unwind': {'path': '$tablones'}},
            {'$group': {'_id': '$tablones'}},

            {'$lookup': {'from': 'dataTable', 'localField': '_id',
                         'foreignField': '_id', 'as': 'dataTables'}},
            {'$unwind': {'path': '$dataTables'}},

            {'$project': {'origin_tables': '$dataTables.origin_tables', 'stateTable': '$dataTables.stateTable', 'periodicity': '$dataTables.periodicity',
                          'current_depth': '$dataTables.current_depth', 'created_time': '$dataTables.created_time', 'user': '$dataTables.user', 'alias': '$dataTables.alias',
                          'master_name': '$dataTables.master_name', 'master_path': '$dataTables.master_path'}}
        ]
        result = self.project_repository.generate_aggreation(pipeline)
        return result

    def generate_excel_todo(self):
        """
        :param table_id:
        :return:
        """
        resF = []
        proyects = self.project_repository.find_all()

        for pro in proyects:
            if pro["name"] != 'Unassigned':
                tablones = self.get_dt_excel(str(pro['_id']))
                for tablon in tablones:
                    user_name = self.user_repository.find_one(
                        str(tablon['user']))
                    state_name = self.values[tablon['stateTable']]
                    origin_dd_tmp = []
                    origin_dt_tmp = []
                    for i in tablon["origin_tables"]:
                        try:
                            if i["originType"] == "DD" or i["originType"] == "T":
                                origin_dd_tmp.append(
                                    self.data_dictionary_repository.find_one(i["_id"])["alias"])
                            if i["originType"] == "DT":
                                origin_dt_tmp.append(
                                    self.data_table_repository.find_one(i["_id"])["alias"])
                        except Exception:
                            origin_dd_tmp.append(
                                self.data_dictionary_repository.find_one(i["_id"])["alias"])
                    origin_dd = ";".join(origin_dd_tmp)
                    origin_dt = ";".join(origin_dt_tmp)
                    try:
                        resF.append({
                            "Proyecto": pro["name"],
                            "Estado actual": state_name,
                            "Dependencias/Tablas": origin_dd,
                            "Periodicidad de actualización": tablon['periodicity'],
                            "Profundidad histórica (meses)": tablon['current_depth'],
                            "Fecha inicio": tablon['created_time'],
                            "Célula": "",
                            "Responsable": user_name['name'],
                            "Fecha estimada producción": "",
                            "Tipo de Solución": "",
                            "Estado ACL Work": "",
                            "Estado ACL Live": "",
                            "ID Tag Datio (alias) ": tablon['alias'],
                            "Nombre de tabla en Sophia": tablon["master_name"],
                            "Dependencias/Tablones": origin_dt,
                            "Ruta Master": tablon["master_path"]
                        })
                    except Exception as e:
                        print(e, "Fallo-----------------------")
                        raise ValueError(
                            "Error generando alguno de los campos en el objeto.")

        return resF
    
    def _reset_all_naming_origins(self, table_id):
        """
        :param table_id:
        :return:
        """
        result = self.data_table_repository.reset_all_naming_origins(table_id)
        return result.raw_result

    def _clean_columns(self, table_id):
        """
        :param table_id:
        :return:
        """
        data_table = self.data_table_repository.find_one(table_id)

        for j in range(0, len(data_table["fields"])):
            data_table["fields"][j]["column"] = int(j)
            self.data_table_repository.update_row_fields(table_id, data_table["fields"][j]["column"], data_table["fields"][j])

        return "columns done"

