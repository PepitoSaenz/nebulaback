from nebula.api.persistence.user_repository import *
from nebula.api.persistence.project_repository import *
from nebula.api.persistence.data_dictionary_repository import *
from nebula.api.persistence.backlog_repository import *
from nebula.api.persistence.team_repository import *
from nebula.api.persistence.use_case_repository import *
from nebula.api.persistence.data_table_repository import *

from nebula.api.services.project_service import *


class User_service(object):
    """
     A class used to represent a User services.

     Attributes
            -------
            user_repository : User_repository
                Reference to User repository class to interact with database
            project_repository: Project_repository
                Reference to Project repository class to interact with database
            data_dictionary_repository: Data_dictionary_repository
                Reference to Data dictionary repository class to interact with database
            backlog_repository: Backlog_repository
                Reference to Backlog repository class to interact with database
            team_repository: Team_repository
                Reference to Team repository class to interact with database
    """

    def __init__(self):
        """

        """
        self.user_repository = User_repository()
        self.use_cases_repository = Use_case_repository()
        self.project_repository = Project_repository()
        self.data_dictionary_repository = Data_dictionary_repository()
        self.backlog_repository = Backlog_repository()
        self.team_repository = Team_repository()
        self.use_case_repository = Use_case_repository()
        self.data_table_repository = Data_table_repository()

        self.project_service = Project_service()

    def find_all(self):
        """Get all users

        :return: bson, dictionary with all user data
        """
        return self.user_repository.find_all()

    def find_all_names(self):
        """Get all users names

        :return:bson, dictionary with users names
        """
        res = []
        tmp = self.user_repository.find_all_names()
        for i in tmp:
            res.append({"name": i["name"], "email": i["email"], "_id": str(i["_id"])})
        return res

    def login(self, user):
        """Validate users credentials to interact with a user

        :param user: str, string that represent a user id
        :return: dict, with user public data
        """
        tmp_user = self.user_repository.login(user["email"], user["password"])
        if tmp_user is None:
            raise ValueError("Email or password are wrong")
        tmp_user["_id"] = str(tmp_user["_id"])
        self.user_repository.last_login(str(tmp_user["_id"]))
        del tmp_user["password"]
        return tmp_user

    def find_one(self, user_id):
        """Get one public user data

        :param user_id: str, string that represent a user id
        :return: bson, dictionary with user data
        """
        user_data = self.user_repository.find_one(user_id)
        teams = []
        if "last_login" in user_data:
            user_data["last_login"] = str(user_data["last_login"])
        for j in user_data["teams"]:
            team_tmp = self.team_repository.find_one(str(j))
            teams.append(team_tmp["name"])

        user_data["teams"] = teams
        user_data["vobos"] = self.project_service._check_vobo_in_projects(user_id)

        try:
            del user_data["password"]
            del user_data["modification"]
            del user_data["created_time"]
        except:
            pass

        return user_data

    def create_user(self, admin_id, new_user_data):
        """Insert one new user

        :param user: dict, dictionary with user data to save
        :return:
        """
        admin = self.user_repository.find_one(admin_id)
        if admin is not None and admin["rol"] == "A":
            if not self.user_repository.find_by_email(new_user_data["email"]) is None:
                raise ValueError("Ya existe un usuario con ese email.")
            tmp_teams = []
            for team in new_user_data["teams"]:
                tmp_teams.append(ObjectId(str(team["_id"])))
            user_data = {
                "name": new_user_data["name"],
                "rol": new_user_data["rol"],
                "email": new_user_data["email"],
                "teams": tmp_teams,
                "password": new_user_data["password"],
                "created_time": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            }
            self.user_repository.insert_one(user_data)
        else:
            raise ValueError("You don't have permission to access to this resource.")

    def insert_one(self, new_user_data):
        """Insert one new user

        :param user: dict, dictionary with user data to save
        :return:
        """
        if not self.user_repository.find_by_email(new_user_data["email"]) is None:
            raise ValueError("Ya existe un usuario con ese email.")
        user_data = {
            "name": new_user_data["name"],
            "rol": "",
            "email": new_user_data["email"],
            "teams": [],
            "password": new_user_data["password"],
            "created_time": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        }
        self.user_repository.insert_one(user_data)

    def find_projects_users(self, user_type, user_id):
        """Get all product projects from one product owner users

        :param user_type: str, string that represent user rol
        :param user_id: str, string that represent a user id
        :return: bson, dictionary with projects associate to one product owner
        """
        res = []
        if user_type == "productOwner":
            res = self._find_product_owner_projects(user_id)
        return res

    def _find_product_owner_projects(self, product_owner_id):
        """Get all product projects from one product owner users

        :param product_owner_id: str, string that represent product owner id
        :return: bson, dictionary with projects data
        """
        return self.project_repository.find_product_owner_projects(product_owner_id)

    def user_tables(self, user_id):
        """Get all tables associate to one user

        :param user_id: str, string that represent a user id
        :return: bson, dictionary with tables data
        """
        return self.data_dictionary_repository.find_user_tables(user_id)

    def data_table_state(self, user_id, state):
        """

        :param user_id:
        :param state:
        :return:
        """
        user_type = self.find_one(user_id)
        tables = []
        if user_type["rol"] == "I":
                tables = self.data_table_repository.find_user_tables_state(user_id, state)
        elif user_type["rol"] == "G":
            if state == "G":
                tables = self.data_table_repository.find_rol_tables_state(state)
            else:
                tables = self.data_table_repository.find_user_tables_state(user_id, state)

        res = []
        for table in tables:
            table["fields"] = len(table["fields"])
            try:
                query = {"tablones": {"$in": [ObjectId(table["_id"])]}}
                use_case_id = self.use_cases_repository.find_any_query(query).limit(1)
                project = self.project_repository.find_any_query(
                    {"useCases": {"$in": [ObjectId(use_case_id[0]["_id"])]}}).limit(1)
                if use_case_id is None:
                    table["use_case_name"] = "Unassigned"
                else:
                    table["use_case_name"] = use_case_id[0]["name"]

                if project is None:
                    table["project_name"] = "Unassigned"
                else:
                    table["project_name"] = project[0]["name"]
                res.append(table)
            except:
                table["project_name"] = "unassigned"
                res.append(table)
        return res

    def table_state(self, user_id, state):
        """Get tables associate to one user depending on their rol

        :param user_id: str, string that represent a user id
        :param state: str, string that represent a table state
        :return: bson, dictionary with tables data associate to one user or rol
        """
        user_type = self.find_one(user_id)
        tables = []
        if user_type["rol"] == "I":
            tables = self.data_dictionary_repository.find_user_tables_state(user_id, state)
        elif user_type["rol"] == "G":
            if state == "G":
                tables = self.data_dictionary_repository.find_rol_tables_state(state)
            else:
                tables = self.data_dictionary_repository.find_user_tables_state(user_id, state)
        res = []
        for table in tables:
            try:
                query = {"tables": {"$in": [ObjectId(table["_id"])]}}
                use_case_id = self.use_cases_repository.find_any_query(query).limit(1)
                project = self.project_repository.find_any_query(
                    {"useCases": {"$in": [ObjectId(use_case_id[0]["_id"])]}}).limit(1)
                if project is None:
                    table["project_name"] = "unassigned"
                else:
                    table["project_name"] = project[0]["name"]
                res.append(table)
            except Exception as e:
                print("Error >--", e)
                table["project_name"] = "unassigned"
                res.append(table)
        return res

    def _find_table_project(self,table_id):
        use_case = None
        project_name = None
        use_cases = self.use_case_repository.find_all()
        projects = self.project_repository.find_all()
        stop = True
        stop_1 = True
        for useCase in use_cases:
            if not stop:
                break
            for table in useCase["tables"]:
                if ObjectId(table_id) == table:
                    use_case = useCase
                    stop = False
                    break
        if use_case is not None:
            for project in projects:
                if not stop_1:
                    break
                for useCase in project["useCases"]:
                    if use_case["_id"] == useCase:
                        project_name = project
                        break
        return project_name["name"]

    def find_by_rol(self, rol):
        """Get users by rol

        :param rol: str, String that represent a rol
        :return: bson, dictionary with users public data
        """
        return self.user_repository.find_by_rol(rol)

    def user_project(self, group, code):
        """Get all projects where an user or a team are subscribe

        :param group: str, string that represent a team or user, where go to search
        :param code: str, string that represent a id
        :return: array of dicts, contains project data associate to an user or a team
        """
        team = self.user_repository.find_one(code)["teams"]
        teams_all = []
        for i in team:
            tmp = self.project_repository.find_teams(i)
            for j in tmp:
                for k in j["teams"]:
                    if k not in teams_all:
                        teams_all.append(k)
        result = []
        temp = []
        if group == "users":
            temp = self.user_repository.find_partners_by_name(teams_all)

        elif group == "teams":
            temp = self.team_repository.find_teams(teams_all)

        for i in temp:
            i["_id"] = str(i["_id"])
            result.append(i)

        return result
    
    def find_vobos(self, user_id):
        """Get all users associate to a project

        :param user_id: str, string that represent an user id
        :return: arrays of dicts, id project and name associate to an user
        """
        pipeline = [
            {'$match': {'_id': ObjectId(str(user_id).strip())}},
            {
                '$lookup': {
                    'from': 'projects',
                    'localField': '_id',
                    'foreignField': 'VoBo',
                    'as': 'users_projects'
                }
            }, {
                '$project': {'_id': 0, 'users_projects.name': 1, 'users_projects._id': 1}
            }
        ]
        projects = []
        for i in self.user_repository.generate_aggregate(pipeline):
            for j in i["users_projects"]:
                projects.append({'_id': str(j["_id"]), 'name': j["name"]})
        return projects

    def find_projects(self, user_id):
        """Get all users associate to a project

        :param user_id: str, string that represent an user id
        :return: arrays of dicts, id project and name associate to an user
        """
        pipeline = [
            {'$match': {'_id': ObjectId(str(user_id).strip())}},
            {
                '$lookup': {
                    'from': 'projects',
                    'localField': 'teams',
                    'foreignField': 'teams',
                    'as': 'users_projects'
                }
            }, {
                '$project': {'_id': 0, 'users_projects.name': 1, 'users_projects._id': 1}
            }
        ]
        projects = []
        for i in self.user_repository.generate_aggregate(pipeline):
            for j in i["users_projects"]:
                projects.append({'_id': str(j["_id"]), 'name': j["name"]})
        return projects

    def change_password(self, user_data):
        """
            Update the user password
        """
        old_pass = self.find_one(user_data["user"])["password"]
        if user_data["oldPasswrd"] == "reset":
            self.user_repository.update_password(user_data)
        elif old_pass != user_data["oldPasswrd"]:
            raise ValueError("Contraseña vieja no concuerda.")
        else:
            self.user_repository.update_password(user_data)

    def find_pending_users(self, user_id):
        """

        :return:
        """
        users = []
        admin = self.user_repository.find_one(user_id)
        if admin is not None and admin["rol"] == "A":
            for j in self.user_repository.find_pending():
                users.append({"_id": str(j["_id"]), "name": j["name"], "email": j["email"]})
        return users

    def find_by_email(self, email):
        """

        :param email:
        :return:
        """
        teams = []
        user = self.user_repository.find_by_email(email)
        if user is not None:
            user["_id"] = str(user["_id"])
            for j in user["teams"]:
                team_tmp = self.team_repository.find_one(str(j))
                teams.append({"_id": str(team_tmp["_id"]), "name": team_tmp["name"]})
            user["teams"] = teams
        return user
    
    def get_all_users(self, data):
        """
            Returns the list of all registered users
        :param data: data to verify access
        :return:
        """
        result = []
        if data["admin"]:
            respone = self.user_repository.get_registered_users()
            if respone is not None:
                for user in respone:
                    try:
                        teams = []
                        user["_id"] = str(user["_id"])
                        if "last_login" in user:
                            user["last_login"] = str(user["last_login"])
                        for j in user["teams"]:
                            team_tmp = self.team_repository.find_one(str(j))
                            teams.append({"_id": str(team_tmp["_id"]), "name": team_tmp["name"]})
                        user["teams"] = teams
                        user["vobos"] = self.project_service._check_vobo_in_projects(user["_id"])
                    except Exception as e:
                        print(e)
                    result.append(user)
        else:
            raise ValueError("You don't have permission to access to this resource.")
        return result

    def update_user_data(self, user_id, user_data):
        """
            Updates all sent data to one user
        :param user_id:
        :param user_data:
        :return:
        """
        admin = self.user_repository.find_one(user_id)
        if admin is not None and admin["rol"] == "A":
            old_user_data = self.user_repository.find_one(user_data["_id"])
            tmp_teams = []
            for team in user_data["teams"]:
                tmp_teams.append(ObjectId(str(team["_id"])))

            user_update = user_data
            user_update["teams"] = tmp_teams
            user_update["modification"] = { "old_data": str(old_user_data), "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}

            del user_update["_id"]
            self.user_repository.update_user_data(old_user_data["_id"], user_update)
        else:
            raise ValueError("You don't have permission to access to this resource.")
    
    def users_roles(self):
        """Returns the array of user roles with their letter ID and their natural name.

        :return: json array with the rol info
        """
        roles_list = [
            {
                "rol": "I",
                "name": "Ingesta"
            },
            {
                "rol": "G",
                "name": "Gobierno"
            },
            {
                "rol": "PO",
                "name": "Product Owner"
            },
            {
                "rol": "SPO",
                "name": "Super Product Owner"
            },
            {
                "rol": "A",
                "name": "Administrator"
            },
            {
                "rol": "",
                "name": "Pendiente por Asignar"
            },
            {
                "rol": "L",
                "name": "Invitado"
            },
            {
                "rol": "D",
                "name": "Depreciado"
            }
        ]

        return roles_list



