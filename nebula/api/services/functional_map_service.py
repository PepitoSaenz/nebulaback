from nebula.api.persistence.functional_map_repository import *
from nebula.api.persistence.project_repository import *
from nebula.api.persistence.data_dictionary_repository import *
from nebula.api.persistence.data_table_repository import *

import datetime

class Functional_map_service(object):

    """
        A class used to represent a Functional map services.

        Attributes
             -------
            functional_map_repository : Functional_map_repository
                Reference to Functional map repository class to interact with database
            project_repository: Project_repository
                Reference to project repository class to interact with database
    """

    def __init__(self):
        """

        """
        self.functional_map_repository = Functional_map_repository()
        self.project_repository = Project_repository()
        
        self.data_dictionary_repository = Data_dictionary_repository()
        self.data_table_repository = Data_table_repository()

    def find_uuaas_project(self,project_id):
        """Find all uuaa's to one project

        :param project_id: str, String that reference to project id
        :return: Array of uuaa's
        """
        countries = self.project_repository.find_one(project_id)["countries"]
        uuaas = self.functional_map_repository.find_all()
        res = []
        for i in countries:
            first_letter = i[0].upper()
            for j in uuaas:
                res.append(first_letter+j["uuaa"])
        return sorted(res,key=str.lower)

    def find_uuaas_data_table(self, project_id):
        """Find all uuaa's to one project

        :param project_id: str, String that reference to project id
        :return: Array of uuaa's
        """
        project = self.project_repository.find_one(project_id)
        res = []
        if project is not None:
            uuaas = self.functional_map_repository.find_all()
            for j in uuaas:
                res.append(j["uuaa"])
        return sorted(res, key=str.lower)

    def find_level_uua(self, uuaa):
        """

        :param uuaa:
        :return:
        """
        res = ""
        try:
            temp = self.functional_map_repository.find_uuaa(uuaa[1:])["route"]
            level = temp.split("/")[3]
            if level.strip().lower() == uuaa[1:].strip().lower():
                res = ""
            else:
                res = level
        except:
            res = ""

        return res
    
    def find_uuaas_information(self,project_id):
        """Find all uuaa's to one project

        :param project_id: str, String that reference to project id
        :return: Array of uuaa's
        """
        countries = self.project_repository.find_one(project_id)["countries"]
        uuaas = self.functional_map_repository.find_all()
        res = []
        for i in countries:
            first_letter = i[0].upper()
            for j in uuaas:
                res.append(first_letter+j["uuaa"])
        return sorted(res,key=str.lower)
    
    def remove_uuaa(self,data):
        """Delete an uuaa from the collection

        :param data: json with information of the uuaa to delete
        :return:
        """
        uuaa = data["uuaa"]
        if len(uuaa) == 4:
            uuaa = uuaa[1:-1]
        repository_uuaa = self.functional_map_repository.find_uuaa(str(uuaa))       
        if repository_uuaa is None:
            raise ValueError("La UUAA no se encuentra registrada.")
        self.functional_map_repository.remove_uuaa(uuaa)
        
    def upload_uuaa(self,data):
        """

        :param information:
        :return:
        """

        uuaa = data["uuaa"]
        newUuaa = {}
        newUuaa["uuaa"] = uuaa
        newUuaa["ambit"] = data["ambit"]
        newUuaa["level1"] = ""
        newUuaa["level2"] = data["level2"]
        newUuaa["level3"] = data["level3"]
        newUuaa["example"] = ""
        newUuaa["data_source"] = ""
        newUuaa["functionalGroupDataSource"] = ""
        newUuaa["systemDescription"] = data["systemDescription"]
        newUuaa["systemName"] = ""
        newUuaa["tecnicalResponsible"] = ""
        newUuaa["route"] = ""

        raw_uuaa = self.functional_map_repository.find_uuaa(uuaa)
        if raw_uuaa is None:
            self.functional_map_repository.insert_uuaa(newUuaa)
        else:
            self.functional_map_repository.update_uuaa(newUuaa)
    
    #New functions

    def query_master_search_props(self):
        """
        Get the tables with the parameter
        :param tables: Array, tables
        :param table_name: str, the physical name of the table
        :return: an array of tables
        """
        try:
            data_sources = []
            countries = []
            clean_countries = []
            data_sources = self.functional_map_repository.find_distinct_property("data_source")
            countries = self.functional_map_repository.find_distinct_property("country")

            
            for country in countries:
                if ',' in country:
                    for spliti in country.split(','):
                        if not spliti.strip() in clean_countries:
                            clean_countries.append(spliti.strip())
            
            res = []
            res.append(data_sources)
            res.append(clean_countries)
        except Exception as e:
            print(e)
            res = e

        return res

    def _filter_by_property_name(self, uuaas, prop_value, prop_name):
        """
        Get the tables with the parameter
        :param tables: Array, tables
        :param table_name: str, the physical name of the table
        :return: an array of tables
        """
        res = []
        for ua in uuaas:
            if ua is not None and ua not in res:
                if prop_name in ua:
                    if prop_value.lower() in ua[prop_name].lower():
                        res.append(ua)
        return res

    def query_master_uuaas(self, uuaa_name, system_name, data_source, desc, level1, level2, country):
        """Find all uuaa's based on the query data

        :param uuaa_name: str, String that references the uuaa name to search for
        :param data_source: str, String that references the data source name 
        :param level1: str, String that references the uuaa level 1 of information
        :param level2: str, String that references the uuaa level 2 of information
        :return: Array of uuaa's
        """
        uuaas = []
        uuaas = self.functional_map_repository.find_all_fullname()

        if len(str(uuaa_name).strip()) != 0:
            uuaas = self._filter_by_property_name(uuaas, str(uuaa_name).strip(), "full_name")
        if len(str(system_name).strip()) != 0:
            uuaas = self._filter_by_property_name(uuaas, str(system_name).strip(), "system_name")
        if len(str(data_source).strip()) != 0:
            uuaas = self._filter_by_property_name(uuaas, str(data_source).strip(), "data_source")
        if len(str(desc).strip()) != 0:
            uuaas = self._filter_by_property_name(uuaas, str(desc).strip(), "system_description")
        if len(str(level1).strip()) != 0:
            uuaas = self._filter_by_property_name(uuaas, str(level1).strip(), "information_level_1")
        if len(str(level2).strip()) != 0:
            uuaas = self._filter_by_property_name(uuaas, str(level2).strip(), "information_level_2")
        if len(str(country).strip()) != 0:
            uuaas = self._filter_by_property_name(uuaas, str(country).strip(), "country")
        
        return uuaas

    def query_uuaas(self, uuaa_name, desc, desc_eng, data_source, country, deploy_country, level1, level2):
        """Find all uuaa's based on the query data

        :param uuaa_name: str, String that references the uuaa name to search for
        :param data_source: str, String that references the data source name 
        :param level1: str, String that references the uuaa level 1 of information
        :param level2: str, String that references the uuaa level 2 of information
        :return: Array of uuaa's
        """
        uuaa_name = str(uuaa_name).strip()
        desc = str(desc).strip()
        desc_eng = str(desc_eng).strip()
        data_source = str(data_source).strip()
        country = str(country).strip()
        deploy_country = str(deploy_country).strip()
        level1 = str(level1).strip()
        level2 = str(level2).strip()

        uuaas = []
        uuaas = self.functional_map_repository.find_all_shortname()

        if len(uuaa_name) != 0:
            uuaas = self._filter_by_property_name(uuaas, uuaa_name, "uuaa")
        if len(desc) != 0:
            uuaas = self._filter_by_property_name(uuaas, desc, "systemDescription")
        if len(desc_eng) != 0:
            uuaas = self._filter_by_property_name(uuaas, desc_eng, "systemDescriptionEng")
        if len(data_source) != 0:
            uuaas = self._filter_by_property_name(uuaas, data_source, "data_source")
        if len(country) != 0:
            uuaas = self._filter_by_property_name(uuaas, country, "country")
        if len(deploy_country) != 0:
            uuaas = self._filter_by_property_name(uuaas, deploy_country, "deploy_country")
        if len(level1) != 0:
            uuaas = self._filter_by_property_name(uuaas, level1, "level1")
        if len(level2) != 0:
            uuaas = self._filter_by_property_name(uuaas, level2, "level2")

        return uuaas
    
    def get_uuaa_fullname(self):
        return self.functional_map_repository.find_all_fullname()

    def get_uuaa(self, uuaa_id):
        """Get raw uuaas based on query params

        :param table_id: str, String that reference a table id
        :return: Array, all uuaa that can change
        """
        check_uuaa = self.functional_map_repository.find_one(uuaa_id)
        check_uuaa["_id"] = str(check_uuaa["_id"])
        return check_uuaa
    
    def get_property_options(self, property_name):
        """Get raw uuaas based on query params

        :param table_id: str, String that reference a table id
        :return: Array, all uuaa that can change
        """
        return self.functional_map_repository.find_distinct_property(property_name)

    def new_uuaa(self, data):
        """Get raw uuaas based on query params

        :param table_id: str, String that reference a table id
        :return: Array, all uuaa that can change
        """
        check_uuaa = self.functional_map_repository.find_uuaa(data["uuaa"])
        if check_uuaa is not None:
            raise ValueError("La UUAA con el nombre " + data["uuaa"] + " ya se encuentra registrada.")
        
        data["insertDate"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        temp_mod = {"user_id": data["user_id"],
                    "type": "Create UUAA",
                    "additional_data": str(data),
                    "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
        try:
            data["modifications"].append(temp_mod)
        except Exception:
            data["modifications"] = []
            data["modifications"].append(temp_mod)

        del data["user_id"]
        new_id = self.functional_map_repository.insert_uuaa(data)
        uuaa_dic = self.functional_map_repository.find_one(new_id)
        uuaa_dic["_id"] = str(uuaa_dic["_id"])
        return uuaa_dic
    
    def update_uuaa(self, uuaa_id, data):
        """Update uuaa info

        :param table_id: str, string that reference a table id
        :param data: json, dictionary with data to update
        :return:
        """
        errVal = 0
        uuaa_dic = self.functional_map_repository.find_one(uuaa_id)
        if uuaa_dic is None:
            raise ValueError("La uuaa no existe.")
        else:
            if data["uuaa"] == uuaa_dic["uuaa"]:
                errVal = 1
            
        uuaas = self._filter_by_property_name(self.functional_map_repository.find_all(), data["uuaa"], "uuaa")
        flago = len(uuaas) > errVal

        if flago:
            raise ValueError("El nombre de la UUAA esta repetida.")
        else:
            temp_mod = {"user_id": data["user_id"],
                        "modType": "Detail Update",
                        "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
            try:
                data["modifications"].append(temp_mod)
            except Exception:
                data["modifications"] = []
                data["modifications"].append(temp_mod)

            if len(data["modifications"]) > 6:
                data["modifications"].pop()

            del data["user_id"]
            self.functional_map_repository.update_uuaa(uuaa_id, data)
            uuaa_dic = self.functional_map_repository.find_one(uuaa_id)
        
        uuaa_dic["_id"] = str(uuaa_dic["_id"])
        return uuaa_dic
    
    def update_object(self, data):
        """Update object info with updated uuaa data

        :param data: json, uuaa data used to update
        :return:
        """
        uuaa_data = {}
        if data["type"] == "DD":
            object_data = self.data_dictionary_repository.find_one(data["id"])
        if data["type"] == "DT":
            object_data = self.data_table_repository.find_one(data["id"])

        if object_data is not None:
            uuaa_data = self.functional_map_repository.find_uuaa(object_data["uuaaMaster"][1:])

            object_data["data_source"] = uuaa_data["data_source"] if "data_source" in uuaa_data else uuaa_data["system_name"] if "system_name" in uuaa_data else ""
            object_data["information_group_level_1"] = uuaa_data["information_level_1"] if "information_level_1" in uuaa_data else ""
            object_data["information_group_level_2"] = uuaa_data["information_level_2"] if "information_level_2" in uuaa_data else ""
  
            if data["type"] == "DD":
                self.data_dictionary_repository.update_field(data["id"], object_data)
            if data["type"] == "DT":
                self.data_table_repository.update_field(data["id"], object_data)
             
        if uuaa_data != {}:
            uuaa_data["_id"] = str(uuaa_data["_id"])
              
        return uuaa_data
    
    def report_tables(self, param):
        res = []
        all_tables = self.data_dictionary_repository.find_all_full()

        for table in all_tables:
            try:
                elem = {
                     "BASE NAME": table["baseName"],
                     "RAW NAME": table["raw_name"],
                     "MASTER NAME": table["master_name"],
                     "ALIAS": table["alias"],
                     "UUAA RAW": table["uuaaRaw"],
                     "UUAA MASTER": table["uuaaMaster"],
                     "DATA SOURCE": table["data_source"] if "data_source" in table else "",
                     "INFORMATION LEVEL 1": table["information_group_level_1"] if "information_group_level_1" in table else "",
                     "INFORMATION LEVEL 2": table["information_group_level_2"] if "information_group_level_2" in table else "",
                     "NUMBER OF FIELDS RAW": len(table["fieldsRaw"]) if "fieldsRaw" in table else 0,
                     "NUMBER OF FIELDS MASTER": len(table["fieldsMaster"]) if "fieldsMaster" in table else 0,
                }
            except Exception as e:
                print(table["alias"])
                print(e)

            if param == "1":
                if "data_source" in table and table["data_source"] == "":
                    res.append(elem)
            else:
                res.append(elem)
        
        return res




    def get_ambitos(self):
        """Update object info with updated uuaa data

        :param data: json, uuaa data used to update
        :return:
        """

        ambitos_list = [
            {
                "letter": 'C',
                "country": "Colombia"
            },
            {
                "letter": 'B',
                "country": "Global"
            },
            {
                "letter": 'E',
                "country": "Global"
            },
            {
                "letter": 'F',
                "country": "Global"
            },
            {
                "letter": 'G',
                "country": "Global"
            },
            {
                "letter": 'I',
                "country": "Global"
            },
            {
                "letter": 'J',
                "country": "Global"
            },
            {
                "letter": 'K',
                "country": "Global"
            },
            {
                "letter": 'L',
                "country": "Global"
            },
            {
                "letter": 'M',
                "country": "Global"
            },
            {
                "letter": 'N',
                "country": "Global"
            },
            {
                "letter": 'P',
                "country": "Global"
            },
            {
                "letter": 'R',
                "country": "Global"
            },
            {
                "letter": 'T',
                "country": "Global"
            },
            {
                "letter": 'X',
                "country": "Global"
            },
            {
                "letter": 'Y',
                "country": "Global"
            },
            {
                "letter": 'W',
                "country": "Local"
            }
        ]

        return ambitos_list