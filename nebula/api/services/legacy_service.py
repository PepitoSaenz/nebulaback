from nebula.api.persistence.data_dictionary_repository import *

import re


class Legacy_service(object):

    """
        A class used to represent a legacy services.

        Attributes
             -------
            dictionary_repository : Data_dictionary_repository
                 Reference to Data dictionary repository class to interact with database
            Fase: Array
                Field fase predefine to check
    """

    def __init__(self):
        """

        """
        self.dictionary_repository = Data_dictionary_repository()
        self.fase = ["fieldsRaw","fieldsMaster"]
        self.convenciones= {"fieldsRaw": "Raw", "fieldsMaster":"Master","P":"Producción",
                            "I":"Listo para ingestar","IN":"Ingestado","M":"Mallas","Q":"Calidad",
                            "G":"Gobierno","A":"Arquitectura","N":"En propuesta", "F": "Gobierno funcional",
                            "R": "Revisión solicitud", "D": "Descartado"}

    def legacy_query(self,legacy):
        """Get all tables and namings that have the save legacy name

        :param legacy: str, string, that represent a legacy name to find
        :return: Array of json, with tables and namings data
        """
        res = []
        for i in self.fase:
            query = {}
            query[i] = {
                "$elemMatch":{
                    "legacy.legacy": re.compile(legacy.upper(), re.I)
                }
            }
            result = self.dictionary_repository.find_any_query(query)
            if result is not None:
                for j in result:
                    tmp_table=self.dictionary_repository.find_one(j["_id"])
                    try:
                        for k in tmp_table[i]:
                            if type(k["legacy"]) is dict:
                                if k != "":
                                    if legacy.upper() in k["legacy"]["legacy"].upper() or legacy.lower()==k["legacy"]["legacy"].lower():
                                        res.append({
                                            "alias":tmp_table["alias"],"rawName":tmp_table["raw_name"],"masterName":tmp_table["master_name"],
                                            "fase": self.convenciones[i],"naming":k["naming"]["naming"],"tableDesc":tmp_table["observationField"],
                                            "state":self.convenciones[tmp_table["stateTable"]], "legacy":k["legacy"]
                                        })
                    except Exception as e:
                        print(e)
        return res

