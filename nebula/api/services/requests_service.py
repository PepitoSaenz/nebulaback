from nebula.api.entities.use_case import Use_case
from nebula.api.entities.requests import Requests
from nebula.api.entities.backlog import Backlog


from nebula.api.persistence.data_table_repository import Data_table_repository
from nebula.api.persistence.use_case_repository import Use_case_repository
from nebula.api.persistence.backlog_repository import Backlog_repository
from nebula.api.persistence.requests_repository import Requests_repository
from nebula.api.persistence.user_repository import User_repository
from nebula.api.persistence.project_repository import Project_repository
from nebula.api.persistence.data_dictionary_repository import Data_dictionary_repository


from nebula.api.services.use_case_service import Use_case_service
from nebula.api.services.backlog_service import Backlog_service
from nebula.api.services.data_table_service import Data_table_service
from nebula.api.services.project_service import Project_service


from datetime import datetime
from bson import ObjectId


class Requests_service(object):

    def __init__(self):
        self.project_repository = Project_repository()
        self.project_service = Project_service()
        self.use_case_repository = Use_case_repository()
        self.backlog_repository = Backlog_repository()
        self.requests_repository = Requests_repository()
        self.user_repository = User_repository()
        self.use_case_service = Use_case_service()
        self.backlog_service = Backlog_service()
        self.data_dictionary_repository = Data_dictionary_repository()
        self.data_table_service = Data_table_service()
        self.data_table_repository = Data_table_repository()
        self.values = {"G": "Gobierno", "A": "Arquitectura", "N": "En propuesta namings", "I": "Para ingestar",
                       "IN": "Ingestada en desarrollo", "Q": "Calidad", "M": "Mallas", "P": "Produccion",
                       "ungoverned": "En backlog", "PA": "Pendiente aprobación backlog", "RN": "Revisión Negocio",
                       "R": "Revisión Solicitud", "D": "Descartados", "F": "Gobierno funcional"}

    def find_request_by_id(self, request_id):
        """

        :param request_id:
        :return:
        """
        result = self.requests_repository.find_one(request_id)
        if result is None:
            raise ValueError("La solicitud no se encuentra registrada.")

        if result["target"] != "T":
            raise ValueError("La solicitud no hace referencia a un tablón.")

        temp_str = {
            "_id": str(result["_id"]),
            "user": str(result["user"]),
            "user_name": self.user_repository.find_one(str(result["user"]))["email"],
            "action": result["action"],
            "idProject": str(result["idProject"]),
            "project_name": self.project_repository.find_one(str(result["idProject"]))["name"],
            "use_case_id": str(result["idUsecase"]),
            "use_case_name": self.use_case_repository.find_one(str(result["idUsecase"]))["name"],
            "created_time": result["created_time"].strftime("%Y-%m-%d"),
            "request_number": result["request_number"],
            "requestStatus": result["stateRequest"]
        }

        if "comment" in result:
            temp_str["comment"] = result["comment"]
        if "approved_time" in result:
            temp_str["approved_time"] = result["approved_time"].strftime("%Y-%m-%d")
        if "spo" in result:
            temp_str["spo"] = self.user_repository.find_one(str(result["spo"]))["email"]

        return temp_str

    def find_all_data_tables_approved(self):
        """
            Returns the data table requests left to Approve.
        :return:
        """
        data_tables = []
        tmp_backlogs = self.requests_repository.find_data_tables_to_approved()
        if tmp_backlogs is not None:
            for i in tmp_backlogs:
                document = self.data_table_service.find_head(str(i["idBacklog"]))
                if document is not None:
                    tmp_struc = {"_id": str(i["_id"]),
                                "user": str(i["user"]),
                                "user_name": self.user_repository.find_one(str(i["user"]))["email"],
                                "action": i["action"],
                                "idProject": str(i["idProject"]),
                                "idBacklog": str(i["idBacklog"]),
                                "project_name": self.project_repository.find_one(str(i["idProject"]))["name"],
                                "backlog": document,
                                "use_case_id": str(i["idUsecase"]),
                                "use_case_name": self.use_case_repository.find_one(str(i["idUsecase"]))["name"],
                                "created_time": i["created_time"].strftime("%Y-%m-%d"),
                                "requestStatus": "P",
                                "request_number": i["request_number"]
                                }
                    data_tables.append(tmp_struc)
                else:
                    self.requests_repository.delete_request(str(i["_id"]))
        try:
            data_tables.sort(key=lambda x: datetime.strptime(x['created_time'], '%Y-%m-%d'), reverse=True)
        except Exception as e:
            print(e)
        return data_tables

    def find_all_backlogs_approved(self):
        """ Return all requests which are pending for verification.(Backlog)
           In other case return an empty array.
        :return: an array
        """
        backlogs = []
        tmp_backlogs = self.requests_repository.find_backlogs_to_approved()
        if tmp_backlogs is not None:
            for i in tmp_backlogs:
                document = self.backlog_service.find_one(str(i["idBacklog"]))
                tmp_struc = {   "_id": str(i["_id"]),
                                "user": str(i["user"]),
                                "user_name": self.user_repository.find_one(str(i["user"]))["email"],
                                "action": i["action"],
                                "idProject": str(i["idProject"]),
                                "project_name": self.project_repository.find_one(str(i["idProject"]))["name"],
                                "backlog": document,
                                "use_case_id": str(i["idUsecase"]),
                                "use_case_name": self.use_case_repository.find_one(str(i["idUsecase"]))["name"],
                                "created_time": i["created_time"].strftime("%Y-%m-%d"),
                                "requestStatus": "P",
                                "request_number": i["request_number"]}
                if i["action"] == "Mod":
                    tmp_struc["oldBacklog"] = i["oldBacklog"]
                backlogs.append(tmp_struc)

        try:
            backlogs.sort(key=lambda x: datetime.strptime(x['created_time'], '%Y-%m-%d'), reverse=True)
        except Exception as e:
            print(e)

        return backlogs

    def find_all_use_cases_approved(self):
        """ Return all requests which are pending for verification.(useCases)
           In other case return an empty array.
        :return: an array
        """
        use_cases = []
        tmp_use_cases = self.requests_repository.find_use_cases_to_approved()
        if tmp_use_cases is not None:
            for i in tmp_use_cases:
                document = self.use_case_service.find_use_case(str(i["idUsecase"]))
                document["tables"] = len(document["tables"])
                document["tablones"] = len(document["tablones"])
                tmp_struc = {"_id": str(i["_id"]),
                             "user": str(i["user"]),
                             "user_name": self.user_repository.find_one(str(i["user"]))["email"],
                             "action": i["action"],
                             "idProject": str(i["idProject"]),
                             "project_name": self.project_repository.find_one(str(i["idProject"]))["name"],
                             "use_case": document,
                             "created_time": i["created_time"].strftime("%Y-%m-%d"),
                             "requestStatus": "P",
                             "request_number": i["request_number"]
                             }
                if i["action"] == "Mod":
                    old_tmp = {
                        "name": i["oldUseCase"]["name"],
                        "description": i["oldUseCase"]["description"],
                        "startDate": i["oldUseCase"]["startDate"].strftime("%Y-%m-%d"),
                        "finishDate": i["oldUseCase"]["finishDate"].strftime("%Y-%m-%d")
                    }
                    tmp_struc["oldUseCase"] = old_tmp
                use_cases.append(tmp_struc)
        return use_cases

    def find_all_tickets_backlogs(self):
        """
        :return:
        """
        result = []
        tmp_request = self.requests_repository.find_any_query({"$and": [{"$or": [{"stateRequest": "D"},
            {"stateRequest": "A"}]}, {"target": "B"}]})
        if tmp_request is not None:
            for i in tmp_request:
                document = self.backlog_service.find_one(str(i["idBacklog"]))
                if document != "None": 
                    tmp_struc = {"_id": str(i["_id"]),
                             "user": str(i["user"]),
                             "user_name": self.user_repository.find_one(str(i["user"]))["email"],
                             "action": i["action"],
                             "idProject": str(i["idProject"]),
                             "project_name": self.project_repository.find_one(str(i["idProject"]))["name"],
                             "backlog": document,
                             "use_case_id": str(i["idUsecase"]),
                             "use_case_name": self.use_case_repository.find_one(str(i["idUsecase"]))["name"],
                             "created_time": i["created_time"].strftime("%Y-%m-%d"),
                             "spo": self.user_repository.find_one(str(i["spo"]))["email"],
                             "comment": i["comment"],
                             "requestStatus": i["stateRequest"],
                             "approved_time": i["approved_time"].strftime("%Y-%m-%d"),
                             "request_number": i["request_number"]}
                    if i["action"] == "Mod":
                        tmp_struc["oldBacklog"] = i["oldBacklog"]
                    result.append(tmp_struc)
                else:
                    self.requests_repository.delete_request(str(i["_id"]))


        return result[::-1]

    def find_all_tickets_use_cases(self):
        """

        :return:
        """
        result = []
        tmp_request = self.requests_repository.find_any_query({"$and": [{"$or": [{"stateRequest": "D"}, {"stateRequest": "A"}]}, {"target": "U"}]})
        if tmp_request is not None:
            for i in tmp_request:
                document = self.use_case_service.find_use_case(str(i["idUsecase"]))
                document["tables"] = len(document["tables"])
                document["tablones"] = len(document["tablones"])
                tmp_struc = {"_id": str(i["_id"]),
                             "user": str(i["user"]),
                             "user_name": self.user_repository.find_one(str(i["user"]))["email"],
                             "action": i["action"],
                             "idProject": str(i["idProject"]),
                             "project_name": self.project_repository.find_one(str(i["idProject"]))["name"],
                             "use_case": document,
                             "created_time": i["created_time"].strftime("%Y-%m-%d"),
                             "spo": self.user_repository.find_one(str(i["spo"]))["email"],
                             "comment": i["comment"],
                             "requestStatus": i["stateRequest"],
                             "approved_time": i["approved_time"].strftime("%Y-%m-%d"),
                             "request_number": i["request_number"]
                             }
                if i["action"] == "Mod":
                    old_tmp = {
                        "name": i["oldUseCase"]["name"],
                        "description": i["oldUseCase"]["description"],
                        "startDate": i["oldUseCase"]["startDate"].strftime("%Y-%m-%d"),
                        "finishDate": i["oldUseCase"]["finishDate"].strftime("%Y-%m-%d")
                    }
                    tmp_struc["oldUseCase"] = old_tmp
                result.append(tmp_struc)
        return result[::-1]

    def find_all_tickets_data_tables(self):
        """

        :return:
        """
        result = []
        tmp_request = self.requests_repository.find_any_query({"$and": [{"$or": [{"stateRequest": "D"}, {"stateRequest":"A"}]}, {"target": "T"}]})
        if tmp_request is not None:
            for i in tmp_request:
                document = self.data_table_service.find_head(str(i["idBacklog"]))
                tmp_struc = {"_id": str(i["_id"]),
                             "user": str(i["user"]),
                             "user_name": self.user_repository.find_one(str(i["user"]))["email"],
                             "action": i["action"],
                             "idProject": str(i["idProject"]),
                             "idBacklog": str(i["idBacklog"]),
                             "project_name": self.project_repository.find_one(str(i["idProject"]))["name"],
                             "backlog": document,
                             "use_case_id": str(i["idUsecase"]),
                             "use_case_name": self.use_case_repository.find_one(str(i["idUsecase"]))["name"],
                             "created_time": i["created_time"].strftime("%Y-%m-%d"),
                             "request_number": i["request_number"],
                             "spo": self.user_repository.find_one(str(i["spo"]))["email"],
                             "comment": i["comment"],
                             "requestStatus": i["stateRequest"],
                             "approved_time": i["approved_time"].strftime("%Y-%m-%d"),
                             }
                result.append(tmp_struc)
        return result[::-1]

    def find_backlogs_requests(self, project_id, use_case_name, backlog_name, action):
        """

        :param project_id:
        :param use_case_name:
        :param backlog_name:
        :param action:
        :return:
        """
        project_id = str(project_id).strip()
        use_case_name = str(use_case_name).strip()
        backlog_name = str(backlog_name).strip()
        action = str(action).strip()
        backlogs = []
        tmp_backlogs = self.requests_repository.find_request_by_filter({"target": "B", "stateRequest": "P"})
        for i in tmp_backlogs:
            use_case_tmp = self.use_case_service.find_use_case(str(i["idUsecase"]))
            backlog_tmp = self.backlog_service.find_one(str(i["idBacklog"]))
            document = backlog_tmp
            tmp_struc = {"_id": str(i["_id"]),
                         "user": str(i["user"]),
                         "user_name": self.user_repository.find_one(str(i["user"]))["email"],
                         "action": i["action"],
                         "idProject": str(i["idProject"]),
                         "project_name": self.project_repository.find_one(str(i["idProject"]))["name"],
                         "backlog": document,
                         "use_case_id": str(i["idUsecase"]),
                         "use_case_name": use_case_tmp["name"],
                         "created_time": i["created_time"].strftime("%Y-%m-%d"),
                         "requestStatus": "P",
                         "request_number": i["request_number"]
                         }

            if i["action"] == "Mod":
                i["oldBacklog"]["_id"] = str(i["oldBacklog"]["_id"])
                i["oldBacklog"]["idTable"] = str(i["oldBacklog"]["idTable"])
                tmp_struc["oldBacklog"] = i["oldBacklog"]

            tmp_struc["stateTable"] = ""
            tmp_struc["viewStateTable"] = ""
            if len(document["idTable"]) > 0:
                tmp_struc["stateTable"] = self.data_dictionary_repository.find_one(document["idTable"])["stateTable"]
                tmp_struc["viewStateTable"] = self.values[tmp_struc["stateTable"]]

            backlogs.append(tmp_struc)

        if len(project_id) != 0:
            backlogs = self._find_by_project(backlogs, project_id)
        if len(action) != 0:
            backlogs = self._find_by_action(backlogs, action)
        if len(use_case_name) != 0:
            backlogs = self._find_by_use_case(backlogs, use_case_name)
        if len(backlog_name) != 0:
            backlogs = self._find_by_backlog(backlogs, backlog_name)
        return backlogs

    def find_data_tables_requests(self, project_id, use_case_name, data_table_name, action):
        """

        :param project_id:
        :param use_case_name:
        :param data_table_name:
        :param action:
        :return:
        """
        project_id = str(project_id).strip()
        use_case_name = str(use_case_name).strip()
        backlog_name = str(data_table_name).strip()
        action = str(action).strip()

        tmp_data_tables = self.requests_repository.find_request_by_filter({"target": "T", "stateRequest": "P"})
        result = []
        for i in tmp_data_tables:
            tmp_struc = {
                "_id": "", "user": "", "user_name": "", "action": "",
                "idProject": "", "project_name": "", "use_case_name": "",
                "created_time": "", "requestStatus": "", "backlog": "",
                "request_number": ""
            }
            use_case_tmp = self.use_case_service.find_use_case(str(i["idUsecase"]))
            data_table_tmp = self.data_table_service.find_head(str(i["idBacklog"]))
            tmp_struc["_id"] = str(i["_id"])
            tmp_struc["user"] = str(i["user"])
            tmp_struc["user_name"] = self.user_repository.find_one(str(i["user"]))["email"]
            tmp_struc["action"] = i["action"]
            tmp_struc["idProject"] = str(i["idProject"])
            tmp_struc["project_name"] = self.project_repository.find_one(str(i["idProject"]))["name"]
            tmp_struc["use_case_name"] = use_case_tmp["name"]
            tmp_struc["created_time"] = i["created_time"].strftime("%Y-%m-%d")
            tmp_struc["requestStatus"] = "P"
            tmp_struc["backlog"] = data_table_tmp
            tmp_struc["request_number"] = i["request_number"]
            tmp_struc["stateTable"] = ""
            tmp_struc["viewStateTable"] = ""
            if len(data_table_tmp["stateTable"]) > 0:
                tmp_struc["stateTable"] = data_table_tmp["stateTable"]
                tmp_struc["viewStateTable"] = self.values[tmp_struc["stateTable"]]
            result.append(tmp_struc)

        if len(project_id) != 0:
            result = self._find_by_project(result, project_id)
        if len(action) != 0:
            result = self._find_by_action(result, action)
        if len(use_case_name) != 0:
            result = self._find_by_use_case(result, use_case_name)
        if len(backlog_name) != 0:
            result = self._find_by_backlog(result, backlog_name)
        return result

    def find_data_table_tickets(self, project_id, use_case_name, backlog_name, action, status_req):
        """

        :param project_id:
        :param use_case_name:
        :param backlog_name:
        :param action:
        :param status_req:
        :return:
        """
        project_id = str(project_id).strip()
        use_case_name = str(use_case_name).strip()
        backlog_name = str(backlog_name).strip()
        action = str(action).strip()
        status_req = str(status_req).strip()
        backlogs = []
        tmp_backlogs = self.requests_repository.find_request_by_filter(
            {"$and": [{"$or": [{"stateRequest": "D"}, {"stateRequest": "A"}]}, {"target": "T"}]})

        for i in tmp_backlogs:
            use_case_tmp = self.use_case_service.find_use_case(str(i["idUsecase"]))
            backlog_tmp = self.data_table_service.find_head(str(i["idBacklog"]))
            tmp_struc = {"_id": str(i["_id"]),
                         "user": str(i["user"]),
                         "user_name": self.user_repository.find_one(str(i["user"]))["email"],
                         "action": i["action"],
                         "idProject": str(i["idProject"]),
                         "project_name": self.project_repository.find_one(str(i["idProject"]))["name"],
                         "backlog": backlog_tmp,
                         "use_case_name": use_case_tmp["name"],
                         "created_time": i["created_time"].strftime("%Y-%m-%d"),
                         "spo": self.user_repository.find_one(str(i["spo"]))["email"],
                         "comment": i["comment"],
                         "requestStatus": i["stateRequest"],
                         "approved_time": i["approved_time"].strftime("%Y-%m-%d"),
                         "request_number": i["request_number"]}

            if len(backlog_tmp["stateTable"]) > 0:
                tmp_struc["stateTable"] = backlog_tmp["stateTable"]
                tmp_struc["viewStateTable"] = self.values[tmp_struc["stateTable"]]
            backlogs.append(tmp_struc)

        if len(project_id) != 0:
            backlogs = self._find_by_project(backlogs, project_id)
        if len(action) != 0:
            backlogs = self._find_by_action(backlogs, action)
        if len(use_case_name) != 0:
            backlogs = self._find_by_use_case(backlogs, use_case_name)
        if len(backlog_name) != 0:
            backlogs = self._find_by_backlog(backlogs, backlog_name)
        if len(status_req) != 0:
            backlogs = self._find_by_status(backlogs, status_req)
        return backlogs[::-1]

    def find_backlogs_tickets(self, project_id, use_case_name, backlog_name, action, status_req):
        """

        :param project_id:
        :param use_case_name:
        :param backlog_name:
        :param action:
        :param status_req:
        :return:
        """
        project_id = str(project_id).strip()
        use_case_name = str(use_case_name).strip()
        backlog_name = str(backlog_name).strip()
        action = str(action).strip()
        status_req = str(status_req).strip()
        backlogs = []
        tmp_backlogs = self.requests_repository.find_request_by_filter({"$and": [{"$or": [{"stateRequest": "D"},
            {"stateRequest": "A"}]}, {"target": "B"}]})
        for i in tmp_backlogs:
            try:
                use_case_tmp = self.use_case_service.find_use_case(str(i["idUsecase"]))
                backlog_tmp = self.backlog_service.find_one(str(i["idBacklog"]))
                document = backlog_tmp
                tmp_struc = {"_id": str(i["_id"]),
                            "user": str(i["user"]),
                            "user_name": self.user_repository.find_one(str(i["user"]))["email"],
                            "action": i["action"],
                            "idProject": str(i["idProject"]),
                            "project_name": self.project_repository.find_one(str(i["idProject"]))["name"],
                            "backlog": document,
                            "idUsecase": str(i["idUsecase"]),
                            "use_case_name": use_case_tmp["name"],
                            "created_time": i["created_time"].strftime("%Y-%m-%d"),
                            "spo": self.user_repository.find_one(str(i["spo"]))["email"],
                            "comment": i["comment"],
                            "requestStatus": i["stateRequest"],
                            "approved_time": i["approved_time"].strftime("%Y-%m-%d"),
                            "request_number": i["request_number"]}

                if i["action"] == "Mod":
                    i["oldBacklog"]["_id"] = str(i["oldBacklog"]["_id"])
                    i["oldBacklog"]["idTable"] = str(i["oldBacklog"]["idTable"])
                    tmp_struc["oldBacklog"] = i["oldBacklog"]

                tmp_struc["stateTable"] = ""
                tmp_struc["viewStateTable"] = ""
                if len(document["idTable"]) > 0:
                    tmp_struc["stateTable"] = self.data_dictionary_repository.find_one(document["idTable"])["stateTable"]
                    tmp_struc["viewStateTable"] = self.values[tmp_struc["stateTable"]]
                backlogs.append(tmp_struc)
            except Exception as e:
                print("error: ")
                print(i)
                print(e)

        if len(project_id) != 0:
            backlogs = self._find_by_project(backlogs, project_id)
        if len(action) != 0:
            backlogs = self._find_by_action(backlogs, action)
        if len(use_case_name) != 0:
            backlogs = self._find_by_use_case(backlogs, use_case_name)
        if len(backlog_name) != 0:
            backlogs = self._find_by_backlog(backlogs, backlog_name)
        if len(status_req) != 0:
            backlogs = self._find_by_status(backlogs, status_req)
        return backlogs[::-1]

    def _find_by_project(self, query, project_id):
        """

        :param query:
        :param project_id:
        :return:
        """
        res = []
        for i in query:
            if i["idProject"] == project_id:
                res.append(i)
        return res

    def _find_by_action(self, query, action):
        """

        :param query:
        :param action:
        :return:
        """
        res = []
        for i in query:
            if i["action"] == action:
                res.append(i)
        return res

    def _find_by_backlog(self, query, backlog):
        """

        :param query:
        :param backlog:
        :return:
        """
        res = []
        for i in query:
            if backlog.lower() in i["backlog"]["baseName"].lower():
                res.append(i)
        return res

    def _find_by_use_case(self, query, use_case):
        """

        :param query:
        :param use_case:
        :return:
        """
        res = []
        for i in query:
            if use_case.lower() in i["use_case_name"].lower():
                res.append(i)
        return res

    def _find_by_status(self, query, status_req):
        """

        :param query:
        :param status_req:
        :return:
        """
        res = []
        for i in query:
            if status_req.lower() in i["requestStatus"].lower():
                res.append(i)
        return res

    def find_projects_with_requests(self):
        """

        :return:
        """
        backlog = self.requests_repository.find_request_by_filter({"$and": [{"stateRequest": "P"}, {"target": "B"}]})
        project_options = []
        tmp = []
        for i in backlog:
            tmp_project_name = self.project_repository.find_one(str(i["idProject"]))["name"]
            if tmp_project_name not in tmp:
                tmp.append(tmp_project_name)
                project_options.append({"_id": str(i["idProject"]), "name": tmp_project_name})
        return project_options

    def find_projects_with_requests_data_tables(self):
        """

        :return:
        """
        backlog = self.requests_repository.find_request_by_filter({"$and": [{"stateRequest": "P"}, {"target": "T"}]})
        project_options = []
        tmp = []
        for i in backlog:
            tmp_project_name = self.project_repository.find_one(str(i["idProject"]))["name"]
            if tmp_project_name not in tmp:
                tmp.append(tmp_project_name)
                project_options.append({"_id": str(i["idProject"]), "name": tmp_project_name})
        return project_options

    def find_projects_with_tickets(self):
        """

        :return:
        """
        backlog = self.requests_repository.find_request_by_filter({"$and": [{"$or": [{"stateRequest": "D"},
            {"stateRequest": "A"}]}, {"target": "B"}]})
        project_options = []
        tmp = []
        for i in backlog:
            tmp_project_name = self.project_repository.find_one(str(i["idProject"]))["name"]
            if tmp_project_name not in tmp:
                tmp.append(tmp_project_name)
                project_options.append({"_id": str(i["idProject"]), "name": tmp_project_name})
        return project_options

    def create_new_use_case(self, request):
        """

        :param request:
        :return:
        """
        id_use_case = ""
        if request["action"] == "new_use_case":
            use_case_str = Use_case({}).data
            use_case_str["name"] = request["use_case_body"]["name"].strip().upper()
            use_case_str["description"] = request["use_case_body"]["description"]
            use_case_str["startDate"] = request["use_case_body"]["startDate"]
            use_case_str["finishDate"] = request["use_case_body"]["finishDate"]
            if len(list(self.use_case_repository.find_any_query({"name": request["use_case_body"]["name"].strip().upper()}))) == 0:
                id_use_case = self.use_case_service.add_one(use_case_str)
                self.project_service.action_in_project(str(request["idProject"]), "add",  {"field": "useCases",
                                                                                           "data": id_use_case["_id"],
                                                                                           "user_id": str(request["user"])})
                self._update_tables_use_case(str(id_use_case["_id"]), request)
                use_case_tmp = {"user": ObjectId(str(request["user"])), "target": "U",
                                "action": "Add", "idProject": ObjectId(str(request["idProject"])), "idUsecase": ObjectId(str(id_use_case["_id"])),
                                "idBacklog": "", "stateRequest": "P", "created_time": datetime.now(), "comment": "",
                                "request_number": len(list(self.requests_repository.find_all()))+1}
                request_str = Requests(use_case_tmp).data
                self.requests_repository.insert_one(request_str)
            else:
                raise ValueError("Ya existe un caso de uso con ese nombre.")
        return str(id_use_case)

    def update_use_case(self, request):
        """

        :param request:
        :return:
        """
        use_case_id = request["use_case_id"]
        if request["action"] == "head":
            old_use_case = self.use_case_repository.find_one(use_case_id)
            del old_use_case["_id"]
            request["use_case_body"]["name"] = request["use_case_body"]["name"].strip().upper()
            if len(list(self.use_case_repository.find_any_query({"$and": [{"name": request["use_case_body"]["name"].strip().upper()},
                {"_id": {"$ne": ObjectId(str(use_case_id))}}]}))) == 0:
                if (old_use_case["name"].strip().lower() != request["use_case_body"]["name"].strip().lower() or
                    old_use_case["description"].strip().lower() != request["use_case_body"]["description"].strip().lower() or
                    old_use_case["startDate"].strftime("%Y-%m-%d").strip().lower() != request["use_case_body"]["startDate"].strip().lower() or
                    old_use_case["finishDate"].strftime("%Y-%m-%d").strip().lower() != request["use_case_body"]["finishDate"].strip().lower())and \
                        len(list(self.requests_repository.find_use_case_request(use_case_id))) == 0:
                    self._update_head_use_case(use_case_id, request, old_use_case)
                else:
                    self.use_case_repository.update_head(use_case_id, request["use_case_body"])
            else:
                raise ValueError("Ya existe ese nombre para otro caso de uso")

        elif request["action"] == "tables":
            return self._update_tables_use_case(use_case_id, request)

        elif request["action"] == "delete":
            self._delete_use_case(request["use_case_id"], request["project_id"], request["user_id"])

        elif request["action"] == "tablon":
            self._update_use_cases_tablones(use_case_id,request)

    def _update_use_cases_tablones(self, use_case_id, request):
        """

        :param use_case_id:
        :param request:
        :return:
        """
        use_case_tables = self.use_case_repository.find_one(use_case_id)["tablones"]
        for k in request["new_tablones"]:
            backlog_tmp = self.data_table_repository.find_one(k)
            if backlog_tmp["_id"] not in use_case_tables:
                new_request = {"user": ObjectId(str(request["user"])),
                               "target": "T", "action": "Add", "idProject": ObjectId(str(request["idProject"]).strip()),
                               "idUsecase": ObjectId(str(use_case_id).strip()), "idBacklog": backlog_tmp["_id"],
                               "stateRequest": "P", "created_time": datetime.now(), "comment": "",
                               "request_number": len(list(self.requests_repository.find_all())) + 1}
                request_str = Requests(new_request).data
                self.requests_repository.insert_one(request_str)
                use_case_tables.append(ObjectId(str(backlog_tmp["_id"]).strip()))
        if len(use_case_tables) > 0:
            self.use_case_service.update_tablones(use_case_id, use_case_tables, request["user"])
        return self.use_case_service.find_use_case_data_tables(use_case_id)

    def _delete_use_case(self, use_case_id, project_id, user_id):
        """
            Delete use case from projects
        :param use_case_id:
        """
        if len(list(self.requests_repository.find_any_query({"$and": [{"idUsecase": ObjectId(str(use_case_id))},
            {"stateRequest": "P"}, {"idProject": ObjectId(str(project_id))}]}))) == 0:
            new_request = {"user": ObjectId(str(user_id)),
                           "target": "U", "action": "Del",
                           "idProject": ObjectId(str(project_id)),
                           "idUsecase": ObjectId(use_case_id),
                           "idBacklog": "", "stateRequest": "P",
                           "created_time": datetime.now(), "comment": "",
                           "request_number": len(list(self.requests_repository.find_all())) + 1}
            request_str = Requests(new_request).data
            self.requests_repository.insert_one(request_str)
        else:
            raise ValueError("Ya existe una solicitud pendiente para el caso de uso.")

    def _update_head_use_case(self, use_case_id, request, old_use_case):
        """
            Create request to add tables for the use case
        :param: use_case_id: str
        :param: old_use_case: json
        :param: request
        :return:
        """
        if "tables" in old_use_case:
            del old_use_case["tables"]
        if "tablones" in old_use_case:
            del old_use_case["tablones"]
        if "modifications" in old_use_case:
            del old_use_case["modifications"]
        if "created_time" in old_use_case:
            del old_use_case["created_time"]

        self.use_case_repository.update_head(use_case_id, request["use_case_body"])
        self.use_case_service.update_state(use_case_id, "P", request["user"])
        new_request = {"user": ObjectId(str(request["user"])),
                       "target": "U", "action": "Mod",
                       "idProject": ObjectId(str(request["idProject"])),
                       "idUsecase": ObjectId(str(use_case_id)),
                       "idBacklog": "", "oldUseCase": old_use_case,
                       "stateRequest": "P", "created_time": datetime.now(),
                       "comment": "", "request_number": len(list(self.requests_repository.find_all())) + 1}
        request_str = Requests(new_request).data
        self.requests_repository.insert_one(request_str)

    def _update_tables_use_case(self, use_case_id, request):
        """

        :param use_case_id:
        :param request:
        :return:
        """
        use_case_tables = self.use_case_repository.find_one(use_case_id)["tables"]
        for i in request["old_tables"]:
            new_request = {"user": ObjectId(str(request["user"])),
                           "target": "B", "action": "Add", "idProject": ObjectId(str(request["idProject"].strip())),
                           "idUsecase": ObjectId(str(use_case_id).strip()), "idBacklog": ObjectId(str(i).strip()),
                           "stateRequest": "P", "created_time": datetime.now(),
                           "comment": "", "request_number": len(list(self.requests_repository.find_all()))+1}
            request_str = Requests(new_request).data
            use_case_tables.append(ObjectId(str(i).strip()))
            self.requests_repository.insert_one(request_str)
            self.backlog_repository.update_state_request(str(i).strip(), "P")
        for k in request["new_tables"]:
            k["productOwner"] = ObjectId(str(request["user"]))
            k["idTable"] = ""
            k["tableRequest"] = "P"
            k["request_date"] = datetime.now()
            query = {"$and": [{"baseName": k["baseName"]}, {"originSystem": k["originSystem"]},
                {"periodicity": k["periodicity"]}, {"uuaaRaw": k["uuaaRaw"]}, {"uuaaMaster": k["uuaaMaster"]}]}
            backlog_tmp = self.backlog_repository.find_any_query(query)

            if len(list(backlog_tmp)) == 0:
                id = self.backlog_service.insert_one(k)["_id"]
                new_request = {"user": ObjectId(str(request["user"])),
                               "target": "B", "action": "Add", "idProject": ObjectId(str(request["idProject"].strip())),
                               "idUsecase": ObjectId(use_case_id.strip()), "idBacklog": ObjectId(str(id)),
                               "stateRequest": "P", "created_time": datetime.now(), "comment": "",
                               "request_number": len(list(self.requests_repository.find_all()))+ 1}
                request_str = Requests(new_request).data
                use_case_tables.append(ObjectId(str(id).strip()))
                self.backlog_repository.update_state_request(str(id).strip(), "P")
                self.requests_repository.insert_one(request_str)
            else:
                raise ValueError("Ya existe una tabla con esa información.")


        self.use_case_repository.update_state_request(use_case_id, "P")
        if len(use_case_tables) > 0:
            self.use_case_service.update_tables(use_case_id, use_case_tables, request["user"])
        return self.use_case_service.find_use_case_backlogs(use_case_id)

    def update_backlog_use_case(self, request):
        """

        :param request:
        :return:
        """
        use_case_id = request["use_case_id"]
        user_id = request["user"]
        project_id = request["idProject"]
        backlog_id = request["backlog"]["_id"]
        old_backlog = self.backlog_repository.find_one(backlog_id.strip())
        temp = [{"user": ObjectId(str(user_id)),
                 "date": datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]
        for i in old_backlog["modifications"]:
            temp.append({"user": ObjectId(str(i["user"])), "date": i["date"]})
        if request["action"] == "backlog":
            query = {
                "baseName": request["backlog"]["baseName"],
                "originSystem": request["backlog"]["originSystem"],
                "periodicity": request["backlog"]["periodicity"],
                "uuaaRaw": request["backlog"]["uuaaRaw"],
                "uuaaMaster": request["backlog"]["uuaaMaster"],
            }
            query_2 = {"$and": [
                {"baseName": request["backlog"]["baseName"]},
                {"originSystem": request["backlog"]["originSystem"]},
                {"periodicity": request["backlog"]["periodicity"]},
                {"uuaaRaw": request["backlog"]["uuaaRaw"]},
                {"uuaaMaster": request["backlog"]["uuaaMaster"]},
                {"_id": {"$ne": ObjectId(request["backlog"]["_id"].strip())}}]
            }
            backlog_tmp = self.backlog_repository.find_any_query(query_2)
            if len(list(backlog_tmp)) == 0:
                query["history"] = request["backlog"]["history"]
                query["observationField"] = request["backlog"]["observationField"]
                query["typeFile"] = request["backlog"]["typeFile"]
                query["idTable"] = old_backlog["idTable"]
                query["tableRequest"] = old_backlog["tableRequest"]
                query["productOwner"] = ObjectId(request["backlog"]["productOwner"].strip())
                query["tacticalObject"] = request["backlog"]["tacticalObject"]
                query["perimeter"] = request["backlog"]["perimeter"]

                if old_backlog["baseName"].strip().lower() != query["baseName"].strip().lower() or \
                    old_backlog["originSystem"].strip().lower() != query["originSystem"].strip().lower() or \
                    old_backlog["periodicity"].strip().lower() != query["periodicity"].strip().lower() or \
                    old_backlog["uuaaRaw"].strip().lower() != query["uuaaRaw"].strip().lower() or \
                    old_backlog["uuaaMaster"].strip().lower() != query["uuaaMaster"].strip().lower() or \
                    old_backlog["history"] != query["history"] or \
                    old_backlog["observationField"].strip().lower() != query["observationField"].strip().lower() or \
                    old_backlog["typeFile"].strip().lower() != query["typeFile"].strip().lower() or \
                    old_backlog["tacticalObject"].strip().lower() != query["tacticalObject"].strip().lower() or \
                        old_backlog["perimeter"].strip().lower() != query["perimeter"].strip().lower():

                    if len(list(self.requests_repository.find_backlog_request(request["backlog"]["_id"])))  == 0:
                        old_backlog["_id"] = str(old_backlog["_id"])
                        old_backlog["productOwner"] = str(old_backlog["productOwner"])
                        old_backlog["idTable"] = str(old_backlog["idTable"])
                        if "modifications" in old_backlog:
                            del old_backlog["modifications"]
                        if "created_time" in old_backlog:
                            del old_backlog["created_time"]
                        new_request = {"user": ObjectId(str(user_id)),
                                       "target": "B", "action": "Mod", "idProject": ObjectId(str(project_id.strip())),
                                       "idUsecase": ObjectId(use_case_id.strip()), "idBacklog": ObjectId(old_backlog["_id"]),
                                       "stateRequest": "P", "created_time": datetime.now(), "oldBacklog": old_backlog, "comment":"",
                                       "request_number": len(list(self.requests_repository.find_all())) + 1}
                        query["tableRequest"] = "P"
                        request_str = Requests(new_request).data
                        self.requests_repository.insert_one(request_str)
                        self.backlog_repository.update_one(request["backlog"]["_id"], query)
                        self.use_case_service.update_state(use_case_id, "P", user_id)
                        self.backlog_repository.update_state_request(request["backlog"]["_id"], "P")
                        self.backlog_repository.update_modifications(request["backlog"]["_id"].strip(), temp[:20])
                    else:
                        self.backlog_repository.update_one(request["backlog"]["_id"], query)
                        self.backlog_repository.update_modifications(request["backlog"]["_id"].strip(), temp[:20])
                else:
                    self.backlog_repository.update_one(request["backlog"]["_id"], query)
                    self.backlog_repository.update_modifications(request["backlog"]["_id"].strip(), temp[:20])
            else:
                raise ValueError("Ya existe una tabla con esa información.")

        elif request["action"] == "delete":
            if len(list(self.requests_repository.find_any_query({"$and": [{"idBacklog": ObjectId(str(backlog_id))},
                 {"stateRequest": "P"}, {"idProject": ObjectId(str(project_id))}]}))) == 0:
                new_request = {"user": ObjectId(str(user_id)),
                               "target": "B", "action": "Del",
                               "idProject": ObjectId(str(project_id)),
                               "idUsecase": ObjectId(str(use_case_id)),
                               "idBacklog": ObjectId(str(backlog_id)),
                               "stateRequest": "P", "created_time": datetime.now(),
                               "comment": "", "request_number": self.requests_repository.find_all().count() + 1}
                self.use_case_service.update_state(use_case_id, "P", user_id)
                self.backlog_repository.update_state_request(backlog_id, "P")
                request_str = Requests(new_request).data
                self.requests_repository.insert_one(request_str)
                self.backlog_repository.update_modifications(request["backlog"]["_id"].strip(), temp[:20])
            else:
                raise ValueError("Ya existe una solicitud pediente para la tabla.")

    def update_request(self, request_id, request):
        """

        :param request_id:
        :param request:
        """
        old_request = self.requests_repository.find_request(request_id)
        spo = request["spo"]
        comment = request["comment"]
        if request["action"] == "A":
            if old_request["target"] == "B":
                self._approved_request_backlog(request_id, spo, comment)
            elif old_request["target"] == "U":
                self._approved_request_use_case(request_id, spo, comment)

        elif request["action"] == "P":
            if old_request["target"] == "B":
                self._refuse_request_backlog(request_id, "D", spo, comment)
            elif old_request["target"] == "U":
                self._refuse_request_use_case(request_id, "D", spo, comment)

        elif request["action"] == "D":
            if old_request["target"] == "B":
                self._refuse_request_backlog(request_id, "D", spo, comment)
            elif old_request["target"] == "U":
                self._refuse_request_use_case(request_id, "D", spo, comment)

        temp = [{"user": ObjectId(str(spo)),
                 "date": datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]

        for i in old_request["modifications"]:
            temp.append({"user": ObjectId(str(i["user"])), "date": i["date"]})
        self.requests_repository.update_modifications(request_id, temp[:20])

    def _approved_request_backlog(self, request_id, spo, comment):
        """

        :param request_id:
        :param spo:
        :param comment:
        :return:
        """
        old_request = self.requests_repository.find_request(request_id)
        backlog_id = old_request["idBacklog"]
        use_case_id = old_request["idUsecase"]
        project_id = old_request["idProject"]
        if len(list(self.requests_repository.find_backlog_request_ne(str(backlog_id), request_id)))  <= 0:
            self.backlog_repository.update_state_request(str(backlog_id), "A")
        if len(list(self.requests_repository.find_use_case_request_ne(str(use_case_id), request_id))) <= 0:
            self.use_case_repository.update_state_request(str(use_case_id), "A")
        self.requests_repository.update_one(request_id, "A", spo, comment)
        if old_request["action"] == "Mod":
            backlog_id_table = self.backlog_repository.find_one(str(backlog_id))
            state_table = ""
            if backlog_id_table["idTable"] != "":
                dd_backlog = self.data_dictionary_repository.find_one(str(backlog_id_table))
                state_table = dd_backlog["stateTable"]

                if state_table != "N":
                    raise ValueError("No se puede efectuar los cambios, la tabla ya no se encuentra en propuesta de namings.")
                else:
                    self.data_dictionary_repository.update_change_backlog(backlog_id_table["idTable"],
                        {"perimeter": backlog_id_table["perimeter"], "history": backlog_id_table["perimeter"]})

        elif old_request["action"] == "Del":
            use_cases_tables = self.use_case_repository.find_one(str(old_request["idUsecase"]))["tables"]
            new_tables = []
            for i in use_cases_tables:
                if i != backlog_id:
                    new_tables.append(i)
            self.use_case_repository.update_tables(str(use_case_id), new_tables)
            if self.use_case_repository.find_any_query(
                    {"$and": [{"tables": {"$in": [backlog_id]}}, {"_id": {"$ne": use_case_id}}, {"name": {"$ne": "Unassigned"}}]}).count() == 0:
                unassigned_id = self.use_case_repository.find_any_query({"name": "Unassigned"})
                tables = unassigned_id[0]["tables"]
                tables.append(backlog_id)
                self.backlog_repository.update_state_unassigned(str(backlog_id))
                self.use_case_repository.update_tables(str(unassigned_id[0]["_id"]), tables)

        elif old_request["action"] == "Add":
            if self.use_case_repository.find_any_query({"$and": [
                    {"tables": {"$in": [backlog_id]}}, {"name": {"$ne": "Unassigned"}}]}).count() >0:
                unassigned_id = self.use_case_repository.find_any_query({"name": "Unassigned"})
                tables = unassigned_id[0]["tables"]
                tmp_tables = []
                for j in tables:
                    if j != backlog_id:
                        tmp_tables.append(j)
                self.use_case_repository.update_tables(str(unassigned_id[0]["_id"]), tmp_tables)

    def _approved_request_use_case(self, request_id, spo, comment):
        """

        :param request_id:
        :param spo:
        :param comment:
        :return:
        """
        old_request = self.requests_repository.find_request(request_id)
        use_case_id = old_request["idUsecase"]
        project_id = old_request["idProject"]
        self.requests_repository.update_one(request_id, "A", spo, comment)
        if self.requests_repository.find_use_case_request_ne(str(use_case_id), request_id).count() == 0:
            self.use_case_repository.update_state_request(str(use_case_id), "A")
        if old_request["action"] == "Del":
            use_cases_projects = self.project_repository.find_one(str(project_id))["useCases"]
            new_use_cases = []
            for i in use_cases_projects:
                if i != use_case_id:
                    new_use_cases.append(i)
            self.project_repository.update_use_cases(str(project_id), new_use_cases)
            if self.project_repository.find_any_query(
                    {"$and": [{"useCases": {"$in": [use_case_id]}},
                              {"_id": {"$ne": project_id}}, {"name": {"$ne": "Unassigned"}}]}).count() == 0:
                unassigned_id = self.project_repository.find_any_query({"name": "Unassigned"})
                use_cases_unassigned = unassigned_id[0]["useCases"]
                use_cases_unassigned.append(use_case_id)
                self.use_case_repository.update_state_unassigned(str(use_case_id))
                self.project_repository.update_use_cases(str(unassigned_id[0]["_id"]), use_cases_unassigned)

        elif old_request["action"] == "Add":
            if self.use_case_repository.find_any_query({"$and": [
                    {"useCases": {"$in": [use_case_id]}}, {"name": {"$ne": "Unassigned"}}]}).count() >0:
                unassigned_id = self.project_repository.find_any_query({"name": "Unassigned"})
                tables = unassigned_id[0]["useCases"]
                tmp_tables=[]
                for j in tables:
                    if j != use_case_id:
                        tmp_tables.append(j)
                self.project_repository.update_use_cases(str(unassigned_id[0]["_id"]), tmp_tables)

    def _refuse_request_backlog(self, request_id, action, spo, comment):
        """

        :param request_id:
        :param action:
        :param spo:
        :param comment:
        :return:
        """
        old_request = self.requests_repository.find_request(request_id)
        backlog_id = old_request["idBacklog"]
        use_case_id = old_request["idUsecase"]
        project_id = old_request["idProject"]
        if self.requests_repository.find_backlog_request_ne(str(backlog_id), request_id).count() <= 0:
            self.backlog_repository.update_state_request(str(backlog_id), "A")
        if self.requests_repository.find_use_case_request_ne(str(use_case_id), request_id).count() <= 0:
            self.use_case_repository.update_state_request(str(use_case_id), "A")
        self.requests_repository.update_one(request_id, action, spo, comment)
        if old_request["action"] == "Add":
            use_cases_tables = self.use_case_repository.find_one(str(old_request["idUsecase"]))["tables"]
            new_tables = []
            for i in use_cases_tables:
                if i != backlog_id:
                    new_tables.append(i)
            self.use_case_repository.update_tables(str(use_case_id), new_tables)
            if self.use_case_repository.find_any_query(
                    {"$and": [{"tables": {"$in": [backlog_id]}}, {"_id": {"$ne": use_case_id}},
                              {"stateRequest": {"$ne": "D"}}, {"name": {"$ne": "Unassigned"}}]}).count() == 0:
                unassigned_id = self.use_case_repository.find_any_query({"name": "Unassigned"})
                tables = unassigned_id[0]["tables"]
                tables.append(backlog_id)
                self.backlog_repository.update_state_unassigned(str(backlog_id))
                self.use_case_repository.update_tables(str(unassigned_id[0]["_id"]), tables)

        elif old_request["action"] == "Mod":
            id_table = ""
            if old_request["oldBacklog"]["idTable"] != "": id_table = ObjectId(old_request["oldBacklog"]["idTable"])
            update_backlog = {'baseName': old_request["oldBacklog"]["baseName"],
                              'originSystem': old_request["oldBacklog"]["originSystem"],
                              'periodicity': old_request["oldBacklog"]["periodicity"],
                              'history': old_request["oldBacklog"]["history"],
                              'observationField': old_request["oldBacklog"]["observationField"],
                              'typeFile': old_request["oldBacklog"]["typeFile"],
                              'idTable': id_table,
                              'tableRequest': "P",
                              'uuaaRaw': old_request["oldBacklog"]["uuaaRaw"],
                              'uuaaMaster': old_request["oldBacklog"]["uuaaMaster"],
                              'productOwner': ObjectId(old_request["oldBacklog"]["productOwner"]),
                              'tacticalObject': old_request["oldBacklog"]["tacticalObject"],
                              'perimeter': old_request["oldBacklog"]["perimeter"]}
            self.backlog_repository.update_one(backlog_id, update_backlog)

    def _refuse_request_use_case(self, request_id, action, spo, comment):
        """
        :param request_id:
        :param action:
        :return:
        """
        old_request = self.requests_repository.find_request(request_id)
        backlog_id = old_request["idBacklog"]
        use_case_id = old_request["idUsecase"]
        project_id = old_request["idProject"]
        if self.requests_repository.find_use_case_request_ne(str(use_case_id), request_id).count() <= 0:
            self.use_case_repository.update_state_request(str(use_case_id), "A")
        self.requests_repository.update_one(request_id, action, spo, comment)
        if old_request["action"] == "Add":
            use_cases_projects = self.project_repository.find_one(str(project_id))["useCases"]
            new_use_cases = []
            for i in use_cases_projects:
                if i != use_case_id:
                    new_use_cases.append(i)
            self.project_repository.update_use_cases(str(project_id), new_use_cases)
            if self.project_repository.find_any_query(
                    {"$and": [{"useCases": {"$in": [use_case_id]}}, {"_id": {"$ne": project_id}},
                              {"name": {"$ne": "Unassigned"}}]}).count() == 0:
                unassigned_id = self.project_repository.find_any_query({"name": "Unassigned"})
                use_cases_unassigned = unassigned_id[0]["useCases"]
                use_cases_unassigned.append(use_case_id)
                self.use_case_repository.update_state_unassigned(str(use_case_id))
                self.project_repository.update_use_cases(str(unassigned_id[0]["_id"]), use_cases_unassigned)
            self._close_other_requests(project_id, use_case_id, spo, comment)

        elif old_request["action"] == "Mod":
            old_request["oldUseCase"]["startDate"] = old_request["oldUseCase"]["startDate"].strftime("%Y-%m-%d")
            old_request["oldUseCase"]["finishDate"] = old_request["oldUseCase"]["finishDate"].strftime("%Y-%m-%d")
            self.use_case_repository.update_head(str(use_case_id), old_request["oldUseCase"])

    def _close_other_requests(self, project_id, use_case_id, spo, comment):
        """

        :param project_id:
        :param use_case_id:
        :param spo:
        :param comment:
        """
        for i in self.requests_repository.find_any_query({"$and": [{"stateRequest": "P"},{"idUsecase": ObjectId(str(use_case_id))},{"idProject": ObjectId(str(project_id))}]}):
            if i["target"] == "B":
                self._refuse_request_backlog(str(i["_id"]), "D", spo, comment)

    def request_data_table(self, request):
        """

        :param request:
        :return:
        """
        use_case_id = request["idUsecase"]
        project_id = request["idProject"]
        user_id = request["user"]
        if request["action"] == "new":
            try:
                backlog_id = self.data_table_service.create_table(request["data"], user_id)
                new_request = {"user": ObjectId(str(user_id)),
                               "target": "T",
                               "action": "Add",
                               "idProject": ObjectId(str(project_id)),
                               "idUsecase": ObjectId(str(use_case_id)),
                               "idBacklog": backlog_id,
                               "stateRequest": "P",
                               "created_time": datetime.now(),
                               "comment": "",
                               "request_number": self.requests_repository.find_all().count() + 1}
                request_str = Requests(new_request).data
                self.requests_repository.insert_one(request_str)
                use_cases_tables = self.use_case_repository.find_one(str(use_case_id))["tablones"]
                use_cases_tables.append(backlog_id)
                self.use_case_repository.update_tablones(str(use_case_id), use_cases_tables)
            except Exception as e:
                raise ValueError(e)
        elif request["action"] == "Add":
            backlog_id = request["backlog_id"]
            if self.requests_repository.find_any_query({"$and": [{"idUsecase": ObjectId(str(use_case_id))},{"idBacklog": ObjectId(str(backlog_id))},{"stateRequest": "P"}, {"idProject": ObjectId(str(project_id))}]}).count() == 0:
                new_request = {"user": ObjectId(str(user_id)),
                               "target": "T",
                               "action": "Add",
                               "idProject": ObjectId(str(project_id)),
                               "idUsecase": ObjectId(str(use_case_id)),
                               "idBacklog": ObjectId(str(backlog_id)),
                               "stateRequest": "P",
                               "created_time": datetime.now(),
                               "comment": "",
                               "request_number": self.requests_repository.find_all().count() + 1}
                request_str = Requests(new_request).data
                self.requests_repository.insert_one(request_str)
                use_cases_tables = self.use_case_repository.find_one(str(use_case_id))["tablones"]
                use_cases_tables.append(ObjectId(str(backlog_id)))
                self.use_case_service.update_tablones(str(use_case_id), use_cases_tables, user_id)
            else:
                raise ValueError("Ya existe una solicitud pediente para la tabla.")

        elif request["action"] == "Del":
            backlog_id = request["backlog_id"]
            if self.requests_repository.find_any_query({"$and": [{"idUsecase": ObjectId(str(use_case_id))},{"idBacklog": ObjectId(str(backlog_id))},
                                                                 {"stateRequest": "P"}, {"idProject": ObjectId(str(project_id))}]}).count() == 0:
                new_request = {"user": ObjectId(str(user_id)),
                               "target": "T",
                               "action": "Del",
                               "idProject": ObjectId(str(project_id)),
                               "idUsecase": ObjectId(str(use_case_id)),
                               "idBacklog": ObjectId(str(backlog_id)),
                               "stateRequest": "P",
                               "created_time": datetime.now(),
                               "comment": "",
                               "request_number": self.requests_repository.find_all().count() + 1}
                request_str = Requests(new_request).data
                self.requests_repository.insert_one(request_str)
            else:
                raise ValueError("Ya existe una solicitud pediente para la tabla.")
        return str(backlog_id)

    def update_request_data_table(self, request_id, request):
        """

        :param request:
        :param request_id:
        :return:
        """
        spo = request["spo"]
        comment = request["comment"]
        if request["action"] == "A":
            self._approved_data_table_request(request_id, spo, comment)
        elif request["action"] == "P":
            self._refuse_data_table_request(request_id, "D", spo, comment)
        elif request["action"] == "D":
            self._refuse_data_table_request(request_id, "D", spo, comment)

        old_request = self.requests_repository.find_one(request_id)
        temp = [{"user": ObjectId(str(spo)),
                 "date": datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]
        for i in old_request["modifications"]:
            temp.append({"user": ObjectId(str(i["user"])), "date": i["date"]})
        self.requests_repository.update_modifications(request_id, temp[:20])

    def _approved_data_table_request(self, request_id, spo, comment):
        """

        :return:
        """
        old_request = self.requests_repository.find_request(request_id)
        backlog_id = old_request["idBacklog"]
        use_case_id = old_request["idUsecase"]

        if self.requests_repository.find_use_case_request_ne(str(use_case_id), request_id).count() <= 0:
            self.use_case_repository.update_state_request(str(use_case_id), "A")
        self.requests_repository.update_one(request_id, "A", spo, comment)

        if old_request["action"] == "Del":
            use_cases_tables = self.use_case_repository.find_one(str(old_request["idUsecase"]))["tablones"]
            new_tables = []
            for i in use_cases_tables:
                if i != ObjectId(str(backlog_id)):
                    new_tables.append(i)
            self.use_case_service.update_tablones(str(use_case_id), new_tables, spo)
            if self.use_case_repository.find_any_query(
                    {"$and": [{"tablones": {"$in": [ObjectId(str(backlog_id))]}},
                              {"_id": {"$ne": ObjectId(str(use_case_id))}},
                              {"name": {"$ne": "Unassigned"}}]}).count() == 0:
                unassigned_id = self.use_case_repository.find_any_query({"name": "Unassigned"})
                tables = unassigned_id[0]["tablones"]
                tables.append(ObjectId(str(backlog_id)))
                self.data_table_repository.update_state_unassigned(str(backlog_id))
                self.use_case_service.update_tablones(str(unassigned_id[0]["_id"]), tables, spo)
                self.data_table_repository.change_state_table(backlog_id, "D")

        elif old_request["action"] == "Add":
            if self.use_case_repository.find_any_query({"$and": [{"tablones": {"$in": [ObjectId(str(backlog_id))]}}, {"name": {"$ne": "Unassigned"}}]}).count() > 0:
                unassigned_id = self.use_case_repository.find_any_query({"name": "Unassigned"})
                tables = unassigned_id[0]["tablones"]
                tmp_tables = []
                for j in tables:
                    if j != ObjectId(str(backlog_id)):
                        tmp_tables.append(j)
                self.use_case_repository.update_tablones(str(unassigned_id[0]["_id"]), tmp_tables)
            data_dic_tmp = self.data_table_repository.find_one(backlog_id)
            if data_dic_tmp["stateTable"] == "R":
                self.data_table_repository.change_state_table(backlog_id, "G")

    def _refuse_data_table_request(self, request_id, action, spo, comment):
        """

        :return:
        """
        old_request = self.requests_repository.find_request(request_id)
        backlog_id = old_request["idBacklog"]
        use_case_id = old_request["idUsecase"]
        if self.requests_repository.find_use_case_request_ne(str(use_case_id), request_id).count() <= 0:
            self.use_case_repository.update_state_request(str(use_case_id), "A")
        self.requests_repository.update_one(request_id, action, spo, comment)
        if old_request["action"] == "Add":
            use_cases_tables = self.use_case_repository.find_one(str(old_request["idUsecase"]))["tablones"]
            new_tables = []
            for i in use_cases_tables:
                if i != backlog_id:
                    new_tables.append(i)
            self.use_case_repository.update_tablones(str(use_case_id), new_tables)
            if self.use_case_repository.find_any_query(
                    {"$and": [{"tablones": {"$in": [backlog_id]}}, {"_id": {"$ne": use_case_id}},
                              {"stateRequest": {"$ne": "D"}}, {"name": {"$ne": "Unassigned"}}]}).count() == 0:
                unassigned_id = self.use_case_repository.find_any_query({"name": "Unassigned"})
                tables = unassigned_id[0]["tablones"]
                tables.append(backlog_id)
                self.data_table_repository.update_state_unassigned(str(backlog_id))
                self.use_case_repository.update_tablones(str(unassigned_id[0]["_id"]), tables)
