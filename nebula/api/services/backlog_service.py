from nebula.api.entities.backlog import Backlog
from nebula.api.entities.object_data_dictionary import Object_data_dictionary
from nebula.api.entities.field_review import Field_review
from nebula.api.entities.field_raw import Field_raw
from nebula.api.entities.field_master import Field_master

from nebula.api.persistence.backlog_repository import Backlog_repository
from nebula.api.persistence.use_case_repository import Use_case_repository
from nebula.api.persistence.data_dictionary_repository import Data_dictionary_repository
from nebula.api.persistence.user_repository import User_repository
from nebula.api.persistence.project_repository import Project_repository
from nebula.api.persistence.excels_repository import Excels_repository
from nebula.api.persistence.functional_map_repository import Functional_map_repository
from nebula.api.persistence.field_repository import Field_repository
from nebula.api.persistence.suffix_repository import Suffix_repository

from nebula.api.services.data_dictionary_service import Data_dictionary_service


import re
from bson import ObjectId
import datetime


class Backlog_service(object):

    """
        A class used to represent a Functional map services.

        Attributes
             -------
            backlog_repository : Backlog_repository
                Reference to Backlog repository class to interact with database
            use_case_repository: Use_case_repository
                Reference to Use case repository class to interact with database
            user_repository: User_repository
                Reference to User repository class to interact with database
            project_repository: Project_repository
                Reference to Project repository class to interact with database
            excel_repository: Excel_repository
                Reference to Excel repository class to interact with database
    """

    def __init__(self):
        """

        """
        self.backlog_repository = Backlog_repository()
        self.use_case_repository = Use_case_repository()
        self.user_repository = User_repository()
        self.project_repository = Project_repository()
        self.excel_repository = Excels_repository()
        self.data_dictionary_repository = Data_dictionary_repository()
        self.values = {"G": "Gobierno", "A": "Arquitectura", "N": "En propuesta namings", "I": "Para ingestar",
                       "IN": "Ingestada en desarrollo", "Q": "Calidad", "M": "Mallas", "P": "Produccion",
                       "ungoverned": "En backlog", "PA": "Pendiente aprobación backlog", 'RN': "Revisión Negocio",
                       "R": "Revisión solicitud", "D": "Descartado", "F": "Gobierno funcional"}
        self.yes_no = {"YES": "YES", "NO": "NO", "SI": "YES"}
        self.yes_no_1_0 = {"YES": 1, "NO": 0, "SI": 1, "": 0}
        self.functional_map_repository = Functional_map_repository()
        self.field_repository = Field_repository()
        self.suffix_repository = Suffix_repository()
        
        self.data_dictionary_service = Data_dictionary_service()

    def static_atributes(self, attribute):
        """Get predefine data in excels

        :param attribute: str, String to get data
        :return: Array
        """
        res = []
        if attribute == "periodicity":
            res = self.periodicity()
        elif attribute == "originSystem":
            res = self.origin_system()
        elif attribute == "typeFile":
            res = self.type_file()
        elif attribute == "periodo":
            res = self.periodicity_complete()
        return res

    def periodicity(self):
        """Get all periodicity

        :return: Cursor, array of dict
        """
        return self.excel_repository.find_periodicities()

    def periodicity_complete(self):
        """Get all periodicity

        :return: Cursor, array of dict
        """
        return self.excel_repository.find_periodicities_complete()
     
    def origin_system(self):
        """Get all origin systems

        :return:Cursor, array of dict
        """
        return self.excel_repository.find_origin_systems()

    def type_file(self):
        """Get all type file

        :return: Array
        """
        return Backlog().type_file()

    def insert_one(self, backlog):
        """Insert one backlog

        :param backlog: json, dict with data to insert
        :return: json, contains id generate
        """
        if self.backlog_repository.find_any_query(backlog).count() > 0:
            raise ValueError("Backlog already exist")
        print(backlog)
        backlog_str = Backlog({}).data
        backlog_str["baseName"] = backlog["baseName"]
        backlog_str["originSystem"] = backlog["originSystem"]
        backlog_str["periodicity"] = backlog["periodicity"]
        backlog_str["history"] = int(backlog["history"])
        backlog_str["observationField"] = backlog["observationField"]
        backlog_str["typeFile"] = backlog["typeFile"]
        backlog_str["uuaaRaw"] = backlog["uuaaRaw"]
        backlog_str["uuaaMaster"] = backlog["uuaaMaster"]
        backlog_str["productOwner"] = ObjectId(backlog["productOwner"])
        backlog_str["request_date"] = backlog["request_date"]
        backlog_str["tacticalObject"] = backlog["tacticalObject"]
        backlog_str["perimeter"] = backlog["perimeter"]
        backlog_str["tableRequest"] = backlog["tableRequest"]

        return {"_id": str(self.backlog_repository.insert_one(backlog_str))}

    def find_one(self, backlog_id):
        """Get a specific backlog

        :param backlog_id: str, string that reference to backlog id
        :return: json, dict with backlog data
        """
        tmp_backlog = self.backlog_repository.find_one(backlog_id)
        if not tmp_backlog:
            self.backlog_repository.delete_request(backlog_id)
            return "None"
        else:
            tmp_backlog["_id"] = str(tmp_backlog["_id"])
            tmp_backlog["idTable"] = str(tmp_backlog["idTable"])
            tmp_backlog["productOwner"] = str(tmp_backlog["productOwner"])
            if len(tmp_backlog["modifications"]) != 0:
                tmp_backlog["modifications"] = {
                    "user_name": self.user_repository.find_one(str(tmp_backlog["modifications"][0]["user"]))["name"],
                    "date": tmp_backlog["modifications"][0]["date"]}
            else:
                tmp_backlog["modifications"] = {"user_name": "N/A", "date": "N/A"}
            return tmp_backlog

    def find_all(self):
        """Get all backlogs

        :return: cursor, array of dict with all backlog data
        """
        result = []
        for j in self.backlog_repository.find_all():
            j["_id"] = str(j["_id"])
            j["idTable"] = str(j["idTable"])
            j["productOwner"] = str(j["productOwner"])
            if len(j["modifications"]) != 0:
                j["modifications"] = {"user_name": self.user_repository.find_one(str(j["modifications"][0]["user"]))["name"],
                                      "date": j["modifications"][0]["date"]}
            else:
                j["modifications"] = {"user_name": "N/A", "date": "N/A"}
            result.append(j)
        return result

    def join_to_table(self, table_backlog_id, table_id, user_id):
        """Check that one data dictionary was created with a backlog reference

        :param table_backlog_id: str, String that reference a table backlog id
        :param table_id: str, String that reference a data dictionary id
        :return:
        """
        backlog = self.backlog_repository.find_one(table_backlog_id)
        if backlog is None:
            raise ValueError("La tabla legacy no se encuentra registrada.")

        temp = [{"user": ObjectId(str(user_id)),
                 "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]
        for i in backlog["modifications"]:
            temp.append({"user": ObjectId(str(i["user"])), "date": i["date"]})
        self.backlog_repository.update_modifications(table_backlog_id, temp[:20])
        self.backlog_repository.joint_to_table(table_backlog_id, table_id)

    def actions(self, id_backlog, data):
        """Check a backlog and change their state or remove

        :param id_backlog: str, String that reference backlog id
        :param data: json, dictionary with data to verify action
        :return:
        """
        if data["actions"] == "approve":
            self._approve_table_backlog(id_backlog, data["user_id"])
        elif data["actions"] == "remove":
            self._remove_one_backlog(id_backlog, data["user_id"])

    def _approve_table_backlog(self, backlog_id, user_id):
        """Change state of a backlog from Pending (P) to Approved (A)
        it's ready to start governanceData

        :param backlog_id: str, string that reference a backlog id
        :return:
        """
        backlog = self.backlog_repository.find_one(backlog_id)
        if backlog is None:
            raise ValueError("La tabla legacy no se encuentra registrada.")

        temp = [{"user": ObjectId(str(user_id)),
                 "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]
        for i in backlog["modifications"]:
            temp.append({"user": ObjectId(str(i["user"])),"date": i["date"]})
        self.backlog_repository.update_modifications(backlog_id, temp[:20])
        self.backlog_repository.approve_table_backlog(backlog_id)

    def find_approve_table(self, backlog_id):
        """ Get all approved table

        :param backlog_id: str, string that reference to backlog id
        :return: cursor, array of dict
        """
        result = []
        for j in self.backlog_repository.find_approved(backlog_id):
            j["_id"] = str(j["_id"])
            j["idTable"] = str(j["idTable"])
            j["productOwner"] = str(j["productOwner"])
            if len(j["modifications"]) != 0:
                j["modifications"] = {"user_name": self.user_repository.find_one(str(j["modifications"][0]["user"]))["name"],
                                      "date": j["modifications"][0]["date"]}
            else:
                j["modifications"] = {"user_name": "N/A", "date": "N/A"}
            result.append(j)
        return result

    def find_by_id_table(self, table_id):
        """Get a data dictionary reference from one table backlog

        :param table_id: str, String that reference a data dictionary id
        :return: cursor, Array of json with backlog data
        """
        backlog = self.backlog_repository.find_by_id_table({"idTable": table_id})
        backlog["_id"] = str(backlog["_id"])
        backlog["idTable"] = str(backlog["idTable"])
        backlog["productOwner"] = str(backlog["productOwner"])
        if len(backlog["modifications"]) != 0:
            backlog["modifications"] = {"user_name": self.user_repository.find_one(str(backlog["modifications"][0]["user"]))["name"],
                                        "date": backlog["modifications"][0]["date"]}
        else:
            backlog["modifications"] = {"user_name": "N/A", "date": "N/A"}
        return backlog

    def _remove_one_backlog(self, backlog_id, user_id):
        """Remove backlog of database because it wasn't approved

        :param backlog_id: str, string that reference backlog id
        :return:
        """
        backlog = self.backlog_repository.find_one(backlog_id)
        if backlog is None:
            raise ValueError("La tabla legacy no se encuentra registrada.")

        temp = [{"user": ObjectId(str(user_id)),
                 "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]
        for i in backlog["modifications"]:
            temp.append({"user": ObjectId(str(i["user"])),"date": i["date"]})
        self.backlog_repository.update_modifications(backlog_id, temp[:20])
        self.backlog_repository.remove_one(backlog_id)
        self.use_case_repository.remove_all_tables(backlog_id, user_id)




    def global_backlog_query(self, id_case, search_type, base_name, alias, origin_system, observation_field, uuaa_raw, uuaa_master):
        """Get all backlog with some conditions that are build as filters applies
        A query its build dynamic from data received

        :param base_name: str, string that reference a base name
        :param origin_system: str, string that reference a origin system
        :param observation_field: str, string that reference a observation field in a backlog
        :return:
        """
        print("entre")
        backlogs = self.backlog_repository.find_all()

        if len(str(base_name).strip()) != 0:
            backlogs = self._filter_by_property_name(backlogs, str(base_name).strip(), "baseName")
        #if len(str(alias).strip()) != 0:
        #   backlogs = self._filter_by_property_name(backlogs, str(alias).strip(), "alias")
        if len(str(origin_system).strip()) != 0:
            backlogs = self._filter_by_property_name(backlogs, str(origin_system).strip(), "originSystem")
        if len(str(observation_field).strip()) != 0:
            backlogs = self._filter_by_property_name(backlogs, str(observation_field).strip(), "observationField")
        if len(str(uuaa_raw).strip()) != 0:
            backlogs = self._filter_by_property_name(backlogs, str(uuaa_raw).strip(), "uuaaRaw")
        if len(str(uuaa_master).strip()) != 0:
            backlogs = self._filter_by_property_name(backlogs, str(uuaa_master).strip(), "uuaaMaster")

        return backlogs

    def _filter_by_property_name(self, backlogs, prop_value, prop_name):
        """
        Get the tables with the parameter
        :param tables: Array, tables
        :param table_name: str, the physical name of the table
        :return: an array of tables
        """
        res = []
        for backlog in backlogs:
            if backlog is not None and backlog not in res:
                if prop_name in backlog:
                    if prop_value.lower() in backlog[prop_name].lower():
                        res.append(backlog)
        return res

    def _query_project_use_case(self, backlogs):
        result = []
        for i in backlogs:
            try:
                if len(i["idTable"]) > 0:
                    i["stateTable"] = self.data_dictionary_repository.find_one(str(i["idTable"]))["stateTable"]
                    i["viewStateTable"] = self.values[i["stateTable"]]

                i["_id"] = str(i["_id"])
                i["idTable"] = str(i["idTable"])
                i["productOwner"] = str(i["productOwner"])
                if len(i["modifications"]) != 0:
                    i["modifications"] = {
                        "user_name": self.user_repository.find_one(str(i["modifications"][0]["user"]))["name"],
                        "date": i["modifications"][0]["date"]}
                else:
                    i["modifications"] = {"user_name": "N/A", "date": "N/A"}
                tmp = self.use_case_repository.all_backlog_contains(str(i["_id"]))
                i["use_cases"] = []
                i["projects"] = []
                for j in tmp:
                    if j['name'] not in i["use_cases"]:
                        i["use_cases"].append(j["name"])
                    tmp_pro = self.project_repository.find_all_use_cases_contains(str(j["_id"]))
                    for k in tmp_pro:
                        if k["name"] not in i["projects"]:
                            i["projects"].append(k["name"])
                i["stateTable"] = ''
                i["viewStateTable"] = ''
                result.append(i)
            except Exception as e:
                print(e)
                print(i)

        return result

    def update_backlog(self, backlog_id, backlog, user_id):
        """Update backlog data

        :param backlog_id: str, string that reference a backlog id
        :param backlog: json, dictionary with data to update
        :param user_id: str,
        :return:
        """
        backlog_db = self.backlog_repository.find_one(backlog_id)
        if backlog_db is None:
            raise ValueError("La tabla legacy no se encuentra registrada.")

        temp = [{"user": ObjectId(str(user_id)),
                 "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]

        for i in backlog_db["modifications"]:
            temp.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]
            })
        tmp_table_request = backlog_db["tableRequest"]
        tmp_table_id = backlog["idTable"]
        backlog["productOwner"] = ObjectId(backlog["productOwner"])
        del backlog["idTable"]
        if self.backlog_repository.find_any_query(backlog).count() > 0:
            raise ValueError("Backlog already exist with same format")
        if tmp_table_id == "":
            backlog["idTable"] = ""
        else:
            backlog["idTable"] = ObjectId(tmp_table_id)
        backlog["tableRequest"] = tmp_table_request
        del backlog["_id"]
        if "modifications" in backlog:
            del backlog["modifications"]
        self.backlog_repository.update_one(backlog_id, backlog)
        self.backlog_repository.update_modifications(backlog_id, temp[:20])

    def upload_data_dictionary(self, body):
        """

        :param body:
        :return:
        """
        raw_object = body["object"]["raw"]
        master_object = body["object"]["master"]
        master_fields = body["fields"]["master"]
        raw_fields = body["fields"]["raw"]

        if body["backlog"] == "NEW":
            longo = self.backlog_repository.find_by_basename(body["newBacklog"]["baseName"].strip())
            if len(longo) > 0:
                raise ValueError("Backlog ya existe con ese nombre base")
            else:
                body["newBacklog"]["productOwner"] = ObjectId(str(body["user"]))
                body["newBacklog"]["idTable"] = ""
                body["newBacklog"]["tableRequest"] = "A"
                id = self.insert_one(body["newBacklog"])["_id"]
                backlog = self.backlog_repository.find_one(id)
                self.use_case_repository.insert_one_table(body["newUseCase"], id)
        else:
            backlog = self.backlog_repository.find_one(str(body["backlog"]))
            if backlog is None:
                raise ValueError("Backlog no registrado en la base de datos.")
        
        user = body["user"]
        temp = [{"user": ObjectId(str(user)),
                 "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]

        for i in backlog["modifications"]:
            temp.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]
            })
        object_dd = self._create_structure_object(raw_object, master_object)
        object_dd["user"] = ObjectId(user)
        object_dd["FieldReview"],  object_dd["fieldsRaw"],  object_dd["fieldsMaster"] = self._create_structure_fields(raw_fields, master_fields, user)

        if len(str(backlog["idTable"])) == 0:
            object_dd["_id"] = ObjectId(str(backlog["_id"]))
            self.data_dictionary_repository.insert_one(object_dd)
            self.backlog_repository.joint_to_table(str(backlog["_id"]), str(backlog["_id"]))
        else:
            self.data_dictionary_repository.update_field(str(backlog["idTable"]), object_dd)
        self.backlog_repository.update_modifications(str(backlog["_id"]), temp[:20])

    def _create_structure_object(self, raw_object, master_object):
        head = Object_data_dictionary().data
        uuaaRaw = raw_object[1][2:6]
        if len(master_object) != 0:
            alias = master_object[32].strip().lower()
            info_uuaa = self.functional_map_repository.find_uuaa(master_object[14][1:].strip().upper())
        else:
            alias = raw_object[32].strip().lower()
            info_uuaa = self.functional_map_repository.find_uuaa(raw_object[14][1:].strip().upper())
        current = 0
        required = 0
        volume = 0
        if str(raw_object[19]).strip().split(" ")[0] != "":
            current = int(str(raw_object[19]).strip().split(" ")[0])

        if str(raw_object[20]).strip().split(" ")[0] != "":
            required = int(str(raw_object[20]).strip().split(" ")[0])

        if len(str(raw_object[21]).strip()) != 0:
            volume = int(str(raw_object[21]).strip())
        raw_periodicity = self.excel_repository.find_state_periodicity(raw_object[16].strip().upper())
        if raw_periodicity is None:
            raise ValueError("La periodicidad de raw "+raw_object[16].strip().upper()+" es inválida. ")
        raw_periodicity = raw_periodicity["periodicity"]

        if len(master_object) != 0:
            master_periodicity = self.excel_repository.find_state_periodicity(master_object[16].strip().upper())
            if master_periodicity is None:
                raise ValueError("La periodicidad de master " + master_object[16].strip().upper() + " es inválida. ")
            master_periodicity = master_periodicity["periodicity"]
            head["master_name"] = master_object[1].strip().lower()
            head["periodicity_master"] = master_periodicity
            head["master_route"] = master_object[13].strip()
            head["partitions_master"] = master_object[15].strip()
        head["baseName"] = raw_object[2].strip().upper()
        head["originSystem"] = raw_object[22].strip().upper()
        head["periodicity"] = raw_periodicity
        head["observationField"] = raw_object[3]
        head["typeFile"] = raw_object[27].strip()
        head["uuaaRaw"] = uuaaRaw.strip().upper()
        head["uuaaMaster"] = raw_object[14].strip().upper()
        head["tacticalObject"] = self.yes_no[raw_object[33].strip().upper()]
        head["raw_name"] = raw_object[1].strip().lower()
        head["separator"] = raw_object[28].strip()
        head["alias"] = alias
        head["data_source"] = info_uuaa["data_source"]
        head["information_group_level_1"] = info_uuaa["level1"]
        head["information_group_level_2"] = info_uuaa["level2"]
        head["physical_name_source_object"] = raw_object[23].strip()
        head["master_path"] = raw_object[13].strip()
        head["raw_path"] = raw_object[25].strip()
        head["raw_route"] = raw_object[13].strip()
        head["perimeter"] = raw_object[6].strip()
        head["information_level"] = raw_object[7].strip()
        head["objectType"] = raw_object[12].strip()
        head["loading_type"] = raw_object[18].strip().lower()
        head["partitions_raw"] = raw_object[15].strip()
        head["current_depth"] = current
        head["estimated_volume_records"] = volume
        head["required_depth"] = required
        if len(raw_object) > 34:
            head["deploymentType"] = raw_object[34].strip()
        if len(raw_object) > 35:
            head["modelVersion"] = raw_object[35].strip()
        if len(raw_object) > 36:
            head["objectVersion"] = raw_object[36].strip()
        return head

    def _create_structure_fields(self, raw_fields, master_fields, user):
        fields_raw = []
        fields_master = []
        fields_review = []
        dict_master = {}
        for u in range(0, len(master_fields)):
            tmp_master = Field_master({}, u).data
            naming = self.field_repository.find_exactly_naming(master_fields[u][4].strip().lower())
            if naming is not None:
                tmp_master["naming"]["naming"] = naming["naming"]["naming"]
                tmp_master["naming"]["suffix"] = naming["naming"]["suffix"]
                tmp_master["naming"]["Words"] = naming["naming"]["Words"]
                tmp_master["naming"]["isGlobal"] = "Y"
                tmp_master["naming"]["architectureState"] = ""
                tmp_master["naming"]["governanceState"] = "C"
                tmp_master["naming"]["code"] = naming["code"]
                tmp_master["naming"]["hierarchy"] = naming["level"]

                if "dataOwnerInformation" in naming and len(naming["dataOwnerInformation"]) != 0:
                    tmp_master["domain"] = naming["dataOwnerInformation"][0]["domain"]
                    if len(tmp_master["domain"]) == 0:
                        tmp_master["domain"] = "Unassigned"
                    tmp_master["subdomain"] = naming["dataOwnerInformation"][0]["subdomain"]
                    tmp_master["ownership"] = naming["dataOwnerInformation"][0]["ownership"]
                    tmp_master["conceptualEntity"] = naming["dataOwnerInformation"][0]["conceptualEntity"]
                    tmp_master["operationalEntity"] = naming["dataOwnerInformation"][0]["operationalEntity"]
            else:
                naming = {
                    "naming": {"naming": master_fields[u][4].strip().lower()},
                    "logic": master_fields[u][5],
                    "originalDesc": master_fields[u][6]
                }
                tmp_master["naming"]["naming"] = naming["naming"]["naming"]
                tmp_master["naming"]["suffix"] = self.suffix_repository.find_one(naming["naming"]["naming"].strip().split("_")[-1])["_id"]
                tmp_master["naming"]["Words"] = []
                tmp_master["naming"]["isGlobal"] = "N"
                tmp_master["naming"]["architectureState"] = "C"
                tmp_master["naming"]["governanceState"] = "A"
                tmp_master["naming"]["code"] = "empty"
                tmp_master["naming"]["hierarchy"] = "PRINCIPAL"
                #raise ValueError("Naming not found. "+master_fields[u][4])

            suffix_id = self.suffix_repository.find_one(naming["naming"]["naming"].strip().split("_")[-1])
            if suffix_id is None:
                raise ValueError(
                    "Suffix " + naming["naming"]["naming"].strip().split("_")[-1] + "not found..." + naming["naming"][
                        "naming"])

            cat = str(master_fields[u][7]).strip()
            if len(cat) == 0:
                cat = "N/A"

            form = str(master_fields[u][11]).strip()
            if len(form) == 0:
                form = "empty"

            default = str(master_fields[u][15]).strip()
            if len(default) == 0:
                default = "empty"

            token = str(master_fields[u][23].strip()).lower()
            if len(token) == 0:
                token = "empty"

            dest = master_fields[u][10].strip().upper()
            decimals = 0
            integers = 0
            if "DECIMAL" in dest:
                temp = dest.split("(")[1][:-1].split(",")
                if len(temp) == 1:
                    integers = int(temp[0])
                else:
                    decimals = int(temp[1])
                    integers = int(temp[0]) - decimals

            origin_ta = ""
            for k in range(0, len(raw_fields)):
                if master_fields[u][4].strip().lower() == raw_fields[k][4].strip().lower():
                    origin_ta = "fieldsRaw." + str(k)

            logical = master_fields[u][12].strip().upper()
            tmp_master["logic"] = naming["logic"]
            tmp_master["description"] = naming["originalDesc"].replace("~", "\n")
            tmp_master["catalogue"] = cat
            tmp_master["key"] = self.yes_no_1_0[master_fields[u][13].strip().upper()]
            tmp_master["mandatory"] = self.yes_no_1_0[master_fields[u][14].strip().upper()]
            tmp_master["legacy"]["legacy"] = master_fields[u][17].strip().lower()
            tmp_master["legacy"]["legacyDescription"] = "N/A"
            tmp_master["column"] = u
            tmp_master["format"] = form
            tmp_master["origin"] = origin_ta
            tmp_master["default"] = default
            tmp_master["destinationType"] = dest
            tmp_master["logicalFormat"] = str(logical)
            tmp_master["tokenization"] = str(token)
            tmp_master["tds"] = self.yes_no_1_0[master_fields[u][31].strip().upper()]
            tmp_master["modification"].append({"user": ObjectId(user), "startDate": datetime.datetime.now(),
                                               "state": "GL", "stateValidation": "YES"})
            dict_master[master_fields[u][4].strip().lower()] = {"logical": master_fields[u][12].strip().upper(), "column": u,
                                                                "type": master_fields[u][10].strip().upper(),
                                                                "decimals": decimals, "integers": integers}
            fields_master.append(tmp_master)

        for p in range(0, len(raw_fields)):
            tmp_raw = Field_raw({}, p).data
            naming = self.field_repository.find_exactly_naming(raw_fields[p][4].strip().lower())
            if naming is not None:
                tmp_raw["naming"]["naming"] = naming["naming"]["naming"]
                tmp_raw["naming"]["suffix"] = naming["naming"]["suffix"]
                tmp_raw["naming"]["Words"] = naming["naming"]["Words"]
                tmp_raw["naming"]["isGlobal"] = "Y"
                tmp_raw["naming"]["architectureState"] = ""
                tmp_raw["naming"]["governanceState"] = "C"
                tmp_raw["naming"]["hierarchy"] = naming["level"]
                tmp_raw["naming"]["code"] = naming["code"]

                if "dataOwnerInformation" in naming and len(naming["dataOwnerInformation"]) != 0:
                    tmp_raw["domain"] = naming["dataOwnerInformation"][0]["domain"]
                    tmp_raw["subdomain"] = naming["dataOwnerInformation"][0]["subdomain"]
                    tmp_raw["ownership"] = naming["dataOwnerInformation"][0]["ownership"]
                    tmp_raw["conceptualEntity"] = naming["dataOwnerInformation"][0]["conceptualEntity"]
                    tmp_raw["operationalEntity"] = naming["dataOwnerInformation"][0]["operationalEntity"]
            else:
                naming = {
                    "naming": {"naming": raw_fields[p][4].strip().lower()},
                    "logic": raw_fields[p][5],
                    "originalDesc": raw_fields[p][6]
                }
                tmp_raw["naming"]["naming"] = naming["naming"]["naming"]
                tmp_raw["naming"]["suffix"] = self.suffix_repository.find_one(naming["naming"]["naming"].strip().split("_")[-1])["_id"]
                tmp_raw["naming"]["Words"] = []
                tmp_raw["naming"]["isGlobal"] = "N"
                tmp_raw["naming"]["architectureState"] = "C"
                tmp_raw["naming"]["governanceState"] = "A"
                tmp_raw["naming"]["code"] = "empty"
                tmp_raw["naming"]["hierarchy"] = "PRINCIPAL"
                #raise ValueError("Naming not found. "+master_fields[u][4])

            suffix_id = self.suffix_repository.find_one(naming["naming"]["naming"].strip().split("_")[-1])
            if suffix_id is None:
                raise ValueError("Suffix " + naming["naming"]["naming"].strip().split("_")[-1] + "not found..." + naming["naming"]["naming"])

            cat = str(raw_fields[p][7]).strip()
            if len(cat) == 0:
                cat = "N/A"

            form = str(raw_fields[p][11]).strip()
            if len(form) == 0:
                form = "empty"

            default = str(raw_fields[p][15]).strip()
            if len(default) == 0:
                default = "empty"

            token = str(raw_fields[p][23]).strip().lower()
            if len(token) == 0:
                token = "empty"

            origin_ta = ""
            if raw_fields[p][22].strip().upper() == "NO" or len(raw_fields[p][22].strip().upper()) == 0:
                tmp_review = Field_review({}, p).data
                tmp_review["naming"] = tmp_raw["naming"]
                tmp_review["logic"] = naming["logic"]
                tmp_review["description"] = naming["originalDesc"].replace("~", "\n")
                tmp_review["catalogue"] = cat
                tmp_review["key"] = self.yes_no_1_0[raw_fields[p][13].strip().upper()]
                tmp_review["mandatory"] = self.yes_no_1_0[raw_fields[p][14].strip().upper()]
                tmp_review["legacy"]["legacy"] = raw_fields[p][17].strip().upper()
                tmp_review["legacy"]["legacyDescription"] = "N/A"
                tmp_review["default"] = default
                tmp_review["format"] = form
                tmp_review["domain"] = tmp_raw["domain"]
                tmp_review["subdomain"] = tmp_raw["subdomain"]
                tmp_review["ownership"] = tmp_raw["ownership"]
                tmp_review["conceptualEntity"] = tmp_raw["conceptualEntity"]
                tmp_review["operationalEntity"] = tmp_raw["operationalEntity"]
                tmp_review["outVarchar"] = raw_fields[p][12]

                #tmp_review["length"] = int(raw_fields[p][12][13:-1])
                #tmp_review["outLength"] = int(raw_fields[p][12][13:-1])
                newFormat = raw_fields[p][12].split("(")
                if(len(newFormat)==2):
                    if "," not in newFormat[1]:
                        tmp_review["length"] = int(newFormat[1][:-1])
                        tmp_review["outLength"] = int(newFormat[1][:-1])
                    else:
                        tmp_review["length"] = int(newFormat[1].split(",")[0])
                        tmp_review["outLength"] = int(newFormat[1].split(",")[0])
                else:
                    if newFormat[0].strip().upper() == "TIME":
                        tmp_review["length"] = 15
                        tmp_review["outLength"] = 15
                    elif newFormat[0].strip().upper() == "TIMESTAMP":
                        tmp_review["length"] = 26
                        tmp_review["outLength"] = 26
                    elif newFormat[0].strip().upper() == "NUMERIC BIG":
                        tmp_review["length"] = 255
                        tmp_review["outLength"] = 255
                    elif newFormat[0].strip().upper() == "NUMERIC SHORT":
                        tmp_review["length"] = 10
                        tmp_review["outLength"] = 10
                    elif newFormat[0].strip().upper() == "NUMERIC LARGE":
                        tmp_review["length"] = 255
                        tmp_review["outLength"] = 255    
                    elif newFormat[0].strip().upper() == "CLOB":
                        tmp_review["length"] = 3999
                        tmp_review["outLength"] = 3999
                    elif newFormat[0].strip().upper() == "DATE":
                        tmp_review["length"] = 10
                        tmp_review["outLength"] = 10   
                    else:
                        tmp_review["length"] = 0
                        tmp_review["outLength"] = 0
                tmp_review["outFormat"] = raw_fields[p][12]
                tmp_review["destinationType"] = raw_fields[p][10].strip().upper()
                tmp_review["governanceFormat"] = raw_fields[p][12].strip().upper()

                if naming["naming"]["naming"] in dict_master:
                    tmp_review["integers"] = dict_master[naming["naming"]["naming"]]["integers"]
                    tmp_review["decimals"] = dict_master[naming["naming"]["naming"]]["decimals"]

                    if "DECIMAL" in dict_master[naming["naming"]["naming"]]["type"]:
                        tmp_review["destinationType"] = "DECIMAL"
                        tmp_review["governanceFormat"] = dict_master[naming["naming"]["naming"]]["type"]
                    elif "TIMESTAMP" in dict_master[naming["naming"]["naming"]]["type"]:
                        tmp_review["destinationType"] = "TIMESTAMP"
                        tmp_review["governanceFormat"] = "TIMESTAMP"
                    else:
                        tmp_review["governanceFormat"] = dict_master[naming["naming"]["naming"]]["logical"]
                        tmp_review["destinationType"] = dict_master[naming["naming"]["naming"]]["type"]

                tmp_review["column"] = len(fields_review)
                tmp_review["modification"].append({"user": ObjectId(user), "startDate": datetime.datetime.now(),
                                                   "state": "GL", "stateValidation": "YES"})
                origin_ta = "FieldReview." + str(tmp_review["column"])
                fields_review.append(tmp_review)

            outFormat = raw_fields[p][10].strip().upper()
            if raw_fields[p][4].strip().lower() in dict_master:
                outFormat = dict_master[raw_fields[p][4].strip().lower()]["type"]

            tmp_raw["logic"] = naming["logic"]
            tmp_raw["outFormat"] = outFormat
            tmp_raw["description"] = naming["originalDesc"].replace("~", "\n")
            tmp_raw["catalogue"] = cat
            tmp_raw["key"] = self.yes_no_1_0[raw_fields[p][13].strip().upper()]
            tmp_raw["mandatory"] = self.yes_no_1_0[raw_fields[p][14].strip().upper()]
            tmp_raw["legacy"]["legacy"] = raw_fields[p][17].strip().upper()
            tmp_raw["legacy"]["legacyDescription"] = "N/A"
            tmp_raw["format"] = form
            tmp_raw["tokenization"] = str(token)
            tmp_raw["default"] = default
            tmp_raw["column"] = p
            tmp_raw["origin"] = origin_ta
            tmp_raw["tds"] = self.yes_no_1_0[raw_fields[p][31].strip().upper()]
            tmp_raw["destinationType"] = raw_fields[p][10].strip().upper()
            tmp_raw["logicalFormat"] = raw_fields[p][12].strip().upper()
            tmp_raw["modification"].append({"user": ObjectId(user), "startDate": datetime.datetime.now(),
                                            "state": "GL", "stateValidation": "YES"})
            fields_raw.append(tmp_raw)

        return fields_review, fields_raw, fields_master
    
    def check_backlog_basename(self, baseName):
        """
        Metodo que trae la info de un backlog usando el nombre base (baseName) como parametro. 
        Usado para verificar la existencia de un backlog y para asegurar que no hayan repetidos. 
        :param baseName: El nombre base del backlog a buscar
        :return: El backlog
        """
        data = self.backlog_repository.find_by_basename(baseName.strip())
        if len(data) > 0:
            for p in data:
                p["_id"] = str(p["_id"])
                p["idTable"] = str(p["idTable"])
                p["productOwner"] = str(p["productOwner"])

        return data
    
    def upload_new_data_dictionary(self, body):
        """

        :param body:
        :return:
        """
        raw_object = body["object"]["raw"]
        master_object = body["object"]["master"]
        master_fields = body["fields"]["master"]
        raw_fields = body["fields"]["raw"]


        if body["backlog"] == "NEW":
            longo = self.backlog_repository.find_by_basename(body["newBacklog"]["baseName"].strip())
            if len(longo) > 0:
                raise ValueError("Backlog ya existe con ese nombre base")
            else:
                body["newBacklog"]["productOwner"] = ObjectId(str(body["user"]))
                body["newBacklog"]["idTable"] = ""
                body["newBacklog"]["tableRequest"] = "A"
                id = self.insert_one(body["newBacklog"])["_id"]
                backlog = self.backlog_repository.find_one(id)
                self.use_case_repository.insert_one_table(body["newUseCase"], id)
        else:
            backlog = self.backlog_repository.find_one(str(body["backlog"]))
            if backlog is None:
                raise ValueError("Backlog no registrado en la base de datos.")
        
        user = body["user"]
        temp = [{"user": ObjectId(str(user)),
                 "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]

        for i in backlog["modifications"]:
            temp.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]
            })

        object_dd = self._create_new_structure_object(raw_object, master_object)
        object_dd["user"] = ObjectId(user)
        object_dd["FieldReview"],  object_dd["fieldsRaw"],  object_dd["fieldsMaster"] = self._create_new_structure_fields(raw_fields, master_fields, user)

        if len(str(backlog["idTable"])) == 0:
            object_dd["_id"] = ObjectId(str(backlog["_id"]))
            self.data_dictionary_repository.insert_one(object_dd)
            self.backlog_repository.joint_to_table(str(backlog["_id"]), str(backlog["_id"]))
        else:
            self.data_dictionary_repository.update_field(str(backlog["idTable"]), object_dd)
        self.backlog_repository.update_modifications(str(backlog["_id"]), temp[:20])
        
    def _create_new_structure_object(self, raw_object, master_object):
        head = Object_data_dictionary().data
        uuaaRaw = raw_object[1][2:6]
        if len(master_object) != 0:
            alias = master_object[37].strip().lower()
            info_uuaa = self.functional_map_repository.find_uuaa(master_object[19][1:].strip().upper())
        else:
            alias = raw_object[37].strip().lower()
            info_uuaa = self.functional_map_repository.find_uuaa(raw_object[19][1:].strip().upper())
        current = 0
        required = 0
        volume = 0
        if str(raw_object[24]).strip().split(" ")[0] != "":
            current = int(str(raw_object[24]).strip().split(" ")[0])

        if str(raw_object[25]).strip().split(" ")[0] != "":
            required = int(str(raw_object[25]).strip().split(" ")[0])

        if len(str(raw_object[26]).strip()) != 0:
            volume = int(str(raw_object[26]).strip())

        raw_periodicity = self.excel_repository.find_state_periodicity(raw_object[21].strip().upper())
        if raw_periodicity is None:
            raise ValueError("La periodicidad de raw "+raw_object[21].strip().upper()+" es inválida. ")
        raw_periodicity = raw_periodicity["periodicity"]

        if len(master_object) != 0:
            master_periodicity = self.excel_repository.find_state_periodicity(master_object[21].strip().upper())
            if master_periodicity is None:
                raise ValueError("La periodicidad de master " + master_object[21].strip().upper() + " es inválida. ")
            master_periodicity = master_periodicity["periodicity"]
            head["master_name"] = master_object[1].strip().lower()
            head["periodicity_master"] = master_periodicity
            head["master_route"] = master_object[18].strip()
            head["partitions_master"] = master_object[20].strip()

        head["baseName"] = raw_object[2].strip().upper()
        head["originSystem"] = raw_object[27].strip().upper()
        head["periodicity"] = raw_periodicity
        head["observationField"] = raw_object[3]
        head["typeFile"] = raw_object[32].strip().lower()
        head["uuaaRaw"] = uuaaRaw.strip().upper()
        head["uuaaMaster"] = raw_object[19].strip().upper()
        head["tacticalObject"] = self.yes_no[raw_object[38].strip().upper()]
        head["raw_name"] = raw_object[1].strip().lower()
        head["separator"] = raw_object[33].strip()
        head["alias"] = alias
        head["data_source"] = info_uuaa["data_source"]
        head["information_group_level_1"] = info_uuaa["level1"]
        head["information_group_level_2"] = info_uuaa["level2"]
        head["physical_name_source_object"] = raw_object[28].strip()
        head["master_path"] = raw_object[18].strip()
        head["raw_path"] = raw_object[30].strip()
        head["raw_route"] = raw_object[18].strip()
        head["perimeter"] = raw_object[6].strip()
        head["information_level"] = raw_object[7].strip()
        head["objectType"] = raw_object[17].strip()
        head["loading_type"] = raw_object[23].strip().lower()
        head["partitions_raw"] = raw_object[20].strip()
        head["current_depth"] = current
        head["estimated_volume_records"] = volume
        head["required_depth"] = required
        head["deploymentType"] = raw_object[11].strip()
        head["modelVersion"] = raw_object[12].strip()
        head["objectVersion"] = raw_object[13].strip()
        return head
        
    def _create_new_structure_fields(self, raw_fields, master_fields, user):
        fields_raw = []
        fields_master = []
        fields_review = []
        dict_master = {}
        for u in range(0, len(master_fields)):
            tmp_master = Field_master({}, u).data
            naming = self.field_repository.find_exactly_naming(master_fields[u][5].strip().lower())
            if naming is not None:
                tmp_master["naming"]["naming"] = naming["naming"]["naming"]
                tmp_master["naming"]["suffix"] = naming["naming"]["suffix"]
                tmp_master["naming"]["Words"] = naming["naming"]["Words"]
                tmp_master["naming"]["isGlobal"] = "Y"
                tmp_master["naming"]["architectureState"] = ""
                tmp_master["naming"]["governanceState"] = "C"
                tmp_master["naming"]["code"] = naming["code"]
                tmp_master["naming"]["hierarchy"] = naming["level"]

                if "dataOwnerInformation" in naming and len(naming["dataOwnerInformation"]) != 0:
                    tmp_master["domain"] = naming["dataOwnerInformation"][0]["domain"]
                    if len(tmp_master["domain"]) == 0:
                        tmp_master["domain"] = "Unassigned"
                    tmp_master["subdomain"] = naming["dataOwnerInformation"][0]["subdomain"]
                    tmp_master["ownership"] = naming["dataOwnerInformation"][0]["ownership"]
                    tmp_master["conceptualEntity"] = naming["dataOwnerInformation"][0]["conceptualEntity"]
                    tmp_master["operationalEntity"] = naming["dataOwnerInformation"][0]["operationalEntity"]
            else:
                naming = {
                    "naming": {"naming": master_fields[u][5].strip().lower()},
                    "logic": master_fields[u][7],
                    "originalDesc": master_fields[u][9]
                }
                tmp_master["naming"]["naming"] = naming["naming"]["naming"]
                tmp_master["naming"]["suffix"] = self.suffix_repository.find_one(naming["naming"]["naming"].strip().split("_")[-1])["_id"]
                tmp_master["naming"]["Words"] = []
                tmp_master["naming"]["isGlobal"] = "N"
                tmp_master["naming"]["architectureState"] = "C"
                tmp_master["naming"]["governanceState"] = "A"
                tmp_master["naming"]["code"] = "empty"
                tmp_master["naming"]["hierarchy"] = "PRINCIPAL"
                #raise ValueError("Naming not found. "+master_fields[u][4])
            suffix_id = self.suffix_repository.find_one(naming["naming"]["naming"].strip().split("_")[-1])
            if suffix_id is None:
                raise ValueError(
                    "Suffix " + naming["naming"]["naming"].strip().split("_")[-1] + "not found..." + naming["naming"][
                        "naming"])
                     
            cat = str(master_fields[u][13]).strip()
            if len(cat) == 0:
                cat = "N/A"

            form = str(master_fields[u][20]).strip()
            if len(form) == 0:
                form = "empty"

            default = str(master_fields[u][24]).strip()
            if len(default) == 0:
                default = "empty"

            token = str(master_fields[u][33].strip()).lower()
            if len(token) == 0:
                token = "empty"
            dest = master_fields[u][19].strip().upper()
            decimals = 0
            integers = 0
            if "DECIMAL" in dest:
                temp = dest.split("(")[1][:-1].split(",")
                if len(temp) == 1:
                    integers = int(temp[0])
                else:
                    decimals = int(temp[1])
                    integers = int(temp[0]) - decimals
            
            origin_ta = ""
            for k in range(0, len(raw_fields)):
                if master_fields[u][5].strip().lower() == raw_fields[k][5].strip().lower():
                    origin_ta = "fieldsRaw." + str(k)
            
            logical = master_fields[u][21].strip().upper()
            tmp_master["logic"] = naming["logic"]
            tmp_master["description"] = naming["originalDesc"].replace("~", "\n")
            tmp_master["catalogue"] = cat
            tmp_master["key"] = self.yes_no_1_0[master_fields[u][22].strip().upper()]
            tmp_master["mandatory"] = self.yes_no_1_0[master_fields[u][23].strip().upper()]
            tmp_master["legacy"]["legacy"] = master_fields[u][26].strip().lower()
            tmp_master["legacy"]["legacyDescription"] = "N/A"
            tmp_master["column"] = u
            tmp_master["format"] = form
            tmp_master["origin"] = origin_ta
            tmp_master["default"] = default
            tmp_master["destinationType"] = dest
            tmp_master["logicalFormat"] = str(logical)
            tmp_master["tokenization"] = str(token)
            tmp_master["tds"] = self.yes_no_1_0[master_fields[u][41].strip().upper()]
            tmp_master["modification"].append({"user": ObjectId(user), "startDate": datetime.datetime.now(),
                                               "state": "GL", "stateValidation": "YES"})
            dict_master[master_fields[u][5].strip().lower()] = {"logical": master_fields[u][21].strip().upper(), "column": u,
                                                                "type": master_fields[u][19].strip().upper(),
                                                                "decimals": decimals, "integers": integers}
            fields_master.append(tmp_master)
        
        for p in range(0, len(raw_fields)):
            tmp_raw = Field_raw({}, p).data
            naming = self.field_repository.find_exactly_naming(raw_fields[p][4].strip().lower())
            if naming is not None:
                tmp_raw["naming"]["naming"] = naming["naming"]["naming"]
                tmp_raw["naming"]["suffix"] = naming["naming"]["suffix"]
                tmp_raw["naming"]["Words"] = naming["naming"]["Words"]
                tmp_raw["naming"]["isGlobal"] = "Y"
                tmp_raw["naming"]["architectureState"] = ""
                tmp_raw["naming"]["governanceState"] = "C"
                tmp_raw["naming"]["hierarchy"] = naming["level"]
                tmp_raw["naming"]["code"] = naming["code"]

                if "dataOwnerInformation" in naming and len(naming["dataOwnerInformation"]) != 0:
                    tmp_raw["domain"] = naming["dataOwnerInformation"][0]["domain"]
                    tmp_raw["subdomain"] = naming["dataOwnerInformation"][0]["subdomain"]
                    tmp_raw["ownership"] = naming["dataOwnerInformation"][0]["ownership"]
                    tmp_raw["conceptualEntity"] = naming["dataOwnerInformation"][0]["conceptualEntity"]
                    tmp_raw["operationalEntity"] = naming["dataOwnerInformation"][0]["operationalEntity"]
            else:
                naming = {
                    "naming": {"naming": raw_fields[p][5].strip().lower()},
                    "logic": raw_fields[p][7],
                    "originalDesc": raw_fields[p][9]
                }
                tmp_raw["naming"]["naming"] = naming["naming"]["naming"]
                tmp_raw["naming"]["suffix"] = self.suffix_repository.find_one(naming["naming"]["naming"].strip().split("_")[-1])["_id"]
                tmp_raw["naming"]["Words"] = []
                tmp_raw["naming"]["isGlobal"] = "N"
                tmp_raw["naming"]["architectureState"] = "C"
                tmp_raw["naming"]["governanceState"] = "A"
                tmp_raw["naming"]["code"] = "empty"
                tmp_raw["naming"]["hierarchy"] = "PRINCIPAL"
                #raise ValueError("Naming not found. "+master_fields[u][5])

            suffix_id = self.suffix_repository.find_one(naming["naming"]["naming"].strip().split("_")[-1])
            if suffix_id is None:
                raise ValueError("Suffix " + naming["naming"]["naming"].strip().split("_")[-1] + "not found..." + naming["naming"]["naming"])

            cat = str(raw_fields[p][13]).strip()
            if len(cat) == 0:
                cat = "N/A"

            form = str(raw_fields[p][20]).strip()
            if len(form) == 0:
                form = "empty"

            default = str(raw_fields[p][24]).strip()
            if len(default) == 0:
                default = "empty"

            token = str(raw_fields[p][33]).strip().lower()
            if len(token) == 0:
                token = "empty"
            
            origin_ta = ""
            if raw_fields[p][32].strip().upper() == "NO" or len(raw_fields[p][32].strip().upper()) == 0:
                tmp_review = Field_review({}, p).data
                tmp_review["naming"] = tmp_raw["naming"]
                tmp_review["logic"] = naming["logic"]
                tmp_review["description"] = naming["originalDesc"].replace("~", "\n")
                tmp_review["catalogue"] = cat
                tmp_review["key"] = self.yes_no_1_0[raw_fields[p][22].strip().upper()]
                tmp_review["mandatory"] = self.yes_no_1_0[raw_fields[p][23].strip().upper()]
                tmp_review["legacy"]["legacy"] = raw_fields[p][26].strip().upper()
                tmp_review["legacy"]["legacyDescription"] = "N/A"
                tmp_review["default"] = default
                tmp_review["format"] = form
                tmp_review["domain"] = tmp_raw["domain"]
                tmp_review["subdomain"] = tmp_raw["subdomain"]
                tmp_review["ownership"] = tmp_raw["ownership"]
                tmp_review["conceptualEntity"] = tmp_raw["conceptualEntity"]
                tmp_review["operationalEntity"] = tmp_raw["operationalEntity"]
                tmp_review["outVarchar"] = raw_fields[p][21]
                tmp_review["length"] = int(raw_fields[p][21][13:-1])
                tmp_review["outLength"] = int(raw_fields[p][21][13:-1])
                tmp_review["outFormat"] = raw_fields[p][21]
                tmp_review["destinationType"] = raw_fields[p][19].strip().upper()
                tmp_review["governanceFormat"] = raw_fields[p][21].strip().upper()

                if naming["naming"]["naming"] in dict_master:
                    tmp_review["integers"] = dict_master[naming["naming"]["naming"]]["integers"]
                    tmp_review["decimals"] = dict_master[naming["naming"]["naming"]]["decimals"]

                    if "DECIMAL" in dict_master[naming["naming"]["naming"]]["type"]:
                        tmp_review["destinationType"] = "DECIMAL"
                        tmp_review["governanceFormat"] = dict_master[naming["naming"]["naming"]]["type"]
                    elif "TIMESTAMP" in dict_master[naming["naming"]["naming"]]["type"]:
                        tmp_review["destinationType"] = "TIMESTAMP"
                        tmp_review["governanceFormat"] = "TIMESTAMP"
                    else:
                        tmp_review["governanceFormat"] = dict_master[naming["naming"]["naming"]]["logical"]
                        tmp_review["destinationType"] = dict_master[naming["naming"]["naming"]]["type"]

                tmp_review["column"] = len(fields_review)
                tmp_review["modification"].append({"user": ObjectId(user), "startDate": datetime.datetime.now(),
                                                   "state": "GL", "stateValidation": "YES"})
                origin_ta = "FieldReview." + str(tmp_review["column"])
                fields_review.append(tmp_review)
            
            outFormat = raw_fields[p][19].strip().upper()
            if raw_fields[p][5].strip().lower() in dict_master:
                outFormat = dict_master[raw_fields[p][5].strip().lower()]["type"]

            tmp_raw["logic"] = naming["logic"]
            tmp_raw["outFormat"] = outFormat
            tmp_raw["description"] = naming["originalDesc"].replace("~", "\n")
            tmp_raw["catalogue"] = cat
            tmp_raw["key"] = self.yes_no_1_0[raw_fields[p][22].strip().upper()]
            tmp_raw["mandatory"] = self.yes_no_1_0[raw_fields[p][23].strip().upper()]
            tmp_raw["legacy"]["legacy"] = raw_fields[p][26].strip().upper()
            tmp_raw["legacy"]["legacyDescription"] = "N/A"
            tmp_raw["format"] = form
            tmp_raw["tokenization"] = str(token)
            tmp_raw["default"] = default
            tmp_raw["column"] = p
            tmp_raw["origin"] = origin_ta
            tmp_raw["tds"] = self.yes_no_1_0[raw_fields[p][41].strip().upper()]
            tmp_raw["logicalFormat"] = raw_fields[p][21].strip().upper()
            tmp_raw["modification"].append({"user": ObjectId(user), "startDate": datetime.datetime.now(),
                                            "state": "GL", "stateValidation": "YES"})
            fields_raw.append(tmp_raw)

        return fields_review, fields_raw, fields_master


    def upload_data_dictionary_new(self, data):
        """
            Loads a data table info from a sophia excel file
        :param data:
        :return:
        """
        data_dict_id = self.data_dictionary_service.new_upload_data_dictionary(data, data["user"], data["project_id"])
        if "use_case_id" in data:
            flag = True
            temp_dedes = self.use_case_repository.find_one(str(data["use_case_id"]))["tables"]
            for i in temp_dedes:
                if str(i) == data_dict_id:
                    flag = False
            if flag:
                temp_dedes.append(ObjectId(str(data_dict_id)))
                self.use_case_repository.update_tables(str(data["use_case_id"]), temp_dedes)

        return data_dict_id