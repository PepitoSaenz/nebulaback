
from nebula.api.entities.operational_base import Operational_base
from nebula.api.entities.field_operational_base import Field_operational_base

from nebula.api.persistence.functional_map_repository import Functional_map_repository
from nebula.api.persistence.operational_base_repository import Operational_base_repository
from nebula.api.persistence.user_repository import User_repository
from nebula.api.persistence.project_repository import Project_repository
from nebula.api.persistence.excels_repository import Excels_repository
from nebula.api.persistence.field_repository import Field_repository
from nebula.api.persistence.suffix_repository import Suffix_repository
from nebula.api.persistence.settings_repository import Settings_repository


import datetime
from bson import ObjectId
import ast


class Operational_base_service():

    """
       Operational base services

       Attributes
           -------
           operational_base_repository : Operational_base_repository
    """

    def __init__(self):
        self.operational_base_repository = Operational_base_repository()
        self.functional_map_repository = Functional_map_repository()
        self.user_repository = User_repository()
        self.project_repository = Project_repository()
        self.excels_repository = Excels_repository()
        self.field_repository = Field_repository()
        self.suffix_repository = Suffix_repository()
        self.settings_repository = Settings_repository()
        self.db_type_options = ["ELASTIC SEARCH", "HDFS-AVRO", "HDFS-PARQUET", "MONGODB", "ORACLE PHYSICS"]

    def create_data_table(self, body):
        """
            Create a new operational base

        :param data: json, dictionary with initial operational base data
        :return: Json, with operational base id  db_type object_name
        """
        user_id = self.user_repository.find_one(body["user_id"])
        if user_id is None:
            raise ValueError("El usuario no es válido para la operación.")
        user_id = user_id["_id"]

        body["db_type"] = body["db_type"].strip().upper()
        if not self.operational_base_repository.find_repeat_base(body["object_name"], body["db_type"]):
            new_operational_base = Operational_base({}, body["fields"]).data
            new_operational_base["object_name"] = body["object_name"]
            new_operational_base["db_type"] = body["db_type"]
            new_operational_base["category_id"] = body["category_id"]
            new_operational_base["uuaaMaster"] = body["uuaaMaster"]
            new_operational_base["alias"] = body["alias"]
            new_operational_base["periodicity"] = body["periodicity"]
            new_operational_base["tacticalObject"] = body["tacticalObject"]
            new_operational_base["baseName"] = body["baseName"]
            new_operational_base["delivery_owner"] = body["delivery_owner"]
            new_operational_base["project_owner"] = ObjectId(str(body["project_id"]))
            new_operational_base["observationField"] = body["observationField"]
            tmp = self.functional_map_repository.find_uuaa(body["uuaaMaster"].upper()[1:])
            new_operational_base["user"] = user_id
            new_operational_base["data_source"] = tmp["data_source"]
            new_operational_base["information_group_level_1"] = tmp["level1"]
            new_operational_base["information_group_level_2"] = tmp["level2"]
            project = self.project_repository.find_one(body["project_id"])
            if project is None:
                raise ValueError("El proyecto no se encuentra registrado.")

            return {"newBase": str(self.operational_base_repository.insert_one(new_operational_base))}
        else:
            raise ValueError("Ya existe una base operacional con ese nombre y tipo de base de datos.")

    def update_object(self, operational_id, data):
        """

        :param operational_id:
        :param data:
        :return:
        """
        user_id = self.user_repository.find_one(data["user_id"])
        if user_id is None:
            raise ValueError("El usuario no es válido para la operación.")

        user_id = self.user_repository.find_one(data["user_id"])["_id"]

        base_operational = self.operational_base_repository.find_one(operational_id)
        if base_operational is None:
            raise ValueError("La base operacional no se encuentra registrada.")

        data["db_type"] = data["db_type"].strip().upper()
        if not self.operational_base_repository.find_repeat_base_ne(operational_id, data["object_name"], data["db_type"]):
            tmp = self.functional_map_repository.find_uuaa(data["uuaaMaster"].upper()[1:])
            data["data_source"] = tmp["data_source"]
            data["information_group_level_1"] = tmp["level1"]
            data["information_group_level_2"] = tmp["level2"]
            if "created_time" in data:
                del data["created_time"]
            if "project_owner" in data:
                del data["project_owner"]
            if "user" in data:
                del data["user"]
            if "user_id" in data:
                del data["user_id"]
            if "fields" in data:
                del data["fields"]

            temp = [{"user": ObjectId(str(user_id)),
                     "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
                    ]
            for i in base_operational["modifications"]:
                temp.append({
                    "user": ObjectId(str(i["user"])),
                    "date": i["date"]
                })
            data["modifications"] = temp[:10]
            self.operational_base_repository.update_head(operational_id, data)
        else:
            raise ValueError("Ya existe una base operacional con ese nombre y tipo de base de datos.")

    def find_object(self, operational_id):
        """

        :param operational_id:
        :return:
        """

        operational_base = self.operational_base_repository.find_head(operational_id)
        operational_base["created_time"] = operational_base["created_time"].strftime("%Y-%m-%d")
        operational_base["project_owner"] = self.project_repository.find_one(str(operational_base["project_owner"]))["name"]
        operational_base["user"] = self.user_repository.find_one(str(operational_base["user"]))["email"]
        temp_str = {"user": "", "user_name": "N/A", "date": "N/A"}
        if len(operational_base["modifications"]) != 0:
            temp_str = {
                "user": str(operational_base["modifications"][0]["user"]),
                "user_name": self.user_repository.find_one(str(operational_base["modifications"][0]["user"]))["name"],
                "date": operational_base["modifications"][0]["date"]}
        operational_base["modifications"] = temp_str
        operational_base["_id"] = str(operational_base["_id"])
        return operational_base

    def update_fields(self, operational_id, data):
        """

        :param operational_id:
        :param data:
        :return:
        """
        if "user_id" not in data:
            raise ValueError("El usuario no es válido para la operación.")

        user_id = self.user_repository.find_one(data["user_id"])
        if user_id is None:
            raise ValueError("El usuario no es válido para la operación.")

        user_id = self.user_repository.find_one(data["user_id"])["_id"]
        operational_base = self.operational_base_repository.find_one(operational_id)
        if operational_base is None:
            raise ValueError("La base operacional no se encuentra registrada.")

        """
        for i in data["fields"]:
            i["check"]["user"] = ObjectId(i["check"]["user"])
            for j in i["comments"]:
                j["user"] = ObjectId(str(j["user"]))
                if "user_email" in j:
                    del j["user_email"]

            temp = [{"user": ObjectId(str(user_id)),
                     "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]
            for k in i["modification"]:
                temp.append({
                    "user": ObjectId(str(k["user"])),
                    "date": k["date"]
                })
            i["modification"] = temp[:10]
        """
        self.operational_base_repository.update_all_fields(operational_id, data["fields"])

    def update_field_action(self, operational_id, data, action):
        """

        :param operational_id:
        :param data:
        :param action:
        :return:
        """
        if "user_id" not in data:
            raise ValueError("El usuario no es válido para la operación.")

        user_id = self.user_repository.find_one(data["user_id"])
        if user_id is None:
            raise ValueError("El usuario no es válido para la operación.")
        user_id = self.user_repository.find_one(data["user_id"])["_id"]

        if action.strip().lower() == "insert":
            self._insert_field(operational_id, data, user_id)
        elif action.strip().lower() == "substract":
            self._substract_field(operational_id, data, user_id)

        return self.find_fields(operational_id)

    def _insert_field(self, operational_id, data, user_id):
        """

        :param operational_id:
        :param data:
        :return:
        """
        operational_base = self.operational_base_repository.find_one(operational_id)
        if operational_base is None:
            raise ValueError("La base operacional no se encuentra registrada.")

        res = []
        for i in operational_base["fields"]:
            temp = [{"user": ObjectId(str(user_id)),
                     "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]
            for k in i["modification"]:
                temp.append({
                    "user": ObjectId(str(k["user"])),
                    "date": k["date"]
                })
            i["modification"] = temp[:10]
            if i["column"] == data["column"]:
                res.append(ast.literal_eval(str(Field_operational_base(column=data["column"]))))
                i["column"] += 1
            elif i["column"] > data["column"]:
                i["column"] += 1
            res.append(i)

        operational_base["fields"] = res
        if data["column"] == len(operational_base["fields"]):
            res.append(ast.literal_eval(str(Field_operational_base(column=data["column"]))))
        operational_base["fields"] = res

        self.operational_base_repository.update_all_fields(operational_id, res)

    def _substract_field(self, operational_id, data, user_id):
        """

        :param operational_id:
        :param data:
        :return:
        """
        operational_base = self.operational_base_repository.find_one(operational_id)
        if operational_base is None:
            raise ValueError("La base operacional no se encuentra registrada.")

        res = []
        for i in operational_base["fields"]:
            temp = [{"user": ObjectId(str(user_id)),
                     "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]
            for k in i["modification"]:
                temp.append({
                    "user": ObjectId(str(k["user"])),
                    "date": k["date"]
                })
            i["modification"] = temp[:10]
            if i["column"] > data["column"]:
                i["column"] -= 1
                res.append(i)
            elif i["column"] < data["column"]:
                res.append(i)
        operational_base["fields"] = res
        self.operational_base_repository.update_all_fields(operational_id, res)

    def find_fields(self, operational_id):
        """

        :param operational_id:
        :return:
        """
        operational_base = self.operational_base_repository.find_fields(operational_id)
        for i in operational_base["fields"]:
            i["check"]["user"] = str(i["check"]["user"])
            for j in i["comments"]:
                j["user"] = str(j["user"])
                j["user_email"] = self.user_repository.find_one(str(j["user"]))["email"]

            for k in i["modification"]:
                k["user"] = str(k["user"])
                k["user_email"] = self.user_repository.find_one(str(k["user"]))["email"]
        operational_base["_id"] = str(operational_base["_id"])
        return operational_base

    def find_by_filter(self, logic, desc, name, uuaa):
        """

        :param logic:
        :param desc:
        :param name:
        :param uuaa:
        :return:
        """
        res = []
        query = self.operational_base_repository.find_all()
        if len(name.strip()) != 0:
            query = self.__filter_by_property(query, name, "object_name")
        if len(logic.strip()) != 0:
            query = self.__filter_by_property(query, logic, "baseName")
        if len(desc.strip()) != 0:
            query = self.__filter_by_property(query, desc, "observationField")
        if len(uuaa.strip()) != 0:
            query = self.__filter_by_property(query, uuaa, "uuaaMaster")
        for j in query:
            j["_id"] = str(j["_id"])
            res.append(j)
        return res

    @staticmethod
    def __filter_by_property(array, value, proper):
        """

        :param array:
        :param value:
        :param proper:
        :return:
        """
        res = []
        for i in array:
            if value.lower() in i[proper].lower():
                i["_id"] = str(i["_id"])
                res.append(i)
        return res

    def generate_excels(self, operational_id):
        """

        :param operational_id:
        :return:
        """
        res = {"object": self._generate_excel_object(operational_id),
               "fields": self._generate_excel_fields(operational_id)}
        return res

    def _generate_excel_object(self, operational_id):
        """

        :param operational_id:
        :return:
        """
        res = []
        source_path = ""
        operational_base = self.operational_base_repository.find_one(operational_id)
        if operational_base is None:
            raise ValueError("La base operacional no se encuentra registrada.")

        volume_records = 0
        if int(operational_base['estimated_volume_records']) != 0:
            volume_records = str(int(operational_base['estimated_volume_records']))
        current_depth = str(int(operational_base["current_depth"])) + " months"
        if int(operational_base["current_depth"]) == 1:
            current_depth = str(operational_base["current_depth"]) + " month"
        required_depth = str(int(operational_base["required_depth"])) + " months"
        if int(operational_base["required_depth"]) == 1:
            required_depth = str(int(operational_base["required_depth"])) + " month"
        tactical = "NO"
        if str(operational_base["tacticalObject"]) == "1" or str(operational_base["tacticalObject"]) == "YES":
            tactical = "YES"
        if "source_path" in operational_base.keys():
            source_path = operational_base["source_path"]
        try:
            res.append({
                "COUNTRY OF THE DATA SOURCE": "Colombia",
                "PHYSICAL NAME OBJECT": operational_base["object_name"],
                "LOGICAL NAME OBJECT": operational_base["baseName"],
                "DESCRIPTION OBJECT": operational_base["observationField"],
                "INFORMATION GROUP LEVEL 1": operational_base["information_group_level_1"],
                "INFORMATION GROUP LEVEL 2": operational_base["information_group_level_2"],
                "PERIMETER": operational_base["perimeter"],
                "INFORMATION LEVEL": operational_base["information_level"],
                "DATA SOURCE (DS)": operational_base["data_source"],
                "TECHNICAL RESPONSIBLE": "datahub.co.group@bbva.com",
                "STORAGE TYPE": operational_base["db_type"],
                "STORAGE ZONE": operational_base["storage_zone"],
                "OBJECT TYPE": operational_base["object_type"],
                "DATA PHYSICAL PATH": operational_base["base_path"],
                "SYSTEM CODE/UUAA": operational_base["uuaaMaster"],
                "PARTITIONS": "",
                "FREQUENCY": self.excels_repository.find_one_periodicity(operational_base["periodicity"])["state"],
                "TIME REQUIREMENT": "",
                "LOADING TYPE": "",
                "CURRENT DEPTH": current_depth,
                "REQUIRED DEPTH": required_depth,
                "ESTIMATED VOLUME OF RECORDS": volume_records,
                "STORAGE TYPE OF SOURCE OBJECT": "",
                "PHYSICAL NAME OF SOURCE OBJECT": "",
                "MAILBOX SOURCE TABLE": "",
                "SOURCE PATH": source_path,
                "SCHEMA PATH": "",
                "SOURCE FILE TYPE": "",
                "SOURCE FILE DELIMITER": "",
                "TARGET FILE TYPE": "",
                "TARGET FILE DELIMITER": "",
                "VALIDATED BY DATA ARCHITECT": "NO",
                "TAGS": operational_base["alias"].lower(),
                "MARK OF TACTICAL OBJECT": tactical
            })
        except Exception as e:
            print(e)
            raise ValueError("Error generando los archivos. ")
        return res

    def _generate_excel_fields(self, operational_id):
        """

        :param operational_id:
        :return:
        """
        operational_base = self.operational_base_repository.find_one(operational_id)
        if operational_base is None:
            raise ValueError("La base operacional no se encuentra registrada.")

        res = []
        index = 1
        values = {0: "NO", 1: "YES"}
        if "fields" in operational_base:
            for i in operational_base["fields"]:
                naming = self.field_repository.find_naming_all_data(i["naming"]["naming"].lower())
                log_eng = ""
                desc_eng = ""
                log_spn = i["logic"]
                desc_spn = i["description"]
                domain = "Unassigned"
                subdomain = ""
                ownership = ""
                operational_ent = ""
                origin_desc = ""
                if naming is not None:
                    log_eng = naming["logicEnglish"]
                    desc_eng = naming["descEnglish"]
                    log_spn = naming["logic"]
                    desc_spn = naming["originalDesc"].replace("~", "\n")
                    if "dataOwnerInformation" in naming and len(naming["dataOwnerInformation"]) != 0:
                        domain = naming["dataOwnerInformation"][0]["domain"]
                        if len(domain.strip()) == 0:
                            domain = "Unassigned"
                        subdomain = naming["dataOwnerInformation"][0]["subdomain"]
                        ownership = naming["dataOwnerInformation"][0]["ownership"]
                        operational_ent = naming["dataOwnerInformation"][0]["operationalEntity"]

                catalogue = i["catalogue"]
                if i["catalogue"] == "empty":
                    catalogue = ""

                format_data = i["format"]
                if i["format"] == "empty":
                    format_data = ""
                if "origen_desc" in i.keys():
                    origin_desc = i["origen_desc"]
                
                try:
                    res.append({
                        "COUNTRY": "Colombia",
                        "PHYSICAL NAME OBJECT": operational_base["object_name"],
                        "STORAGE TYPE": operational_base["db_type"],
                        "STORAGE ZONE": operational_base["storage_zone"],
                        "PHYSICAL NAME FIELD": i["naming"]["naming"].strip().lower(),
                        "LOGICAL NAME FIELD": log_eng,
                        "LOGICAL NAME FIELD (SPA)": log_spn.strip(),
                        "SIMPLE FIELD DESCRIPTION": desc_eng,
                        "SIMPLE FIELD DESCRIPTION (SPA)": desc_spn,
                        "CATALOG": catalogue,
                        "DATA TYPE": i["dataType"],
                        "FORMAT": format_data,
                        "LOGICAL FORMAT": i["logicalFormat"].strip(),
                        "KEY": values[int(i["key"])],
                        "MANDATORY": values[int(i["mandatory"])],
                        "DEFAULT VALUE": "",
                        "PHYSICAL NAME OF SOURCE OBJECT": origin_desc,
                        "SOURCE FIELD": origin_desc,
                        "DATA TYPE OF SOURCE FIELD": "",
                        "FORMAT OF SOURCE FIELD": "",
                        "TAGS": operational_base["alias"].lower(),
                        "FIELD POSITION IN THE OBJECT": index,
                        "GENERATED FIELD": "",
                        "TOKENIZATION TYPE": "",
                        "COUNTRY OF THE DATA OWNER": "Colombia",
                        "OPERATIONAL ENTITY DOMAIN": domain,
                        "OPERATIONAL ENTITY SUBDOMAIN": subdomain,
                        "OPERATIONAL ENTITY OWNERSHIP": ownership,
                        "OPERATIONAL ENTITY SPA": operational_ent,
                        "TDS": "NO"
                    })

                except Exception as e:
                    print(e, operational_id, i["column"], "------------*fr")
                    raise ValueError("Error generando el campo {0}.".format(index))
                index += 1

        return res

    def update_structute(self, operational_id, data):
        """

        :param operational_id:
        :return:
        """
        print("base op structure weird")
        operational_base = self.operational_base_repository.find_one(operational_id)
        if operational_base is None:
            raise ValueError("La base operacional no se encuentra registrada.")

        user = self.user_repository.find_one(data["user_id"])
        if user is None:
            raise ValueError("Usuario inválido para esa operación.")

        temp_mod = [{"user": ObjectId(str(user["_id"])),
                     "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]

        for i in operational_base["modifications"]:
            temp_mod.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]})

        temp_fields = operational_base["fields"]
        data = data["fields"]

        for j in range(0, len(data)):
            namings = [i["naming"]["naming"] for i in temp_fields]
            if data[j]["naming"].strip().lower() in namings and j != namings.index(data[j]["naming"].strip().lower()):
                raise ValueError("Naming {0} repetido en la base operacional.".format(data[j]["naming"].strip().lower()))

            if len(data[j]["naming"].strip().lower()) != 0 and j < len(temp_fields):
                naming = self.field_repository.find_exactly_naming(data[j]["naming"].strip().lower())
                suffix = data[j]["naming"].strip().lower().split("_")[-1]
                state_naming = "PS"
                if naming is not None:
                    temp_fields[int(j)]["naming"]["naming"] = data[j]["naming"].strip()
                    temp_fields[int(j)]["naming"]["suffix"] = naming["naming"]["suffix"]
                    temp_fields[int(j)]["naming"]["Words"] = naming["naming"]["Words"]
                    temp_fields[int(j)]["naming"]["isGlobal"] = "Y"
                    temp_fields[int(j)]["naming"]["code"] = naming["code"]
                    temp_fields[int(j)]["naming"]["hierarchy"] = naming["level"]
                    temp_fields[int(j)]["logic"] = naming["logic"]
                    temp_fields[int(j)]["description"] = naming["originalDesc"].replace("~", "\n")
                    state_naming = "GL"
                    if "dataOwnerInformation" in naming and len(naming["dataOwnerInformation"]) != 0:
                        temp_fields[int(j)]["domain"] = naming["dataOwnerInformation"][0]["domain"]
                        if len(temp_fields[int(j)]["domain"]) == 0:
                            temp_fields[int(j)]["domain"] = "Unassigned"
                        temp_fields[int(j)]["subdomain"] = naming["dataOwnerInformation"][0]["subdomain"]
                        temp_fields[int(j)]["ownership"] = naming["dataOwnerInformation"][0]["ownership"]
                        temp_fields[int(j)]["conceptualEntity"] = naming["dataOwnerInformation"][0]["conceptualEntity"]
                        temp_fields[int(j)]["operationalEntity"] = naming["dataOwnerInformation"][0]["operationalEntity"]
                else:
                    suffix_id = self.suffix_repository.find_one(suffix)
                    if suffix_id is None:
                        raise ValueError("Suffix " + data[j]["naming"].strip().split("_")[-1] + "not found..." + data[j]["naming"])
                    temp_fields[int(j)]["naming"]["naming"] = data[j]["naming"].strip()
                    temp_fields[int(j)]["naming"]["suffix"] = suffix_id["_id"]
                    temp_fields[int(j)]["logic"] = data[j]["logic"].strip()
                    temp_fields[int(j)]["description"] = data[j]["description"].strip()

                temp_fields[int(j)]["key"] = int(data[j]["key"])
                temp_fields[int(j)]["mandatory"] = int(data[j]["mandatory"])
                temp_fields[int(j)]["length"] = int(data[j]["length"])
                temp_fields[int(j)]["catalogue"] = data[j]["catalogue"]
                temp_fields[int(j)]["origen_desc"] = data[j]["origin"]

                temp_mod_fi = temp_fields[int(j)]["modification"]
                temp_up_fi = [{"stateValidation": "NO", "startDate": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                               "state": state_naming, "user": ObjectId(str(user["_id"]))}]
                temp_fields[int(j)]["modification"] = temp_up_fi+temp_mod_fi[:4]

        self.operational_base_repository.update_all_fields(str(operational_id), temp_fields)
        self.operational_base_repository.update_modifications_audit(str(operational_id), temp_mod[:10])

    def upload_multiple_operational_bases(self, information):
        """

        :param information:
        :return:
        """
        bases = []
        user = self.user_repository.find_one(information["user"])
        if user is None:
            raise ValueError("Usuario inválido para esa operación.")

        project = self.project_repository.find_one(information["project_id"])
        if project is None:
            raise ValueError("El proyecto no se encuentra registrado.")

        for i in information["base"]:
            object_base = i["object"]
            fields_base = i["fields"]

            load_operational_base = self._structure_operational_base(object_base, fields_base, user["_id"], project["_id"])
            
            db_type = object_base[10].strip().upper()
            """if db_type not in self.db_type_options:
                raise ValueError("El tipo de carga inválido.")
                """

            object_name = object_base[1].strip()
            bases.append([load_operational_base,object_name,db_type])
        for j in bases:
            operational_base = self.operational_base_repository.find_name_base(j[1], j[2])
            if operational_base is None:
                self.operational_base_repository.insert_one(j[0])
            else:
                self.operational_base_repository.update_head(str(operational_base["_id"]), j[0])
                self.operational_base_repository.update_all_fields(str(operational_base["_id"]), j[0]["fields"])

    def upload_operational_base(self, information):
        """
        :param information:
        :return:
        """
        
        user = self.user_repository.find_one(information["user"])
        if user is None:
            raise ValueError("Usuario inválido para esa operación.")

        project = self.project_repository.find_one(information["project_id"])
        if project is None:
            raise ValueError("El proyecto no se encuentra registrado.")

        object_base = information["base"]["object"]
        fields_base = information["base"]["fields"]

        load_operational_base = self._structure_operational_base(object_base, fields_base, user["_id"], project["_id"]) #error
        
        db_type = object_base["Tipo almacenamiento"].strip().upper()
        """if db_type not in self.db_type_options:
            raise ValueError("El tipo de carga inválido.")
            """

        object_name = object_base["Nombre funcional objeto"].strip()
        operational_base = self.operational_base_repository.find_name_base(object_name, db_type)
        if operational_base is None:
            self.operational_base_repository.insert_one(load_operational_base)
        else:
            self.operational_base_repository.update_head(str(operational_base["_id"]), load_operational_base)
            self.operational_base_repository.update_all_fields(str(operational_base["_id"]), load_operational_base["fields"])

    def _structure_operational_base(self, object, fields, user, proyect):
        """

        :param object:
        :param fields:
        :return:
        """
        base = Operational_base({}, len(fields)).data
        info_uuaa = self.functional_map_repository.find_uuaa(object["Código de sistema/uuaa"][1:].strip().upper())
        if info_uuaa is None:
            raise ValueError("La uuaa es inválida.")
        
        base["uuaaMaster"] = object["Código de sistema/uuaa"].upper()
        base["data_source"] = info_uuaa["data_source"]
        base["information_group_level_1"] = info_uuaa["information_level_1"]
        base["information_group_level_2"] = info_uuaa["information_level_2"]
        base["object_name"] = object["Nombre físico objeto"].strip()
        base["db_type"] = object["Tipo almacenamiento"].strip().upper()
        base["baseName"] = object["Nombre lógico objeto"].strip().upper()
        base["observationField"] = object["Descripción"].strip()
        base["alias"] = ""
        base["country"] = object["País del data source"].strip().upper()
        base["technical_responsible"] = object["Responsable técnico"].strip()
        base["object_type"] = object["Tipo objeto"].strip().upper()
        base["partition"] = object["Particiones"].strip().upper()
        base["timing"] = ""
        base["upload_type"] = object["Tipo carga"].strip()
        base["storage_type_source"] = ""
        base["object_physical_name"] = ""
        base["origin_contact"] = ""
        base["origin_route"] = object["Ruta origen"].strip()
        base["scheme_route"] = ""
        base["input_file_type"] = object["Tipo de archivo de entrada"].strip()
        base["input_file_delimiter"] = object["Delimitador del archivo de entrada"].strip()
        base["output_file_type"] = object["Tipo de archivo de salida"].strip()
        base["output_file_delimiter"] = object["Delimitador del archivo de salida"].strip()
        base["governance_validation_status"] = object["Estado validación Gobierno Técnico"].strip().upper()
        base["governance_validation_comments"] = object["Comentarios validación Gobierno Técnico"].strip()
        base["po_validation_status"] = object["Estado validación PO"].strip().upper()
        base["po_validation_comments"] = object["Comentarios validación PO"].strip()
        base["architecture_validation_status"] = object["Estado validación arquitectura"].strip().upper()
        base["architecture_validation_comments"] = object["Comentarios validación arquitectura"].strip()
        
        period = object["Frecuencia"].strip().upper()

        if period == "DIARIA":     
            translatePeriodicity = "DAILY"
        elif period == "SEMANAL":     
            translatePeriodicity = "WEEKLY"  
        elif period == "QUINCENAL":     
            translatePeriodicity = "BIWEEKLY"    
        elif period == "MENSUAL":     
            translatePeriodicity = "MONTHLY"    
        elif period == "TRIMESTRAL":     
            translatePeriodicity = "QUARTERLY"    
        elif period == "SEMESTRAL":     
            translatePeriodicity = "SEMIANNUAL"    
        elif period == "ANUAL":     
            translatePeriodicity = "ANNUALLY"  
            
        periodicity = self.excels_repository.find_state_periodicity(translatePeriodicity)
        if periodicity is None:
            raise ValueError("La periodicidad " + object["Frecuencia"].strip().upper() + " es inválida. ")
        base["periodicity"] = periodicity["periodicity"]

        if object["Marca objeto táctico"].strip().upper() == "YES":
            base["tacticalObject"] = object["Marca objeto táctico"].strip().upper()

        if str(object["Profundidad actual"]).strip().split(" ")[0] != "":
            base["current_depth"] = int(str(object["Profundidad actual"]).strip().split(" ")[0])

        if str(object["Profundidad requerida"]).strip().split(" ")[0] != "":
            base["required_depth"] = int(str(object["Profundidad requerida"]).strip().split(" ")[0])

        if len(str(object["Volumetría estimada"]).strip()) != 0:
            base["estimated_volume_records"] = int(str(object["Volumetría estimada"]).strip())

        base["perimeter"] = object["Perímetro"].strip()
        base["information_level"] = object["Nivel información"].strip()
        base["storage_zone"] = object["Zona de almacenamiento"].strip()
        base["base_path"] = ""
        base["user"] = ObjectId(str(user))
        base["project_owner"] = ObjectId(str(proyect))
        
        for j in range(len(fields)):
            base["fields"][j]["dataType"] = fields[j]["Tipo de dato"].strip().upper()
            base["fields"][j]["format"] = fields[j]["Formato"].strip()
            base["fields"][j]["logicalFormat"] = fields[j]["Formato lógico"].strip()
            base["fields"][j]["origen_desc"] = ""
            base["fields"][j]["country"] = fields[j]["País"].strip()
            base["fields"][j]["functionalName"] = fields[j]["Nombre funcional campo"].strip()
            base["fields"][j]["functionalDesc"] = fields[j]["Descripción funcional campo" ].strip()
            base["fields"][j]["functionalDataType"] = fields[j]["Tipo de dato funcional"].strip()

            naming = self.field_repository.find_exactly_naming(fields[j]["Nombre físico campo"].strip().lower())
            if naming is not None:
                base["fields"][j]["naming"]["naming"] = fields[j]["Nombre físico campo"].strip()
                base["fields"][j]["naming"]["suffix"] = naming["naming"]["suffix"]
                base["fields"][j]["naming"]["Words"] = naming["naming"]["Words"]
                base["fields"][j]["naming"]["isGlobal"] = "Y"
                base["fields"][j]["naming"]["architectureState"] = ""
                base["fields"][j]["naming"]["governanceState"] = ""
                base["fields"][j]["naming"]["code"] = naming["code"]
                base["fields"][j]["naming"]["hierarchy"] = naming["level"]
                base["fields"][j]["logic"] = naming["logic"]
                base["fields"][j]["description"] = naming["originalDesc"]
                
                if "dataOwnerInformation" in naming and len(naming["dataOwnerInformation"]) != 0:
                    base["fields"][j]["domain"] = naming["dataOwnerInformation"][0]["domain"]
                    if len(base["fields"][j]["domain"]) == 0:
                        base["fields"][j]["domain"] = "Unassigned"
                    base["fields"][j]["subdomain"] = naming["dataOwnerInformation"][0]["subdomain"]
                    base["fields"][j]["ownership"] = naming["dataOwnerInformation"][0]["ownership"]
                    base["fields"][j]["conceptualEntity"] = naming["dataOwnerInformation"][0]["conceptualEntity"]
                    base["fields"][j]["operationalEntity"] = naming["dataOwnerInformation"][0]["operationalEntity"]
            else:
                print("!!!!!!!!!!!!!!!!!!!!")
                base["fields"][j]["naming"]["naming"] = fields[j]["Nombre físico campo"].strip()
                base["fields"][j]["logic"] = fields[j]["Nombre lógico campo"].strip().upper()
                base["fields"][j]["description"] = fields[j]["Descripción simple"].strip()
                """suffix_id = self.suffix_repository.find_one(fields[j][3].strip().strip().split("_")[-1])
                if suffix_id is None:
                    raise ValueError("Suffix " + fields[j][3].strip().split("_")[-1] + " inválido en naming " + fields[j][3].strip())
                """
            
            if isinstance(fields[j]["Longitud"], int):
                base["fields"][j]["length"] = fields[j]["Longitud"]

            if len(str(fields[j]["Catálogo"]).strip()) != 0:
                base["fields"][j]["catalogue"] = str(fields[j]["Catálogo"]).strip()

            if len(str(fields[j]["Formato"]).strip()) != 0:
                base["fields"][j]["format"] = str(fields[j]["Formato"]).strip()

            if str(fields[j]["Clave (Key)"]).strip().upper() == "YES":
                base["fields"][j]["key"] = 1
            
            if str(fields[j]["Obligatorio"]).strip().upper() == "YES":
                base["fields"][j]["mandatory"] = 1
            
            if len(str(fields[j]["Valor por defecto"]).strip()) != 0:
                base["fields"][j]["defaultValue"] = str(fields[j]["Valor por defecto"]).strip()
                
            if len(str(fields[j]["Etiquetas"]).strip()) != 0:
                base["fields"][j]["labels"] = str(fields[j]["Etiquetas"]).strip()
                
            if len(str(fields[j]["Tipo de tokenización"]).strip()) != 0:
                base["fields"][j]["tokenized"] = str(fields[j]["Tipo de tokenización"]).strip()
            
            # if len(str(fields[j][27]).strip()) != 0:
            #     base["fields"][j]["domain"] = ""
                
            # if len(str(fields[j][28]).strip()) != 0:
            #     base["fields"][j]["subdomain"] = ""
                
            # if len(str(fields[j][29]).strip()) != 0:
            #     base["fields"][j]["ownership"] = ""
            
            # if len(str(fields[j][30]).strip()) != 0:
            #     base["fields"][j]["conceptualEntity"] = ""
                
            # if len(str(fields[j][31]).strip()) != 0:
            #     base["fields"][j]["exploitationEntity"] = ""
                
            # if len(str(fields[j][32]).strip()) != 0:
            #     base["fields"][j]["tds"] = ""
                
            base["fields"][j]["originName"] = ""
            base["fields"][j]["originField"] = ""
            base["fields"][j]["originDataType"] = ""
            base["fields"][j]["originFormat"] = ""
            base["fields"][j]["objectPosition"] = str(fields[j]["Posición campo objeto"]).strip()
            base["fields"][j]["generatedField"] = ""
            base["fields"][j]["dataOwnerCountry"] = ""
            base["fields"][j]["governance_validation_status"] = str(fields[j]["Estado validación Gobierno Técnico"]).strip()
            base["fields"][j]["governance_validation_comments"] = str(fields[j]["Comentarios validación Gobierno Técnico"]).strip()
            base["fields"][j]["po_validation_status"] = str(fields[j]["Estado validación PO"]).strip()
            base["fields"][j]["po_validation_comments"] = str(fields[j]["Comentarios validación PO"]).strip()
            base["fields"][j]["architecture_validation_status"] = str(fields[j]["Estado validación arquitectura"]).strip()
            base["fields"][j]["architecture_validation_comments"] = str(fields[j]["Comentarios validación arquitectura"]).strip()
            
        return base

    def find_by_state(self, state, project_id):
        """

        :param state:
        :param project_id
        :return:
        """
        result = []
        for j in self.operational_base_repository.find_by_state(state.strip().upper(), project_id.strip()):
            j["created_time"] = j["created_time"].strftime("%Y-%m-%d")
            j["project_owner"] = self.project_repository.find_one(str(j["project_owner"]))["name"]
            j["user"] = self.user_repository.find_one(str(j["user"]))["email"]
            temp_str = {"user": "", "user_name": "N/A", "date": "N/A"}
            if len(j["modifications"]) != 0:
                temp_str = {
                    "user": str(j["modifications"][0]["user"]),
                    "user_name": self.user_repository.find_one(str(j["modifications"][0]["user"]))[
                        "name"],
                    "date": j["modifications"][0]["date"]}
            j["modifications"] = temp_str
        return result

    def update_validation_vobo(self, operational_id, data):
        """

        :param operational_id:
        :param data:
        :return:
        """
        try:
            temp = self.operational_base_repository.find_one(operational_id)["fields"]
            modi = temp[data["column"]]["modification"]
            modification = []
            modification_temp = {
                "user": ObjectId(str(data["user"])),
                "startDate": datetime.datetime.now(),
                "state": modi[0]["state"],
                "stateValidation": "YES"
            }
            modification.append(modification_temp)
            for j in range(0, 5):
                if len(modi) > j:
                    modification.append(modi[j])
            self.operational_base_repository.update_modifications(operational_id, modification, data["column"])
        except Exception as e:
            print(e, "NO se ingreso la modificación...")
        self.operational_base_repository.update_check_status(operational_id, data["user"], data["status"], data["column"], data["comments"])

    
    def get_load_settings(self):
        return self.settings_repository.find_load_op_settings()


    def update_load_settings(self, data):
        """
        return self.settings_repository.update_load_op_settings(data)
        """
        return self.settings_repository.update_one_load_op_setting(data["pos"], data["data"])
    
    def default_load_settings(self, data):
        return self.settings_repository.update_all_op_setting(data)


