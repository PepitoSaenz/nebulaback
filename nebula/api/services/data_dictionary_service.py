from nebula.api.persistence.data_dictionary_repository import *
from nebula.api.persistence.backlog_repository import *
from nebula.api.persistence.use_case_repository import *
from nebula.api.persistence.project_repository import *
from nebula.api.persistence.user_repository import *
from nebula.api.persistence.field_repository import *
from nebula.api.persistence.functional_map_repository import *
from nebula.api.persistence.team_repository import *

from nebula.api.persistence.functional_map_raw_repository import *
from nebula.api.persistence.functional_map_master_repository import *


from nebula.api.entities.naming import *

from nebula.api.entities.object_data_dictionary import Object_data_dictionary
from nebula.api.entities.field_review import Field_review
from nebula.api.entities.field_raw import Field_raw
from nebula.api.entities.field_master import Field_master

from nebula.api.services.field_service import *
from nebula.api.services.legacy_service import *
from nebula.api.entities.field_mirror import *
from nebula.api.persistence.excels_repository import *
from nebula.api.services.word_service import *

import datetime
import ast
import traceback


class Data_dictionary_service(object):

    """
        A class used to represent a Functional map services.

        Attributes
             -------
            backlog_repository : Backlog_repository
                Reference to Backlog repository class to interact with database
            user_repository: User_repository
                Reference to User repository class to interact with database
            project_repository: Project_repository
                Reference to Project repository class to interact with database
            use_case_repository: Use_case_repository
                Reference to Use case repository class to interact with database
            data_dictionary_repository: Data_dictionary_repository
                Reference to Data dictionary repository class to interact with database
            field_repository: Field_repository
                Reference to Field repository class to interact with database
            functional_map_repository: Functional_map_repository
                Reference to Functional map repository class to interact with database
            team_repository: Team_repository
                Reference to Team repository class to interact with database
            field_service: field_service
                Reference to Field repository class to interact with database tih they services
            legacy_service: Legacy_service
                Reference to Legacy repository class to interact with they services
            excels_repository: Excels_repository
                Reference to Excel repository class to interact with database
            fields_options: Array
                Field predefine in data dictionaries fields
            file: Dict
                Format of extend files
            state: Dict
                Correct user format to value of 0 and 1
    """

    def __init__(self):
        """

        """
        self.data_dictionary_repository = Data_dictionary_repository()
        self.backlog_repository = Backlog_repository()
        self.user_repository = User_repository()
        self.project_repository = Project_repository()
        self.use_case_repository = Use_case_repository()
        self.field_repository = Field_repository()
        self.functional_map_repository = Functional_map_repository()
        self.team_repository = Team_repository()
        self.field_service = Field_service()
        self.legacy_service = Legacy_service()
        self.excels_repository = Excels_repository()
        self.suffix_repository = Suffix_repository()
        self.word_service = Word_service()
        self.fields_options = ["FieldReview", "raw", "master"]
        self.file = {"fixed": ".txt", "csv": ".csv", "txt": ".txt", "PARQUET" : ".parquet"}
        self.state = {0: "NO", 1: "YES"}
        self.yes_no_1_0 = {"YES": 1, "NO": 0, "SI": 1, "": 0 , "TRUE": 1, "FALSE": 0}
        
        self.field_review = Field_review()
        
        self.sophia_props_object = {
            "COUNTRY OF THE DATA SOURCE": "",
            "PHYSICAL NAME OBJECT": "raw_name",
            "LOGICAL NAME OBJECT": "baseName",
            "DESCRIPTION OBJECT": "observationField",
            "INFORMATION GROUP LEVEL 1": "information_group_level_1",
            "INFORMATION GROUP LEVEL 2": "information_group_level_2",
            "PERIMETER": "perimeter",
            "INFORMATION LEVEL": "information_level",
            "DATA SOURCE (DS)": "data_source",
            "ENCRYPTION AT REST": "",
            "MODEL VERSION": "modelVersion",
            "DEPLOYMENT TYPE": "deploymentType",
            "SECURITY LEVEL": "securityLevel",
            "TECHNICAL RESPONSIBLE": "",
            "STORAGE TYPE": "",
            "STORAGE ZONE": "",
            "OBJECT TYPE": "objectType",
            "DATA PHYSICAL PATH": "raw_route",
            "SYSTEM CODE/UUAA": "uuaaMaster",
            "PARTITIONS": "partitions_raw",
            "FREQUENCY": "periodicity",
            "TIME REQUIREMENT": "",
            "LOADING TYPE": "loading_type",
            "CURRENT DEPTH": "current_depth",
            "REQUIRED DEPTH": "required_depth",
            "ESTIMATED VOLUME OF RECORDS": "estimated_volume_records",
            "STORAGE TYPE OF SOURCE OBJECT": "originSystem",
            "PHYSICAL NAME OF SOURCE OBJECT": "physical_name_source_object",
            "MAILBOX SOURCE TABLE": "",
            "SOURCE PATH": "raw_path",
            "SCHEMA PATH": "",
            "SOURCE FILE TYPE": "typeFile",
            "SOURCE FILE DELIMITER": "separator",
            "TARGET FILE TYPE": "",
            "TARGET FILE DELIMITER": "",
            "VALIDATED BY DATA ARCHITECT": "",
            "TAGS": "alias",
            "MARK OF TACTICAL OBJECT": "tacticalObject" }
        
        self.sophia_props_outnaming = {
            "ID": "",
            "COUNTRY": "",
            "PHYSICAL NAME OBJECT": "",
            "STORAGE TYPE": "",
            "STORAGE ZONE": "",
            "DEPLOYMENT TYPE": "",
            "PHYSICAL NAME FIELD": "naming",
            "LOGICAL NAME FIELD": "",
            "LOGICAL NAME FIELD (SPA)": "logic",
            "SIMPLE FIELD DESCRIPTION": "",
            "SIMPLE FIELD DESCRIPTION (SPA)": "description",
            "LEVEL": "",
            "COMPLEX STRUCTURE": "",
            "TECHNICAL COMMENTS": "",
            "CATALOG": "catalogue",
            "SECURITY CLASS": "",
            "SECURITY LABEL": "",
            "TOKENIZED AT DATA SOURCE": "tds",
            "TOKENIZED FIELD": "",
            "LOCALE": "",
            "DATA TYPE": "destinationType",
            "FORMAT": "format",
            "LOGICAL FORMAT": "outFormat",
            "KEY": "key",
            "MANDATORY": "mandatory",
            "DEFAULT VALUE": "default",
            "PHYSICAL NAME OF SOURCE OBJECT": "",
            "SOURCE FIELD": "legacy",
            "DATA TYPE OF SOURCE FIELD": "originType",
            "FORMAT OF SOURCE FIELD": "",
            "LENGTH OF SOURCE FIELD": "length",
            "TAGS": "",
            "FIELD POSITION IN THE OBJECT": "column",
            "GENERATED FIELD": "generatedField",
            "TOKENIZATION TYPE": "tokenization",
            "REGISTRATION DATE": ""
        }

        self.datum_props_object = {#missing: modelVersion, raw_path, typeFile, separator
            "OBJECT ID": "",#
            "PHYSICAL NAME OBJECT": "raw_name",
            "DESCRIPTION OBJECT": "observationField",
            "STORAGE TYPE": "originSystem", # Similar a "STORAGE TYPE OF SOURCE OBJECT" en SOFIA
            "STORAGE ZONE": "",
            "LOGICAL NAME OBJECT": "baseName",
            "INFORMATIONAL GROUP LEVEL 1": "", # 
            "OBJECT TYPE": "objectType",
            "PHYSICAL PATH": "raw_route", # Equivalente a "DATA PHYSICAL PATH" en SOFIA 
            "FREQUENCY": "periodicity",
            "CURRENT DEPTH": "current_depth",
            "SOURCE OBJECT STORAGE TYPE": "",
            "PHYSICAL NAME SOURCE OBJECT": "physical_name_source_object", # reference model # Equivalente a "PHYSICAL NAME OF SOURCE OBJECT" de SOFIA
            "MARK OF TACTICAL OBJECT": "tacticalObject",
            "INFORMATIONAL GROUP LEVEL 2": "information_group_level_1", # Equivalente a "INFORMATION GROUP LEVEL 1" en SOFIA
            "PERIMETER": "perimeter",
            "INFORMATION LEVEL": "information_level",
            "PARTITIONS": "partitions_raw",
            "TIME REQUIREMENT": "",
            "LOADING TYPE": "loading_type",
            "REQUIRED DEPTH": "required_depth",
            "ESTIMATED VOLUME OF RECORDS": "estimated_volume_records",
            "CONTACT ORIGIN": "",#
            "REGISTRATION DATE": "",#
            "DATA SYSTEM ID": "",#
            "NAME DATA SYSTEM": "data_source", # Equivalente a "DATA SOURCE (DS)" en SOFIA
            "SYSTEM CODE": "uuaaMaster", # Equivalente a "SYSTEM CODE/UUAA" en SOFIA
            "COUNTRY": "",
            "MAINTAINED BY ENGINEERING": "",#
            "GEOGRAPHICAL SCOPE DATA SYSTEM": "",#
            "DATABASE SCHEMA": "",#
            "SECURITY LEVEL": "securityLevel",
            "ENCRYPTED AT REST": "",#
            "TECHNICAL RESPONSIBLE": "",
            "UPDATE METHOD": "",#
            "TAGS": "alias",
            "SOURCE DATA MODEL CODE": "",#
            "VERSION": "",
            "ALIAS": "",#
            "MODEL NAME": "",#
            "MODEL CODE": "",#
            "DEPLOYMENT TYPE": "deploymentType",
            "INFORMATIONAL GROUP LEVEL 3": "information_group_level_2", # Equivalente a "INFORMATION GROUP LEVEL 2" en SOFIA
            "CALCULATED SECURITYLEVEL": "",#
            "FARM": "" #
        }

        self.datum_props_outnaming = {
            "FIELD ID": "",
            "PHYSICAL NAME FIELD": "naming",
            "DESCRIPTION FIELD": "description", # Equivalente a "SIMPLE FIELD DESCRIPTION (SPA)" en SOFIA
            "LOGICAL NAME FIELD": "logic",# Equivalente a "LOGICAL NAME FIELD (SPA)" en SOFIA
            "DATA TYPE": "destinationType",
            "KEY": "key",
            "CATALOG": "catalogue",
            "FORMAT": "format",
            "LOGICAL FORMAT": "outFormat",
            "MANDATORY": "mandatory",
            "DEFAULT VALUE": "default",# Vacio: nan
            "TAGS": "",
            "FIELD POSITION IN THE OBJECT": "column",
            "GENERATED FIELD": "generatedField",
            "TOKENIZATION TYPE": "tokenization",
            "PHYSICAL NAME OF SOURCE OBJECT": "",   #reference model
            "SOURCE FIELD": "legacy",   #reference model
            "DATA TYPE OF SOURCE FIELD": "originType", #reference model
            "FORMAT OF SOURCE FIELD": "",
            "REGISTRATION DATE": "",
            "OBJECT ID": "",
            "CONCEPT ID": "",
            "SECURITY CLASS": "",
            "SECURITY LABEL": "",
            "TOKENIZED AT DATASOURCE": "tds", # Equivalente a "TOKENIZED AT DATA SOURCE" en SOFIA
            "TOKENIZED": "",
            "LOCALE": "",
            "TECHNICAL COMMENTS": "",
            "SOURCE DATA MODEL CODE": "",
            "SOURCE FIELD LENGTH": "length", # Equivalente a "LENGTH OF SOURCE FIELD en SOFIA" # Vacio: nan
            "TRUSTED DATASOURCE": "",
            "VERSION": "",
            "PHYSICAL NAME OBJECT": "",
            "MODEL CODE": "",
            "DEPLOYMENT TYPE": "",
            "COUNTRY": "",
            "DATA SYSTEM ID": "",
            "NAME DATA SYSTEM": "",
            "SYSTEM CODE": "",
            "STORAGE TYPE": "",
            "STORAGE ZONE": "",
            "RELEVANTE": "",
            "OPTIONAL": "",
            "DELETED": "",
            "FARM": ""
        }

        self.functional_map_raw_repository = Functional_map_raw_repository()
        self.functional_map_master_repository = Functional_map_master_repository()

    def available_to_governance(self, user_id):
        """Check all tables that are available to governance in a project
        That was approved by a Super product owner
        Start with a table backlog data

        :param user_id: str, string that reference a user id
        :return: Array of Json, contains backlog data that can be start a governance
        """
        res = []
        user = self.user_repository.find_one(str(user_id))
        if user is None:
            raise ValueError("El usuario no se encuentra registrado.")

        if user["rol"] == "RN" or user["rol"] == "F" or user["rol"] == "PO":
            res = self.backlog_repository.available_governance()
        elif user["rol"] == "G" or user["rol"] == "SPO":
            res = self.backlog_repository.find_any_query({"$and": [{"tableRequest": "A"}, {"idTable": ""}]})
        else:
            res = []

        return res

    def _project_user(self, user_id):
        """Check what project does a user belong

        :param user_id: str, string that reference user id
        :return: Array, projects id's
        """
        teams = self.user_repository.find_one(user_id)["teams"]
        projects = []
        for i in teams:
            tmp_projects = self.project_repository.find_teams(i)
            for j in tmp_projects:
                projects.append(j)
        return projects

    def updateOldTables(self, table_id):
        """Updates old tables with the raw info so its easier to calculate, without the fieldsRaw

        :param data: json, dictionary with initial Data dictionary data
        :return: Json, with Data dictionary id
        """
        self._raw_field(table_id)
        self._raw_object(table_id, 0)
        return str(self.data_dictionary_repository.find_one(table_id))

    def create_table(self, data):
        """Create a new data dictionary

        :param data: json, dictionary with initial Data dictionary data
        :return: Json, with Data dictionary id
        """
        new_table = self._create_context_table(data)
        new_table["FieldReview"] = [ast.literal_eval(str(Field_review(column=i))) for i in range(int(data["numFields"]))]
        new_table["raw_name"] = new_table["raw_name"].lower()
        new_table["master_name"] = new_table["master_name"].lower()
        new_table["deploymentType"] = data["deploymentType"].strip()
        if new_table["deploymentType"] == 'Global-Implementation':
            new_table["alias"] = data["alias"].strip()
        new_id = self.data_dictionary_repository.insert_one(new_table)
        self.backlog_repository.joint_to_table(str(new_id), str(data["idBacklog"]))

        self._raw_field(new_id)
        self._raw_object(new_id, 0)
        return {"newTable": str(new_id)}

    def create_table_functional(self, information):
        """

        :param information:
        :return:
        """
        new_table = Object_data_dictionary({}).data
        new_table["_id"] = ObjectId(str(information["_id"]))
        new_table["baseName"] = information["baseName"]
        new_table["originSystem"] = information["originSystem"]
        new_table["periodicity"] = information["periodicity"]
        new_table["periodicity_master"] = information["periodicity"]
        new_table["observationField"] = information["observationField"]
        new_table["typeFile"] = information["typeFile"]
        new_table["uuaaRaw"] = information["uuaaRaw"]
        new_table["uuaaMaster"] = information["uuaaMaster"]
        new_table["tacticalObject"] = information["tacticalObject"]
        new_table["perimeter"] = information["perimeter"]
        new_table["stateTable"] = "F"
        tmp = self.functional_map_repository.find_uuaa(information["uuaaMaster"].upper()[1:])
        new_table["data_source"] = tmp["data_source"]
        new_table["information_group_level_1"] = tmp["level1"]
        new_table["information_group_level_2"] = tmp["level2"]
        new_table["current_depth"] = 0
        new_table["estimated_volume_records"] = 0
        new_table["user"] = ObjectId(information["user"])
        new_table["people"] = [{"user": ObjectId(information["user"]), "rol": self.user_repository.find_one(information["user"])["rol"]}]
        new_table["FieldReview"] = [ast.literal_eval(str(Field_review(column=i))) for i in range(int(information["numFields"]))]
        new_id = self.data_dictionary_repository.insert_one(new_table)
        self.backlog_repository.joint_to_table(str(new_id), str(information["user"]))
        return {"newTable": str(new_id)}

    def _create_context_table(self, data):
        """Create data auto-generate that need a Data dictionary table

        :param data: json, dictionary with data that need to autogenerate new data
        :return: json, dictionary with new data generate

        """
        new_table = self.backlog_repository.find_one(data["idBacklog"])
        new_table["raw_name"] = data["raw_name"]
        new_table["master_name"] = data["master_name"]
        new_table["objectType"] = ""
        new_table["user"] = ObjectId(data["user"])
        new_table["separator"] = data["separator"]
        new_table["stateTable"] = "N"
        new_table["alias"] = data["alias"]
        new_table["required_depth"] = data["history"]
        new_table["periodicity"] = data["periodicity"]
        new_table["perimeter"] = data["perimeter"]
        new_table["originSystem"] = data["originSystem"]
        new_table["information_level"] = " "
        new_table["loading_type"] = " "
        new_table["tacticalObject"] = data["tacticalObject"]
        new_table["typeFile"] = data["typeFile"].capitalize()
        new_table["deploymentType"] = " "
        new_table["modelVersion"] = ""
        new_table["objectVersion"] = ""
        new_table["partitions_raw"] = ""
        new_table["partitions_master"] = ""
        new_table["current_depth"] = 0
        new_table["estimated_volume_records"] = 0
        new_table["observationField"] = data["observationField"]
        new_table["people"] = [{"user": ObjectId(data["user"]), "rol": self.user_repository.find_one(data["user"])["rol"]}]

        new_table = self.update_datasourceinfo_head(new_table)
        new_table = self._delete_atributtes(new_table)
        new_table["periodicity_master"] = new_table["periodicity"]
        return new_table

    def _delete_atributtes(self,table):
        """Delete attributes that transfer from backlog data

        :param table: json, dictionary with backlog data
        :return: Json, dictionary with backlog data
        """
        del table["productOwner"]
        del table["idTable"]
        del table["tableRequest"]
        del table["history"]
        return table

    def _name_file(self, backlog, description):
        """Create a raw and master name by standard

        :param backlog: json, dictionary with backlog data
        :param description: str, String that reference and abbreviation name asignate by a user
        :return: Str, str, that reference two new names
        """
        raw_name = "t_"+backlog["uuaaRaw"].lower()+"_"+description
        master_name = "t_"+backlog["uuaaMaster"].lower()+"_"+description
        return raw_name, master_name
    

    def add_modification(self, old_mod, new_mod):
        """Updates the modification array for a data dictionary with the given data for
        auditory reasons. Also 'cleans' the variable by removing any other variable thats not an array

        :param old_mod: json array, current/old modifications array to be worked on
        :param new_mod: json, new modification data
        :return: array, with the updates values for direct replacement with the original
        """
        temp_mod = { "user": new_mod["user"],
                     "type": new_mod["type"],
                     "details": new_mod["details"],
                     "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}

        if not "date" in old_mod:
            if len(old_mod) == 10:
                old_mod.pop(0)
        else:
            old_mod = []
    
        old_mod.append(temp_mod)
        return old_mod
    
    def update_backlog_head(self, table_id, data):
        """Updates the associated Backlog of a DD.
        
        :param table_id: str, string that reference the table id
        :param data: json, dictionary with data to update
        :return:
        """
        backlog = self.backlog_repository.find_by_id_table(table_id)
        if backlog is not None:
            backlog["uuaaRaw"] = data["uuaaRaw"].upper()
            backlog["uuaaMaster"] = data["uuaaMaster"].upper()
            backlog["periodicity"] = data["periodicity"]
            backlog["perimeter"] = data["perimeter"]
            self.backlog_repository.update_one(backlog["_id"], backlog)

    def update_routesandpaths_head(self, data):
        """
            Updates the routes, paths of a DataDictionary depending of certains factors like if they're empty
            or if they have partitions.
        :param table_id: str, string that reference a table id
        :param data: json, dictionary with data to update
        :return: json, same input data with the added info
        """
        data_dic = self.data_dictionary_repository.find_one(data["_id"])
        if "fixed" or "txt" in data["typeFile"].lower():
            type_file = ".txt"
            data["typeFile"] = "Fixed"
        if "parquet" in data["typeFile"].lower():
            type_file = ".parquet"
            data["typeFile"] = "PARQUET"
        
        try:
            if data["originSystem"] == "Datio Holding":
                type_file = ".parquet"
                data["typeFile"] = "PARQUET"
        except Exception:
            pass
        
        else:
            type_file = ".csv"
            data["typeFile"] = "CSV"

        try:
            tmp_split = self.excels_repository.find_one_system(data["originSystem"])["staging"] if "originSystem" in data else ""
        except Exception:
            tmp_split = ""

        if "fieldsRaw" in data_dic and len(data_dic["fieldsRaw"]) == 0:
            data["raw_path"] = data["raw_route"] = ""
            if data["originSystem"] == "Datio Holding": #Condicion especial para Holding
                if len(data["physical_name_source_object"].strip()) == 0:
                    data["physical_name_source_object"] = data["master_name"]
            else:
                data["physical_name_source_object"] = ""
        else:
            try:
                if len(data["raw_path"].strip()) == 0:
                    data["raw_path"] = tmp_split + data["uuaaRaw"].lower() + "/" + "C" + data["uuaaMaster"].upper() + "_D02_${?YEAR}${?MONTH}${?DAY}_" + data["raw_name"][7:] + type_file
            except:
                data["raw_path"] = tmp_split + data["uuaaRaw"].lower() + "/" + "C" + data["uuaaMaster"].upper() + "_D02_${?YEAR}${?MONTH}${?DAY}_" + data["raw_name"][7:] + type_file
            try:
                if len(data["raw_route"].strip()) == 0:
                    data["raw_route"] = "/data/raw/" + data["uuaaRaw"].lower() + "/data/" + data["raw_name"]
            except:
                data["raw_route"] = "/data/raw/" + data["uuaaRaw"].lower() + "/data/" + data["raw_name"]
            try:
                if len(data["physical_name_source_object"].strip()) == 0:
                    data["physical_name_source_object"] = "C"+ data["uuaaMaster"].upper() + "_D02_aaaammdd_" + data["raw_name"][7:].lower() + type_file
            except:
                data["physical_name_source_object"] = "C"+ data["uuaaMaster"].upper() + "_D02_aaaammdd_" + data["raw_name"][7:].lower() + type_file

        if "master_name" in data and len(data["master_name"].strip()) == 0:
            data["master_path"] = ""
            data["master_route"] = ""
            data["partitions_master"] = ""
        else:
            if len(data_dic["fieldsRaw"]) == 0:
                try:
                    if len(data["master_path"].strip()) == 0:
                        data["master_path"] = tmp_split + data["uuaaMaster"].lower() + "/" + "C" + data["uuaaMaster"].upper() + "_D02_${?YEAR}${?MONTH}${?DAY}_" + data["raw_name"][7:] + type_file
                        if data["originSystem"] == "Datio Holding": #Condicion especial para Holding
                            data["master_path"] = "/in/staging/datax/"+ data["uuaaMaster"].lower() + "/x_"+ data["master_name"] +"/*.parquet"
                except:
                    data["master_path"] = tmp_split + data["uuaaMaster"].lower() + "/" + "C" + data["uuaaMaster"].upper() + "_D02_${?YEAR}${?MONTH}${?DAY}_" + data["raw_name"][7:] + type_file
            else:
                try:
                    if len(data["master_path"].strip()) == 0:
                        data["master_path"] = data["raw_route"]
                except:
                    data["master_path"] = data["raw_route"]
                try:
                    if len(data["master_route"].strip()) == 0:
                        data["master_route"] = "/data/master/" + data["uuaaMaster"].lower() + "/data/" + data["master_name"]
                except:
                    data["master_route"] = "/data/master/" + data["uuaaMaster"].lower() + "/data/" + data["master_name"]

        return data

    def update_datasourceinfo_head(self, data):
        """Updates the Data Source and Information Levels 1 & 2 of a DD head.
        Created for future development.

        :param data: json, dictionary with data to update
        :return: json, same input data with the added info
        """
        tmp = self.functional_map_repository.find_uuaa(data["uuaaMaster"].upper()[1:])
        data["data_source"] = "" if tmp is None else tmp["data_source"] if "data_source" in tmp else tmp["system_name"] if "system_name" in tmp else ""
        data["information_group_level_1"] = "" if tmp is None else tmp["information_level_1"] if "information_level_1" in tmp else ""
        data["information_group_level_2"] = "" if tmp is None else tmp["information_level_2"] if  "information_level_2" in tmp else ""

        return data

    def update_head(self, table_id, data):
        """Update data dictionary data without generate new names
        except phases

        :param table_id: str, string that reference a table id
        :param data: json, dictionary with data to update
        :return:
        """
        data_dic = self.data_dictionary_repository.find_one(table_id)
        data["_id"] = table_id
        if data_dic is None:
            raise ValueError("La tabla no se encuentra registrada.")

        user_id = self.user_repository.find_one(data["user_id"])
        if user_id is None:
            raise ValueError("Permisos denegados para el usuario.")

        try:
            desc_table = "_".join(data["raw_name"].split("_")[3:]).lower()
        except:
            desc_table = "_".join(data["raw_name"].split("_")[2:]).lower()

        if len(data["raw_name"].split("_")) >= 4:
            if data["alias"] != data_dic["alias"]:
                data["raw_name"] = "t_" + data["uuaaRaw"].lower() + "_" + data["alias"] + "_" + desc_table
            else:
                if data["deploymentType"] != 'Global-Implementation':
                    data["alias"] = data["raw_name"].split("_")[2].lower()
                else:
                    data["alias"] = data["alias"].strip()

        if not self.data_dictionary_repository.find_alias_repeat(data["alias"], table_id):
            data["uuaaRaw"] = data["uuaaRaw"].upper()
            data["uuaaMaster"] = data["uuaaMaster"].upper()

            if "fieldsMaster" not in data_dic or len(data_dic["fieldsMaster"]) == 0 or len(data["master_name"].strip()) == 0:
                data["fieldsMaster"] = []
                data["master_name"] = ""
            else:
                desc_table = "_".join(data["raw_name"].split("_")[2:]).lower()
                data["master_name"] = "t_" + data["uuaaMaster"].lower() + "_" + desc_table

            data = self.update_routesandpaths_head(data)
            data = self.update_datasourceinfo_head(data)
            data["modifications"] = self.add_modification(data["modifications"], {"user": data["user_id"], "type": "UPDATE HEAD", "details": ""})

            del data["fase"]
            if "_id" in data:
                del data["_id"]
            if "user_id" in data:
                del data["user_id"]
            if "created_time" in data:
                del data["created_time"]

            #finally update stuff
            self.update_backlog_head(table_id, data)
            self.data_dictionary_repository.update_field(table_id, data)
        else:
            raise ValueError("Una tabla con este alias ya existe.")

    def update_fase_field(self, fase, table_id, user_id, data):
        """Update a data dictionary phase (FieldReview, fieldRaw or fieldMaster)

        :param fase: str, String that reference a fase to update
        :param table_id: str, String that reference a table id
        :param user_id: str, String that reference a user id
        :param data: json, dictionary with data to update
        :return:
        """
        data_dic = self.data_dictionary_repository.find_one(table_id)
        if data_dic is None:
            raise ValueError("La tabla no se encuentra registrada.")

        user_repo = self.user_repository.find_one(user_id)
        if user_repo is None:
            raise ValueError("El usuario no es válido.")

        flag = True
        for k in data_dic["people"]:
            if user_id == str(k["user"]):
                flag = False
        if flag:
            data_dic["people"].append({"user": ObjectId(str(user_repo["_id"])), "rol": user_repo["rol"]})

        if data_dic["stateTable"] == "N" or data_dic["stateTable"] == "G":
            if "notSave" in data:
                del data["notSave"]

                naming_state = self._naming_state(table_id, data["row"]["naming"]["naming"], data["row"]["logic"], data["row"]["description"])
                if len(data["row"]["modification"]) > 0:
                    data["row"]["modification"][0]["state"] = naming_state

                self._save_field(table_id, data["row"], fase)
            elif "all" not in data:
                self._update_field(table_id, user_id, data["row"], fase)
            else:
                self._update_all_fields(table_id, user_id, data["row"], fase)
            self.data_dictionary_repository.update_people(table_id, data_dic["people"])
        else:
            raise ValueError("Propuesta de diccionario cerrada, no es posible su edición.")

    def _update_governance_formats(self,table_id,field):
        """

        :param field:
        :return:
        """
        table = self.data_dictionary_repository.find_one(table_id)
        if "fieldsRaw" in table:
            column = "FieldReview."+str(field["column"])
            find_raw = self.data_dictionary_repository.find_raw_field_origin(table_id, column)
            if find_raw.count() > 0:
                tmpDes = field["destinationType"]
                if ("DECIMAL" in field["destinationType"]) or ("DECIMAL".lower() in field["destinationType"].lower()):
                    if ("DECIMAL(p)" in field["destinationType"]):
                        tmpDes = "DECIMAL("+ str(int(field["integers"])) + ")"
                    else:
                        tmpDes = "DECIMAL(" + str(int(field["decimals"]) + int(field["integers"])) + "," + str(field["decimals"]) + ")"
                destTmp = tmpDes
                raw_format = field["outFormat"]
                if "outVarchar" in field:
                    raw_format = field["outVarchar"]
                self.data_dictionary_repository.update_raw_phase_field(table_id, destTmp, raw_format, field["format"], str(find_raw[0]["fieldsRaw"][0]["column"]))
                if "fieldsMaster" in table:
                    column = "fieldsRaw." + str(find_raw[0]["fieldsRaw"][0]["column"])
                    find_master = self.data_dictionary_repository.find_master_field_origin(table_id, column)
                    if find_master.count() > 0:
                        if ("TIMESTAMP" in field["destinationType"]) or ("TIMESTAMP".lower() in field["destinationType"].lower()):
                            destTmp = "TIMESTAMP"
                        self.data_dictionary_repository.update_master_phase_field(table_id, destTmp, str(field["governanceFormat"]),field["format"],str(find_master[0]["fieldsMaster"][0]["column"]))

    def _update_origin_formats(self, table_id, field):
        """

        :param table_id:
        :param field:
        :return:
        """
        table = self.data_dictionary_repository.find_one(table_id)
        if "fieldsMaster" in table:
            column = "fieldsRaw." + str(field["column"])
            find_master = self.data_dictionary_repository.find_master_field_origin(table_id, column)
            if find_master.count() > 0:
                format = field["format"]
                outFormat = field["logicalFormat"]
                destType = field["outFormat"]
                if "DATE".lower() in field["outFormat"].lower():
                    format = "yyyy-MM-dd"
                    outFormat = "DATE"
                    destType = "DATE"
                elif "TIMESTAMP".lower() in field["outFormat"].lower():
                    format = "yyyy-MM-dd HH:mm:ss.SSSSSS"
                    destType = "TIMESTAMP"
                    outFormat = "TIMESTAMP"
                elif "DECIMAL".lower() in field["outFormat"].lower():
                    format = field["outFormat"][7:]
                    destType = field["outFormat"]
                    outFormat = field["outFormat"]
                elif "INT".lower() in field["outFormat"].lower():
                    destType = "INT"
                    format = "empty"
                    if len(field["outFormat"]) > 0 and int(field["logicalFormat"][13:].split(")")[0]) > 4:
                        outFormat = "NUMERIC LARGE"
                    else:
                        outFormat = "NUMERIC SHORT"
                elif "STRING".lower() in field["outFormat"].lower():
                    format = "empty"
                    outFormat = field["logicalFormat"]
                    destType = "STRING"
                if field["outFormat"] == "empty":
                    outFormat = field["logicalFormat"]
                    destType = field["destinationType"]
                self.data_dictionary_repository.update_master_phase_field(table_id, destType, outFormat, format, str(find_master[0]["fieldsMaster"][0]["column"]))

    def _update_field(self, table_id, user, data, field):
        """Update a field in a data dictionary phase
        If a field has a son or father transfer data, because use a mirror

        :param table_id: str, String that reference a table id
        :param user: str, string that reference a user id
        :param data: json, dictionary with field data to update
        :param field: str, string that reference what phase field update
        :return:
        """
        if field == "fieldsRaw" and data["check"]["user"] != "":
            data["check"]["user"] = ObjectId(data["check"]["user"])

        naming_state = self._naming_state(table_id, data["naming"]["naming"], data["logic"], data["description"])

        if len(data["modification"]) > 0:
            data["modification"][0]["state"] = naming_state
        if naming_state == "PC":
            self._change_common_field(data["naming"]["naming"], naming_state)

        data = self._generate_data_owner_data(data, naming_state)
        self._save_field(table_id, data, field)

    def _check_integers_decimals(self, data):
        res = True
        if "destinationType" in data:
            if data["destinationType"].strip().upper() == "DECIMAL":
                if "integers" in data and "decimals" in data:
                    if data["integers"] is not None and data["decimals"] is not None:
                        integers = int(data["integers"])
                        decimals = int(data["decimals"])
                        length = int(data["length"])
                        total = ((integers + decimals) - length)
                        if (data["signo"]==1):
                            total+=1
                        if total != 0 and total != 1:
                            res = False
        elif "type" in data:
            if data["type"] is not None:
                if data["type"].strip().upper() == "DECIMAL":
                    if data["enteros"] is not None and data["decimales"] is not None and data["length"] is not None:
                        integers = int(data["enteros"])
                        decimals = int(data["decimales"])
                        length = int(data["length"])
                        total = ((integers + decimals) - length)
                        if (data["signo"]==1):
                            total+=1
                        if total != 0 and total != 1:
                            res = False
                if data["type"].strip().upper() == "DECIMALP":
                    if data["enteros"] is not None and data["decimales"] is not None and data["length"] is not None:
                        integers = int(data["enteros"])
                        decimals = int(data["decimales"])
                        length = int(data["length"])*2
                        if data["signo"] == 1:
                            if ((integers + decimals) - length + 1) != 0 and ((integers + decimals) - length + 1) != 1:
                                res = False
                        else:
                            if ((integers + decimals) - length) != 0 and ((integers + decimals) - length) != 1:
                                res = False
        return res

    def _generate_data_owner_data(self, data, naming_state):
        """Cross a naming with a excels define with data owner data
        If dont match with anything, domain is unassigned

        :param data: json, dictionary with data to check and add
        :param naming_state: str, string that reference a naming state
        :return: json, dictionary with new cross data
        """
        check = True
        if naming_state == "GL" or naming_state == "GC":
            owner_data = self.field_repository.find_naming_data_owner_information(data["naming"]["naming"])
            try:
                if "dataOwnerInformation" in owner_data:
                    data["domain"] = owner_data["dataOwnerInformation"][0]["domain"]
                    if len(data["domain"].strip()) == 0:
                        data["domain"] = "Unassigned"
                    data["subdomain"] = owner_data["dataOwnerInformation"][0]["subdomain"]
                    data["ownership"] = owner_data["dataOwnerInformation"][0]["ownership"]
                    data["conceptualEntity"] = owner_data["dataOwnerInformation"][0]["conceptualEntity"]
                    data["operationalEntity"] = owner_data["dataOwnerInformation"][0]["operationalEntity"]
                    check = False
            except:
                data["domain"] = "Unassigned"
                data["subdomain"] = ""
                data["ownership"] = ""
                data["conceptualEntity"] = ""
                data["operationalEntity"] = ""
        if check:
            data["domain"] = "Unassigned"
            data["subdomain"] = ""
            data["ownership"] = ""
            data["conceptualEntity"] = ""
            data["operationalEntity"] = ""
        return data

    def _update_all_fields(self,table_id,user_id,data,field):
        """Update all fields that receive

        :param table_id: str, string that reference a table id
        :param user_id: str, string that reference a user id
        :param data: Array of json, all data fields to update
        :param field: str, string that reference what field update
        :return:
        """
        for i in data:
            self._update_field(table_id,user_id,i,field)

    def _save_field(self, table_id, data, field):
        """Save a field in a correct format
        Put objectId's and IsoDate format to be save

        :param table_id: str, string that reference a table id
        :param data: json, dictionary with data to be save
        :param field: str, String that reference a fase to save
        :return:
        """
        for i in data["modification"]:
            try:
                i["user"] = ObjectId(i["user"])
                i["startDate"] = datetime.datetime.strptime(i["startDate"],"%Y-%m-%d")
            except:
                continue
        query1 = self.data_dictionary_repository.find_legacys(table_id, "FieldReview", data["legacy"]["legacy"])
        query2 = self.data_dictionary_repository.find_legacys(table_id, "fieldsRaw",  data["legacy"]["legacy"])
        query3 = self.data_dictionary_repository.find_legacys(table_id, "fieldsMaster",  data["legacy"]["legacy"])
        if query1.count() != 0 or query2.count() != 0 or query3.count() != 0:
            data["legacy"]["repeat"] = "Y"

        #Build a graph with field son a parents to transfer data
        graph().build_graph(table_id, data, field)
        if field == "FieldReview":
            self._update_governance_formats(table_id, data)
        if field == "fieldsRaw" and data["origin"] != "":
            self._update_origin_formats(table_id, data)

    def _change_common_field(self,naming,new_state):
        """Change naming state with a proposed naming is in other table

        :param naming: str, String that reference a naming
        :param new_state: str, string with naming to check
        :return:
        """
        tables = self.data_dictionary_repository.same_naming(naming)
        for i in tables:
            for j in i["FieldReview"]:
                try:
                    if j["naming"]["naming"] == naming and j["modification"][0]["state"] == "PS":
                        j["modification"][0]["state"] = new_state
                        query = {"FieldReview."+str(j["column"]):j}
                        self.data_dictionary_repository.update_field(str(i["_id"]),query)
                        break
                except:
                    continue

    def _naming_state(self, table_id, naming, logical_name, description):
        """Check what state has to be assigned to one naming
                GL: Is global, belong to global naming repository
                GC: Global and common, belong to global naming repository and is in other table
                PS: Proposed and simple, only is proposed in one data dictionary
                PC: proposed and common, isn't a global naming repository but is proposed in other table

                :param table_id: str, String that reference a table id
                :param naming: str, String that reference a naming
                :return: str, state asigned to a naming
                """
        global_naming = self.field_repository.find_exactly_naming(naming)
        local_naming = self.data_dictionary_repository.find_naming(table_id, naming).count()
        if global_naming is not None and local_naming == 0:
            return "GL"
        elif global_naming is not None and local_naming > 0:
            return "GC"
        elif global_naming is None and local_naming == 0:
            return "PS"
        else:
            return "PC"

    def find_tables(self, state_id, state):
        """
            Finds all data dictionaries with in specific state

        :param state: str, string that reference a state where going to find
        :return: Array of json, with data get it
        """
        return self._find_state_tables(state_id, state)

    def _find_state_tables(self, project_id, state):
        """
            Finds tables in a specific state

        :param project_id: str, String that reference a project id
        :param state: str, String that reference a state to get it
        :return: Array of json, contains id's and name of data dictionaries tables
        """
        tables = self.data_dictionary_repository.find_tables_state(state)
        project = self.project_repository.find_project_use_cases(project_id)
        res = []
        ids = []
        for i in project["useCases"]:
            use_cases = self.use_case_repository.find_one(i)
            for j in use_cases["tables"]:
                for k in tables:
                    del k["modifications"]
                    if str(k["_id"]) == j and j not in ids:
                        res.append(k)
                        ids.append(j)
        return res

    def find_blocker_field(self, table_id, field):
        """Get all blocker fields in a phase field at data dictionary specified

        :param table_id: str, string that reference a table id
        :param field: str, string that reference a phase to check
        :return: Array of Json, columns that are blocker
        """
        table = self.data_dictionary_repository.find_one(table_id)
        columns = []
        if len(table[field])>0:
            for i in table[field]:
                if i["blocker"] != [] and i["blocker"][0]["endDate"] == "":
                    columns.append(i["column"])
        return columns

    def action_field(self, table_id, data):
        """Do a specific action in a field

        :param table_id: str, string that reference a table id
        :param data: json, dictionary to data to check (action to do, field and column)
        :return:
        """
        user_id = self.user_repository.find_one(data["user_id"])
        if user_id is None:
            raise ValueError("Permisos denegados para el usuario.")

        data_dic = self.data_dictionary_repository.find_one(table_id)
        if data_dic is None:
            raise ValueError("La tabla no se encuentra registrada.")

        if data["action"] == "move": self._move_field(table_id, data)
        elif data["action"] == "blocker": self._blocker_field(table_id, data)
        elif data["action"] == "unBlocker": self._un_blocker_field(table_id, data)
        elif data["action"] == "insert": self._insert_field(table_id, data)
        elif data["action"] == "substract": self._substract_field(table_id, data)

        data_dic["modifications"] = self.add_modification(data_dic["modifications"], {"user": data["user_id"], "type": "FIELD ACTION - " + data["action"], "details": "Column: " + str(data["column"])})
        self.data_dictionary_repository.update_modifications_audit(table_id, data_dic["modifications"])

    
    def _move_field(self, table_id, data):
        """
            Moves a naming to another position, either up or down of it one space.

        :param table_id: str, String that reference a table id
        :param data: json, dictionary with a data to check including the naming row that has to be moved
        :return:
        """
        table = self.data_dictionary_repository.find_one(table_id)
        arrayFields = ["FieldReview", "fieldsRaw", "fieldsMaster"]

        try:
            if data["move_dir"] == "up":
                if data["column"] > 0:
                    for array in arrayFields:
                        try:
                            tmp_field = table[array][data["column"]]
                            tmp_field["column"] = data["column"] - 1

                            table[array][data["column"] - 1]["column"] = data["column"]
                            table[array][data["column"]] = table[array][data["column"] - 1]
                            if "origin" in tmp_field:
                                tmp_field["origin"] = ("FieldReview" if array == "fieldsRaw" else "fieldsRaw") + "." + str(tmp_field["column"])
                                table[array][data["column"]]["origin"] = ("FieldReview" if array == "fieldsRaw" else "fieldsRaw") + "." + str(data["column"])

                            table[array][data["column"] - 1] = tmp_field
                            self._update_field_action(table_id, {array: table[array]})
                        except Exception as e:
                            print(e)
                if data["column"] <= 0:
                    raise ValueError("Posicion de movimiento invalida.")
            elif data["move_dir"] == "down":
                    for array in arrayFields:
                        try:
                            tmp_field = table[array][data["column"]]
                            tmp_field["column"] = data["column"] + 1

                            table[array][data["column"] + 1]["column"] = data["column"]
                            table[array][data["column"]] = table[array][data["column"] + 1]
                            if "origin" in tmp_field:
                                tmp_field["origin"] = ("FieldReview" if array == "fieldsRaw" else "fieldsRaw") + "." + str(tmp_field["column"])
                                table[array][data["column"]]["origin"] = ("FieldReview" if array == "fieldsRaw" else "fieldsRaw") + "." + str(data["column"])

                            table[array][data["column"] + 1] = tmp_field
                            self._update_field_action(table_id, {array: table[array]})
                        except Exception as e:
                            print(e)
        except Exception as e:
            print(e)

    def _blocker_field(self, table_id, data):
        """Put a field in blocker state for some reason

        :param table_id: str, String that reference a table id
        :param data: json, dictionary with a data to check
        :return:
        """
        table = self.data_dictionary_repository.find_one(table_id)
        for i in table[data["field"]]:
            if data["column"] == i["column"]:
                i["blocker"].insert(0,{"startDate":datetime.datetime.now(),"endDate":""})
                graph().build_graph(table_id, i, data["field"])
                break
        #self._update_field_action(table_id,{data["field"]:table[data["field"]]})

    def _un_blocker_field(self,id_table,data):
        """Put a field in un-blocker state

        :param id_table: str, string that reference a table id
        :param data: json, dictionary with a data to check
        :return:
        """
        table = self.data_dictionary_repository.find_one(id_table)
        for i in table[data["field"]]:
            if data["column"] == i["column"]:
                i["blocker"][0]["endDate"] = datetime.datetime.now()
                graph().build_graph(id_table, i, data["field"])
                break
        #self._update_field_action(id_table,{data["field"]:table[data["field"]]})

    def _insert_field(self, table_id, data):
        """Insert a field in a specific position and data dictionary fase (FieldReview, fieldRaw or fieldMaster)

        :param table_id: str, String that reference a table id
        :param data: json, dictionary with a data to check
        :return:
        """
        table = self.data_dictionary_repository.find_one(table_id)
        res = []
        for i in table[data["field"]]:
            if i["column"] == data["column"]:
                #Check what type of field has to insert
                if data["field"]=="FieldReview":
                    res.append(ast.literal_eval(str(Field_review(column=data["column"]))))
                elif data["field"]=="fieldsRaw":
                    res.append(ast.literal_eval(str(Field_raw(column=data["column"]))))
                elif data["field"]=="fieldsMaster":
                    res.append(ast.literal_eval(str(Field_master(column=data["column"]))))
                i["column"] += 1
            elif i["column"] > data["column"]:
                i["column"] += 1
            res.append(i)
        #if It is the last field
        if data["column"] == len(table[data["field"]]):
            if data["field"] == "FieldReview":
                res.append(ast.literal_eval(str(Field_review(column=data["column"]))))
            elif data["field"] == "fieldsRaw":
                res.append(ast.literal_eval(str(Field_raw(column=data["column"]))))
            elif data["field"] == "fieldsMaster":
                res.append(ast.literal_eval(str(Field_master(column=data["column"]))))
        table[data["field"]] = res
        self._update_field_action(table_id, {data["field"]: table[data["field"]]})
        
        if data["field"] == "FieldReview":
            if "fieldsRaw" in table:
                find_raw = self.data_dictionary_repository.find_raw_field_origin(table_id, "FieldReview." + str(data["column"]))
                if find_raw.count() > 0:
                    column_raw_tmp = find_raw[0]["fieldsRaw"][0]["column"]
                    self._update_origin_insert(table_id, {"column": column_raw_tmp, "field": "fieldsRaw"})
                    column = "fieldsRaw." + str(column_raw_tmp)
                    find_master = self.data_dictionary_repository.find_master_field_origin(table_id, column)
                    if find_master.count() > 0:
                        column_master_tmp=find_master[0]["fieldsMaster"][0]["column"]
                        self._update_origin_insert(table_id, {"column": column_master_tmp, "field":"fieldsMaster"})
                else:
                    self._update_origin_fieldReview(table_id, {"column": data["column"], "field": "fieldsRaw"})
        if data["field"] == "fieldsRaw":
            if "fieldsMaster" in table:
                column = "fieldsRaw." + str(data["column"])
                find_master = self.data_dictionary_repository.find_master_field_origin(table_id, column)
                if find_master.count() > 0:
                    column_master_tmp = find_master[0]["fieldsMaster"][0]["column"]
                    self._update_origin_insert(table_id, {"column": column_master_tmp, "field": "fieldsMaster"})
                else:
                    self._update_origin_fieldReview(table_id, {"column": data["column"], "field": "fieldsMaster"})

    def _substract_field(self, table_id, data):
        """Substract a field in a specific position and data dictionary fase (FieldReview, fieldRaw or fieldMaster)

        :param table_id: str, String that reference a table id
        :param data: json, dictionary with a data to check
        :return:
        """
        table = self.data_dictionary_repository.find_one(table_id)
        res = []
        for i in table[data["field"]]:
            if i["column"] > data["column"]:
                i["column"] -= 1
                res.append(i)
            elif i["column"] < data["column"]:
                res.append(i)
        table[data["field"]] = res
        self._update_field_action(table_id,{data["field"]:table[data["field"]]})
        if data["field"] == "FieldReview":
            if "fieldsRaw" in table:
                find_raw = self.data_dictionary_repository.find_raw_field_origin(table_id, "FieldReview."+str(data["column"]))
                if find_raw.count() > 0:
                    column_raw_tmp = find_raw[0]["fieldsRaw"][0]["column"]
                    self._update_origin(table_id, {"column": column_raw_tmp, "field": "fieldsRaw"})
                    column = "fieldsRaw." + str(column_raw_tmp)
                    find_master = self.data_dictionary_repository.find_master_field_origin(table_id, column)
                    if find_master.count() > 0:
                        column_master_tmp = find_master[0]["fieldsMaster"][0]["column"]
                        self._update_origin(table_id, {"column": column_master_tmp, "field":"fieldsMaster"})
                else:
                    self._update_origin_fieldReview(table_id, {"column": data["column"], "field": "fieldsRaw"})
        if data["field"] == "fieldsRaw":
            if "fieldsMaster" in table:
                column = "fieldsRaw." + str(data["column"])
                find_master = self.data_dictionary_repository.find_master_field_origin(table_id, column)
                if find_master.count() > 0:
                    column_master_tmp = find_master[0]["fieldsMaster"][0]["column"]
                    self._update_origin(table_id, {"column": column_master_tmp, "field": "fieldsMaster"})
                else:
                    self._update_origin_fieldReview(table_id, {"column": data["column"], "field": "fieldsMaster"})

    def _update_origin(self, table_id, data):
        """

        :param table_id:
        :param column_fr:
        :return:
        """
        table = self.data_dictionary_repository.find_one(table_id)
        res = []
        for i in table[data["field"]]:
            if i["column"] > data["column"]:
                i["column"] -= 1
                if data["field"] == "fieldsMaster" or data["field"] == "fieldsRaw":
                    if i["origin"] != "":
                        tmp_split = i["origin"].split(".")
                        tmp = int(tmp_split[1])-1
                        i["origin"] = tmp_split[0]+"."+str(tmp)
                res.append(i)
            elif i["column"] < data["column"]:
                res.append(i)
        table[data["field"]] = res
        self._update_field_action(table_id, {data["field"]: table[data["field"]]})

    def _update_origin_fieldReview(self, table_id, data):
        """

        :param table_id:
        :param column_fr:
        :return:
        """
        table = self.data_dictionary_repository.find_one(table_id)
        res = []
        for i in table[data["field"]]:
            if i["column"] > data["column"]:
                if data["field"] == "fieldsMaster" or data["field"] == "fieldsRaw":
                    if i["origin"] != "":
                        tmp_split = i["origin"].split(".")
                        tmp = int(tmp_split[1])-1
                        i["origin"] = tmp_split[0]+"."+str(tmp)
                res.append(i)
            elif i["column"] < data["column"]:
                res.append(i)
        table[data["field"]] = res
        self._update_field_action(table_id, {data["field"]: table[data["field"]]})

    def _update_origin_insert(self, table_id, data):
        """

        :param table_id:
        :param column_fr:
        :return:
        """
        table = self.data_dictionary_repository.find_one(table_id)
        res = []
        for i in table[data["field"]]:
            if i["column"] == data["column"]:
                if data["field"] == "fieldsMaster" or data["field"] == "fieldsRaw":
                    if i["origin"] != "":
                        tmp_split = i["origin"].split(".")
                        tmp = int(tmp_split[1]) + 1
                        i["origin"] = tmp_split[0] + "." + str(tmp)
                #i["column"] += 1
            elif i["column"] > data["column"]:
                if data["field"] == "fieldsMaster" or data["field"] == "fieldsRaw":
                    if i["origin"] != "":
                        tmp_split = i["origin"].split(".")
                        tmp = int(tmp_split[1]) + 1
                        i["origin"] = tmp_split[0] + "." + str(tmp)
                #i["column"] += 1
            res.append(i)
        table[data["field"]] = res
        self._update_field_action(table_id, {data["field"]: table[data["field"]]} )

    def _update_field_action(self, table_id, data):
        """Save a field from a dictionary
        N: in Proposed
        G: Governance
        A: Architecture
        I: To ingest
        IN: Ingest
        M: Mallas
        Q: Quality
        P: Production
        :param table_id: str, String that reference a table id
        :param data: json, dictionary with data to update
        :return:
        """
        self.data_dictionary_repository.update_field(table_id, data)

    def send_to(self,table_id,data):
        """Change a data dictionary state

        :param table_id: str, String that reference a table id
        :param data:json, dictionary with data to check (from and to)
        :return:
        """
        user_id = self.user_repository.find_one(data["user_id"])
        if user_id is None:
            raise ValueError("Permisos denegados para el usuario.")

        data_dic = self.data_dictionary_repository.find_one(table_id)
        if data_dic is None:
            raise ValueError("La tabla no se encuentra registrada.")

        flag = True
        for k in data_dic["people"]:
            if str(user_id["_id"]) == str(k["user"]):
                flag = False
        if flag:
            data_dic["people"].append({"user": ObjectId(str(user_id["_id"])), "rol": user_id["rol"]})

        user_id = user_id["_id"]

        temp_mod = [{"user": ObjectId(str(user_id)),
                     "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]

        for i in data_dic["modifications"]:
            temp_mod.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]})

        if data["from"] == "governance" and data["to"] == "ingesta": self._change_state(table_id,"N","G")
        #elif data["from"] == "ingesta" and data["to"] == "governance": self._ingest_to_governance(table_id)
        elif data["from"] == "ingesta" and data["to"] == "governance": self._change_state(table_id,"G","N")
        elif data["from"] == "functional" and data["to"] == "ingesta": self._change_state(table_id, "N", "F")
        elif data["from"] == "ingesta" and data["to"] == "functional": self._change_state(table_id, "F", "N")
        elif data["from"] == "governance" and data["to"] == "architecture": self._change_state(table_id, "A", "G")
        elif data["from"] == "governance" and data["to"] == "business":self._change_state(table_id,"RN","G")
        elif data["from"] == "business" and data["to"] == "governance":self._change_state(table_id, "G", "RN")
        elif data["from"] == "architecture" and data["to"] == "governance": self._change_state(table_id,"G","A")
        elif data["from"] == "architecture" and data["to"] == "ingesta": self._change_state(table_id,"P","A")
        elif data["from"] == "ingesta" and data["to"] == "architecture":self._change_state(table_id, "A", "I")
        elif data["from"] == "ingesta" and data["to"] == "ingesta-table": self._change_state(table_id,"IN","I")
        elif data["from"] == "ingesta-table" and data["to"] == "ingesta":self._change_state(table_id, "I", "IN")
        elif data["from"] == "ingest-table" and data["to"] == "meshes": self._change_state(table_id,"M","IN")
        elif data["from"] == "meshes" and data["to"] == "ingest-table": self._change_state(table_id, "IN", "M")
        elif data["from"] == "meshes" and data["to"] == "qualified": self._change_state(table_id,"Q","M")
        elif data["from"] == "qualified" and data["to"] == "meshes": self._change_state(table_id, "Q", "M")
        elif data["from"] == "qualified" and data["to"] == "production": self._change_state(table_id,"P","Q")
        elif data["from"] == "production" and data["to"] == "qualified": self._change_state(table_id, "P", "Q")

        if "ingestOwner" in data:
            self.data_dictionary_repository.update_field(table_id,{"ingestOwner":data["ingestOwner"]})

        self.data_dictionary_repository.update_modifications_audit(table_id, temp_mod[:10])
        self.data_dictionary_repository.update_people(table_id, data_dic["people"])

    def _ingest_to_governance(self,table_id):
        """Validate if a table can be send to governance

        :param table_id: str, string that reference a table id
        :return:
        """
        table = self.data_dictionary_repository.find_one(table_id)
        check = False
        for i in table["FieldReview"]:
            if len(i["modification"]) == 0:
                check = True
        if not check and self.data_dictionary_repository.find_one(table_id)["stateTable"] == "N":
            self.data_dictionary_repository.sent_to(table_id,"G")
        else:
            raise ValueError("something to validate in table")

    def free_change_state(self,table_id,body):
        """Change a data dictionary state

        :param table_id: str, String that reference a table id
        :param new: str, string that reference new state
        :param old: str, string that reference a old state
        :return:
        """
        self.data_dictionary_repository.sent_to(table_id, body["newState"])

    def _change_state(self,table_id,new,old):
        """Change a data dictionary state

        :param table_id: str, String that reference a table id
        :param new: str, string that reference new state
        :param old: str, string that reference a old state
        :return:
        """
        if new != old:
            self.data_dictionary_repository.sent_to(table_id, new)
        else:
            raise ValueError("Cant be pass to this state")

    def _functional_groups(self, table):
        """Get level 1 and 2 of a uuaa master

        :param table: json, dictionary with data dictionary data
        :return: str, str, String that reference a functional map matched
        """
        uuaa = table["uuaaMaster"][1:]
        functional_map = self.functional_map_repository.find_uuaa(uuaa)
        return functional_map["level1"], functional_map["level2"]

    def generate_raw_body(self, table_id):
        """Generate a Raw fields

        :param table_id: str, string that reference a table id
        :return:
        """
        self._raw_field(table_id)
        self._raw_object(table_id, 1)

    def generate_master_body(self, table_id):
        """Generate a Master fields

        :param table_id: str, string that reference a table id
        :return:
        """
        self._master_field(table_id)
        self._master_object(table_id)

    def generate_body(self, table_id, state, data):
        """Generate a new data dictionary phase

        :param table_id: str, string that reference a table id
        :param state: str, String that check what fields generate
        :param data: dict
        :return:
        """
        user_id = self.user_repository.find_one(data["user_id"])
        if user_id is None:
            raise ValueError("Permisos denegados para el usuario.")

        data_dict = self.data_dictionary_repository.find_one(table_id)
        if data_dict is None:
            raise ValueError("La tabla no se encuentra registrada.")

        flag = True
        for k in data_dict["people"]:
            if str(user_id["_id"]) == str(k["user"]):
                flag = False
        if flag:
            data_dict["people"].append({"user": ObjectId(str(user_id["_id"])), "rol": user_id["rol"]})

        user_id = user_id["_id"]

        temp_mod = [{"user": ObjectId(str(user_id)),
                     "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]

        for i in data_dict["modifications"]:
            temp_mod.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]})

        if state == "raw":
            self.generate_raw_body(table_id)
        elif state == "master":
            self.generate_master_body(table_id)

        self.data_dictionary_repository.update_modifications_audit(table_id, temp_mod[:10])
        self.data_dictionary_repository.update_people(table_id, data_dict["people"])
    

    def _raw_field(self, id_table):
        """Generate raw fields except a fieldsRaw

        :param id_table: str, string that reference a table id
        :return:
        """
        table = self.data_dictionary_repository.find_one(id_table)

        table["raw_comment"] = []
        table["typeFile"] = self.backlog_repository.find_one(id_table)["typeFile"]

        table = self.update_routesandpaths_head(table)
        table = self.update_datasourceinfo_head(table)
        self.data_dictionary_repository.update_field(id_table, table)

    def _master_field(self, id_table):
        """Generate Master fields except a fieldMaster

        :param id_table: str, String that reference a table id
        :return:
        """
        table = self.data_dictionary_repository.find_one(id_table)

        table["master_comment"] = []
        table["typeFile"] = self.backlog_repository.find_one(id_table)["typeFile"]

        table = self.update_routesandpaths_head(table)
        table = self.update_datasourceinfo_head(table)
        self.data_dictionary_repository.update_field(id_table, table)

    def _raw_object(self, id_table, master_check):
        """Generate raw routes

        :param id_table: str, String that reference a table id
        :return:
        """
        table = self.data_dictionary_repository.find_one(id_table)

        table["raw_path"] = ""
        table["raw_route"] = ""
        table["partitions_raw"] = ""
        
        table = self.update_routesandpaths_head(table)

        if master_check:
            self._master_object(id_table)

        self.data_dictionary_repository.update_field(id_table, table)

    def _master_object(self, id_table):
        """Generate Master routes

        :param id_table: str, String that reference a table id
        :return:
        """
        table = self.data_dictionary_repository.find_one(id_table)
        backlog = self.backlog_repository.find_one(id_table)
        query = {}
        alias_desc = table["raw_name"].split("_")[2:]
        alias_desc = "_".join(alias_desc)
        query["master_path"] = "/data/raw/" + table["uuaaRaw"].lower() + "/data/" + alias_desc +"/"

        if "route" in self.functional_map_repository.find_uuaa(query["uuaaMaster"][1:].upper()):
            query["master_route"] = self.functional_map_repository.find_uuaa(query["uuaaMaster"][1:].upper())["route"] + "/" + alias_desc + "/"
        else:
            query["master_route"] = "/data/master/" + query["uuaaMaster"].lower() + "/data/" + alias_desc + "/"

        try:
            query["partitions_master"] = table["partitions_master"]
            query["periodicity_master"] = table["periodicity_master"]
        except Exception as e:
            query["partitions_master"] = ""
            query["periodicity_master"] = backlog["periodicity"]

        if "partitions_master" in table and len(table["partitions_master"]) < 1:
            query["master_path"] = table["raw_route"]
            try:
                query["master_route"] = query["master_route"] + self.excels_repository.find_one_periodicity(table["periodicity_master"])["frequency"]
            except Exception as e:
                query["master_route"] = query["master_route"] + self.excels_repository.find_one_periodicity(backlog["periodicity"])["frequency"]
        self.data_dictionary_repository.update_field(id_table, query)

    def generate_new_fase(self, table_id, data):
        """Generate new data dictionary fields
        Data need to now what phase generate

        :param table_id: str, String that reference a table id
        :param data: json, dictionary with data what phase generate
        :return:
        """
        user_id = self.user_repository.find_one(data["user_id"])
        if user_id is None:
            raise ValueError("Permisos denegados para el usuario.")

        data_dict = self.data_dictionary_repository.find_one(table_id)
        if data_dict is None:
            raise ValueError("La tabla no se encuentra registrada.")

        flag = True
        for k in data_dict["people"]:
            if str(user_id["_id"]) == str(k["user"]):
                flag = False
        if flag:
            data_dict["people"].append({"user": ObjectId(str(user_id["_id"])), "rol": user_id["rol"]})

        user_id = user_id["_id"]

        temp_mod = [{"user": ObjectId(str(user_id)),
                     "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]

        for i in data_dict["modifications"]:
            temp_mod.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]})

        if data["to"] == "raw":
            self._generate_raw(table_id, data)
        elif data["to"] == "master":
            self._generate_master(table_id, data)
        elif data["to"] == "FieldReview":
            self._generate_field_review(table_id, data)

        self.data_dictionary_repository.update_modifications_audit(table_id, temp_mod[:10])
        self.data_dictionary_repository.update_people(table_id, data_dict["people"])

    def _generate_raw(self, table_id, data):
        """Generate FieldsRaw, with all raw data need it to continue a cicle of governance data

        :param table_id: str, String that reference a table id
        :param data: json, dictionary that has from where generate field generate Raw
        :return:
        """
        table = self.data_dictionary_repository.find_one(table_id)
        column = 0
        query = {"fieldsRaw": []}
        for i in table["FieldReview"]:
            # print(i)
            format = i["format"]
            tmpDes = i["destinationType"]
            """
            if "DECIMAL" in i["destinationType"] or "DECIMAL".lower() in i["destinationType"].lower():
                tmpDes = i["destinationType"] + "(" + str(int(i["decimals"]) + int(i["integers"])) + "," + str( i["decimals"]) + ")"
                format = "(" + str(int(i["decimals"]) + int(i["integers"])) + "," + str( i["decimals"]) + ")"
            """

            temp_field = Field_raw({}, column).data
            temp_field["naming"] = i["naming"]
            temp_field["logic"] = i["logic"]
            temp_field["description"] = i["description"]
            temp_field["outFormat"] = tmpDes
            temp_field["outGov"] = i["governanceFormat"]
            temp_field["catalogue"] = i["catalogue"]
            temp_field["key"] = i["key"]
            temp_field["mandatory"] = i["mandatory"]
            temp_field["legacy"] = i["legacy"]
            temp_field["format"] = format
            temp_field["modification"] = i["modification"]
            temp_field["column"] = column
            temp_field["tokenization"] = "empty"
            temp_field["origin"] = data["from"]+"."+str(i["column"])
            temp_field["logicalFormat"] = i["outFormat"]
            temp_field["default"] = i["default"]
            temp_field["comments"] = i["comments"]
            temp_field["domain"] = i["domain"]
            temp_field["subdomain"] = i["subdomain"]
            temp_field["ownership"] = i["ownership"]
            temp_field["commentResponse"] = i["commentResponse"]
            temp_field["conceptualEntity"] = i["conceptualEntity"]
            temp_field["operationalEntity"] = i["operationalEntity"]
            temp_field["check"] = {"status": "NOK", "user": "", "lastComment": "", "comments": []}

            query["fieldsRaw"].append(temp_field)
            column += 1
        self.data_dictionary_repository.update_field(table_id, query)

    def _generate_master(self, table_id, data):
        """Generate FieldsMaster, with all Master data to need it to continue a cicle of governance data

        :param table_id: str, String that reference a table id
        :param data: json, dictionary that has from where generate field generate Master
        :return:
        """
        globalDeleteRaw = False
        table = self.data_dictionary_repository.find_one(table_id)

        table["raw_name"], table["master_name"] = self._name_file(table, table["raw_name"][7:])
        if len(table["partitions_raw"].strip()) > 0:
            table["partitions_master"] = table["partitions_raw"]

        table = self.update_routesandpaths_head(table)
        self.data_dictionary_repository.update_field(table_id, table)

        if table["uuaaMaster"][0] != 'C' and len(table["fieldsRaw"]) == 0:
            globalDeleteRaw = True
            self._generate_raw(table_id, data)
            table = self.data_dictionary_repository.find_one(table_id)

        column = 0
        query = {"fieldsMaster": []}
        for i in table["fieldsRaw"]:
            if "free_field" not in i["naming"]["naming"]:
                format = i["format"]
                outFormat = i["logicalFormat"]
                destType = i["outFormat"]

                if i["outFormat"] == "empty":
                    outFormat = i["logicalFormat"]
                    destType = i["destinationType"]

                if i["outGov"] and i["outGov"] != "empty":
                    outFormat = i["outGov"]

                temp_field = Field_master({}, column).data
                temp_field["naming"] = i["naming"]
                temp_field["logic"] = i["logic"]
                temp_field["description"] = i["description"]
                temp_field["outFormat"] = i["outGov"]
                temp_field["catalogue"] = i["catalogue"]
                temp_field["key"] = i["key"]
                temp_field["mandatory"] = i["mandatory"]
                temp_field["legacy"] = i["legacy"]
                temp_field["legacy"]["repeat"] = "N"
                temp_field["legacy"]["legacyDescription"] = "N/A"
                temp_field["legacy"]["legacy"] = i["naming"]["naming"]
                temp_field["modification"] = i["modification"]
                temp_field["default"] = i["default"]
                temp_field["column"] = column
                temp_field["format"] = format
                temp_field["origin"] = data["from"] + "." + str(i["column"])
                temp_field["destinationType"] = destType
                temp_field["logicalFormat"] = outFormat
                temp_field["default"] = i["default"]
                temp_field["comments"] = i["comments"]
                temp_field["commentResponse"] = i["commentResponse"]
                temp_field["domain"] = i["domain"]
                temp_field["subdomain"] = i["subdomain"]
                temp_field["ownership"] = i["ownership"]
                temp_field["conceptualEntity"] = i["conceptualEntity"]
                temp_field["operationalEntity"] = i["operationalEntity"]
                try:
                    temp_field["tokenization"] = i["tokenization"]
                except:
                    temp_field["tokenization"] = ""
                query["fieldsMaster"].append(temp_field)
                column += 1

        #Operational naming
        if len(self._find_naming(table["fieldsRaw"], "operational_audit_insert_date")) == 0:
            if table["uuaaMaster"][0] == "C":
                naming = self.field_repository.find_naming_all("operational_audit_insert_date")
                temp_field_ope = Field_master({}, column).data
                temp_field_ope["naming"]["suffix"] = naming["naming"]["suffix"]
                temp_field_ope["naming"]["Words"] = naming["naming"]["Words"]
                temp_field_ope["naming"]["naming"] = naming["naming"]["naming"]
                temp_field_ope["naming"]["isGlobal"] = naming["naming"]["isGlobal"]
                temp_field_ope["naming"]["code"] = naming["code"]
                temp_field_ope["naming"]["hierarchy"] = naming["level"]
                temp_field_ope["logic"] = naming["logic"]
                temp_field_ope["description"] = naming["originalDesc"]
                temp_field_ope["mandatory"] = 1
                temp_field_ope["column"] = column
                temp_field_ope["format"] = "yyyy-MM-dd HH:mm:ss.SSSSSS"
                temp_field_ope["destinationType"] = "TIMESTAMP"
                temp_field_ope["logicalFormat"] = "TIMESTAMP"
                temp_field_ope["logicalFormat"] = "TIMESTAMP"
                query["fieldsMaster"].append(temp_field_ope)

        self.data_dictionary_repository.update_field(table_id, query)

        if globalDeleteRaw:
            self.delete_raw(table_id, data) #Deletes raw if needed

    def _generate_field_review(self,table_id,data):
        """Generate again a first fase of governance data, Field review

        :param table_id: str, string that reference a table id
        :param data: json, dictionary that has from where generate field generate FieldReview
        :return:
        """
        table = self.data_dictionary_repository.find_one(table_id)
        field_review = table["FieldReview"]
        for i in table[data["from"]]:
            column,origin = i["origin"].split(".")
            if origin == "FieldReview":
                for j in i:
                    if j != column and j != origin:
                        field_review[column][j] = j[i]
        self.data_dictionary_repository.update_field(table_id,field_review)

    def find_in_table(self, table_id, fase, naming, logic, desc, state, legacy, descLegacy, suffix, comment_state):
        """Find a field in a data dictionary and specific phase

        All query its transform by every filter define

        :param table_id: str, string that reference a table id
        :param fase: str, String that reference a phase to search
        :param naming: str, String that reference a naming
        :param logic: str, String that reference a logical name
        :param desc: str, String that reference description naming
        :return:
        """
        query = self.data_dictionary_repository.find_one(table_id)[fase]
        if len(state.strip()) != 0:
            query = self._find_naming_state(query, state)
        if len(logic.strip()) != 0:
            query = self._find_logic(query, logic)
        if len(desc.strip()) != 0:
            query = self._find_desc(query, desc)
        if len(naming.strip()) != 0:
            query = self._find_naming(query, naming)
        if len(legacy.strip()) != 0:
            query = self._find_legacy(query, legacy)
        if len(descLegacy.strip()) != 0:
            query = self._find_legacy_description(query, descLegacy)
        if len(suffix.strip()) != 0:
            query = self._find_suffix_name(query, suffix)
        if len(comment_state.strip()) != 0:
            query = self._find_comment_state(query, comment_state)

        return query

    def _find_naming_state(self, query, state):
        """Check what namings are Ok or noOk in a comment

        :param query: dictionary, with data to check if state conditions its the same
        :param state: str, condition to check
        :return: dictionary, new data with condition ok
        """
        res = []
        for i in query:
            if len(i["comments"]) == 0:
                if state == "NOK":
                    res.append(i)
            else:
                if state == "NOK":
                    if i["comments"][len(i["comments"])-1]["reasonReturn"] != "OK" and i["comments"][len(i["comments"])-1]["reasonReturn"] != "OBSERVACIÓN":
                        res.append(i)
                else:
                    if i["comments"][len(i["comments"])-1]["reasonReturn"] == state:
                        res.append(i)
        return res

    def _find_logic(self, query, logic):
        """Check what naming has the same logic name

        :param query: dictionary, with data to check if logic name its the same
        :param logic: str, condition to check
        :return: dictionary, new data with condition ok
        """
        res = []
        logic_transform = self.field_service._structure_text(logic)
        for i in query:
            if logic_transform in i["logic"]:
                res.append(i)
        return res

    def _find_desc(self, query, desc):
        """Check what naming has the same description or part of them

        :param query: dictionary, with data to check if description naming its the same or part of them
        :param logic: str, condition to check
        :return: dictionary, new data with condition ok
        """
        res = []
        desc_transform = self.field_service._structure_text(desc)
        for i in query:
            if desc_transform in self.field_service._structure_text(i["description"]):
                res.append(i)
        return res

    def _find_naming(self, query, naming):
        """
        Check what naming has the same naming of part of them

        :param query: dictionary, with data to check if a naming its the same of part of them
        :param naming: str, condition to check
        :return: dictionary, new data with condition ok
        """
        res = []
        for field in query:
            if naming in field["naming"]["naming"]:
                res.append(field)
        return res

    def _find_legacy(self, query, legacy):
        """
        Get all fields with a specific legacy
        :param query: dictionary, with data to check if a naming its the same of part of them
        :param legacy: str, legacy name of the table's field
        :return: a list of fields
        """
        res = []

        for field in query:
            if legacy in field["legacy"]["legacy"]:
                res.append(field)
        return res

    def _find_legacy_description(self, query, legacy_description):
        """
        Get all fields with a specific legacy description
        :param query: dictionary, with data to check if a naming its the same of part of them
        :param legacy_description: str, legacy description of the table's field
        :return: a list of fields
        """
        res = []

        for field in query:
            if legacy_description in field["legacy"]["legacyDescription"]:
                res.append(field)
        return res

    def _find_suffix_name(self, query, suffix):
        """
        Get fields with especific suffix
        :param query: dictionary, with data to check if a naming its the same of part of them
        :param suffix: str, represent the suffix
        :return: list of fields
        """
        res = []
        for field in query:
            if field["naming"]["naming"].endswith("_"+suffix):
                res.append(field)
        return res

    def _find_legacy_and_legacy_description(self, query, legacy, legacyDesc):
        """
        Get all fields with a specific legacy or legacy description
        :param query:
        :param legacy: str, legacy name of the table's field
        :param legacyDesc: str, legacy description of the table's field
        :return: a list of fields
        """
        res = []
        legacyDesc_transform = self.field_service._structure_text(legacyDesc)
        legacy_transform = self.field_service._structure_text(legacy)
        for field in query:
            if legacy_transform in field["legacy"]["legacy"] and legacyDesc_transform in field["legacy"]["legacyDescription"]:
                res.append(field)
        return res

    def _find_comment_state(self, query, state):
        """Check what namings are Ok or noOk in a comment

        :param query: dictionary, with data to check if state conditions its the same
        :param state: str, condition to check
        :return: dictionary, new data with condition ok
        """
        res = []
        for i in query:
            if len(i["comments"]) == 0:
                if state == "NOK":
                    res.append(i)
            else:
                if state == "NOK":
                    if i["comments"][len(i["comments"])-1]["reasonReturn"] != "OK" and i["comments"][len(i["comments"])-1]["reasonReturn"] != "OBSERVACIÓN":
                        res.append(i)
                else:
                    if i["comments"][len(i["comments"])-1]["reasonReturn"] == state:
                        res.append(i)
        return res

    def find_head(self, table_id, state):
        """Get data of one phase different at they field fase

        :param table_id: str, String that reference a table id
        :param state: str, String that reference to what fase want
        :return: Array, data fase
        """
        res = []
        if state == "FieldReview":
            res = self._basic_information(table_id)
        elif state == "raw":
            res = self._basic_information(table_id)
            #res = self._raw_information(table_id)
        elif state == "master":
            #res = self._master_information(table_id)
            res = self._basic_information(table_id)
        elif state == "functional":
            res = self._functional_information(table_id)
        elif state == "complete":
            res = self._completeobject_information(table_id)
        return res

    def _basic_information(self,table_id):
        """Get basic data of every data dictionary fase

        :param table_id: str, String that reference a table id
        :return: json
        """
        """
        information = self.data_dictionary_repository.find_fields(table_id,{
            "originSystem": 1, "periodicity": 1, "observationField": 1,
            "typeFile": 1, "uuaaRaw": 1, "uuaaMaster": 1, "raw_name": 1, "master_name": 1,
            "user": 1, "separator": 1, "alias": 1, "baseName": 1, "tacticalObject": 1, "stateTable": 1,
            "modifications": 1, "created_time": 1
        })
        """
        information = self.data_dictionary_repository.find_fields(table_id,{
            "information_group_level_1": 1, "information_group_level_2": 1,
            "raw_comment": 1, "raw_route": 1, "raw_path": 1, "raw_name": 1,
            "baseName": 1, "alias": 1, "observationField": 1, "uuaaRaw": 1, "periodicity": 1, "uuaaMaster": 1,
            "_id": 1, "perimeter": 1, "data_source": 1, "tacticalObject": 1, "partitions_raw": 1,
            "estimated_volume_records": 1, "information_level": 1, "current_depth": 1, "objectType": 1,
            "loading_type": 1, "required_depth": 1, "separator": 1,  "physical_name_source_object": 1,
            "originSystem": 1, "typeFile": 1, "master_route": 1, "master_path": 1, "master_name": 1,
            "periodicity_master": 1, "partitions_master": 1, "modifications": 1, "created_time":1, 
            "deploymentType":1, "modelVersion":1, "objectVersion":1, "user": 1, "stateTable": 1, "securityLevel": 1, 
            "entryType": 1, "modelVersionMaster":1, "objectVersionMaster":1,
        })
        project_name = self._get_table_project(table_id)
        information["user"] = self.user_repository.find_one(information["user"])["name"]
        information["project_name"] = project_name
        information["_id"] = str(information["_id"])
        if len(information["modifications"]) != 0:
            information["modifications"] = {
                "user_name": self.user_repository.find_one(str(information["modifications"][0]["user"]))["name"],
                "date": information["modifications"][0]["date"]}
        else:
            information["modifications"] = {"user_name": "N/A", "date": "N/A"}
        return information

    def _raw_information(self,table_id):
        """Get Raw data less FieldRaw

        :param table_id: str, String that reference a table id
        :return: json
        """
        new_information = self.data_dictionary_repository.find_fields(table_id,{
            "information_group_level_1": 1, "information_group_level_2": 1,
            "raw_comment": 1, "raw_route": 1, "raw_path": 1, "raw_name": 1,
            "baseName": 1, "alias": 1, "observationField": 1, "uuaaRaw": 1, "periodicity": 1, "uuaaMaster": 1,
            "_id":0, "perimeter": 1, "data_source": 1, "tacticalObject": 1, "partitions_raw": 1,
            "estimated_volume_records": 1, "information_level": 1, "current_depth": 1, "objectType": 1,
            "loading_type": 1, "required_depth": 1, "separator": 1,  "physical_name_source_object": 1,
            "originSystem": 1, "typeFile": 1, "master_route": 1, "master_path": 1, "master_name": 1,
            "periodicity_master": 1, "partitions_master": 1, "modifications": 1, "created_time":1, 
            "deploymentType":1, "modelVersion":1, "objectVersion":1
        })
        if len(new_information["modifications"]) != 0:
            new_information["modifications"] = {
                "user_name": self.user_repository.find_one(str(new_information["modifications"][0]["user"]))["name"],
                "date": new_information["modifications"][0]["date"]}
        else:
            new_information["modifications"] = {"user_name": "N/A", "date": "N/A"}
        return new_information

    def _master_information(self,table_id):
        """Generate Master data less FieldMaster

        :param table_id: str, String that reference a table id
        :return: json
        """
        new_information = self.data_dictionary_repository.find_fields(table_id, {
            "partitions_raw": 1, "physical_name_source_object": 1,
            "information_group_level_1": 1, "information_group_level_2": 1,
            "master_comment": 1, "master_route": 1, "master_path": 1, "master_name": 1,
            "baseName": 1, "alias": 1, "observationField": 1, "uuaaMaster": 1, "periodicity": 1, "uuaaRaw": 1,
            "_id": 0, "perimeter": 1, "data_source": 1, "tacticalObject": 1, "partitions_master": 1,
            "estimated_volume_records": 1, "information_level": 1, "current_depth": 1, "objectType": 1, "typeFile": 1,
            "loading_type": 1, "required_depth": 1, "originSystem": 1, "raw_route": 1, "raw_path": 1,
            "raw_name": 1, "periodicity_master": 1, "modifications": 1, "created_time": 1,
            "deploymentType":1, "modelVersion":1, "objectVersion": 1
        })
        if len(new_information["modifications"]) != 0:
            new_information["modifications"] = {
                "user_name": self.user_repository.find_one(str(new_information["modifications"][0]["user"]))["name"],
                "date": new_information["modifications"][0]["date"]}
        else:
            new_information["modifications"] = {"user_name": "N/A", "date": "N/A"}
        return new_information

    def _functional_information(self, table_id):
        """

        :param table_id:
        :return:
        """
        new_information = self.data_dictionary_repository.find_fields(table_id, {
            "baseName": 1, "alias": 1, "observationField": 1, "uuaaMaster": 1, "periodicity": 1, "uuaaRaw": 1,
            "_id": 0, "perimeter": 1, "data_source": 1, "tacticalObject": 1, "information_level": 1,
            "objectType": 1, "typeFile": 1, "information_group_level_1": 1, "information_group_level_2": 1,
            "required_depth": 1, "originSystem": 1, "estimated_volume_records": 1,
            "periodicity_master": 1, "modifications": 1, "created_time": 1, "current_depth": 1,
        })
        if len(new_information["modifications"]) != 0:
            new_information["modifications"] = {
                "user_name": self.user_repository.find_one(str(new_information["modifications"][0]["user"]))["name"],
                "date": new_information["modifications"][0]["date"]}
        else:
            new_information["modifications"] = {"user_name": "N/A", "date": "N/A"}
        return new_information
    
    def _completeobject_information(self, table_id):
        """Get all object data

        :param table_id: str, String that reference a table id
        :return: json
        """
        new_information = self.data_dictionary_repository.find_fields(table_id, {
            "FieldReview": 0, "fieldsRaw": 0, "fieldsMaster": 0, "people": 0
        })
        new_information["_id"] = str(new_information["_id"])

        
        if len(new_information["modifications"]) != 0:
            new_information["modifications"] = {
                "user_name": self.user_repository.find_one(str(new_information["modifications"][0]["user"]))["name"],
                "date": new_information["modifications"][0]["date"]}
        else:
            new_information["modifications"] = {"user_name": "N/A", "date": "N/A"}
        
        """
        if len(new_information["people"]) != 0:
            new_information["people"] = {
                "user_name": self.user_repository.find_one(str(new_information["people"][0]["user"]))["name"],
                "rol": new_information["people"][0]["rol"]}
        else:
            new_information["people"] = {"user_name": "N/A", "rol": "N/A"}
        """

        if new_information["user"]:
            new_information["user"] = self.user_repository.find_one(str(new_information["user"]))["name"]

        return new_information

    def find_field(self, table_id, field):
        """Get a users that modified a fields in a fase

        :param table_id: str, string that reference a table id
        :param field: str, string that reference a fase to check
        :return: Array, fields modified
        """
        try:
            table = self.data_dictionary_repository.find_field(table_id, field)
            if field in table:
                for i in table[field]:
                    try:
                        if field == "fieldsRaw":
                            i["check"]["user"] = str(i["check"]["user"])
                    except:
                       pass
                    for j in i["modification"]:
                        try:
                            j["name"] = self.user_repository.find_one(j["user"])["name"]
                        except:
                            j["name"] = "Unassigned"
            try:
                return table[field]
            except:
                return []
        except Exception as e:
            print(e, "---------->")

    def find_uuaa_data_dictionary(self,table_id):
        """Get uuaas that can be change in a data dictionary

        :param table_id: str, String that reference a table id
        :return: Array, all uuaa that can change
        """
        ambit_list = ["C", "B", "K", "N", "W"]
        #country = self.team_repository.find_one(self.user_repository.find_one(
            #self.data_dictionary_repository.find_one(table_id)["user"])["teams"][0])["country"]
        #uuaas = self.functional_map_repository.find_all()
        #return [(country[0]+i["uuaa"]).upper() for i in uuaas]
        #return [("C" + i["uuaa"]).upper() for i in uuaas]

        
        res = []
        for ambit in ambit_list:
            uuaas = self.functional_map_repository.find_all()
            for ua in uuaas:
                res.append((ambit + ua["uuaa"]).upper())
        
        return res
    
    def update_legacy_fields(self, table_id, data):
        """Update all legacy names in a data dictionary field

        :param table_id: str, String that reference a table id
        :param data: json, dictionary with data to charge
        :return:
        """
        warningDec = ""
        user_id = self.user_repository.find_one(data["user_id"])
        if user_id is None:
            raise ValueError("Permisos denegados para el usuario.")
        user_id = user_id["_id"]

        temp = self.data_dictionary_repository.find_one(table_id)
        if temp is None:
            raise ValueError("La tabla no se encuentra registrada.")

        temp_mod = [{"user": ObjectId(str(user_id)),
                     "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]

        for i in temp["modifications"]:
            temp_mod.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]})

        try:
            self.update_allload_object(table_id, data["object"])
        except Exception as e:
            print("sin object")


        data = data["fields"]
        for i in range(len(data)):
            if not self._check_integers_decimals(data[i]):
                warningDec = 'La suma de los decimales y enteros no concuerda con la longitud, en la fila: ' + str(i+1)
                break
                #raise Exception('La suma de los decimales y enteros no concuerda con la longitud, en la fila: {}'.format(i+1))
        validateType = ["CHAR", "DATE", "INTEGER", "INTEGERP", "TIMESTAMP", "DECIMAL", "DECIMALP", "TIME"]
        table = self.data_dictionary_repository.find_one(table_id)

        j = 0
        for i in range(len(data)):
            if i > len(table["FieldReview"]):
                table["FieldReview"].append(ast.literal_eval(str(Field_review(column = i))))

            table["FieldReview"][i]["legacy"]["legacy"] = data[j]["legacy"]
            table["FieldReview"][i]["legacy"]["legacyDescription"] = data[j]["description"]
            table["FieldReview"][i]["length"] = int(data[j]["length"]) #+ int(data[j]["signo"])
            table["FieldReview"][i]["format"] = data[j]["format"]
            table["FieldReview"][i]["key"] = int(data[j]["key"])
            table["FieldReview"][i]["mandatory"] = int(data[j]["mandatory"])
            table["FieldReview"][i]["outFormat"] = "ALPHANUMERIC("+ str(data[j]["length"]) +")"
            table["FieldReview"][i]["symbol"] = int(data[j]["signo"])
            table["FieldReview"][i]["catalogue"] = data[j]["catalogue"]
            table["FieldReview"][i]["integers"] = data[j]["enteros"]
            table["FieldReview"][i]["decimals"] = data[j]["decimales"]

            if data[j]["type"].strip().upper() in validateType:
                if data[j]["type"] == "DECIMALP":
                    table["FieldReview"][i]["originType"] = "DECIMAL"
                elif data[j]["type"] == "INTEGERP":
                    table["FieldReview"][i]["originType"] = "INTEGER"
                else: 
                    table["FieldReview"][i]["originType"] = data[j]["type"]
            else:
                raise Exception('Tipo de dato no válido en la fila: {}'.format(i+1))
    
            query1 = self.data_dictionary_repository.find_legacys(table_id, "FieldReview", data[j]["legacy"])
            query2 = self.data_dictionary_repository.find_legacys(table_id, "fieldsRaw", data[j]["legacy"])
            query3 = self.data_dictionary_repository.find_legacys(table_id, "fieldsMaster", data[j]["legacy"])

            if query1.count() != 0 or query2.count() != 0 or query3.count() != 0:
                table["FieldReview"][i]["legacy"]["repeat"] = "Y"
            if "HOST".lower() in table["originSystem"].lower():
                table = self._update_legacy_information(data, i, j, table)
            j += 1
            if j >= len(data):
                break
        del table["_id"]

        self.data_dictionary_repository.update_field(table_id, table)
        self.data_dictionary_repository.update_modifications_audit(table_id, temp_mod[:10])
        self.data_dictionary_repository.update_people(table_id, temp["people"])

        return warningDec

    def update_allload_object(self, table_id, object_data):
        """Updates values in the object fields of a table

        :param table_id: str, String that reference a table id
        :param data: json, dictionary with data to charge
        :return:
        """
        data_dic = self.data_dictionary_repository.find_one(table_id)
        
        object_data["_id"] = data_dic["_id"]
        object_data["raw_name"] = data_dic["raw_name"]
        object_data["uuaaRaw"] = data_dic["uuaaRaw"]
        object_data["master_name"] = data_dic["master_name"]
        object_data["uuaaMaster"] = data_dic["uuaaMaster"]
        object_data["typeFile"] = data_dic["typeFile"]
        desc_table = "_".join(data_dic["raw_name"].split("_")[2:]).lower()

        try:
            if len(data_dic["raw_path"].strip()) == 0:
                tmp_split = self.excels_repository.find_one_system(data_dic["originSystem"])["staging"]
                object_data["raw_path"] = tmp_split + object_data["uuaaRaw"].lower() + "/" + "C" + object_data["uuaaMaster"].upper() + "_D02_${?YEAR}${?MONTH}${?DAY}" + "_" + desc_table + self.file[data_dic["typeFile"].lower()]
        except:
            tmp_split = self.excels_repository.find_one_system(data_dic["originSystem"])["staging"]
            object_data["raw_path"] = tmp_split + object_data["uuaaRaw"].lower() + "/" + "C" + object_data["uuaaMaster"].upper() + "_D02_${?YEAR}${?MONTH}${?DAY}" + "_" + desc_table + self.file[data_dic["typeFile"].lower()]
        try:
            if "physical_name_source_object" not in data_dic or len(data_dic["physical_name_source_object"].strip()) == 0:
                object_data["physical_name_source_object"] = "C"+ object_data["uuaaMaster"].upper() + "_D02_aaaammdd_" + "_".join(data_dic["raw_name"].split("_")[2:]).lower() + self.file[data_dic["typeFile"].lower()]
        except:
                object_data["physical_name_source_object"] = "C"+ object_data["uuaaMaster"].upper() + "_D02_aaaammdd_" + "_".join(data_dic["raw_name"].split("_")[2:]).lower() + self.file[data_dic["typeFile"].lower()]

        object_data["raw_route"] = "/data/raw/" + object_data["uuaaRaw"].lower() + "/data/" + desc_table + "/"
        object_data["master_path"] = object_data["raw_route"]

        if "route" in self.functional_map_repository.find_uuaa(object_data["uuaaMaster"][1:].upper()):
            object_data["master_route"] = self.functional_map_repository.find_uuaa(object_data["uuaaMaster"][1:].upper())["route"] + "/" + desc_table + "/"
        else:
            object_data["master_route"] = "/data/master/" + object_data["uuaaMaster"].lower() + "/data/" + desc_table + "/"

        object_data = self.update_routesandpaths_head(object_data)
        object_data = self.update_datasourceinfo_head(object_data)

        self.data_dictionary_repository.update_field(table_id, object_data)

    def update_load_all_fields(self, table_id, data):
        """Update all names in a data dictionary field

        :param table_id: str, String that reference a table id
        :param data: json, dictionary with data to charge
        :return:
        """
        warningDec = ""
        user_id = self.user_repository.find_one(data["user_id"])
        if user_id is None:
            raise ValueError("Permisos denegados para el usuario.")
        user_id = user_id["_id"]

        temp = self.data_dictionary_repository.find_one(table_id)
        if temp is None:
            raise ValueError("La tabla no se encuentra registrada.")

        temp_mod = [{"user": ObjectId(str(user_id)),
                     "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                     "type": "Carga Completa" }]

        for i in temp["modifications"]:
            temp_mod.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]})
        print(table_id)
        self.update_allload_object(table_id, data["object"])

        data = data["fields"]
        table = self.data_dictionary_repository.find_one(table_id)

        j = 0
        for i in range(len(data)):
            if i >= len(table["FieldReview"]):
                table["FieldReview"].append(ast.literal_eval(str(Field_review(column = i))))

            table["FieldReview"][i]["legacy"]["legacy"] = data[j]["legacy"]
            table["FieldReview"][i]["legacy"]["legacyDescription"] = data[j]["legacyDescription"]

            table["FieldReview"][i]["length"] = int(data[j]["length"]) #+ int(data[j]["signo"])
            table["FieldReview"][i]["format"] = data[j]["format"] if "format" in data[j] else ""
            table["FieldReview"][i]["key"] = int(data[j]["key"])
            table["FieldReview"][i]["mandatory"] = int(data[j]["mandatory"])
            table["FieldReview"][i]["outFormat"] = data[j]["outFormat"] if "outFormat" in data[j] else ""
            table["FieldReview"][i]["symbol"] = int(data[j]["symbol"])
            table["FieldReview"][i]["catalogue"] = data[j]["catalogue"]
            table["FieldReview"][i]["governanceFormat"] = data[j]["governanceFormat"] if "governanceFormat" in data[j] else ""

            table["FieldReview"][i]["originType"] = data[j]["originType"]
            table["FieldReview"][i]["destinationType"] = data[j]["destinationType"]

            table["FieldReview"][i]["naming"] = data[j]["naming"]
            table["FieldReview"][i]["logic"] = data[j]["logic"]
            table["FieldReview"][i]["description"] = data[j]["description"]

            table = self._update_legacy_information(data, i, j, table)

            j += 1
            if j >= len(data):
                break

        del table["_id"]

        self.data_dictionary_repository.update_field(table_id, table)
        self.data_dictionary_repository.update_modifications_audit(table_id, temp_mod[:10])
        self.data_dictionary_repository.update_people(table_id, temp["people"])
        return warningDec

    def upload_legacy_functional(self, table_id, data):
        """

        :param table_id:
        :param data:
        :return:
        """
        user_id = self.user_repository.find_one(data["user_id"])
        if user_id is None:
            raise ValueError("Permisos denegados para el usuario.")

        data_dict = self.data_dictionary_repository.find_one(table_id)
        if data_dict is None:
            raise ValueError("La tabla no se encuentra registrada.")

        flag = True
        for k in data_dict["people"]:
            if str(user_id["_id"]) == str(k["user"]):
                flag = False
        if flag:
            data_dict["people"].append({"user": ObjectId(str(user_id["_id"])), "rol": user_id["rol"]})

        user_id = user_id["_id"]

        temp_mod = [{"user": ObjectId(str(user_id)),
                     "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]

        for i in data_dict["modifications"]:
            temp_mod.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]})

        for i in range(len(data)):
            if not self._check_integers_decimals(data[i]):
                raise Exception('La suma de los decimales y enteros no concuerda con la longitud, en la fila: {}'.format(i+1))

        validateType = ["CHAR", "DATE", "INTEGER", "TIMESTAMP", "DECIMAL", "DECIMALP", "TIME"]
        table = self.data_dictionary_repository.find_one(table_id)

        j = 0
        for i in range(len(table["FieldReview"])):
            table["FieldReview"][i]["legacy"]["legacy"] = data[j]["legacy"]
            table["FieldReview"][i]["legacy"]["legacyDescription"] = data[j]["description"]
            table["FieldReview"][i]["catalogue"] = data[j]["catalogue"].strip()
            table["FieldReview"][i]["length"] = int(data[j]["length"])# + int(data[j]["signo"])
            table["FieldReview"][i]["format"] = data[j]["format"]
            table["FieldReview"][i]["key"] = int(data[j]["key"])
            table["FieldReview"][i]["mandatory"] = int(data[j]["mandatory"])
            table["FieldReview"][i]["scope"] = data[j]["scope"].strip()
            table["FieldReview"][i]["outFormat"] = "ALPHANUMERIC(" + str(data[j]["length"]) + ")"
            table["FieldReview"][i]["symbol"] = int(data[j]["signo"])

            if data[j]["type"].strip() in validateType:
                table["FieldReview"][i]["originType"] = data[j]["type"]
                if data[j]["type"] == "DECIMALP":
                    table["FieldReview"][i]["originType"] = "DECIMAL"
            else:
                raise Exception('Tipo de dato no válido en la fila: {}'.format(i + 1))
            query1 = self.data_dictionary_repository.find_legacys(table_id, "FieldReview", data[j]["legacy"])
            query2 = self.data_dictionary_repository.find_legacys(table_id, "fieldsRaw", data[j]["legacy"])
            query3 = self.data_dictionary_repository.find_legacys(table_id, "fieldsMaster", data[j]["legacy"])
            if query1.count() != 0 or query2.count() != 0 or query3.count() != 0:
                table["FieldReview"][i]["legacy"]["repeat"] = "Y"
            if "HOST".lower() in table["originSystem"].lower():
                table = self._update_legacy_information(data, i, j, table)
            j += 1
            if j >= len(data):
                break
        del table["_id"]

        self.data_dictionary_repository.update_field(table_id, table)
        self.data_dictionary_repository.update_modifications_audit(table_id, temp_mod[:10])
        self.data_dictionary_repository.update_people(table_id, data_dict["people"])

    def _update_legacy_information(self, data, i, j, table):
        """Update Field that are generate with legacy data upload

        :param data: json, dictionary that has to be check
        :param i:
        :param j:
        :param table:
        :return:
        """
        if len(data) == 1:
            table["FieldReview"][i] = self._one_legacy_field(data, table["FieldReview"][i], j)
        elif j == 0:
            table["FieldReview"][i]["startPosition"] = 1
            table["FieldReview"][i]["endPosition"] = int(data[j]["length"])
            table["FieldReview"][i]["outLength"] = int(data[j]["length"])
            table["FieldReview"][i]["outVarchar"] = "ALPHANUMERIC(" + str(data[j]["length"]) + ")"
            table["FieldReview"][i]["outFormat"] = "ALPHANUMERIC(" + str(data[j]["length"]) + ")"
            table["FieldReview"][i]["outRec"] = "OUTREC FIELDS=("
            table["FieldReview"][i] = self._update_decimal_type(data, table["FieldReview"][i], j)
            table["FieldReview"][i]["outRec"] += ","
            table["FieldReview"][i]["decimals"] = data[j]["decimales"]
            table["FieldReview"][i]["integers"] = data[j]["enteros"]
        elif j == len(data) - 1 or i == len(table["FieldReview"])-1:
            table["FieldReview"][i]["startPosition"] = int(table["FieldReview"][i-1]["endPosition"])+1
            table["FieldReview"][i]["endPosition"] = int(table["FieldReview"][i]["startPosition"]) + int(data[j]["length"])-1
            table["FieldReview"][i]["outLength"] = int(data[j]["length"])
            table["FieldReview"][i]["outVarchar"] = "ALPHANUMERIC(" + str(str(data[j]["length"])) + ")"
            table["FieldReview"][i]["outFormat"] = "ALPHANUMERIC(" + str(data[j]["length"]) + ")"
            table["FieldReview"][i]["outRec"]=""
            table["FieldReview"][i]["decimals"] = data[j]["decimales"]
            table["FieldReview"][i]["integers"] = data[j]["enteros"]
            table["FieldReview"][i] = self._update_decimal_type(data, table["FieldReview"][i], j)
            table["FieldReview"][i]["outRec"] += ")"
        else:
            table["FieldReview"][i]["startPosition"] = int(table["FieldReview"][i-1]["endPosition"])+1
            table["FieldReview"][i]["endPosition"] = int(table["FieldReview"][i]["startPosition"]) + int(data[j]["length"])-1
            table["FieldReview"][i]["outLength"] = int(data[j]["length"])
            table["FieldReview"][i]["outVarchar"] = "ALPHANUMERIC(" + str(data[j]["length"]) + ")"
            table["FieldReview"][i]["outFormat"] = "ALPHANUMERIC(" + str(data[j]["length"]) + ")"
            table["FieldReview"][i]["outRec"] = ""
            table["FieldReview"][i]["decimals"] = data[j]["decimales"]
            table["FieldReview"][i]["integers"] = data[j]["enteros"]
            table["FieldReview"][i] = self._update_decimal_type(data, table["FieldReview"][i], j)
            table["FieldReview"][i]["outRec"] += ","

        return table

    def _update_decimal_type(self, data, i, j):
        """Update decimal types
        Check if origin is host or other

        :param data: json, dictionary to check legacy data
        :param i:
        :param j:
        :return:
        """
        if str(data[j]["type"]) == "DECIMALP":
            i["outLength"] = int(data[j]["length"]) * 2
            i["originType"]="DECIMALEMP"
            i["outVarchar"] = "ALPHANUMERIC(" + str(i["outLength"]) + ")"
            i["outFormat"] = "ALPHANUMERIC(" + str(i["outLength"]) + ")"
            i["outRec"] += str(i["startPosition"]) + "," + str(data[j]["length"]) + ",PD,M25"
        else:
            i["outRec"] += str(i["startPosition"]) + "," + str(data[j]["length"]) + ",TRAN=ALTSEQ"
        return i

    def _one_legacy_field(self, data, i, j):
        """

        :param data:
        :param i:
        :param j:
        :return:
        """
        i["startPosition"] = 1
        i["endPosition"] = int(data[j]["length"])
        i["outLength"] = int(data[j]["length"])
        i["outVarchar"] = "ALPHANUMERIC(" + str(i["outLength"]) + ")"
        i["outFormat"] = "ALPHANUMERIC(" + str(i["outLength"]) + ")"
        i["decimals"] = data["decimals"]
        i["integers"] = data["integers"]
        if str(data[j]["type"]) == "DECIMALP":
            i["originType"] = "DECIMAL"
            i["outLength"] = int(data[j]["length"]) * 2
            i["outVarchar"] = "ALPHANUMERIC(" + str(i["outLength"]) + ")"
            i["outFormat"] = "ALPHANUMERIC(" + str(i["outLength"]) + ")"
            i["outRec"] = "OUTREC FIELDS=(" + str(i["startPosition"]) + "," + str(data[j]["length"]) + ",PD,M25)"
        else:
            i["outRec"] = "OUTREC FIELDS=(" + str(i["startPosition"]) + "," + str(data[j]["length"]) + ",TRAN=ALTSEQ)"
        return i

    def find_no_ok_fields(self,table_id, fase):
        """This dont need, dont use anymore

        :param table_id:
        :param fase:
        :return:
        """
        res = []
        table = self.data_dictionary_repository.find_one(table_id)
        for i in table[fase]:
            if len(i["comments"]) > 0 and i["comments"][len(i["comments"])-1]["reasonReturn"] != "OK":
                res.append(i)
        return res

    def generate_excel(self, table_id, fase):
        """Generate excel to download by fase

        :param table_id:
        :param fase:
        :return:
        """
        res = []
        if fase == "Object":
            res = self.generate_excel_object(table_id)
        elif fase == "Fields":
            res = self.generate_excel_fields(table_id)
        elif fase == "Legacy":
            res = self.generate_only_legacy_fields(table_id)
        elif fase == "VoBo":
            res = self._generate_vobo_fields(table_id)
        return res
    
    def _generate_vobo_fields(self, table_id):
        """Generate excel object to download

        :param table_id: str, String that reference a table id
        :return: json, dictionary with field data of data dictionary
        """
        table = self.data_dictionary_repository.find_one(table_id)
        res = []
        #country = self.team_repository.find_one(self.user_repository.find_one(table["user"])["teams"][0])["country"]
        country = "Colombia"
        index = 1
        values = {0: "NO", 1: "YES"}

        if not table["physical_name_source_object"]:
            table["physical_name_source_object"] = ""
            
        if "fieldsRaw" in table:
            for i in table["fieldsRaw"]:
                try:
                    naming = self.field_repository.find_naming_all_data(i["naming"]["naming"])
                    log_eng = ""
                    desc_eng = ""
                    log_spn = i["logic"]
                    desc_spn = i["description"]
                    domain = "Unassigned"
                    subdomain = ""
                    ownership = ""
                    operational_ent = ""
                    source_field_length = 0
                    source_field_datatype = "string"
                    dataType = "string"
                    if naming is not None:
                        log_eng = naming["logicEnglish"]
                        desc_eng = naming["descEnglish"]
                        log_spn = naming["logic"]
                        desc_spn = naming["originalDesc"].replace("~", "\n")
                        if "dataOwnerInformation" in naming and len(naming["dataOwnerInformation"]) != 0:
                            domain = naming["dataOwnerInformation"][0]["domain"]
                            if len(domain.strip()) == 0:
                                domain = "Unassigned"
                            subdomain = naming["dataOwnerInformation"][0]["subdomain"]
                            ownership = naming["dataOwnerInformation"][0]["ownership"]
                            operational_ent = naming["dataOwnerInformation"][0]["operationalEntity"]

                    table_source = table["physical_name_source_object"]
                    source_field = i["legacy"]["legacy"]
                    generate_field = "NO"

                    if "origin" in i:
                        if len(i["origin"]) == 0:
                            generate_field = "YES"
                            if "calculated" not in i["legacy"]["legacy"].strip().lower():
                                table_source = table["physical_name_source_object"]
                                source_field = i["legacy"]["legacy"]
                            else:
                                table_source = "Calculated"
                                source_field = "Calculated"
                        else:
                            pos = i["origin"].split(".")[1]
                            source_field_length = table["FieldReview"][int(pos)]["length"]
                         
                    if table["uuaaRaw"] == "KEXC":
                        source_field_length = ""
                        source_field_datatype = ""
                        dataType = i["outFormat"].lower()
                        
                    tokenization = i["tokenization"]
                    if i["tokenization"] == "empty":
                        tokenization = ""

                    if i["outFormat"] == "DATE":
                        format = "yyyy-MM-dd"
                    elif i["outFormat"] == "TIMESTAMP" or i["outFormat"] == "timestamp_millis" or i["outFormat"] == "TIMESTAMP_MILLIS":
                        format = "yyyy-MM-dd HH:mm:ss.SSSSSS"
                    elif len(i["format"].strip()) != 0 and i["format"].lower().strip()!="empty":
                        format = i["format"].strip()
                    else:
                        format = ""
                    
                    default = i["default"]
                    if i["default"] == "empty":
                        default = ""
                    try: 
                        security_class = i["securityClass"]
                    except:
                        security_class = ""
                    
                    try:
                        res.append({
                            "COUNTRY": country,
                            "PHYSICAL NAME OBJECT": table["raw_name"],
                            "STORAGE TYPE": "HDFS-Avro",
                            "STORAGE ZONE": "RAWDATA",
                            "DEPLOYMENT TYPE": table["deploymentType"],
                            #"MODEL VERSION": table["modelVersion"],
                            #"OBJECT VERSION": table["objectVersion"],
                            "PHYSICAL NAME FIELD": i["naming"]["naming"].strip().lower(),
                            "LOGICAL NAME FIELD": log_eng,
                            "LOGICAL NAME FIELD (SPA)": log_spn.strip(),
                            "SIMPLE FIELD DESCRIPTION": desc_eng,
                            "SIMPLE FIELD DESCRIPTION (SPA)": desc_spn,
                            "LEVEL": "",
                            "COMPLEX STRUCTURE": "",
                            "TECHNICAL COMMENTS": "",
                            "CATALOG": i["catalogue"],
                            #"SECURITY CLASS": security_class,
                            #"SECURITY LABEL": "",
                            "TOKENIZED AT DATA SOURCE": "",
                            "LOCALE": "",
                            "DATA TYPE": dataType,
                            "FORMAT": format,
                            "LOGICAL FORMAT": i["logicalFormat"].strip(),
                            "KEY": values[int(i["key"])],
                            "MANDATORY": values[int(i["mandatory"])],
                            "DEFAULT VALUE": default,
                            "PHYSICAL NAME OF SOURCE OBJECT": table_source,
                            "SOURCE FIELD": source_field,
                            "DATA TYPE OF SOURCE FIELD": source_field_datatype,
                            "FORMAT OF SOURCE FIELD": "",
                            "LENGTH OF SOURCE FIELD": source_field_length,
                            "TAGS": table["alias"].lower(),
                            "FIELD POSITION IN THE OBJECT": index,
                            "GENERATED FIELD": generate_field,
                            "TOKENIZATION TYPE": tokenization,
                            "VoBo COMMENTS": str(i["check"]["comments"])
                            #"COUNTRY OF THE DATA OWNER": country,
                            #"OPERATIONAL ENTITY DOMAIN": domain,
                            #"OPERATIONAL ENTITY SUBDOMAIN": subdomain,
                            #"OPERATIONAL ENTITY OWNERSHIP": ownership,
                            #"OPERATIONAL ENTITY SPA": operational_ent,
                            #"TDS": values[int(i["tds"])]
                        })
                    except Exception as e:
                        traceback.print_exc()
                        print(e, table_id, i["column"], "------------*fr")
                        raise ValueError("Error generando alguno de los campos en raw. " + str(e) + " columna " + str(i["column"]))
                    index += 1
                except Exception as e:
                    traceback.print_exc()
                    print(e, table_id, i["column"], "------------*fr")
                    raise ValueError("Error raw " + str(e))
        return res

    def generate_excel_backlog(self, table_id, fase):
        """Generate excel to download by fase

        :param table_id:
        :param fase:
        :return:
        """
        res = []
        if fase == "Object":
            res = self.generate_excel_object(table_id)
        elif fase == "Fields":
            res.append(self._gen_prop_review(table_id))
            res.append(self._generate_excel_fields_raw(table_id))
            res.append(self._generate_excel_fields_master(table_id))
        return res

    def generate_excel_object(self, table_id):
        """Generate excel object to download
        Are Raw and Master fase

        :param table_id: str, String that reference a table id
        :return: json, dictionary with object data of data dictionary
        """
        try:
            res = []
            table = self.data_dictionary_repository.find_one(table_id)
            made_raw = False
            made_master = False
    
            try:
                volume_records = str(int(table['estimated_volume_records']))
            except:
                volume_records = 0

            try:
                current_depth = str(int(table["current_depth"])) + " months"
            except:
                current_depth = "0 months"

            required_depth = str(int(table["required_depth"])) + " months"
            if int(table["required_depth"]) == 1:
                required_depth = str(int(table["required_depth"])) + " month"
            tactical = "NO"
            if str(table["tacticalObject"]) == "1" or str(table["tacticalObject"]) == "YES":
                tactical = "YES"
    
            frequencia = ""
            if self.excels_repository.find_one_periodicity(table["periodicity"]):
                frequencia = self.excels_repository.find_one_periodicity(table["periodicity"])["state"]
            
            frequencia_master = ""
            if self.excels_repository.find_one_periodicity(table["periodicity_master"]):
                frequencia_master = self.excels_repository.find_one_periodicity(table["periodicity_master"])["state"]
    
            originSystem = ""
            if self.excels_repository.find_one_system(table["originSystem"]):
                originSystem = self.excels_repository.find_one_system(table["originSystem"])["db"]

        except Exception as e:
            traceback.print_exc()

        if "fieldsRaw" in table and len(table["fieldsRaw"]) > 0:
            try:
                res.append({
                    "COUNTRY OF THE DATA SOURCE": "Colombia",
                    "PHYSICAL NAME OBJECT": table["raw_name"],
                    "LOGICAL NAME OBJECT": table["baseName"],
                    "DESCRIPTION OBJECT": table["observationField"],
                    "INFORMATION GROUP LEVEL 1": table["information_group_level_1"],
                    "INFORMATION GROUP LEVEL 2": table["information_group_level_2"],
                    "PERIMETER": table["perimeter"],
                    "INFORMATION LEVEL": table["information_level"],
                    "DATA SOURCE (DS)": table["data_source"],
                    "SECURITY LEVEL": table["securityLevel"] if "securityLevel" in table else "",
                    "ENCRYPTION AT REST": "",
                    "DEPLOYMENT TYPE": table["deploymentType"],
                    "MODEL NAME": "",
                    "MODEL VERSION": table["modelVersion"],
                    "OBJECT VERSION": table["objectVersion"],
                    "TECHNICAL RESPONSIBLE": "datahub.co.group@bbva.com",
                    "STORAGE TYPE": "HDFS-Avro",
                    "STORAGE ZONE": "RAWDATA",
                    "OBJECT TYPE": table["objectType"],
                    "DATA PHYSICAL PATH": table["raw_route"],
                    "SYSTEM CODE/UUAA": table["uuaaMaster"],
                    "PARTITIONS": table["partitions_raw"],
                    #"FREQUENCY": frequencia,
                    "TIME REQUIREMENT": "",
                    #"LOADING TYPE": table["loading_type"],
                    "CURRENT DEPTH": current_depth,
                    "REQUIRED DEPTH": required_depth,
                    "ESTIMATED VOLUME OF RECORDS": volume_records,
                    "STORAGE TYPE OF SOURCE OBJECT": originSystem,
                    "PHYSICAL NAME OF SOURCE OBJECT": table["physical_name_source_object"],
                    "MAILBOX SOURCE TABLE": "",
                    "SOURCE PATH": table["raw_path"],
                    "SCHEMA PATH": "",
                    "SOURCE FILE TYPE": table["typeFile"].upper(),
                    "SOURCE FILE DELIMITER": table["separator"],
                    "TARGET FILE TYPE": "avro",
                    "TARGET FILE DELIMITER": "",
                    "VALIDATED BY DATA ARCHITECT": "NO",
                    "TAGS": table["alias"].lower(),
                    "MARK OF TACTICAL OBJECT": tactical
                })
                made_raw = True
            except Exception as e:
                print(e, "Fallo-----------------------")
                raise ValueError("Error generando alguno de los campos en el objeto raw" )

        if "fieldsMaster" in table and len(table["fieldsMaster"]) > 0:
            try:
                res.append({
                    "COUNTRY OF THE DATA SOURCE": "Colombia",
                    "PHYSICAL NAME OBJECT": table["master_name"],
                    "LOGICAL NAME OBJECT": table["baseName"],
                    "DESCRIPTION OBJECT": table["observationField"],
                    "INFORMATION GROUP LEVEL 1": table["information_group_level_1"],
                    "INFORMATION GROUP LEVEL 2": table["information_group_level_2"],
                    "PERIMETER": table["perimeter"],
                    "INFORMATION LEVEL": table["information_level"],
                    "DATA SOURCE (DS)": table["data_source"],
                    "SECURITY LEVEL": table["securityLevel"] if "securityLevel" in table else "",
                    "ENCRYPTION AT REST": "",
                    "DEPLOYMENT TYPE": table["deploymentType"],
                    "MODEL NAME": "",
                    "MODEL VERSION": table["modelVersionMaster"] if "modelVersionMaster" in table else "",
                    "OBJECT VERSION": table["objectVersionMaster"] if "objectVersionMaster" in table else "",
                    "TECHNICAL RESPONSIBLE": "datahub.co.group@bbva.com",
                    "STORAGE TYPE": "HDFS-Parquet",
                    "STORAGE ZONE": "MASTERDATA",
                    "OBJECT TYPE": table["objectType"],
                    "DATA PHYSICAL PATH": table["master_route"],
                    "SYSTEM CODE/UUAA": table["uuaaMaster"],
                    "PARTITIONS": table["partitions_master"],
                    #"FREQUENCY": frequencia_master,
                    "TIME REQUIREMENT": "",
                    #"LOADING TYPE": table["loading_type"],
                    "CURRENT DEPTH": current_depth,
                    "REQUIRED DEPTH": required_depth,
                    "ESTIMATED VOLUME OF RECORDS": volume_records,
                    "STORAGE TYPE OF SOURCE OBJECT": "HDFS-Avro" if made_raw else "HDFS-Parquet",
                    "PHYSICAL NAME OF SOURCE OBJECT": table["raw_name"] if made_raw else table["master_name"],
                    "MAILBOX SOURCE TABLE": "",
                    "SOURCE PATH": table["master_path"],
                    "SCHEMA PATH": "",
                    "SOURCE FILE TYPE": "AVRO" if made_raw else "PARQUET",
                    "SOURCE FILE DELIMITER": "",
                    "TARGET FILE TYPE": "parquet",
                    "TARGET FILE DELIMITER": "",
                    "VALIDATED BY DATA ARCHITECT": "NO",
                    "TAGS": table["alias"].lower(),
                    "MARK OF TACTICAL OBJECT": tactical
                })
                made_master = True
            except Exception as e:
                print(e, "Fallo-------------*")
                raise ValueError("Error generando alguno de los campos en el objeto master ")

        if not made_raw and not made_master:
            try:
                res.append({
                    "COUNTRY OF THE DATA SOURCE": "Colombia", "PHYSICAL NAME OBJECT": table["raw_name"],
                    "LOGICAL NAME OBJECT": table["baseName"], "DESCRIPTION OBJECT": table["observationField"],
                    "INFORMATION GROUP LEVEL 1": table["information_group_level_1"],
                    "INFORMATION GROUP LEVEL 2": table["information_group_level_2"],
                    "PERIMETER": table["perimeter"],
                    "INFORMATION LEVEL": table["information_level"],
                    "DATA SOURCE (DS)": table["data_source"],
                    "SECURITY LEVEL": table["securityLevel"] if "securityLevel" in table else "",
                    "ENCRYPTION AT REST": "",
                    "DEPLOYMENT TYPE": table["deploymentType"],
                    "MODEL NAME": "",
                    "MODEL VERSION": table["modelVersion"],
                    "OBJECT VERSION": table["objectVersion"],
                    "TECHNICAL RESPONSIBLE": "datahub.co.group@bbva.com",
                    "STORAGE TYPE": "LEGACY",
                    "STORAGE ZONE": "LEGACY",
                    "OBJECT TYPE": table["objectType"],
                    "DATA PHYSICAL PATH": "",
                    "SYSTEM CODE/UUAA": table["uuaaMaster"],
                    "PARTITIONS": "",
                    #"FREQUENCY": self.excels_repository.find_one_periodicity(table["periodicity"])["state"],
                    "TIME REQUIREMENT": "",
                    #"LOADING TYPE": "",
                    "CURRENT DEPTH": current_depth,
                    "REQUIRED DEPTH": required_depth,
                    "ESTIMATED VOLUME OF RECORDS": volume_records,
                    "STORAGE TYPE OF SOURCE OBJECT": self.excels_repository.find_one_system(table["originSystem"])["db"],
                    "PHYSICAL NAME OF SOURCE OBJECT": table["physical_name_source_object"] ,
                    "MAILBOX SOURCE TABLE": "",
                    "SOURCE PATH": "",
                    "SCHEMA PATH": "",
                    "SOURCE FILE TYPE": table["typeFile"].upper(),
                    "SOURCE FILE DELIMITER": table["separator"],
                    "TARGET FILE TYPE": "",
                    "TARGET FILE DELIMITER": "",
                    "VALIDATED BY DATA ARCHITECT": "NO",
                    "TAGS": table["alias"].lower(),
                    "MARK OF TACTICAL OBJECT": tactical
                })
            except Exception as e:
                print(e, "Fallo-------------*")
                raise ValueError("Error generando alguno de los campos en el objeto LEGACY ")
        
        return res

    def comment_governance_return(self):
        """Get all reason return of governance check

        :return: Array
        """
        return self.excels_repository.find_governance_statistics()

    def _gen_prop_review(self, table_id):
        """Generate excel object to download

        :param table_id: str, String that reference a table id
        :return: json, dictionary with field data of data dictionary
        """
        table = self.data_dictionary_repository.find_one(table_id)
        res = []
        #country = self.team_repository.find_one(self.user_repository.find_one(table["user"])["teams"][0])["country"]
        country = "Colombia"
        index = 1
        values = {0: "NO", 1: "YES"}
        if "FieldReview" in table:
            for i in table["FieldReview"]:
                naming = self.field_repository.find_naming_all_data(i["naming"]["naming"])
                log_eng = ""
                desc_eng = ""
                log_spn = i["logic"]
                desc_spn = i["description"]
                domain = "Unassigned"
                subdomain = ""
                ownership = ""
                operational_ent = ""
                if naming is not None:
                    log_eng = naming["logicEnglish"]
                    desc_eng = naming["descEnglish"]
                    log_spn = naming["logic"]
                    desc_spn = naming["originalDesc"].replace("~", "\n")
                    if "dataOwnerInformation" in naming and len(naming["dataOwnerInformation"]) != 0:
                        domain = naming["dataOwnerInformation"][0]["domain"]
                        if len(domain.strip()) == 0:
                            domain = "Unassigned"
                        subdomain = naming["dataOwnerInformation"][0]["subdomain"]
                        ownership = naming["dataOwnerInformation"][0]["ownership"]
                        operational_ent = naming["dataOwnerInformation"][0]["operationalEntity"]

                table_source = table["physical_name_source_object"]
                source_field = i["legacy"]["legacy"]
                source_description = i["legacy"]["legacyDescription"]
                generate_field = "NO"
                org_type = ""

                if "originType" in i:
                    org_type = i["originType"]

                if "origin" in i:
                    if len(i["origin"]) == 0:
                        generate_field = "YES"
                        if "calculated" not in i["legacy"]["legacy"].strip().lower():
                            table_source = table["physical_name_source_object"]
                            source_field = i["legacy"]["legacy"]
                            source_description = i["legacy"]["legacyDescription"]
                        else:
                            table_source = "Calculated"
                            source_field = "Calculated"
                            source_description = "Calculated"

                if i["outFormat"] == "DATE":
                    format = "yyyy-MM-dd"
                elif i["outFormat"] == "TIMESTAMP" or i["outFormat"] == "timestamp_millis" or i["outFormat"] == "TIMESTAMP_MILLIS":
                    format = "yyyy-MM-dd HH:mm:ss.SSSSSS"
                else:
                    format = ""

                default = i["default"]
                if i["default"] == "empty":
                    default = ""
                    
                try: 
                    security_class = i["securityClass"]
                except:
                    security_class = ""

                try:
                    res.append({
                        "COUNTRY": country,
                        "PHYSICAL NAME OBJECT": table["raw_name"],
                        "ORIGIN SYSTEM": table["originSystem"],
                        "STORAGE ZONE": "LEGACY",
                        "LENGTH": i["length"], 
                        "START POSITION": i["startPosition"], 
                        "END POSITION": i["endPosition"], 
                        "ORIGIN TYPE": org_type,
                        "PHYSICAL NAME FIELD": i["naming"]["naming"].strip().lower(),
                        "LOGICAL NAME FIELD": log_eng,
                        "LOGICAL NAME FIELD (SPA)": log_spn.strip(),
                        "SIMPLE FIELD DESCRIPTION": desc_eng,
                        "SIMPLE FIELD DESCRIPTION (SPA)": desc_spn,
                        "CATALOG": i["catalogue"],
                        #"SECURITY CLASS": security_class,
                        #"SECURITY LABEL": "",
                        "DATA TYPE": "string",
                        "FORMAT": format,
                        "KEY": values[int(i["key"])],
                        "MANDATORY": values[int(i["mandatory"])],
                        "DEFAULT VALUE": default,
                        "PHYSICAL NAME OF SOURCE OBJECT": table_source,
                        "SOURCE FIELD": source_field,
                        "SOURCE DESCRIPTION": source_description,
                        "DATA TYPE OF SOURCE FIELD": "string",
                        "FORMAT OF SOURCE FIELD": "",
                        "TAGS": table["alias"].lower(),
                        "FIELD POSITION IN THE OBJECT": index,
                        "GENERATED FIELD": generate_field,
                        "COUNTRY OF THE DATA OWNER": country,
                        "OPERATIONAL ENTITY DOMAIN": domain,
                        "OPERATIONAL ENTITY SUBDOMAIN": subdomain,
                        "OPERATIONAL ENTITY OWNERSHIP": ownership,
                        "OPERATIONAL ENTITY SPA": operational_ent,
                        "Comentarios": str(i["comments"])
                    })
                except Exception as e:
                    print(e, table_id, i["column"], "------------*fr")
                    raise ValueError("Error generando alguno de los campos en review.")
                index += 1
        return res

    def _clean_origin_namings(self, table_id, naming):
        """
            Cleans the origin data of a naming to be exported
        :param table_id: str, String that reference a table id
        :param naming: json, Object with full single naming data
        :return: json, dictionary with naming origin data
        """
        table = self.data_dictionary_repository.find_one(table_id)
        source_field = naming["legacy"]["legacy"]
        source_field_length = 0
        source_field_datatype = "string"
        generated_field = "NO"

        try:
            if "fieldsRaw" in naming["origin"]:
                table_source = table["raw_name"] if len(table["fieldsRaw"]) > 0 else table["master_name"]
            else:
                table_source = table["physical_name_source_object"] if "physical_name_source_object" in table else ""
        except:
            table_source = table["physical_name_source_object"] if "physical_name_source_object" in table else ""
        try:
            if "origin" in naming and len(naming["origin"]) == 0:
                generated_field = "YES"
                if "calculated" not in naming["legacy"]["legacy"].strip().lower():
                    table_source = table["physical_name_source_object"]
                    source_field = naming["legacy"]["legacy"]
                else:
                    table_source = "Calculated"
                    source_field = "Calculated"
                    try:
                        source_field_length = naming["logicalFormat"].split('(')[1].split(')')[0]
                    except:
                        source_field_length = 0
            else:
                source_field_length = table["FieldReview"][int(naming["origin"].split(".")[1])]["length"]
        except Exception as e:
            print(e)
        try:
            if table["originSystem"] == "Datio Holding": #Condicion especial para Holding
                table_source = table["physical_name_source_object"]
        except:
            pass

        return table_source, source_field, source_field_length, source_field_datatype, generated_field


    def _generate_excel_fields_raw(self, table_id):
        """
            Generate raw fields for excel object to download
        :param table_id: str, String that reference a table id
        :return: json, dictionary with field data of data dictionary
        """
        res = []
        values = {0: "NO", 1: "YES"}
        try:
            table = self.data_dictionary_repository.find_one(table_id)
            for index, i in enumerate(table["fieldsRaw"]):
                naming = self.field_repository.find_naming_all_data(i["naming"]["naming"])
                table_source, source_field, source_field_length, source_field_datatype, generated_field = self._clean_origin_namings(table_id, i)
                #Adding the naming to the array excel result
                res.append({
                        "COUNTRY": "Colombia",
                        "PHYSICAL NAME OBJECT": table["raw_name"],
                        "STORAGE TYPE": "HDFS-Avro",
                        "STORAGE ZONE": "RAWDATA",
                        "DEPLOYMENT TYPE": table["deploymentType"],
                        "PHYSICAL NAME FIELD": i["naming"]["naming"].strip().lower(),
                        "LOGICAL NAME FIELD": naming["logicEnglish"].strip() if naming is not None else "",
                        "LOGICAL NAME FIELD (SPA)": naming["logic"].strip() if naming is not None else i["logic"].strip(),
                        "SIMPLE FIELD DESCRIPTION": naming["descEnglish"].strip() if naming is not None else "",
                        "SIMPLE FIELD DESCRIPTION (SPA)": naming["originalDesc"].replace("~", "\n").strip() if naming is not None else i["description"].strip(),
                        "LEVEL": "",
                        "COMPLEX STRUCTURE": "",
                        "TECHNICAL COMMENTS": "",
                        "CATALOG": i["catalogue"],
                        "TOKENIZED AT DATA SOURCE": "",
                        "LOCALE": "",
                        "DATA TYPE": i["destinationType"].lower() if table["entryType"] != "PARQUET" else i["outFormat"].lower(),
                        "FORMAT": i["format"] if i["format"].lower().strip() != "empty" else "",
                        "LOGICAL FORMAT": i["logicalFormat"].upper() if table["entryType"] != "PARQUET" else i["outGov"].upper(),
                        "KEY": values[int(i["key"])],
                        "MANDATORY": values[int(i["mandatory"])],
                        "DEFAULT VALUE": i["default"] if i["default"] != "empty" else "",
                        "PHYSICAL NAME OF SOURCE OBJECT": table_source,
                        "SOURCE FIELD": source_field,
                        "DATA TYPE OF SOURCE FIELD": source_field_datatype,
                        "FORMAT OF SOURCE FIELD": "",
                        "LENGTH OF SOURCE FIELD": str(source_field_length),
                        "TAGS": table["alias"].lower(),
                        "FIELD POSITION IN THE OBJECT": index + 1,
                        "GENERATED FIELD": generated_field,
                        "TOKENIZATION TYPE": i["tokenization"] if i["tokenization"] != "empty" else ""
                })
        except Exception as e:
            print(e, table_id, i["column"], "------------*fr")
            raise ValueError(f"Error generando alguno de los campos en raw. {str(e)} columna {str(i.column)}" ) from e

        return res

    def _generate_excel_fields_master(self, table_id):
        """Generate excel object to download

        :param table_id: str, String that reference a table id
        :return: json, dictionary with field data of data dictionary
        """
        res = []
        values = {0: "NO", 1: "YES"}
        try:
            table = self.data_dictionary_repository.find_one(table_id)
            for index, i in enumerate(table["fieldsMaster"]):
                naming = self.field_repository.find_naming_all_data(i["naming"]["naming"])
                table_source, source_field, source_field_length, source_field_datatype, generated_field = self._clean_origin_namings(table_id, i)
                #Adding the naming to the array excel result
                res.append({
                    "COUNTRY": "Colombia",
                    "PHYSICAL NAME OBJECT": table["master_name"],
                    "STORAGE TYPE": "HDFS-Parquet",
                    "STORAGE ZONE": "MASTERDATA",
                    "DEPLOYMENT TYPE": table["deploymentType"],
                    "PHYSICAL NAME FIELD": i["naming"]["naming"].strip().lower(),
                    "LOGICAL NAME FIELD": naming["logicEnglish"].strip() if naming is not None else "",
                    "LOGICAL NAME FIELD (SPA)": naming["logic"].strip() if naming is not None else i["logic"].strip(),
                    "SIMPLE FIELD DESCRIPTION": naming["descEnglish"].strip() if naming is not None else "",
                    "SIMPLE FIELD DESCRIPTION (SPA)": naming["originalDesc"].replace("~", "\n").strip() if naming is not None else i["description"].strip(),
                    "LEVEL": "",
                    "COMPLEX STRUCTURE": "",
                    "TECHNICAL COMMENTS": "",
                    "CATALOG": i["catalogue"],
                    "TOKENIZED AT DATA SOURCE": "",
                    "LOCALE": "",
                    "DATA TYPE": i["destinationType"].lower(),
                    "FORMAT": i["format"],
                    "LOGICAL FORMAT": i["logicalFormat"].upper().strip(),
                    "KEY": values[int(i["key"])],
                    "MANDATORY": values[int(i["mandatory"])],
                    "DEFAULT VALUE": i["default"] if i["default"] != "empty" else "",
                    "PHYSICAL NAME OF SOURCE OBJECT": table_source,
                    "SOURCE FIELD": source_field,
                    "DATA TYPE OF SOURCE FIELD": source_field_datatype if len(table["fieldsRaw"]) > 0 else i["destinationType"].lower(),
                    "FORMAT OF SOURCE FIELD": "",
                    "LENGTH OF SOURCE FIELD": str(source_field_length),
                    "TAGS": table["alias"].lower(),
                    "FIELD POSITION IN THE OBJECT": index + 1,
                    "GENERATED FIELD": generated_field,
                    "TOKENIZATION TYPE": i["tokenization"] if i["tokenization"] != "empty" else "",
                })
        except Exception as e:
            print(e, table_id, i["column"], "------------*fm")
            raise ValueError(f"Error generando alguno de los campos en master. {str(e)} columna {str(i.column)}") from e
        return res


    def generate_only_legacy_fields(self, table_id):
        """Generate excel legacy fields only

        :param table_id: str, String that reference a table id
        :return: json, dictionary with field data of data dictionary
        """
        return self._gen_prop_review(table_id)


    def generate_excel_fields(self, table_id):
        """
            Generate excel object to download based if the field phase is empty or not. Returns review phase if empty phase table.
        :param table_id: str, String that reference a table id
        :return: json, dictionary with field data of data dictionary
        """
        raw = self._generate_excel_fields_raw(table_id)
        master = self._generate_excel_fields_master(table_id)
        if len(raw) != 0 and len(master) !=0:
            return raw+master
        elif len(raw) != 0:
            return raw
        elif len(master) != 0:
            return master
        else:
            return self._gen_prop_review(table_id)

    def find_table_by_filter(self, table_name, alias, project_id, table_state, logic_name, table_desc, backlog_name):
        """
        Return a list of tables that have this physical name or alias
        :param table_name: str, that represents the physical name of the table
        :param alias: str, that represents the alias of the table
        :param project_id: ObjectId, that represents the identifier of the project
        :param table_state: str, represents the state of the table
        :param legacy_name: str, represents the base name of the table
        :param legacy_desc: str, represents the observation field of the table
        :return: a list whit tables
        """
        table_name = str(table_name).strip()
        alias = str(alias).strip()
        table_state = str(table_state).strip()
        logic_name = str(logic_name).strip()
        table_desc = str(table_desc).strip()
        backlog_name = str(backlog_name).strip()
        tables = []

        if len(str(project_id).strip()) == 0:
            tables = self.data_dictionary_repository.find_all()
        else:
            use_cases = self.project_repository.find_project_use_cases(str(project_id).strip())
            for case in use_cases["useCases"]:
                casetmp = self.use_case_repository.find_one(str(case))
                for table_id in casetmp["tables"]:
                    table = self.data_dictionary_repository.find_one_filter(str(table_id))
                    if table not in tables:
                        tables.append(table)

        if len(table_name) != 0:
            tables = self._filter_by_table_name(tables, table_name)
        if len(alias) != 0:
            tables = self._filter_by_alias(tables, alias)
        if len(table_state) != 0:
            tables = self._filter_by_table_state(tables, table_state)
        if len(logic_name) != 0:
            tables = self._filter_by_legacy_name(tables, logic_name)
        if len(table_desc) != 0:
            tables = self._filter_by_legacy_desc(tables, table_desc)
        if len(backlog_name) != 0:
            tables = self._filter_by_backlog_name(tables, backlog_name)
        tables_query = []
        for table in tables:
            if table is not None:
                try:
                    table["legacy_name"] = self.backlog_repository.find_one(table["_id"])["baseName"]
                except:
                    table["legacy_name"] = "Unassigned"
                try:
                    email = self.user_repository.find_one(str(table["user"]))
                    table["user"] = email["email"]
                except Exception as e:
                    table["user"] = "Unassigned"
                try:
                    table["stateTable"] = self._define_table_state(table["stateTable"])
                except Exception as e:
                    table["stateTable"] = "Unassigned"
                try:
                    table["project_name"] = self._get_table_project(table["_id"])
                except Exception as e:
                    table["project_name"] = ["Unassigned"]

                table["type"] = "T"
                table["_id"] = str(table["_id"])
                try:
                    table["masterLength"] = len(table["fieldsMaster"])
                    del table["fieldsMaster"]
                except Exception:
                    table["masterLength"] = 0
                try:
                    table["rawLength"] = len(table["fieldsRaw"])
                    del table["fieldsRaw"]
                except Exception:
                    table["rawLength"] = 0

                tables_query.append(table)
        return tables_query

    def _define_table_state(self, state):
        """
        Return the name of the state of the table
        :param state: str, a letter that represents the state of the table
        :return: str, the name of the state
        """
        final_state = ""
        states = {"I": "Listo para ingestar", "IN": "Ingestado", "M": "Mallas", "P": "Producción", "Q": "Calidad",
                  "G": "Gobierno", "A": "Arquitectura", "N": "En propuesta", "RN": "VoBo Negocio", "F": "Gobierno Funcional"}
        if state in states:
            final_state = states[state]
        return final_state

    def _get_table_project(self, table_id):
        """
        Get the project name of the table
        :param table_id: ObjectId, identifier of the table
        :return: str, name of the project
        """
        project_name = ""
        project_name_arr = []
        table_use_case = None
        table_uc_arr = []
        uses_cases = self.use_case_repository.find_all()
        break_use_case = False
        for useCase in uses_cases:
            case_tmp = self.use_case_repository.find_one(useCase["_id"])
            #if break_use_case:
                #break
            for table in case_tmp["tables"]:
                if str(table_id) == str(table):
                    table_uc_arr.append(case_tmp)
                    table_use_case = case_tmp
                    break_use_case = True
                    #break

        projects = self.project_repository.find_all()
        break_project = False
        for project in projects:
            #if break_project:
                #break
            for use_case in project["useCases"]:
                if str(table_use_case["_id"]) == str(use_case):
                    project_name = project["name"]
                    break_project = True
                    #break
                for mini_uc in table_uc_arr: #This is the one being used
                    if str(mini_uc["_id"]) == str(use_case):
                        if not project["name"] in project_name_arr:
                            project_name_arr.append(project["name"])

        #return project_name
        return project_name_arr

    def _filter_by_table_name(self, tables, table_name):
        """
        Get the tables with the parameter
        :param tables: Array, tables
        :param table_name: str, the physical name of the table
        :return: an array of tables
        """
        res = []
        for t in tables:
            if t is not None and t not in res:
                if "raw_name" in t:
                    if table_name.lower() in t["raw_name"].lower():
                        res.append(t)
                elif "master_name" in t:
                    if table_name.lower() in t["master_name"].lower():
                        res.append(t)
        return res

    def _filter_by_alias(self, tables, alias):
        """
        Get the tables with the parameter
        :param tables: Array, tables
        :param alias: str, the alias of the table
        :return: an array of tables
        """
        res = []
        for t in tables:
            if t is not None and "alias" in t:
                if alias.lower() in t["alias"].lower():
                    res.append(t)
        return res

    def _filter_by_table_state(self, tables, table_state):
        """
        Get the tables with the parameter
        :param tables: Array, tables
        :param table_state: str, the state of the table
        :return: an array of tables
        """
        res = []
        for t in tables:
            if t is not None and "stateTable" in t:
                if table_state.upper() == "D":
                    project = self.__is_discard_table(t["_id"])
                    if project:
                        res.append(t)
                elif table_state.lower() == t["stateTable"].lower():
                    res.append(t)
        return res

    def __is_discard_table(self, table_id):
        """

        :param table_id:
        :return:
        """
        unassigned = self.project_repository.find_any_query({"name": "Unassigned"})
        flag = False
        if unassigned.count() != 0:
            for j in unassigned[0]["useCases"]:
                temp = self.use_case_repository.find_any_query({"$and": [{"_id": ObjectId(str(j))}, {"tables": {"$in": [ObjectId(str(table_id))]}}]})
                if temp.count() != 0:
                    flag = True
        return flag

    def _filter_by_legacy_name(self, tables, logic_name):
        """
        Get the tables with the parameter
        :param tables: Array, tables
        :param legacy_name_name: str, the base name of the table
        :return: an array of tables
        """
        res = []
        for t in tables:
            if t is not None and "baseName" in t:
                if logic_name.lower() in t["baseName"].lower():
                    res.append(t)
        return res

    def _filter_by_legacy_desc(self, tables, table_desc):
        """
        Get the tables with the parameter
        :param tables: Array, tables
        :param legacy_desc: str, the observation field of the table
        :return: an array of tables
        """
        res = []
        for t in tables:
            if t is not None and "observationField" in t:
                if table_desc.lower() in t["observationField"].lower():
                    res.append(t)
        return res

    def _filter_by_backlog_name(self, tables, backlog_name):
        """
        Get the tables with the parameter
        :param tables: Array, tables
        :param legacy_desc: str, the observation field of the table
        :return: an array of tables
        """
        res = []
        for t in tables:
            if t is not None and "_id" in t:
                tmp_backlog = self.backlog_repository.find_one(str(t["_id"]))
                if tmp_backlog is not None:
                    if backlog_name.lower() in tmp_backlog["baseName"].lower():
                        res.append(t)

        return res

    def update_validation_po(self, table_id, data):
        """

        :param table_id:
        :param data:
        :return:
        """
        user_id = self.user_repository.find_one(data["user"])
        if user_id is None:
            raise ValueError("Permisos denegados para el usuario.")

        data_dict = self.data_dictionary_repository.find_one(table_id)
        if data_dict is None:
            raise ValueError("La tabla no se encuentra registrada.")

        flag = True
        for k in data_dict["people"]:
            if str(user_id["_id"]) == str(k["user"]):
                flag = False
        if flag:
            data_dict["people"].append({"user": ObjectId(str(user_id["_id"])), "rol": user_id["rol"]})

        try:
            temp = self.data_dictionary_repository.find_one(table_id)
            array_name = "fieldsRaw" if len(temp["fieldsRaw"]) > 0 else "fieldsMaster"

            modi = temp[array_name][data["column"]]["modification"]
            modification = []
            modification_temp = {
                "user": ObjectId(str(data["user"])),
                "startDate": datetime.datetime.now(),
                "state": modi[0]["state"],
                "stateValidation": "YES"
            }
            modification.append(modification_temp)
            for j in range(0, 4):
                if len(modi) > j:
                    modification.append(modi[j])

            self.data_dictionary_repository.update_modifications(table_id, array_name, modification, data["column"])
            #self.data_dictionary_repository.update_people(table_id, data_dict["people"])
        except Exception as e:
            print(e, "NO se ingreso la modificación...")
        self.data_dictionary_repository.update_check_status(table_id, array_name, data["user"], data["status"], data["column"], data["comments"])

    def update_head_functional(self, table_id, information):
        """

        :param table_id:
        :param information:
        :return:
        """
        data_dic = self.data_dictionary_repository.find_one(table_id)
        if data_dic is None:
            raise ValueError("La tabla no se encuentra registrada.")

        user_id = self.user_repository.find_one(information["user_id"])
        if user_id is None:
            raise ValueError("Permisos denegados para el usuario.")

        flag = True
        for k in data_dic["people"]:
            if user_id == str(k["user"]):
                flag = False
        if flag:
            data_dic["people"].append({"user": ObjectId(user_id["_id"]), "rol": user_id["rol"]})

        user_id = user_id["_id"]

        temp_mod = [{"user": ObjectId(str(user_id)),
                     "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]

        for i in data_dic["modifications"]:
            temp_mod.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]})

        tmp = self.functional_map_repository.find_uuaa(information["uuaaMaster"].upper()[1:])
        if tmp is None:
            raise ValueError("La UUAA de master {0} no se encuentra registrada.".format(information["uuaaMaster"]))

        update_fields_str = {
            "baseName": information["baseName"],
            "observationField": information["observationField"],
            "uuaaMaster": information["uuaaMaster"],
            "periodicity": information["periodicity"],
            "uuaaRaw":  information["uuaaRaw"],
            "perimeter": information["perimeter"],
            "tacticalObject": information["tacticalObject"],
            "information_level": information["information_level"],
            "objectType": information["objectType"],
            "typeFile": information["typeFile"],
            "required_depth": int(information["required_depth"]),
            "current_depth": int(information["current_depth"]),
            "estimated_volume_records": int(information["estimated_volume_records"]),
            "originSystem": information["originSystem"],
            "periodicity_master": information["periodicity_master"],
            "modifications": temp_mod[:10],
            "people": data_dic["people"],
            "data_source": tmp["data_source"],
            "information_group_level_1": tmp["level1"],
            "information_group_level_2": tmp["level2"],
        }
        self.data_dictionary_repository.update_field(table_id, update_fields_str)

    def find_fields_functional(self, table_id):
        """

        :param table_id:
        :return:
        """
        query = [{'$match': {'_id': ObjectId(str(table_id))}},
                 {'$project': {
                    'FieldReview.column': 1,
                    'FieldReview.length': 1,
                    'FieldReview.startPosition': 1,
                    'FieldReview.endPosition': 1,
                    'FieldReview.originType': 1,
                    'FieldReview.outRec': 1,
                    'FieldReview.outVarchar': 1,
                    'FieldReview.outLength': 1,
                    'FieldReview.outFormat': 1,
                    'FieldReview.legacy': 1,
                    'FieldReview.catalogue': 1,
                    'FieldReview.format': 1,
                    'FieldReview.key': 1,
                    'FieldReview.mandatory': 1,
                    'FieldReview.decimals': 1,
                    'FieldReview.integers': 1,
                    'FieldReview.symbol': 1,
                    'FieldReview.modification': 1,
                    'FieldReview.comments': 1,
                    'FieldReview.checkFunctional': 1,
                    '_id': 0
                 }}]
        data_dic = self.data_dictionary_repository.generate_aggreation(query)
        if data_dic is None:
            raise ValueError("La tabla no se encuentra registrada.")

        res = []
        for k in data_dic:
            for h in k["FieldReview"]:
                for j in h["modification"]:
                    try:
                        j["name"] = self.user_repository.find_one(j["user"])["name"]
                    except:
                        j["name"] = "Unassigned"
                for p in h["comments"]:
                    try:
                        p["name"] = self.user_repository.find_one(p["user"])["name"]
                    except:
                        p["name"] = "Unassigned"
                res.append(h)
        return res

    def find_tables_state_only(self, state):
        """
            Finds all tables in a specific state only

        :param state: str, String that reference a state to get it
        :return: Array of json, contains id's and name of data dictionaries tables
        """
        tables = self.data_dictionary_repository.find_tables_state(state)
        resTables = []
        for table in tables:
            try:
                if table is not None:
                    try:
                        table["reviewLength"] = len(table["FieldReview"])
                        del table["FieldReview"]
                    except:
                        table["reviewLength"] = 0
                    try:
                        table["rawLength"] = len(table["fieldsRaw"])
                        del table["fieldsRaw"]
                    except:
                        table["rawLength"] = 0
                    try:
                        table["masterLength"] = len(table["fieldsMaster"])
                        del table["fieldsMaster"]
                    except:
                        table["masterLength"] = 0
                    try:
                        table["last_mod"] = table["modifications"][0]["date"]
                        del table["modifications"]
                    except:
                        table["last_mod"] = "1999-01-01 00:00:00"

                resTables.append(table)
            except Exception as e:
                print(table["alias"] + e)
        try:
            resTables.sort(key=lambda x: datetime.datetime.strptime(x['last_mod'], '%Y-%m-%d %H:%M:%S'))
        except Exception as e:
            print(e)

        return resTables[::-1]

    def update_fields_functional(self, table_id, fields):
        """

        :param table_id:
        :param fields:
        :return:
        """
        data_dic = self.data_dictionary_repository.find_one(table_id)
        if data_dic is None:
            raise ValueError("La tabla no se encuentra registrada.")

        user_id = self.user_repository.find_one(fields["user_id"])
        if user_id is None:
            raise ValueError("Permisos denegados para el usuario.")

        flag = True
        for k in data_dic["people"]:
            if str(user_id["_id"]) == str(k["user"]):
                flag = False
        if flag:
            data_dic["people"].append({"user": ObjectId(str(user_id["_id"])), "rol": user_id["rol"]})

        user_id = user_id["_id"]

        temp_mod = [{"user": ObjectId(str(user_id)),
                     "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}]

        for i in data_dic["modifications"]:
            temp_mod.append({
                "user": ObjectId(str(i["user"])),
                "date": i["date"]})

        update_query = {}
        for j in fields["row"]:
            comments = []
            for k in j['comments']:
                if "name" in k:
                    del k["name"]
                k["user"] = ObjectId(str(k["user"]))
                comments.append(k)

            modifications = []
            for e in j['modification']:
                if "name" in e:
                    del e["name"]
                e["user"] = ObjectId(str(e["user"]))
                modifications.append(e)
            query1 = self.data_dictionary_repository.find_legacys(table_id, "FieldReview", j["legacy"]["legacy"])
            query2 = self.data_dictionary_repository.find_legacys(table_id, "fieldsRaw", j["legacy"]["legacy"])
            query3 = self.data_dictionary_repository.find_legacys(table_id, "fieldsMaster", j["legacy"]["legacy"])
            if query1.count() != 0 or query2.count() != 0 or query3.count() != 0:
                j["legacy"]["repeat"] = "Y"
            update_query['FieldReview.'+str(j['column'])+'.length'] = j['length']
            update_query['FieldReview.'+str(j['column'])+'.startPosition'] = j['startPosition']
            update_query['FieldReview.'+str(j['column'])+'.endPosition'] = j['endPosition']
            update_query['FieldReview.'+str(j['column'])+'.originType'] = j['originType']
            update_query['FieldReview.'+str(j['column'])+'.outRec'] = j['outRec']
            update_query['FieldReview.'+str(j['column'])+'.outVarchar'] = j['outVarchar']
            update_query['FieldReview.'+str(j['column'])+'.outLength'] = j['outLength']
            update_query['FieldReview.'+str(j['column'])+'.outFormat'] = j['outFormat']
            update_query['FieldReview.'+str(j['column'])+'.legacy'] = j['legacy']
            update_query['FieldReview.'+str(j['column'])+'.catalogue'] = j['catalogue']
            update_query['FieldReview.'+str(j['column'])+'.format'] = j['format']
            update_query['FieldReview.'+str(j['column'])+'.key'] = j['key']
            update_query['FieldReview.'+str(j['column'])+'.mandatory'] = j['mandatory']
            update_query['FieldReview.'+str(j['column'])+'.decimals'] = j['decimals']
            update_query['FieldReview.'+str(j['column'])+'.integers'] = j['integers']
            update_query['FieldReview.'+str(j['column'])+'.symbol'] = j['symbol']
            update_query['FieldReview.'+str(j['column'])+'.modification'] = modifications
            update_query['FieldReview.'+str(j['column'])+'.comments'] = comments
            update_query['FieldReview.'+str(j['column'])+'.checkFunctional'] = j['checkFunctional'] #For gov. functional
        self.data_dictionary_repository.update_field(table_id, update_query)
        self.data_dictionary_repository.update_modifications_audit(table_id, temp_mod[:10])
        self.data_dictionary_repository.update_people(table_id, data_dic["people"])


    def find_uuaas_raw(self, uuaa_name, app_name, level1, level2):
        """Get raw uuaas based on query params

        :param table_id: str, String that reference a table id
        :return: Array, all uuaa that can change
        """
        uuaa_name = str(uuaa_name).strip()
        app_name = str(app_name).strip()
        level1 = str(level1).strip()
        level2 = str(level2).strip()

        uuaas = []
        uuaas = self.functional_map_raw_repository.find_all()

        if len(uuaa_name) != 0:
            uuaas = self._filter_by_uuaa_name(uuaas, uuaa_name)
        if len(app_name) != 0:
            uuaas = self._filter_by_property_name(uuaas, app_name, "app")
        if len(level1) != 0:
            uuaas = self._filter_by_property_name(uuaas, level1, "level1")
        if len(level2) != 0:
            uuaas = self._filter_by_property_name(uuaas, level2, "level2")

        uuaas_query = []
        uuaas_query = uuaas

        return uuaas_query
    

    def find_uuaas_master(self, uuaa_name, scope_name, level2, level3):
        """Get raw uuaas based on query params

        :param table_id: str, String that reference a table id
        :return: Array, all uuaa that can change
        """
        uuaa_name = str(uuaa_name).strip()
        scope_name = str(scope_name).strip()
        level2 = str(level2).strip()
        level3 = str(level3).strip()

        uuaas = []
        uuaas = self.functional_map_master_repository.find_all()

        if len(uuaa_name) != 0:
            uuaas = self._filter_by_uuaa_name(uuaas, uuaa_name)
        if len(scope_name) != 0:
            uuaas = self._filter_by_property_name(uuaas, scope_name, "scope")
        if len(level2) != 0:
            uuaas = self._filter_by_property_name(uuaas, level2, "level2")
        if len(level3) != 0:
            uuaas = self._filter_by_property_name(uuaas, level3, "level3")

        uuaas_query = []
        uuaas_query = uuaas

        return uuaas_query

    
    def _filter_by_uuaa_name(self, uuaas, uuaa_name):
        """
        Get the tables with the parameter
        :param tables: Array, tables
        :param table_name: str, the physical name of the table
        :return: an array of tables
        """
        res = []
        for ua in uuaas:
            if ua is not None and ua not in res:
                if "uuaa" in ua:
                    if uuaa_name.lower() in ua["uuaa"].lower():
                        res.append(ua)
        return res

    def _filter_by_property_name(self, uuaas, prop_name, property):
        """
        Get the tables with the parameter
        :param tables: Array, tables
        :param table_name: str, the physical name of the table
        :return: an array of tables
        """
        res = []
        for ua in uuaas:
            if ua is not None and ua not in res:
                if property in ua:
                    if prop_name.lower() in ua[property].lower():
                        res.append(ua)
        return res

    def find_raw_property_options(self, property_name):
        """Get raw uuaas based on query params

        :param table_id: str, String that reference a table id
        :return: Array, all uuaa that can change
        """
        res = []
        res = self.functional_map_raw_repository.find_distinct_property(property_name)
        return res
    
    def find_master_level_options(self):
        """Get raw uuaas based on query params

        :param table_id: str, String that reference a table id
        :return: Array, all uuaa that can change
        """
        res2 = []
        res2 = self.functional_map_master_repository.find_distinct_property("level2")
        res3 = []
        res3 = self.functional_map_master_repository.find_distinct_property("level3")
        
        res = {"level2": res2, "level3": res3}
        return res
  
    def new_uuaa_raw(self, data):
        """Get raw uuaas based on query params

        :param table_id: str, String that reference a table id
        :return: Array, all uuaa that can change
        """
        check_uuaa = self.functional_map_raw_repository.find_uuaa(data["uuaa"])
        if check_uuaa is not None:
            raise ValueError("La UUAA con el nombre " + data["uuaa"] + " ya se encuentra registrada.")
        
        data["insertDate"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        temp_mod = {"user_id": data["user_id"],
                    "type": "Create UUAA",
                    "additional_data": str(data),
                    "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
        try:
            data["modifications"].append(temp_mod)
        except Exception:
            data["modifications"] = []
            data["modifications"].append(temp_mod)

        del data["user_id"]
        new_id = self.functional_map_raw_repository.insert_one(data)
        uuaa_dic = self.functional_map_raw_repository.find_one(new_id)
        uuaa_dic["_id"] = str(uuaa_dic["_id"])
        return uuaa_dic

    def update_uuaa_raw(self, uuaa_id, data):
        """Update data dictionary data without generate new names
        except phases

        :param table_id: str, string that reference a table id
        :param data: json, dictionary with data to update
        :return:
        """
        user_id = self.user_repository.find_one(data["user_id"])
        if user_id is None:
            raise ValueError("Permisos denegados para el usuario.")

        errVal = 0
        uuaa_dic = self.functional_map_raw_repository.find_one(uuaa_id)
        if uuaa_dic is None:
            raise ValueError("La uuaa no existe.")
        else:
            if data["uuaa"] == uuaa_dic["uuaa"]:
                errVal = 1
            
        uuaas = self._filter_by_property_name(self.functional_map_raw_repository.find_all(), data["uuaa"], "uuaa")
        flago = len(uuaas) > errVal

        if flago:
            raise ValueError("El nombre de la UUAA esta repetida.")
        else:
            temp_mod = {"user_id": data["user_id"],
                        "modType": "Detail Update",
                        "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
            try:
                data["modifications"].append(temp_mod)
            except Exception:
                data["modifications"] = []
                data["modifications"].append(temp_mod)

            if len(data["modifications"]) > 6:
                data["modifications"].pop()

            self.functional_map_raw_repository.update_uuaa(uuaa_id, data)
            uuaa_dic = self.functional_map_raw_repository.find_one(uuaa_id)
        
        uuaa_dic["_id"] = str(uuaa_dic["_id"])
        return uuaa_dic
    
    def update_uuaa_master(self, uuaa_id, data):
        """Update data dictionary data without generate new names
        except phases

        :param table_id: str, string that reference a table id
        :param data: json, dictionary with data to update
        :return:
        """
        uuaa_dic = self.functional_map_master_repository.find_one(uuaa_id)
        if uuaa_dic is None:
            raise ValueError("La uuaa no existe.")

        user_id = self.user_repository.find_one(data["user_id"])
        if user_id is None:
            raise ValueError("Permisos denegados para el usuario.")

        uuaas = self._filter_by_uuaa_name(self.functional_map_master_repository.find_all(), data["uuaa"])
        flago = len(uuaas) > 1

        if flago:
            raise ValueError("El nombre de la UUAA esta repetida.")
        else:
            try:
                temp_mod = {"user_id": data["user_id"],
                        "modType": "Detail Update",
                        "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
                data["modifications"].append(temp_mod)
            except Exception:
                data["modifications"] = []
                data["modifications"].append(temp_mod)

            if len(data["modifications"]) > 6:
                data["modifications"].pop()

            self.functional_map_master_repository.update_uuaa(uuaa_id, data)
            uuaa_dic = self.functional_map_master_repository.find_one(uuaa_id)
        
        uuaa_dic["_id"] = str(uuaa_dic["_id"])
        return uuaa_dic

    def new_uuaa_master(self, data):
        """Get raw uuaas based on query params

        :param table_id: str, String that reference a table id
        :return: Array, all uuaa that can change
        """
        data["insertDate"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        data["modifications"] = []
        temp_mod = {"user_id": data["user_id"],
                        "modType": "Detail Create",
                        "date": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
        data["modifications"].append(temp_mod)
        new_id = self.functional_map_master_repository.insert_one(data)
        uuaa_dic = self.functional_map_master_repository.find_one(new_id)
        uuaa_dic["_id"] = str(uuaa_dic["_id"])
        return uuaa_dic

    def find_dd_property_options(self, property_name):
        """Get raw uuaas based on query params

        :param table_id: str, String that reference a table id
        :return: Array, all uuaa that can change
        """
        res = []
        res = self.data_dictionary_repository.find_distinct_property(property_name)
        return res
    
    def delete_raw(self, table_id, data):
        """Modifies fieldsMaster to an empty array, effectively deleting its master phase

        :param table_id: str, String that reference a table id
        :return: Array, all uuaa that can change
        """
        self.data_dictionary_repository.update_raw(table_id, data)
        return "ok"

    def update_master(self, table_id, data):
        """Modifies fieldsMaster to an empty array, effectively deleting its master phase

        :param table_id: str, String that reference a table id
        :return: Array, all uuaa that can change
        """
        self.data_dictionary_repository.update_master(table_id, data)
        return "ok"
    
    def get_uuaa(self, uuaa_id):
        """Get raw uuaas based on query params

        :param table_id: str, String that reference a table id
        :return: Array, all uuaa that can change
        """
        check_uuaa = self.functional_map_raw_repository.find_one(uuaa_id)
        check_uuaa["_id"] = str(check_uuaa["_id"])
        return check_uuaa


    def new_upload_data_dictionary(self, data, user_id, project_owner):
        """
            Creates or just loads a data table without calculations from a sophia file
        :param data: dict
        :param user_id: id of the user creating it
        :param project_owner: id of the project which this data table belongs to
        :return: new data table id
        """
        data_dictionary = self.init_data_dictionary_info(data)
        try:
            temp_review = data_dictionary["FieldReview"]
        except:
            temp_review = []
            
        try:
            temp_raw = data_dictionary["fieldsRaw"]
        except:
            temp_raw = []
            
        try:
            temp_master = data_dictionary["fieldsMaster"]
        except:
            temp_master = []
        
        data["newData"] = self.datum_transformations(data["newData"], data["datum"])

        if data["load_type"] == "full" or data["load_type"] == "object":
            data_dictionary = self.new_create_structure_head(data["newData"])
            try:
                data_dictionary["FieldReview"] = temp_review
                data_dictionary["fieldsRaw"]  =  temp_raw
                data_dictionary["fieldsMaster"] = temp_master
            except Exception as e:
                print("DATA" + str(e))
                pass

        if data["load_type"] == "full" or data["load_type"] == "fields":
            data_dictionary["FieldReview"] = self.new_create_structure_fields(data["newData"])

        data_dictionary["user"] = ObjectId(str(user_id))
        data_dictionary["modifications"] = self.add_modification(data_dictionary["modifications"], {"user": data_dictionary["user"], "type": "LOAD SOPHIA", "details": "LoadType: " + data["load_type"]})
        data_dic = self.data_dictionary_repository.find_one_by_alias(data_dictionary["alias"])
        if not data_dic:
            data_dictionary_id = self.data_dictionary_repository.insert_one(data_dictionary)
        else:
            data_dictionary_id = data_dic["_id"]
            self.data_dictionary_repository.update_field(str(data_dictionary_id), data_dictionary)

        if data["load_type"] == "full" or data["load_type"] == "fields":
            fases = self.get_new_fields_fases(data["newData"][1])
            if fases["raw"] == True:
                self.generate_new_fase(data_dictionary_id, {'from': 'FieldReview', 'to': 'raw', 'user_id': '5c2f81db5862a60969ae84f2'})
            if fases["master"] == True:
                self.generate_new_fase(data_dictionary_id, {'from': 'fieldsRaw', 'to': 'master', 'user_id': '5c2f81db5862a60969ae84f2'})
            self.add_generated_fields(data_dictionary_id, data["newData"][1])

        return str(data_dictionary_id)

    def init_data_dictionary_info(self, data):
        """
            Check if the data_dictionary is in the DB and retrieve that info otherwise initialize a 
            blank data_dictionary with the new info to be loaded
        :param data: dict
        :return: data_dictionary: dictionary object initialized
        """
        alias = data["alias"]
        if alias.strip() == '':
            raise ValueError("El alias de la tabla esta vacio, favor diligenciar: {}.".format(data_dictionary["alias"]))

        data_dictionary = {}
        data_dictionary_from_alias = self.data_dictionary_repository.find_one_by_alias(alias)

        if data_dictionary_from_alias is not None:
            data_dictionary = data_dictionary_from_alias
            print(f"Found in repository with 'ALIAS': '{alias}'")
        else:
            # Modifications to load NEW tables with no info in the repository
            data_dictionary["FieldReview"] = []
            data_dictionary["fieldsRaw"] = []
            data_dictionary["fieldsMaster"] = []
            data_dictionary["modifications"] = []

        return data_dictionary

    def get_new_fields_fases(self, fields):
        """
            Finds the fases to generate namings for a table
        :param data: dict
        :return: object, contains 'raw' and 'master' flags to indetify the respective fases found
        """
        raw_found = False
        master_found = False
        
        for field in fields:
            if "raw" in field["STORAGE ZONE"].lower():
                raw_found = True
            elif "master" in field["STORAGE ZONE"].lower():
                master_found = True
            
        return {"raw": raw_found, "master": master_found}


    def clean_raw_master_fields(self, fields):
        """
            Finds the fases to generate namings for a table
        :param data: dict
        :return: object, contains 'raw' and 'master' flags to indetify the respective fases found
        """
        fases = self.get_new_fields_fases(fields)
        storage_zone = "MASTERDATA" if fases["master"] == True else "RAWDATA" 
        final_fields = []
        
        for field in fields:
            if str(field["STORAGE ZONE"]).upper() == storage_zone:
                final_fields.append(field)
                
        return final_fields
        
    
    def new_create_structure_head(self, data):
        """
            Creates a new data table head object from data to load
        :param data: dict
        :return:
        """
        if data[-1]["datum"]: # Pregunta si es un objeto de DATUM
            try:
                object_data = data[0][1] # Porque en DATUM se ordenan primero MASTER y luego RAW
            except:
                object_data = data[0][0] # Caso OBJETO con una sola FASE

            new_data_dictionary = Object_data_dictionary({}).data
            for key in object_data:
                try:
                    new_data_dictionary[self.datum_props_object[key]] = str(object_data[key]).strip()
                except Exception: pass
        else:
            object_data = data[0][0] #to change
            new_data_dictionary = Object_data_dictionary({}).data

            for key in object_data:
                try:
                    new_data_dictionary[self.sophia_props_object[key]] = object_data[key].strip()
                except Exception: pass
        try:
            del new_data_dictionary[""]
        except Exception: pass
        new_data_dictionary = self.clean_new_head(new_data_dictionary)

        return new_data_dictionary

    def clean_new_head(self, data):
        """
            Cleans some data of a new data dictionary object previously loaded
        :param data: dict
        :return: object cleaned, dict
        """
        #UUAA Raw
        data["uuaaRaw"] = data["raw_name"].split("_")[1].upper(); 
        
        #Integer types
        data["current_depth"] = 0 if len(str(data["current_depth"]).strip()) == 0 else int(str(data["current_depth"]).strip().split(" ")[0])  
        data["required_depth"] = 0 if len(str(data["required_depth"]).strip()) == 0 else int(str(data["required_depth"]).strip().split(" ")[0])  
        data["estimated_volume_records"] = 0 if len(str(data["estimated_volume_records"]).strip()) == 0 else int(str(data["estimated_volume_records"]).strip().split(" ")[0])  

        #Loading type in lower case
        data["loading_type"] = data["loading_type"].lower() 
        data["typeFile"] = data["typeFile"].lower() 
        
        #Proper spanish periodicity based on the database if needed
        temp_per = self.excels_repository.find_state_periodicity(data["periodicity"].upper())
        if temp_per is not None:
            data["periodicity"] = temp_per["periodicity"]
            data["periodicity_master"] = temp_per["periodicity"]
            
        #Final comment
        data["raw_comment"] = "Carga mediante archivos: " + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        return data
    
    def generate_fases_sophia_load(self, fields):
        """
            Generates fases
        :param old_mod: json array, current/old modifications array to be worked on
        :param new_mod: json, new modification data
        :return: array, with the updates values for direct replacement with the original
        """
    
    def add_generated_fields(self, data_dictionary_id, fields):
        new_tmp_field = Field_review({}).data
        
        for field in fields:
            if field["GENERATED FIELD"] == "YES":
                if str(field["STORAGE ZONE"]).upper() == "RAWDATA":
                    # print("raw gene")
                    self.add_generated_field("raw", data_dictionary_id, field)
                if str(field["STORAGE ZONE"]).upper() == "MASTERDATA":
                    # print("master gene")
                    self.add_generated_field("master", data_dictionary_id, field)
                    

    def add_generated_field(self, fase, data_dictionary_id, field):
        data_dictionary = self.data_dictionary_repository.find_one(data_dictionary_id)
        field_property = "" 
        found_pos = -1
        
        if fase == "raw":
            new_field = Field_raw({}).data
            field_property = "fieldsRaw"
        elif fase == "master":
            new_field = Field_master({}).data
            field_property = "fieldsMaster"
        fields = data_dictionary[field_property]
        for pos in range(0, len(fields)):
            if str(fields[pos]["naming"]["naming"]) == str(field["PHYSICAL NAME FIELD"]):
                found_pos = pos
                break
        
        if found_pos >= 0:
            data_dictionary[field_property][found_pos]["origin"] = ""
            data_dictionary[field_property][found_pos]["generatedField"] = "YES"
            data_dictionary[field_property][found_pos]["legacy"] = {"legacy": "Calculated", "legacyDescription": "Calculated"}
        else:
            for key in field:
                try:
                    new_field[self.sophia_props_outnaming[key]] = str(field[key]).strip()
                except Exception as e:
                    print("SophiaLoad: Excel columna not found: " + str(e))
            new_field = self.clean_new_field(new_field, "5c2f81db5862a60969ae84f2")
            new_field["column"] = len(data_dictionary[field_property])
            new_field["generatedField"] = "YES"
            new_field["legacy"] = {"legacy": "Calculated", "legacyDescription": "Calculated"}
            data_dictionary[field_property].append(new_field)
        
        self.data_dictionary_repository.update_field(str(data_dictionary["_id"]), data_dictionary)

        
    
    def new_create_structure_fields(self, data):
        """
            Updates the modification array for a data dictionary with the given data for
        :param old_mod: json array, current/old modifications array to be worked on
        :param new_mod: json, new modification data
        :return: array, with the updates values for direct replacement with the original
        """
        fields_data = data[1] #to change
        fields_data = self.clean_raw_master_fields(fields_data)
        new_fields = []

        for naming in fields_data:
            new_tmp_field = Field_review({}).data
            try:
                if str(naming["GENERATED FIELD"].upper()) == "NO" or str(naming["GENERATED FIELD"].upper()) == "FALSE":
                    for key in naming:
                        if data[-1]['datum']:
                            try:
                                new_tmp_field[self.datum_props_outnaming[key]] = str(naming[key]).strip()
                            except Exception as e:
                                print("DatumLoad: Excel columna not found: " + str(e))
                        else:
                            try:
                                new_tmp_field[self.sophia_props_outnaming[key]] = str(naming[key]).strip()
                            except Exception as e:
                                print("SophiaLoad: Excel columna not found: " + str(e))
                    new_tmp_field = self.clean_new_field(new_tmp_field, "5c2f81db5862a60969ae84f2")
                    new_fields.append(new_tmp_field)
            except Exception as e:
                print("SophiaLoad: Excel columna GENERATED FIELD not found: " + str(e))
        new_fields = self.sort_new_fields(new_fields)
        new_fields = self.clean_start_end_positions_fields(new_fields)

        return new_fields

    def clean_new_field(self, field, user_id):
        """
            Cleans some data of a new data table object previously loaded
        :param data: dict
        :return: object cleaned, dict
        """
        #Load naming from DB and clean data types
        field = self.load_naming(field)
        field = self.clean_new_field_types(field)
        field = self.clean_length_type_fields(field)

        #Column
        field["column"] = int(field["column"]) - 1
        
        #Generated Field
        field["generatedField"] = "NO"

        #Catalogue
        if field["catalogue"].strip() == "":
            field["catalogue"] = "N/A" 
        
        #Legacy
        field["legacy"] = {"legacy": field["legacy"], "legacyDescription": "empty"}
        
        #Clean natural words to 1 or 0
        field["key"] = self.yes_no_1_0[field["key"].strip().upper()]
        field["mandatory"] = self.yes_no_1_0[field["mandatory"].strip().upper()]
        field["tds"] = self.yes_no_1_0[field["tds"].strip().upper()]

        #Naming Validation flag
        state = "GL" if field["naming"]["isGlobal"] == "Y" else "PS"
        field["modification"].append({"user": ObjectId(user_id), "startDate": datetime.datetime.now(), "state": state, "stateValidation": "YES"})
        
        del field[""]
        
        return field
    
    def load_naming(self, field):
        """
            Loads naming data from the Database to fill in to the proper naming structure inside a single field
        :param data: dict
        :return: object cleaned, dict
        """
        new_naming = Naming({}).data
        global_naming = self.field_repository.find_exactly_naming(field["naming"].strip().lower())

        if global_naming is not None:
            new_naming["naming"] = global_naming["naming"]["naming"]
            new_naming["isGlobal"] = "Y"
            new_naming["code"] = global_naming["code"]
            new_naming["suffix"] = global_naming["naming"]["suffix"]
            new_naming["Words"] = global_naming["naming"]["Words"]
            new_naming["hierarchy"] = global_naming["level"]

            field["logic"] = global_naming["logic"]
            field["description"] = global_naming["originalDesc"].replace("~", "\n")

            if "dataOwnerInformation" in global_naming and len(global_naming["dataOwnerInformation"]) != 0:
                field["domain"] = global_naming["dataOwnerInformation"][0]["domain"]
                if len(field["domain"]) == 0:
                    field["domain"] = "Unassigned"
                field["subdomain"] = global_naming["dataOwnerInformation"][0]["subdomain"]
                field["ownership"] = global_naming["dataOwnerInformation"][0]["ownership"]
                field["conceptualEntity"] = global_naming["dataOwnerInformation"][0]["conceptualEntity"]
                field["operationalEntity"] = global_naming["dataOwnerInformation"][0]["operationalEntity"]
        else:
            new_naming["naming"] = field["naming"]
            suffix_id = self.suffix_repository.find_one(field["naming"].strip().split("_")[-1])
            new_naming["suffix"] = suffix_id["_id"]

            field["logic"] = field["logic"].strip()
            field["description"] = field["description"].strip()

        field["naming"] = new_naming
        
        return field
    
    def clean_new_field_types(self, field):
        """
            Cleans data types of a new data field previously loaded
        :param field: dict
        :return: field cleaned, dict
        """
        
        field["destinationType"] = field["destinationType"].strip().upper()
        try:
            field["length"] = self.type_lengths[field["outFormat"].strip()]
        except Exception:
            try:
                field["length"] = int(field["outFormat"].strip()[:-1].strip().split("(")[1])  
            except Exception:
                field["length"] = 1

        if "DECIMAL" in field["destinationType"]:
            field["destinationType"] = "DECIMAL"
            field["length"] = int(field["outFormat"].strip().split("(")[1][:-1].split(",")[0])
        elif "INT32" in field["destinationType"] or "INT" in field["destinationType"]:
            field["destinationType"] = "INT32"
                   
        return field
    
    def clean_length_type_fields(self, field):
        """
            Adds the proper length of a field based on its data type
        :param array: json array
        :return: field cleaned, dict
        """
        dataType = field["outFormat"].split('(')
        if len(dataType) > 1:
            dataType = dataType[1].split(')')[0]
            if ',' in dataType:
                field["length"] = int(dataType.split(',')[0])
                field["destinationType"] = "DECIMAL(p,s)"
                field["integers"] = int(dataType.split(',')[0])
                field["decimals"] = int(dataType.split(',')[1])
            else:
                field["length"] = int(dataType)
                if 'DECIMAL' in field["outFormat"]:
                    field["destinationType"] = "DECIMAL(p)"
                    field["integers"] = int(dataType.split(',')[0])
        elif "DATE" in field["outFormat"]:
            field["length"] = 10
        
        field["governanceFormat"] = field["outFormat"]
        field["outFormat"] = "ALPHANUMERIC(" + str(field["length"]) + ")"
        field["originType"] = field["originType"].upper()
        
        return field

    def sort_new_fields(self, array):
        """
            Sorts the fields based on their column number
        :param array: json array
        :return: array, with sorted fields
        """
        dirty = True
        while dirty:
            dirty = False
            for pos in range(0, len(array)):
                if array[pos]["column"] != pos:
                    dirty = True
                    array[array[pos]["column"]], array[pos] = array[pos], array[array[pos]["column"]]
                    
        return array
    
    def clean_start_end_positions_fields(self, array):
        """
            Adds proper start and end positions to an array of fields already sorted out
        :param array: json array
        :return: array, with sorted position fields
        """
        for pos in range(0, len(array)):
            if array[pos]["column"] == 0:
                array[pos]["endPosition"] = array[pos]["length"]
            elif array[pos]["column"] != 0:
                array[pos]["startPosition"] = array[pos-1]["endPosition"] + 1
                array[pos]["endPosition"] = array[pos]["startPosition"] + array[pos]["length"] 
            
        return array
    
    def clean_null_values_data_dictionary(self, table_id):
        data_dic = self.data_dictionary_repository.find_one(table_id)
        
        if data_dic["current_depth"] is None:
            data_dic["current_depth"] = 0
        
        if data_dic["estimated_volume_records"] is None:
            data_dic["estimated_volume_records"] = 0
        
        if data_dic["required_depth"] is None:
            data_dic["required_depth"] = 0
            
        self.data_dictionary_repository.update_field(str(table_id), data_dic)
        return "Cleaned"


    def datum_transformations(self, data, datum_flag):
        """
            Transform DATUM data and add 'datum' flag to both: SOPHIA & DATUM 
        :param data: list with info [[object_data], [fields_data]]
        :param datum_flag: mark of a datum element (boolean)
        :return: data, with corrected keys and datum flag [[object_data], [fields_data], [datum_flag]]
        """
        object_data = []
        fields_data = []
        if datum_flag:
            if data[0] is not None:
                for val in data[0]:
                    val = {k.upper().strip(): v for k, v in val.items()}
                    object_data.append(val)
                data[0] = object_data
            elif data[1] is not None:
                for val in data[1]:
                    val = {k.upper().strip(): v for k, v in val.items()}
                    fields_data.append(val)
                data[1] = fields_data
            data.append({"datum": True})
        else:
            data.append({"datum": False})

        return data