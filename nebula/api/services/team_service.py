from nebula.api.persistence.team_repository import *
from nebula.api.persistence.user_repository import *
from nebula.api.persistence.data_dictionary_repository import *
from nebula.api.persistence.data_table_repository import *
from nebula.api.persistence.use_case_repository import *
from nebula.api.persistence.project_repository import *

from nebula.api.entities.team import Team
import datetime


class Team_service(object):

    """
   A class used to represent a team services.

   Attributes
          -------
          team_repository : Team_repository
              Reference to Team repository class to interact with database
          user_repository: User_repository
              Reference to User repository class to interact with database
          data_dictionary_repository: Data_dictionary_repository
              Reference to Data dictionary repository class to interact with database

  """

    def __init__(self):
        """

        """
        self.team_repository = Team_repository()
        self.user_repository = User_repository()
        self.data_dictionary_repository = Data_dictionary_repository()
        self.data_table_repository = Data_table_repository()
        self.use_cases_repository = Use_case_repository()
        self.project_repository = Project_repository()

    def insert_one(self, team):
        """Insert a team

        :param team: json, dictionary that contains a team data
        :return:
        """
        if not self.team_repository.exist(team["name"]):
            team["creationDate"] = datetime.datetime.utcnow()
            self.team_repository.insert_one(team)
        else:
            raise ValueError("Team already exist")

    def find_one(self,team_id):
        """Get a team

        :param team_id: str, String that represent a team id
        :return: cursor, dictionary that represent a team data
        """
        return self.team_repository.find_one(team_id)

    def find_all(self):
        """Get all teams

        :return: cursor, dictionary with all team names
        """
        res = []
        for j in self.team_repository.find_all():
            j["_id"] = str(j["_id"])
            res.append(j)
        return res

    def update_one(self, team_id, team):
        """Update a team id

        :param team_id: str, string that represent a team id
        :param team: json, dictionary that represent a data to update
        :return:
        """
        if "_id" in team:
            del team["_id"]
        new_team = {"name": team["name"], "country": team["country"]}
        team["modification"] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.team_repository.update_one(team_id, new_team)

    def find_country_teams(self,country):
        """Get teams associate to a country

        :param country: str, string that represent a country name
        :return:
        """
        return self.team_repository.find_country_teams(country)

    def countries(self):
        """Get all available countries

        :return: array, with countries names
        """
        return Team().countries()

    def find_state_tables(self, team_id, state):
        """Get all table associate to a team with a specific state

        :param team_id: str, string that represent a team id
        :param state: str, string that represent a state to check
        :return: arrays of jsons, id's tables
        """
        users = self.user_repository.find_team(team_id)
        res = []
        for i in users:
            table_users = self.data_dictionary_repository.find_user_tables_state(str(i["_id"]), state)
            for j in table_users:
                res.append(j)
        return res

    def find_state_data_tables(self, team_id, state):
        """Get all table associate to a team with a specific state

        :param team_id: str, string that represent a team id
        :param state: str, string that represent a state to check
        :return: arrays of json, id's tables
        """
        users = self.user_repository.find_team(team_id)
        res = []
        for i in users:
            table_users = self.data_table_repository.find_user_tables_state(str(i["_id"]), state)
            for table in table_users:
                table["fields"] = len(table["fields"])
                try:
                    query = {"tablones": {"$in": [ObjectId(table["_id"])]}}
                    use_case_id = self.use_cases_repository.find_any_query(query).limit(1)
                    project = self.project_repository.find_any_query(
                        {"useCases": {"$in": [ObjectId(use_case_id[0]["_id"])]}}).limit(1)
                    if use_case_id is None:
                        table["use_case_name"] = "Unassigned"
                    else:
                        table["use_case_name"] = use_case_id[0]["name"]

                    if project is None:
                        table["project_name"] = "Unassigned"
                    else:
                        table["project_name"] = project[0]["name"]
                    res.append(table)
                except:
                    table["project_name"] = "unassigned"
                    res.append(table)
        return res
