from nebula.api.persistence.word_repository import *


class Word_service(object):
    """
        A class used to represent a word services.

        Attributes
            -------
            word_repository : Word_repository
                Reference to work repository class to interact with database
    """

    def __init__(self):
        """

        """
        self.word_repository = Word_repository()

    def find_all_information(self, word, abbreviation, word_spanish, synonymous, type):
        """ Get all associate work data, synonymous and abbreviations

        :param word: str, string that represent a word to find
        :param abbreviation
        :param word_spanish
        :param synonymous
        :param type
        :return: array of json with associate work data
        """
        temp = []
        if len(word.strip()) != 0:
            temp.append({"longName": {"$regex": word.strip().upper(), "$options": "si"}})

        if len(abbreviation.strip()) != 0:
            temp.append({"abbreviation": {"$regex": abbreviation.strip().upper(), "$options": "si"}})

        if len(word_spanish.strip()) != 0:
            temp.append({"spa_word": {"$regex": word_spanish.strip().upper(), "$options": "si"}})

        if len(synonymous.strip()) != 0:
            temp.append({"synonymous": {"$regex": synonymous.strip().upper(), "$options": "si"}})

        if type.strip() == "and":
            if len(temp) != 0:
                query = {"$and": temp}
                res = self.word_repository.find_any_query(query)
            else:
                res = self.word_repository.find_all()

        elif type.strip() == "or":
            if len(temp) != 0:
                query = {"$or": temp}
                res = self.word_repository.find_any_query(query)
            else:
                res = self.word_repository.find_all()
        else:
            res = self.word_repository.find_all()

        data = []
        for i in res:
            data.append(i)

        return data
