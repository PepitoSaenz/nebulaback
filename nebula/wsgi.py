"""
WSGI config for nebula project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os
import sys

path = "/home/datio/governanceProject"

if path not in sys.path:
    sys.path.append(path)

from django.core.wsgi import get_wsgi_application

#Always have to be in development
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nebula.settings.production')

application = get_wsgi_application()
