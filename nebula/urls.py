"""nebula URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path

from nebula.api.controllers import naming_controller, project_controller, user_controller, data_dictionary_controller, \
    statistics_controller, data_table_controller, acl_validator_controller, operational_base_controller, functional_map_controller

urlpatterns = [

    # Namings
    path('namings/suffixes/', naming_controller.find_all_suffix),
    path('namings/words/word=<word>&abbreviation=<abbreviation>&spanish=<word_spanish>&synonymous=<synonymous>&type=<type>/',
         naming_controller.find_all_word_information),
    path('namings/dataDictionary/<naming>/', naming_controller.find_naming_table),
    path('namings/<naming>', naming_controller.find_exactly_naming),
    path('namings/machine/naming=<naming>&suffix=<suffix>&description=<description>/', naming_controller.machine_global_query),
    path('namings/naming=<naming>&logic=<logic>&desc=<description>&type=<type>&level=<level>/', naming_controller.global_query),
    
    #
    path('namings/levels/', naming_controller.find_hierarchy),
    path('namings/export/', naming_controller.export_namings),
    path('namings/export/skip/<skip>', naming_controller.export_namings_skip),
    path('namings/export/limit/<limit>', naming_controller.export_namings_limit),
    
    path('data/export/', project_controller.export_data),
    
    

    # Projects
    path('projects/', project_controller.project),
    path('projects/<user_id>/create/', project_controller.create_project), #user id es admin id

    path('projects/user=<user_id>/', project_controller.find_project_VoBo),
    path('projects/functionalMap/<project_id>/', project_controller.find_functional_map),
    path('projects/FunctionalMap/', project_controller.upload_functional_map),
    path('projects/RawFunctionalMap/', project_controller.upload_raw_functional_map),
    path('projects/functionalMap/<project_id>/uuaa/', project_controller.remove_functional_map),
    path('projects/uuaa/dataTable/<project_id>/', project_controller.find_functional_map_data_table),
    path('projects/excels/project=<project_id>&use_case=<use_case_id>/', project_controller.find_excels_files),

    path('projects/<user_id>/changes/', project_controller.project_update_changes), #user id es admin id
    path('projects/checkVoBoUser/<user_id>/', project_controller.check_vobo_in_projects),
    path('projects/updateVobo/<project_id>/', project_controller.update_vobo_project),


    

    # Use case
    path('projects/useCases/<use_case_id>/', project_controller.use_case_actions),
    path('projects/useCases/<use_case_id>/backlogs/', project_controller.find_use_case_backlogs),
    path('projects/useCases/<use_case_id>/dataTables/', project_controller.find_use_case_data_tables),
    path('projects/useCases/<use_case_id>/datadictionaries/', project_controller.find_use_case_datadictionaries_to_vobo),   # VoBo Negocio
    path('projects/useCases/<use_case_id>/dataTables/VoBo/', project_controller.find_use_case_data_tables_vobo),  # VoBo Negocio
    path('projects/useCases/<use_case_id>/query/backlog=<backlog_name>&request=<request_state>&state=<state>/', project_controller.find_backlog_name),
    path('projects/useCases/<use_case_id>/query/dataTable=<data_table_name>&request=<request_state>&state=<state>/', project_controller.find_data_table_name),

    # Backlog
    path('projects/backlogs/<backlog_id>/', project_controller.backlog_actions),
    path('projects/backlogs/atributes/<attributes>/', project_controller.static_atributes_backlog), #Periodicity, originsystem and typeFile options
    path('projects/backlogs/query/id_case=<id_case>&search_type=<search_type>&base_name=<base_name>&alias=<alias>&origin_system=<origin_system>&observation_field=<observation_field>&uuaa_raw=<uuaa_raw>&uuaa_master=<uuaa_master>/',
         project_controller.global_backlog_query),
    
     
    path('projects/backlogs/dataDictionaryNew/import/', project_controller.update_table_from_file_new),  # ADMIN 2 
    
    path('projects/backlogs/dataDictionary/import/', project_controller.update_table_from_file),  # ADMIN
    path('projects/backlogs/dataTable/import/', project_controller.update_data_table_from_file),  # ADMIN


    path('projects/backlogs/check/<baseName>/', project_controller.check_backlog_basename),  


    # Data table
    path('projects/query/dataTables/name=<master_name>&tag=<alias>&description=<description>&uuaa=<uuaa>/',
         project_controller.find_data_tables),

    path('projects/useCases/<use_case_id>/tablones/name=<master_name>&request=<request_state>&state=<state>/',
         project_controller.find_data_table_use_case),

    # Requests -- SPOs and POs
    path('requests/<request_id>/', project_controller.update_request),
    path('requests/dataTable/<request_id>/', project_controller.update_request_data_table),
    path('projects/requests/options/', project_controller.project_request_options),
    path('projects/requests/dataTables/options/', project_controller.project_request_options_data_tables),
    path('projects/requests/useCases/', project_controller.requests_use_cases),
    path('projects/requests/backlogs/', project_controller.requests_backlogs),
    path('projects/requests/dataTables/', project_controller.requests_data_tables),
    path('projects/requests/project=<project_id>&usecase=<use_case_name>&backlog=<backlog_name>&action=<action>/',
         project_controller.find_backlog_requests),
    path('projects/dataTables/requests/project=<project_id>&usecase=<use_case_name>&dataTable=<data_name>&action=<action>/',
         project_controller.find_data_tables_requests),
    path('projects/tickets/options/', project_controller.project_tickets_options),
    path('projects/tickets/useCases/', project_controller.tickets_use_cases),
    path('projects/tickets/backlogs/', project_controller.tickets_backlogs),
    path('projects/tickets/dataTables/', project_controller.tickets_data_tables),
    path('projects/tickets/project=<project_id>&usecase=<use_case_name>&backlog=<backlog_name>&action=<action>&status=<status_req>/',
         project_controller.find_backlog_tickets),
    path('projects/dataTables/tickets/project=<project_id>&usecase=<use_case_name>&dataTable=<data_name>&action=<action>&status=<status_req>/',
         project_controller.find_data_tables_tickets),
    path('projects/<project_id>/', project_controller.one_project),
    path('projects/<project_id>/useCases/', project_controller.find_use_cases_project),
    path('projects/rol=<rol>&user=<rol_id>', project_controller.find_project_to_rol),
    path('projects/<project_id>/<action>/', project_controller.action_in_project),
    path('projects/<project_id>/query/useCase=<use_case_name>&backlog=<backlog_name>&tablon=<tablon_name>/',
         project_controller.find_use_case_by_query),
    path('projects/query/<project_id>/teams/', project_controller.find_team_projects),

    # Users
    path('users/', user_controller.users),
    path('users/login/', user_controller.login),
    path('users/data/', user_controller.user_changes),

    path('users/roles/', user_controller.users_roles),

    path('users/listAll/', user_controller.get_all_users),
    
    path('users/teams/', user_controller.teams),
    path('users/teams/countries/', user_controller.countries),
    path('users/teams/<team_id>/state=<state>/', user_controller.team_tables_state),


    path('users/tables/<user_id>/state=<state>/', user_controller.tables_state),  # Revisar donde esta la tabla de una persona
    path('users/dataTables/<user_id>/state=<state>/', user_controller.data_table_state),
    path('users/partners/group=<group>&code=<code>', user_controller.find_parnerts),


    path('users/<user_id>/', user_controller.user_actions),
    path('users/<user_id>/vobos/', user_controller.find_user_vobos),

    path('users/<user_id>/projects/', user_controller.find_user_projects),
    path('users/<user_id>/pending/', user_controller.pending_users),

    path('users/projects/<user_type>/<user_id>/', user_controller.product_owner_projects),
    path('users/teams/<team_id>/', user_controller.teams_actions),
    path('users/dataTables/teams/<team_id>/state=<state>/', user_controller.team_data_tables_state),


    #Admin
    path('users/<user_id>/create/', user_controller.create_user), #user id es admin id
    path('users/<user_id>/changes/', user_controller.user_update_changes), #user id es admin id

    path('users/query/email=<email>/', user_controller.find_user_email),
    path('users/vobos/projects/<project_id>/', project_controller.update_VoBos),

    path('users/roles/<rol>/', user_controller.rol),


    # Data Dictionary
    path('dataDictionaries/', data_dictionary_controller.table),
    path('dataDictionaries/functional/', data_dictionary_controller.create_table),
    path('dataDictionaries/legacys/<legacy>/', data_dictionary_controller.legacy),
    path('dataDictionaries/comments/reasons/', data_dictionary_controller.comment_return_reasons),
    path('dataDictionaries/governances/<user_id>/', data_dictionary_controller.available_to_governance),
    path('dataDictionaries/state=<state>/', data_dictionary_controller.find_tables_state), #el todoooo
    path('dataDictionaries/projects/id=<state_id>&state=<state>/', data_dictionary_controller.find_tables),
    path('dataDictionaries/<table_id>/fields/', data_dictionary_controller.field),  # bloquea, desbloquea, etc
    path('dataDictionaries/<table_id>/excels/<fase>/', data_dictionary_controller.generate_excels),
    path('dataDictionaries/<table_id>/state/', data_dictionary_controller.change_state),  # Lo cambia de estado, lo envia a gobierno, arquitectura, etc
    path('dataDictionaries/<table_id>/legacys/', data_dictionary_controller.insert_legacy),
    path('dataDictionaries/<table_id>/functional/legacys/', data_dictionary_controller.update_legacy),
    path('dataDictionaries/<table_id>/fases/', data_dictionary_controller.generate_new_fase),
    path('dataDictionaries/<table_id>/fases/<fase>/naming=<naming>&logic=<logic>&desc=<desc>&state=<state>&legacy=<legacy>&descLegacy=<descLegacy>&suffix=<suffix>&comment=<comment_state>/', data_dictionary_controller.find_in_table),
    path('dataDictionaries/<table_id>/uuuaa/', data_dictionary_controller.find_uuaa_data_dictionary),
    path('dataDictionaries/<table_id>/head/', data_dictionary_controller.update_object_head),
    path('dataDictionaries/<table_id>/functional/head/', data_dictionary_controller.update_object_functional),
    path('dataDictionaries/<table_id>/head/<state>/', data_dictionary_controller.find_head),
    path('dataDictionaries/<table_id>/fields/<field>/', data_dictionary_controller.find_field),#Consulta
    path('dataDictionaries/<table_id>/functional/fields/', data_dictionary_controller.fields_functional),
    path('dataDictionaries/<table_id>/blockers/<field>/', data_dictionary_controller.find_blocker_fields),
    path('dataDictionaries/<table_id>/fase=<fase>&user=<user_id>/', data_dictionary_controller.update_field),  # Actualiza el campo
    path('dataDictionaries/<table_id>/pending/fase=<fase>/', data_dictionary_controller.find_no_ok_fields),
    path('dataDictionaries/<table_id>/validations/', data_dictionary_controller.update_validation),  # Actualizar validación PO
    path('dataDictionaries/<table_id>/updateOld/', data_dictionary_controller.update_old_tables),  # Actualizar rutas tablas
    path('dataDictionaries/uuaas/rawOptions/', data_dictionary_controller.get_raw_uuaa_dd_options), #new UUAAs
    path('dataDictionaries/uuaas/masterOptions/', data_dictionary_controller.get_master_uuaa_dd_options),

    path('dataDictionaries/options/<property_name>/', data_dictionary_controller.get_property_options),


    path('dataDictionaries/<table_id>/deleteRaw/', data_dictionary_controller.delete_raw),
    path('dataDictionaries/<table_id>/updateMaster/', data_dictionary_controller.update_master),
    path('dataDictionaries/<table_id>/uploadAll/', data_dictionary_controller.update_load_all_fields),
    
    path('dataDictionaries/clean/<table_id>/', data_dictionary_controller.get_clean_null_data_dictionary),

    
    # NEW NEW Functional Map
    path('uuaa/master/new/', functional_map_controller.new_uuaa),
    path('uuaa/master/all/', functional_map_controller.query_all_uuaa_master),
    path('uuaa/master/allfull/', functional_map_controller.query_all_uuaa_master_full),

    path('uuaa/master/search_props/', functional_map_controller.query_master_search_props),
    path('uuaa/master/uuaa=<uuaa_name>&system_name=<system_name>&data_source=<data_source>&desc=<desc>&level1=<level1>&level2=<level2>&country=<country>/', functional_map_controller.query_new_uuaa_master),

    path('uuaa/master/uuaa=<uuaa_name>&desc=<desc>&desc_eng=<desc_eng>&data_source=<data_source>&country=<country>&deploy_country=<deploy_country>&level1=<level1>&level2=<level2>/', functional_map_controller.query_uuaa_master),
    path('uuaa/master/<uuaa_id>/', functional_map_controller.get_uuaa),
    path('uuaa/master/update/<uuaa_id>/', functional_map_controller.update_uuaa),
    path('uuaa/master/options/<property_name>/', functional_map_controller.get_property_options),

    path('uuaa/ambitos/', functional_map_controller.get_ambitos),

    path('uuaa/raw/new/', data_dictionary_controller.new_uuaa_raw),
    path('uuaa/raw/all/', data_dictionary_controller.get_all_uuaas_raw),
    path('uuaa/raw/uuaa=<uuaa_name>&app=<app_name>&level1=<level1>&level2=<level2>/', data_dictionary_controller.get_uuaas_raw),
    path('uuaa/raw/<uuaa_id>/', data_dictionary_controller.get_uuaa_raw),
    path('uuaa/raw/update/<uuaa_id>/', data_dictionary_controller.update_uuaa_raw),
    
    path('uuaa/update_object/', functional_map_controller.update_object),

    path('uuaa/report_tables/<param>/', functional_map_controller.report_tables),


    
    
    # NEW Functional Map
    path('functionalMap/raw/uuaa=<uuaa_name>&app=<app_name>&origin=<origin_name>/', data_dictionary_controller.get_uuaas_raw),
    path('functionalMap/master/uuaa=<uuaa_name>&scope=<scope_name>&level2=<level2>&level3=<level3>/', data_dictionary_controller.get_uuaas_master),
    path('functionalMap/raw/appOptions/', data_dictionary_controller.get_raw_app_options),
    path('functionalMap/raw/originOptions/', data_dictionary_controller.get_raw_origin_options),
    path('functionalMap/master/levelOptions/', data_dictionary_controller.get_master_level_options),
    path('functionalMap/raw/new/', data_dictionary_controller.new_uuaa_raw),
    path('functionalMap/raw/update/<uuaa_id>', data_dictionary_controller.update_uuaa_raw),
    path('functionalMap/master/new/', data_dictionary_controller.new_uuaa_master),
    path('functionalMap/master/update/<uuaa_id>', data_dictionary_controller.update_uuaa_master),

    

    # Data Table
    path('dataTables/<table_id>/excels/<phase>/', data_table_controller.generate_excels),
    path('dataTables/<table_id>/head/', data_table_controller.object_head),
    path('dataTables/<table_id>/state/', data_table_controller.change_state),
    path('dataTables/<table_id>/fields/', data_table_controller.fields_data_table),
    path('dataTables/<table_id>/fields/actions/', data_table_controller.fields_actions_data_table),
    path('dataTables/<table_id>/originTables/', data_table_controller.validate_origin_tables),
    path('dataTables/<table_id>/validations/', data_table_controller.update_validation),
    path('dataTables/<table_id>/comparisons/<type>/', data_table_controller.find_similarities),
    path('dataTables/<table_id>/information/<category>/', data_table_controller.update_origins),
    path('dataTables/tables/origin/alias=<alias>&datio=<datio_name>&uuaa_raw=<uuaa_raw>&uuaa_master=<uuaa_master>&logic=<logic>/', data_table_controller.find_origin_tables),
    path('dataTables/tablones/origin/alias=<alias>&datio=<datio_name>&uuaa=<uuaa>&logic=<logic>/', data_table_controller.find_origin_data_tables),
    path('dataTables/<table_id>/suffix=<suffix>&naming=<naming>&logic=<logic>&desc=<desc>&state=<state>/', data_table_controller.find_inside_tablon),
    path('dataTables/state=<state>/', data_table_controller.find_data_tables_state), #el todoooo

    path('dataTables/todo/excels/', data_table_controller.generate_excels_todo),
    path('dataTables/restartOrigins/<table_id>/', data_table_controller.generate_excels_todo),
    
    path('dataTables/clean_columns/<table_id>/', data_table_controller.clean_columns),




    # ACLs
    path('acl/<env>/alhambra/', acl_validator_controller.find_all_alhambra),
    path('acl/<env>/groups/', acl_validator_controller.find_all_groups),
    path('acl/<env>/groups/<group>/', acl_validator_controller.find_routes_by_group),
    path('acl/<env>/association/', acl_validator_controller.find_all_relationship_between_uuaas),
    path('acl/<env>/association/<uuaa>', acl_validator_controller.find_relationship_uuaa),
    path('acl/folders/validation/', acl_validator_controller.folders_validation),
    path('acl/<env>/acls/validation/', acl_validator_controller.acl_validation),
    path('acl/<env>/accounts/validation/', acl_validator_controller.accounts_validation),

    # Statistics
    path('statistics/coso/', statistics_controller.get_usedtables_in_datatables),

    path('statistics/tables/<table_id>/', statistics_controller.find_state_table),
    path('statistics/status/dd/where=<where>&code=<code>', statistics_controller.find_general_status),
    path('statistics/status/dt/where=<where>&code=<code>', statistics_controller.find_general_status_dt),

    path('statistics/<group>/code=<code>&startDate=<start_date>&endDate=<end_date>', statistics_controller.find_between_days),
    

    path('statistics/main/', statistics_controller.get_main_statistics),
    path('statistics/main/dt', statistics_controller.get_main_statistics_dt),
    path('statistics/tables/<table_id>/', statistics_controller.find_state_table),
    path('statistics/status/where=<where>&code=<code>', statistics_controller.find_general_status),
    path('statistics/projects/tables/', statistics_controller.get_tables_by_project),
    path('statistics/projects/states/', statistics_controller.get_tables_by_state_and_project),
    path('statistics/projects/uuaas/', statistics_controller.get_tables_by_uuaa),
    path('statistics/uuaa/uuaa=<uuaa>&type=<type>&loc=<loc>/', statistics_controller.get_uuaa_statistics),
    path('statistics/allproys/dd/', statistics_controller.get_proyects_statistics),
    path('statistics/allproys/dt/', statistics_controller.get_proyects_statistics_dt),
    path('statistics/general/', statistics_controller.get_general_statistics),



    # Operational base
    path('operationalBase/', operational_base_controller.create_operational_base),
    path('operationalBase/<operational_id>/head/', operational_base_controller.operational_object),
    path('operationalBase/<operational_id>/fields/', operational_base_controller.operational_fields),
    path('operationalBase/<operational_id>/fields/<action>/', operational_base_controller.field_action),
    path('operationalBase/logic=<logic>&desc=<desc>&name=<name>&uuaa=<uuaa>/', operational_base_controller.find_operational_bases),
    path('operationalBase/<operational_id>/structure/', operational_base_controller.update_operational_base),
    path('operationalBase/<operational_id>/excels/', operational_base_controller.generate_excels),
    path('operationalBase/file/', operational_base_controller.create_operational_base_file),
    path('operationalBase/state=<state>&project=<project_id>/', operational_base_controller.find_by_state),
    path('operationalBase/<operational_id>/validation/', operational_base_controller.update_validation_vobo),

    path('operationalBase/settings/', operational_base_controller.load_settings),



    # Query
    path('query/table_name=<table_name>&alias=<alias>&project_id=<project_id>&table_state=<table_state>&logic_name=<logic_name>&table_desc=<table_desc>&backlog=<backlog_name>&type=<base_type>/', project_controller.get_tables_by_filter)
]
