from nebula.settings.base import *

DEBUG = False

ALLOWED_HOSTS = ['82.250.88.212','localhost', '127.0.0.1','[::1]','82.57.180.146']

"""
Do secure a deployment to production
"""
SESSION_COOKIE_SECURE = True
SECURE_HSTS_SECONDS = 3600
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True
SECURE_SSL_REDIRECT = False
CSRF_COOKIE_SECURE = True
SECURE_HSTS_PRELOAD = True
X_FRAME_OPTIONS = 'DENY'
CORS_ORIGIN_ALLOW_ALL=True

"""
Connect to production Database
"""
MONGO_DATABASE_NAME = 'governanceDatabase'
MONGO_HOST = '127.0.0.1'
MONGO_PORT = 27017
#MONGO_USER = 'platform'
#MONGO_PWD = 'd@tioPl0tform'
MONGO_CLIENT = MongoClient(MONGO_HOST,MONGO_PORT)
#MONGO_CLIENT.readWrite.authenticate(MONGO_USER,MONGO_PWD,mechanism='SCRAM-SHA-1',source=MONGO_DATABASE_NAME)
MONGO_DATABASE = MONGO_CLIENT[MONGO_DATABASE_NAME]

"""
Peticiones REST que permiten los servicios
del back
"""
CORS_ALLOW_METHODS = (
    'GET',
    'POST',
    'PUT'
)