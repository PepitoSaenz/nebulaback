#!/usr/bin/env python
import os
import sys
from oauth2client import tools

if __name__ == '__main__':

    #Always have to be in development
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nebula.settings.production')

    try:
        from django.core.management import execute_from_command_line
    except ImportError:
        try:
            import django
        except ImportError:
            raise ImportError(
                "Couldn't import Django. Are you sure it's installed and "
                "available on your PYTHONPATH environment variable? Did you "
                "forget to activate a virtual environment?"
            )
        raise
    execute_from_command_line(sys.argv)
